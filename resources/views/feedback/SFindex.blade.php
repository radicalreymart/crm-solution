@extends('layouts.app', [
    'class' => 'sidebar-mini ',
    'namePage' => 'Feedback Index',
    'activePage' => 'feedback-management',
    'activeNav' => 'settings',
])

@section('content')

<div class="panel-header panel-header-sm">
  </div>
  <div class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header">
            @include('flash-message')
                <div class="float-left">
                    <h4 class="card-title">Website Feedback</h4>
                    <a  class="btn btn-warning" href="{{ url('/feedback') }}">
                        Walk-in Feedback
                    </a>
                </div>
                <div class="row float-right">
                    <form action="{{ route('exportSiteFeedback') }}"  >
                        {{ csrf_field() }}
                        Start Date
                    <input style="right: 5px;" class="form-control  start" max="{{ Carbon\Carbon::now()->format('Y-m-d'); }}" value="{{Carbon\Carbon::now()->format('Y-m-d'); }}" type="date" id="start" name="start">
                        End Date
                    <input style="right: 5px;" class="form-control  start" max="{{ Carbon\Carbon::now()->format('Y-m-d'); }}" value="{{Carbon\Carbon::now()->format('Y-m-d'); }}" type="date" id="end" name="end">
                        <button class="btn btn-primary" id="start"   >Download Excel </button>
                    </form>
                </div>
          </div>
          <div class="card-body">
            <div id="modalCatcher" class="table-responsive">
              <input type="hidden" value="{{url('/')}}" id="url" name="url">
                <table id="datatablesSimple" class="table">
                <thead class=" text-primary">
                    <th >Action</th>
                    <th>Member</th>
                    <th>Last Feedback</th>
                    <th>Date Created</th>
                    <th>Date Updated</th>
                    <th>Status</th>
                </thead>
                <tbody>
                    @if($sitefeedback->count())
                    @foreach ($sitefeedback as $data)
                    <tr>
                    <td>
                      <a href="javascript:void(0)" title="Show Feedback Details" class="show" data-id="{{ $data->id }}">
                        <i class="fas fa-eye  fa-lg"></i>
                      </a>
                    @csrf
                    @method('DELETE')
                    </td>
                    <td>{{ $data->member_no }} : {{$data->fullname}}</td>
                    <td>{{ Carbon\Carbon::parse($data->last_feedback)->subMonth(3)->format('m-d-Y'); }}</td>
                    <td>{{ date_format($data->created_at, 'm-d-Y') }}</td>

                    @if(isset($data->updated_at))
                    <td>{{ date_format($data->updated_at, 'jS M Y') }}</td>
                    @else
                    <td></td>
                    @endif
                    <td>
                    @if(isset($data->status))
                    Uploaded
                    @else
                    Pending for upload
                    @endif
                    </td>
                    </tr>
                    @endforeach
                    @endif
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      
    </div>
  </div>
    <!-- boostrap model -->

<div class="modal fade" id="ajax-feedback-model" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
            <h4 class="modal-title" id="ajaxFeedbackModel"></h4>
            <button type="button" class="btn btn-default"  id="close-modal" data-dismiss="modal">Close</button>
            </div>
            <div class="modal-body">
                <table class="table" id="dynamicAddRemove">
                    <tr>
                        <td>Member Number</td>
                        <td id="member_no"></td>
                    </tr>
                    <tr>
                        <td>Fullname </td>
                        <td id="fullname"></td>
                    </tr>
                    <tr>
                        <td>Gender </td>
                        <td id="gender"></td>
                    </tr>
                    <tr>
                        <td>Branch </td>
                        <td id="branch_id"></td>
                    </tr>
                    <tr>
                        <td>Member Type </td>
                        <td id="member_type"></td>
                    </tr>
                    <tr>
                        <td>Transaction Experience </td>
                        <td id="transaction_exp"></td>
                    </tr>
                    <tr>
                        <td>Comment/Suggestion </td>
                        <td id="comment_suggestion"></td>
                    </tr>
                    <tr>
                        <td>Signature</td>
                        <td id="signiture">
                            <a id="href" target="_blank" rel="noopener noreferrer">
                                <img id="sigimg" height="100" width="200">
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <td>Last Feedback</td>
                        <td id="last_feedback"></td>
                    </tr>
                    <tr>
                        <td>Date Created </td>
                        <td id="created_at"></td>
                    </tr>
                    <tr>
                        <td>Created By</td>
                        <td id="crea_by"></td>
                    </tr>
                    <tr>
                        <td>Date Updated</td>
                        <td id="updated_at"></td>
                    </tr>
                    <tr>
                        <td>Updated By</td>
                        <td id="upd_by"></td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- end bootstrap model -->
<!-- <script src="http://www.datejs.com/build/date.js" type="text/javascript"></script> -->

<script>
  // window.addEventListener("load", function() {
  //   window.addEventListener('DOMContentLoaded', event => {
  //   // Simple-DataTables
  //   // https://github.com/fiduswriter/Simple-DataTables/wiki
    
  //   const datatablesSimple = document.getElementById('datatablesSimple');
  //   if (datatablesSimple) {
  //       new simpleDatatables.DataTable(datatablesSimple, {
  //   fixedHeight: true
  //       });
  //   }
  // });
$(document).ready(function(){
    $('#datatablesSimple').DataTable({

  });
  const swalWithBootstrapButtons = Swal.mixin({
                  customClass: {
                    confirmButton: 'btn btn-success',
                    cancelButton: 'btn btn-danger'
                  },
                  buttonsStyling: false
                })
  $('#close-modal').click(function () {
       $('#ajax-user-model').modal('hide');
    });
 $.clearInput = function () {
        $('insert_form').find('input[type=text], input[type=password], input[type=number], input[type=email], select, textarea').val('');
    };
     $('.modal').on('hidden.bs.modal', function(){
             // window.location.reload();
             // $('#insert_form').removeData('bs.modal')
    $.clearInput();
    // $(this).find('form')[0].reset();
    });

    $.ajaxSetup({
        headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

      $('#modalCatcher').on('click', '.show', function () 
    {
        $('#ajaxFeedbackModel').html("Feedback Details");
        $('#ajax-feedback-model').modal('show');
        var id = $(this).data('id');
        var base_path = $("#url").val();
        var transaction_nat = [];
        $.ajax({
                type:"get",
                url: base_path+"/empsitefeedbackAPI/"+id,
                data: { id: id },
                dataType: 'json',
                success: function(res){
                  // console.log(res.regions);
                var mydate = new Date(res[0].last_feedback);
                var month = ["January", "February", "March", "April", "May", "June",
                "July", "August", "September", "October", "November", "December"][mydate.getMonth()-3];
                var str = month + ' '+mydate.getDate()+ ', ' + mydate.getFullYear();
                // alert(str);
                  
                  // var newDate = new Date(res[0].last_feedback.split(' ')[0].setMonth(res[0].last_feedback.split(' ')[0].getMonth()+8));
                  // alert(newDate);
                  // alert(JSON.stringify(res[0].last_feedback.split(' ')[0]));
                
                // alert(transaction_nat);
                  $('#member_no').html(res[0].member_no);
                  $('#fullname').html(res[0].fullname);
                  $('#transaction_exp').html('<a title="" href="javascript:void(0);">How was the experience with our website?</a><p>'+res[0].transaction_exp['rate1']+'★</p><br><a title="" href="javascript:void(0);">Did this page help you?</a><p>'+res[0].transaction_exp['rate2']+'★</p><br><a title="Staff provide complete and accurate information to you" href="javascript:void(0);">How easy was it to find the information you were looking for?</a><p>'+res[0].transaction_exp['rate3']+'★</p><br><a title="" href="javascript:void(0);">Have you got the necessary information that you want?</a><p>'+res[0].transaction_exp['rate4']+'★</p><br><a title="" href="javascript:void(0);">Overall, how happy are you with the website?</a><p>'+res[0].transaction_exp['rate5']+'★</p><br>');
                    // 
                  if(res[0].gender = 1){
                      $('#gender').html('Male');
                  }else{
                      $('#gender').html('Female');  
                  }
                  $('#branch_id').html(res[0].branch.name);
                  $('#comment_suggestion').html(res[0].comment_suggestion);
                  if(res[0].member_type = 1){
                      $('#member_type').html('Associate');
                  }else{
                      $('#member_type').html('Regular');  
                  }
                  // $('#signiture').html(res[0].signiture);
                  var src = res[0].signiture;
                    var splitfile = res[0].signiture.split('-');
                    var imgsrc1 = base_path+'/assets/img/uploads/sitefeedback/'+splitfile[0]+'/'+src;
                    var img1 = document.getElementById("sigimg");
                    var aid1 = document.getElementById("href");
                    var src1 = document.createAttribute("src");
                    var href1 = document.createAttribute("href");
                    src1.value = imgsrc1;
                    img1.setAttributeNode(src1);  
                    href1.value = imgsrc1;
                    aid1.setAttributeNode(href1);
                  $('#last_feedback').html(str);
                    // date_format(, 'jS M Y'));
                  $('#upload_by').html(res[0].upload_by);
                  $('#upload_date').html(res[0].upload_date);
                  $('#crea_by').html(res[0].crea_by);
                  $('#upd_by').html(res[0].upd_by);
                  $('#created_at').html(res[0].created_at.split('T')[0]);
                  $('#updated_at').html(res[0].updated_at.split('T')[0]);
                }
                ,error: function (data) {
                     // alert('please delete the preloan connected to this preloan/preloan');
                     alert(data.responseText);  
                }
            });
    });
});
</script>
@endsection


<!-- {
"deposit":null
,"savings":null
,"time_deposit":"1"
,"share_capital":null
,"withdraw":null
,"member_capital":null
,"member_app":null
,"member_termination":"1"
,"member_benefits":null
,"membership_inquiry":null
,"PMES":null
,"loan_payment":null
,"loan_application":null
,"loan_release":"1"
,"bayad_center":null
}
{"rate1":"3"
,"rate2":"4"
,"rate3":"4"
,"rate4":"4"
,"rate5":"3"
,"rate6":"2"
}



 -->
   

