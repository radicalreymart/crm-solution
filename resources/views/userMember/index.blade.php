@extends('layouts.app', [
    'class' => 'sidebar-mini ',
    'namePage' => 'UserMember Index',
    'activePage' => 'userMember-management',
    'activeNav' => 'settings',
])

@section('indexdeleteall')
  <script src="{{ asset('assets') }}/js/indexdeleteall.js"></script>
@endsection
@section('content')
    @include('auth.passwords.confirmmodal')

<div class="panel-header panel-header-sm">
  </div>
  <div class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header">
            @include('flash-message')
                <div class="float-left">
                    <h4 class="card-title">UserMember Index</h4>
                </div>
                <div class="float-right">
                      <!-- <a href="javascript:void(0)" title="Add User" class="btn btn-success" id="addNewUser">
                        <li class="fas fa-plus-circle fa-lg" aria-hidden="true"></li>
                      </a> -->
                    <button class="btn btn-danger delete_all" data-url="{{ url('DeleteAllUserMember') }}">Delete All Selected</button>
                </div>
          </div>
          <div class="card-body">
            <div id="modalCatcher" class="table-responsive">
              <input type="hidden" value="{{url('/')}}" id="url" name="url">
                <table id="datatablesSimple" class="table">
                <thead class=" text-primary">
                    <th><input type="checkbox" id="master"></th>
                    <th >Action</th>
                    <th>User Name</th>
                    <th>Email</th>
                    <th>Role</th>
                    <th>Created By</th>
                    <th>Date Created</th>
                    <th>Updated By</th>
                    <th>Date Updated</th>
                </thead>
                <tbody>
                    @if($usermember->count())
                    @foreach ($usermember as $data)
                    <tr>
                    <td ><input type="checkbox" class="sub_chk" data-id="{{$data->id}}"></td>
                    <td>
                      <a href="javascript:void(0)" title="Show User" class="show" data-id="{{ $data->id }}">
                        <i class="fas fa-eye  fa-lg"></i>
                      </a>
                    @csrf
                    @method('DELETE')
                    </td>
                    <td>{{ $data->user->uname }}</td>
                    <td>{{ $data->user->email }}</td>
                    <td>Enduser</td>
                    <td>{{ $data->crea_by }}</td>
                    <td>{{ date_format($data->created_at, 'm-d-Y') }}</td>
                    @if(isset($data->upd_by))
                    <td>{{ $data->upd_by }}</td>
                    @else
                    <td></td>
                    @endif
                    @if(isset($data->updated_at))
                    <td>{{ date_format($data->updated_at, 'jS M Y') }}</td>
                    @else
                    <td></td>
                    @endif

                    </tr>
                    @endforeach
                    @endif
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      
    </div>
  </div>
    <!-- boostrap model -->
    
<div class="modal fade" id="ajax-usermember-model" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
            <h4 class="modal-title" id="ajaxUserMemberModel"></h4>
            <button type="button" class="btn btn-default"  id="close-modal" data-dismiss="modal">Close</button>
            </div>
            <div class="modal-body">
                <table class="table" id="dynamicAddRemove">
                    <tr>
                        <td>Member Number</td>
                        <td id="member_no2"></td>
                    </tr>
                    <tr>
                        <td>User Name</td>
                        <td id="uname"></td>
                    </tr>
                    <tr>
                        <td>Name</td>
                        <td id="long_name1"></td>
                    </tr>
                    <tr>
                        <td>Branch</td>
                        <td id="branch1"></td>
                    </tr>
<!--                     <tr>
                        <td>Status</td>
                        <td id="status1"></td>
                    </tr> -->
                    <tr>
                        <td>Type</td>
                        <td id="type1"></td>
                    </tr>
                    <tr>
                        <td>Gender</td>
                        <td id="gender1"></td>
                    </tr>
                    <tr>
                        <td>Contact Number</td>
                        <td id="contact_no"></td>
                    </tr>
                    <tr>
                        <td>Email</td>
                        <td id="email"></td>
                    </tr>
                    <tr>
                        <td>Facebook</td>
                        <td id="fb_link"></td>
                    </tr>
                    <tr>
                        <td>Address</td>
                        <td id="address"></td>
                    </tr>
                    <tr>
                        <td>Date Created</td>
                        <td id="created_at"></td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- end bootstrap model -->

<script>
  // window.addEventListener("load", function() {
    window.addEventListener('DOMContentLoaded', event => {
    // Simple-DataTables
    // https://github.com/fiduswriter/Simple-DataTables/wiki
    
    const datatablesSimple = document.getElementById('datatablesSimple');
    if (datatablesSimple) {
        new simpleDatatables.DataTable(datatablesSimple, {
    fixedHeight: true
        });
    }
  });
$(document).ready(function(){
  const swalWithBootstrapButtons = Swal.mixin({
                  customClass: {
                    confirmButton: 'btn btn-success',
                    cancelButton: 'btn btn-danger'
                  },
                  buttonsStyling: false
                })
  $('#close-modal').click(function () {
       $('#ajax-user-model').modal('hide');
    });
 $.clearInput = function () {
        $('insert_form').find('input[type=text], input[type=password], input[type=number], input[type=email], select, textarea').val('');
    };
     $('.modal').on('hidden.bs.modal', function(){
             // window.location.reload();
             // $('#insert_form').removeData('bs.modal')
    $.clearInput();
    $(this).find('form')[0].reset();
    });

    $.ajaxSetup({
        headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('#addNewUser').click(function () {
       $('#addEditUserForm').trigger("reset");
       $('#ajaxUserModel').html("Add User");
       $('#ajax-user-model').modal('show');
    });
      // $('#modalCatcher').on('click', '#btn-save', function () {
    $('#btn-save').click(function (event) {
         // alert('hello');
          try{
          var id = $("#id").val();
          var uname = $("#uname").val();
          var branch_id = $("#branch_id").val();
          var employee_role = $("#employee_role").val();
          var email = $("#email").val();
          var password = $("#password_reg").val();
          var password_confirm = $("#password-confirm").val();
          // alert(id+cd+description+trxn_type_id);
          $("#btn-save").html('Please Wait...');
          $("#btn-save"). attr("disabled", true);           
              }
            catch(err) {
            swalWithBootstrapButtons.fire({
                  title: 'Error!',
                  text: err,
                  icon: 'warning',
                  confirmButtonText: 'Confirmed',
                }).then((result) => {
             // window.location.reload();
            $("#btn-save").html('Submit');
            $("#btn-save"). attr("disabled", false);
                })
            }

        // event.preventDefault();
        // ajax
        $.ajax({
            type:"POST",
            url: "{{ url('add-update-userv2') }}",
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            data: {
              id:id,
              uname:uname,
              branch_id:branch_id,
              employee_role:employee_role,
              email:email,
              password:password,
              password_confirm:password_confirm,
            },
            dataType: 'json',
            success: function(res){
              // alert('success');
             swalWithBootstrapButtons.fire({
                  title: 'Success!',
                  icon: 'success',
                  confirmButtonText: 'Confirmed',
                }).then((result) => {
             window.location.reload();})
              // alert(JSON.stringify(res));
            $("#btn-save").html('Submit');
            $("#loading").attr("hidden", true);
            $("#btn-save").attr("disabled", false);
           }
          //   ,error: function(xhr, status, error){
          //  var errorMessage = xhr.status + ': ' + xhr.statusText
          //  alert('Error? - ' + errorMessage);
          // }          
           ,error: function (data) {
             swalWithBootstrapButtons.fire({
                  title: 'Error!',
                  text: data.responseText,
                  icon: 'warning',
                  confirmButtonText: 'Confirmed',
                }).then((result) => {
             // window.location.reload();
            $("#btn-save").html('Submit');
            $("#loading").attr("hidden", true);
            $("#btn-save").attr("disabled", false);
           })
            }
        });

    });
    $('#modalCatcher').on('click', '.show', function () 
    {
        $('#ajaxUserMemberModel').html("UserMember Details");
        $('#ajax-usermember-model').modal('show');
        var id = $(this).data('id');
        var base_path = $("#url").val();
        $.ajax({
                type:"post",
                url: base_path+"/edit-usermember",
                data: { id: id },
                dataType: 'json',
                success: function(res){
                  // alert(JSON.stringify(res));
                  // console.log(res);
                  if(res.type == 'RM'){
                    var type = 'Regular Member';
                  }else{
                    var type = 'Associate Member';  
                  }if(res.gender == 'F'){
                    var gender = 'Female';
                  }else{
                    var gender = 'Male';  
                  }
                  $('#member_no2').html(res.member_no);
                  $('#contact_no').html('+63'+res.contact_no);
                  $('#uname').html(res.uname);
                  $('#email').html(res.email);
                  $('#fb_link').html(res.fb_link);
                  $('#address').html(res.region_id+', '+res.province_id+', '+res.city_id+', '+res.barangay_id+', '+res.street);
                  $('#long_name1').html(res.long_name);
                  $('#branch1').html(res.branch);
                  // $('#status1').html(res.status);
                  $('#type1').html(type);
                  $('#gender1').html(gender);
                  $('#created_at').html(res.created_at);
                }
                ,error: function (data) {
                     // alert('please delete the preloan connected to this preloan/preloan');
                     alert(data.responseText);  
                }
            });
    });

});
</script>
@endsection

   

