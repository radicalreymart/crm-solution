<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePhAddressTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('regions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code', 9)->unique();
            $table->string('name');
            $table->string('region_id')->unique();
        });

        Schema::create('provinces', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code', 9)->unique();
            $table->string('name');
            $table->string('region_id')->index();
            $table->string('province_id')->unique();
            $table->foreign('region_id', 'cm_fk_region_id')->references('region_id')->on('regions');
        });

        Schema::create('cities', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code', 9)->unique();
            $table->string('name');
            $table->string('region_id')->index();
            $table->string('province_id')->index();
            $table->string('city_id')->unique();
            $table->foreign('province_id', 'cm_fk_province_id')->references('province_id')->on('provinces');

            $table->index(['province_id', 'region_id'], 'cities_province_regions');
        });

        Schema::create('barangays', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code', 9)->unique()->index();
            $table->string('name');
            $table->string('region_id')->index();
            $table->string('province_id')->index();
            $table->string('city_id')->index();
            $table->foreign('city_id', 'cm_fk_cities')->references('city_id')->on('cities');

            $table->index(['province_id', 'region_id'], 'barangay_idx_1');
            $table->index(['city_id', 'province_id', 'region_id'], 'barangay_idx_2');
        });
    }

    public function down()
    {
        Schema::drop('barangays');
        Schema::drop('cities');
        Schema::drop('provinces');
        Schema::drop('regions');
    }
}
