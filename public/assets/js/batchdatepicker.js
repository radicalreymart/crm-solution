window.addEventListener("load", function() {
    var now = new Date();
    var utcString = now.toISOString().substring(0,19);
    var year = now.getFullYear();
    var month = now.getMonth() + 1;
    var day = now.getDate() + 1;
    // var day = now.getDate() - 2;
    // alert(month+"-"+day)
    // var day = now('d');
    var hour = 10;
    var minute = 00;
    var second = 00;
    var localDatetime = year + "-" +
                        (month < 10 ? "0" + month.toString() : month) + "-" +
                        (day < 10 ? "0" + day.toString() : day) + "T" +
                        (hour < 10 ? "0" + hour.toString() : hour) + ":" +
                        (minute < 10 ? "0" + minute.toString() : minute) + 
                        ":"+second+second+"."+second+second+second;
    var datetimeField = document.getElementById("sched_date");
    // alert(localDatetime);
    datetimeField.min = localDatetime;
    datetimeField.value = localDatetime;
    });
