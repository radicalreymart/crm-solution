<?php

namespace App\Exports;

use App\Models\Sitefeedback;
use App\Http\Controllers\SiteFeedbackController;
use Maatwebsite\Excel\Concerns\FromCollection;
use Carbon;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithCustomCsvSettings;
use PhpOffice\PhpSpreadsheet\Cell\Cell;
use PhpOffice\PhpSpreadsheet\Cell\DataType;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class SitefeedbackExport implements  FromQuery, WithHeadings, WithMapping,ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */
    protected $from_date;
    protected $to_date;

    public function __construct($from_date, $to_date) {
        $this->from_date = $from_date;
        $this->to_date = $to_date;
    }
    public function query()
    {
        $from = $this->from_date;
        $to = $this->to_date;

         $branch = auth()->user()->branch_id;
            $sitefeedback = SiteFeedback::with('member', 'branch');
            // dd($miuf->get());
            if($branch == 9){
                $sitefeedback->orderBy('id');
            }else{
                $sitefeedback->where('branch_id', $branch)
                ->orderBy('id');
                // ->orderBy('serv_status');
            }
            $sitefeedback->whereBetween('created_at', [$from, $to]);
            $sitefeedback = $sitefeedback->orderBy('id');
            // dd($feedback->get());
        return $sitefeedback;
    }


    public function headings(): array
    {
        return [
         'member_no'
         , 'fullname'
         , 'gender'
         , 'branch_id'
         , 'member_type'
        ,'rate1'
        , 'rate2'
        ,'rate3'
        ,'rate4'
        ,'rate5'
         , 'comment_suggestion'
         , 'last_sitefeedback'
         , 'crea_by'
         , 'upd_by'
         , 'created_at'
         , 'updated_at'
        ];
    }

    public function map($sitefeedback): array
    {
        // dd($sitefeedback);
        $from = $this->from_date;
        $to = $this->to_date;
        
        if($sitefeedback->gender == 1){
            $gender = 'Male';
        }else{
            $gender = 'Female';
        }
        if($sitefeedback->member->member_type == 1){
            $type = 'Associate Member';
        }else{
            $type = 'Regular Member';
        }

         $sitefeedback_controller = new SiteFeedbackController;
         $sitefeedback_controller->updateFeedback($sitefeedback, $from, $to);
        
        return [
            // 'hello'
              "'".$sitefeedback->member_no
            , $sitefeedback->fullname
            , $gender
            , $sitefeedback->branch->name
            , $type
            , $sitefeedback->transaction_exp['rate1']
            , $sitefeedback->transaction_exp['rate2']
            , $sitefeedback->transaction_exp['rate3']
            , $sitefeedback->transaction_exp['rate4']
            , $sitefeedback->transaction_exp['rate5']
            , $sitefeedback->comment_suggestion
            ,Carbon\Carbon::parse($sitefeedback->last_feedback)->format('m-d-Y')
            , $sitefeedback->crea_by
            , $sitefeedback->upd_by
            , Carbon\Carbon::parse($sitefeedback->created_at)->format('m-d-Y')
            , Carbon\Carbon::parse($sitefeedback->updated_at)->format('m-d-Y')
        ];
    }

}
