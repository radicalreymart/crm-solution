<?php

namespace App\Imports;

use App\Models\MIUF;
use Maatwebsite\Excel\Concerns\ToModel;

class MIUFImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new MIUF([
            //
             'member_no'        => $row[0]
            , 'fname'        => $row[1]
            , 'mname'        => $row[2]
            , 'lname'        => $row[3]
            , 'fullname'        => $row[4]
            , 'email'        => $row[5]
            , 'street'        => $row[6]
            , 'barangay_id'        => $row[7]
            , 'city_id'        => $row[8]
            , 'province_id'        => $row[9]
            , 'region_id'        => $row[10]
            , 'zipcode'        => $row[11]
            , 'fb_link'        => $row[12]
            , 'mobile_no'        => $row[13]
            , 'landline_no'        => $row[14]
            , 'of_offm'        => $row[15]
            , 'occupation_id'        => $row[16]
            , 'other_occupation'        => $row[17]
            , 'montly_income'        => $row[18]
            , 'emp_bus_name'        => $row[19]
            , 'emp_bus_address'        => $row[20]
            , 'emp_bus_startdate'        => $row[21]
            , 'benificiaries'        => $row[22]
            // , 'signiture'        => $row[0]
            , 'editable_at'        => $row[23]
            , 'upload_by'        => $row[24]
            , 'upload_date'        => $row[25]
            , 'crea_by'        => $row[26]
            , 'upd_by'        => $row[27]
        ]);
    }
}
