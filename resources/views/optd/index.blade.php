@extends('layouts.app', [
    'class' => 'sidebar-mini ',
    'namePage' => 'OPTD Index',
    'activePage' => 'optd-management',
    'activeNav' => 'settings',
])

@section('indexdeleteall')
  <script src="{{ asset('assets') }}/js/indexdeleteall.js"></script>
  <style>
    .dataTable-top > nav:last-child, .dataTable-top > div:last-child, .dataTable-bottom > nav:last-child, .dataTable-bottom > div:last-child {
    float: left;
    position: -webkit-sticky;
    position: sticky;
  }
  </style>
@endsection
@section('content')
    @include('auth.passwords.confirmmodal')

<div class="panel-header panel-header-sm">
  </div>
  <div class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header">
            @include('flash-message')
                <div class="float-left" >
                    <div style="padding-left: 20px;" class="row"><p  class="card-title h4">OPTD</p><i style="padding-top:25px; padding-left: 10px;" class="text-muted">Online Payment Transaction Details</i></div>
                </div>
                <div class="float-right">
<!--                       <a href="javascript:void(0)" title="Add OPTD" class="btn btn-success" id="addNewOPTD">
                        <li class="fas fa-plus-circle fa-lg" aria-hidden="true"></li>
                      </a> -->
                    <button class="btn btn-danger delete_all" data-url="{{ url('DeleteAllOPTD') }}">Delete All Selected</button>
                </div>
          </div>
          <div class="card-body">
            <div id="modalCatcher" class="table-responsive">
              <input type="hidden" value="{{url('/')}}" id="url" name="url">
                <table id="datatablesSimple" class="table">
                <thead class=" text-primary">
                    <th><input type="checkbox" id="master"></th>
                    <!-- <th >Action</th> -->
                    <th>Online Payment ID</th>
                    <th>Account</th>
                    <th>Amount</th>
                    <th>Created By</th>
                    <th>Date Created</th>
                    <th>Updated By</th>
                    <th>Date Updated</th>
                </thead>
                <tbody>
                    @if($optd->count())
                    @foreach ($optd as $data)
                    <tr>
                    <td ><input type="checkbox" class="sub_chk" data-id="{{$data->id}}"></td>
                    <!-- <td>
                      <a href="javascript:void(0)" title="Edit OPTD" class="edit" data-id="{{ $data->id }}">
                        <i class="fas fa-edit  fa-lg"></i>
                      </a>
                    @csrf
                    @method('DELETE')
                    </td> -->
                    <td>{{ $data->online_pymnt_id}}</td>
                    @if(isset($data->account->description))
                    <td>{{ $data->account->description }}</td>
                    @else
                    <td>Null</td>
                    @endif
                    @if(isset($data->amount))
                    <td>{{ number_format($data->amount, 2) }}</td>
                    @else
                    <td>Null</td>
                    @endif
                    <td>{{ $data->crea_by }}</td>
                    <td>{{ date_format($data->created_at, 'jS M Y') }}</td>

                    @if(isset($data->upd_by))
                    <td>{{ $data->upd_by }}</td>
                    @else
                    <td></td>
                    @endif
                    @if(isset($data->updated_at))
                    <td>{{ date_format($data->updated_at, 'jS M Y') }}</td>
                    @else
                    <td></td>
                    @endif
                    

                    </tr>
                    @endforeach
                    @endif
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      
    </div>
  </div>
    <!-- boostrap model -->
    <div class="modal fade" id="ajax-OPTD-model" aria-hidden="true" data-backdrop="static" data-keyboard="false">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title" id="ajaxOPTDModel"></h4>
            <button type="button" class="btn btn-default close-modal" id="close-modal" >Close</button>
          </div>
          <div class="modal-body">
            <form action="javascript:void(0)" class="form-horizontal" method="POST">
              <input type="hidden" name="id" id="id">

              <div class="form-group">
                <label for="name" class="col-sm-2 control-label">OPT ID</label>
                <div class="col-sm-12">
                  <select class="form-control m-bot15" id="online_pymnt_id" name="online_pymnt_id">
                  @if ($optm->count())
                  <option value="" selected>Select OPT ID</option> 
                  @foreach($optm as $data)
                  <option value="{{ $data->id }}" >{{ $data->id }} | {{ $data->total_amt }} | {{ $data->date_trxn_rcvd }}</option> 
                  @endforeach   
                  @endif
                  </select>
                </div>
              </div> 

              <div class="form-group">
                <label for="name" class="col-sm-2 control-label">Account</label>
                <div class="col-sm-12">
                  <select class="form-control m-bot15" id="acct_id" name="acct_id">
                  @if ($account->count())
                  <option value="" selected>Select Account</option> 
                  @foreach($account as $data)
                  <option value="{{ $data->id }}" >{{ $data->acct_cd }}</option> 
                  @endforeach   
                  @endif
                  </select>
                </div>
              </div> 

              <div class="form-group">
                <label for="name" class="col-sm-12 control-label">Total Amount</label>
                <div class="col-sm-12">
                  <input  oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"
                    type = "number" maxlength="12"  class="form-control" id="amount" name="amount" step=".01" required="">
                </div>
              </div> 

              <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-primary btn-save" id="btn-save">Save
                </button>
              </div>
            </form>
          </div>
          <div class="modal-footer">
            
          </div>
        </div>
      </div>
    </div>
<!-- end bootstrap model -->  
 
<script>
   
  // window.addEventListener("load", function() {
  //   window.addEventListener('DOMContentLoaded', event => {
  //   // Simple-DataTables
  //   // https://github.com/fiduswriter/Simple-DataTables/wiki
    
  //   const datatablesSimple = document.getElementById('datatablesSimple');
  //   if (datatablesSimple) {
  //       new simpleDatatables.DataTable(datatablesSimple, {
  //   searchable: false,
  //   fixedHeight: true
  //       });
  //   }
  // });
$(document).ready(function(){
    $('.close-modal').click(function () {
     var modal_array = 
    [
      '#ajax-pmes-model'
    ];
        closeModal(modal_array);
       // $('#ajax-pmes-model').modal('hide');
    });
  $('#datatablesSimple').DataTable({

  });
  const swalWithBootstrapButtons = Swal.mixin({
                  customClass: {
                    confirmButton: 'btn btn-success',
                    cancelButton: 'btn btn-danger'
                  },
                  buttonsStyling: false
                }) 

  $.clearInput = function () {
        $('insert_form').find('input[type=text], input[type=password], input[type=number], input[type=email], select, textarea').val('');
    };
     $('.modal').on('hidden.bs.modal', function(){
             // window.location.reload();
             // $('#insert_form').removeData('bs.modal')
    $.clearInput();
    $(this).find('form')[0].reset();
    });
    $.ajaxSetup({
        headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('#addNewOPTD').click(function () {
       $('#addEditOPTDForm').trigger("reset");
       $('#ajaxOPTDModel').html("Add OPTD");
       $('#ajax-OPTD-model').modal('show');
    });
      // $('#modalCatcher').on('click', '#btn-save', function () {
    $('#btn-save').click(function (event) {
         // alert('hello');
          try{
          var id = $("#id").val();
          var online_pymnt_id = $("#online_pymnt_id").val();
          var acct_id = $("#acct_id").val();
          var amount = $("#amount").val();
          $("#btn-save").html('Please Wait...');
          $("#btn-save"). attr("disabled", true);
            event.preventDefault();           
          }
            catch(err) {
            swalWithBootstrapButtons.fire({
                  title: 'Error!',
                  text: err,
                  icon: 'warning',
                  confirmButtonText: 'Confirmed',
                }).then((result) => {
             // window.location.reload();
            $("#btn-save").html('Submit');
            $("#btn-save"). attr("disabled", false);
                })
            }
        // ajax
        $.ajax({
            type:"POST",
            url: "{{ url('add-update-optd') }}",
            // headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            data: {
              id:id,
              online_pymnt_id:online_pymnt_id,
              acct_id:acct_id,
              amount:amount,
            },
            dataType: 'json',
            success: function(res){
              // alert('success');
                swalWithBootstrapButtons.fire({
                  title: 'Success!',
                  icon: 'success',
                  confirmButtonText: 'Confirmed',
                }).then((result) => {
             window.location.reload();})              // alert(JSON.stringify(res));
            $("#btn-save").html('Submit');
            $("#btn-save").attr("disabled", false);
           }
          //   ,error: function(xhr, status, error){
          //  var errorMessage = xhr.status + ': ' + xhr.statusText
          //  alert('Error? - ' + errorMessage);
          // }
          
           ,error: function (data) {
            console.log(data.responseText);
                alert(data.responseText);
            }
        });

    });
     $('#modalCatcher').on('click', '.edit', function () {

        var id = $(this).data('id');
                var base_path = $("#url").val();

        // ajax
          // alert( OPTD_no );
        $.ajax({
            type:"POST",
            url: base_path+"/edit-optd",
            data: { id: id },
            dataType: 'json',
            success: function(res){
                // alert(JSON.stringify(res));
              $('#ajaxOPTDModel').html("Edit OPTD");
              $('#ajax-OPTD-model').modal('show');
              $('#id').val(res.id);
              $("#online_pymnt_id").val(res.online_pymnt_id);
              $("#acct_id").val(res.acct_id);
              $("#amount").val(res.amount);
           },
            error: function (data) {
                 alert(data.responseText);
             }
        });

    });


});
</script>
@endsection

   

