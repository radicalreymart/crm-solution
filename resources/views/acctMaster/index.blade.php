@extends('layouts.app', [
    'class' => 'sidebar-mini ',
    'namePage' => 'AcctMaster Index',
    'activePage' => 'acctMaster-management',
    'activeNav' => 'settings',
])

@section('indexdeleteall')
  <script src="{{ asset('assets') }}/js/indexdeleteall.js"></script>
   <style>
    .dataTable-top > nav:last-child, .dataTable-top > div:last-child, .dataTable-bottom > nav:last-child, .dataTable-bottom > div:last-child {
    float: left;
  }
  </style>
@endsection
@section('content')
    @include('auth.passwords.confirmmodal')

<div class="panel-header panel-header-sm">
  </div>
  <div class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header">
            @include('flash-message')
                <div class="float-left">
                    <h4 class="card-title">Account Master</h4>
                </div>
                <div class="float-right">
                      <a href="javascript:void(0)" title="Add Account" class="btn btn-success" id="addNewAccMaster">
                        <li class="fas fa-plus-circle fa-lg" aria-hidden="true"></li>
                      </a>
                    <button class="btn btn-danger delete_all" data-url="{{ url('DeleteAllAcctMaster') }}">Delete All Selected</button>
                </div>
          </div>
          <div class="card-body">
            <div id="modalCatcher" class="table-responsive">
              <input type="hidden" value="{{url('/')}}" id="url" name="url">
                <table id="datatablesSimple" class="table">
                <thead class=" text-primary">
                    <th><input type="checkbox" id="master"></th>
                    <th >Action</th>
                    <th>Code</th>
                    <th>Description</th>
                    <th>Transaction Type</th>
                    <th>Created By</th>
                    <th>Date Created</th>
                    <th>Updated By</th>
                    <th>Date Updated</th>
                </thead>
                <tbody>
                    @if($acctMaster->count())
                    @foreach ($acctMaster as $data)
                    <tr>
                    <td ><input type="checkbox" class="sub_chk" data-id="{{$data->id}}"></td>
                    <td>
                      <a href="javascript:void(0)" title="Edit AccMaster" class="edit" data-id="{{ $data->id }}">
                        <i class="fas fa-edit  fa-lg"></i>
                      </a>
                    @csrf
                    @method('DELETE')
                    </td>
                    <td>{{ $data->acct_cd }}</td>
                    <td>{{ $data->description }}</td>
                    <td>{{ $data->transaction->description }}</td>
                    <td>{{ $data->crea_by }}</td>
                    <td>{{ date_format($data->created_at, 'jS M Y') }}</td>

                    @if(isset($data->upd_by))
                    <td>{{ $data->upd_by }}</td>
                    @else
                    <td></td>
                    @endif
                    @if(isset($data->updated_at))
                    <td>{{ date_format($data->updated_at, 'jS M Y') }}</td>
                    @else
                    <td></td>
                    @endif

                    </tr>
                    @endforeach
                    @endif
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      
    </div>
  </div>
    <!-- boostrap model -->
    <div class="modal fade" id="ajax-AccMaster-model" aria-hidden="true" data-backdrop="static" data-keyboard="false">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title" id="ajaxAccMasterModel"></h4>
            <button type="button" class="btn btn-default close-modal" >Close</button>
          </div>
          <div class="modal-body">
            <form action="javascript:void(0)" class="form-horizontal" method="POST">
              {{ csrf_field() }}
              <input type="hidden" name="_method" value="POST">
              <input type="hidden" name="id" id="id">
              
              <div class="form-group">
                <label for="name" class="col-sm-2 control-label">Code</label>
                <div class="col-sm-12">
                  <input type="text" class="form-control" id="acct_cd" name="acct_cd" value="" maxlength="20" required="">
                </div>
              </div>

              <div class="form-group">
                <label for="name" class="col-sm-2 control-label">Transaction Type</label>
                <div class="col-sm-12">
                  <select class="form-control m-bot15" id="trxn_type_id" name="trxn_type_id">
                  @if ($transaction->count())
                  @foreach($transaction as $data)
                  <option value="{{ $data->id }}" >{{ $data->cd }}</option> 
                  @endforeach   
                  @endif
                  </select>
                </div>
              </div>  

              <div class="form-group">
                <label for="name" class="col-sm-2 control-label">Description</label>
                <div class="col-sm-12">
                  <textarea class="form-control" id="description" name="description"  value="" required="">
                  </textarea>
                </div>
              </div> 

              <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-primary btn-save" id="btn-save">Save
                </button>
              </div>
            </form>
          </div>
          <div class="modal-footer">
            
          </div>
        </div>
      </div>
    </div>
<!-- end bootstrap model -->

<script>
  // window.addEventListener("load", function() {
    window.addEventListener('DOMContentLoaded', event => {
    // Simple-DataTables
    // https://github.com/fiduswriter/Simple-DataTables/wiki
    
    const datatablesSimple = document.getElementById('datatablesSimple');
    if (datatablesSimple) {
        new simpleDatatables.DataTable(datatablesSimple, {
    searchable: false,
    fixedHeight: true
        });
    }
  });
$(document).ready(function(){

 const swalWithBootstrapButtons = Swal.mixin({
                  customClass: {
                    confirmButton: 'btn btn-success',
                    cancelButton: 'btn btn-danger'
                  },
                  buttonsStyling: false
                }) 

  $('.close-modal').click(function () {
     var modal_array = 
    [
      '#ajax-AccMaster-model'
    ];
        closeModal(modal_array);
       // $('#ajax-AccMaster-model').modal('hide');
    });

    $.clearInput = function () {
        $('insert_form').find('input[type=text], input[type=password], input[type=number], input[type=email], select, textarea').val('');
    };
     $('.modal').on('hidden.bs.modal', function(){
             // window.location.reload();
             // $('#insert_form').removeData('bs.modal')
    $.clearInput();
    $(this).find('form')[0].reset();
    });

    $.ajaxSetup({
        headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('#addNewAccMaster').click(function () {
       $('#addEditAccMasterForm').trigger("reset");
       $('#ajaxAccMasterModel').html("Add Account");
       $('#ajax-AccMaster-model').modal('show');
    });
      // $('#modalCatcher').on('click', '#btn-save', function () {
    $('#btn-save').click(function (event) {
         // alert('hello');
          try {
          var id = $("#id").val();
          var acct_cd = $("#acct_cd").val();
          var trxn_type_id = $("#trxn_type_id").val();
          var description = $("#description").val();
          // alert(id+cd+description+trxn_type_id);
          $("#btn-save").html('Please Wait...');
          $("#btn-save"). attr("disabled", true);
        }
          catch(err) {
            swalWithBootstrapButtons.fire({
                  title: 'Error!',
                  text: err,
                  icon: 'warning',
                  confirmButtonText: 'Confirmed',
                }).then((result) => {
             // window.location.reload();
            $("#btn-save").html('Submit');
            $("#btn-save"). attr("disabled", false);
                })
            }
        // event.preventDefault();
        // ajax
        $.ajax({
            type:"POST",
            url: "{{ url('add-update-acctMaster') }}",
            // headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            data: {
              id:id,
              acct_cd:acct_cd,
              trxn_type_id:trxn_type_id,
              description:description,
            },
            dataType: 'json',
            success: function(res){
              // alert('success');
              swalWithBootstrapButtons.fire({
                  title: 'Success!',
                  icon: 'success',
                  confirmButtonText: 'Confirmed',
                }).then((result) => {
             window.location.reload();})
              // alert(JSON.stringify(res));
            $("#btn-save").html('Submit');
            $("#btn-save"). attr("disabled", false);
           }
           // ,error: function (data) {
           //       // alert('please delete the client connected to this client/client');
           //       swalWithBootstrapButtons.fire({
           //        title: 'Error!',
           //        text: data.responseText,
           //        icon: 'warning',
           //        confirmButtonText: 'Confirmed',
           //      }).then((result) => {
           //       // window.location.reload();
           //          $("#btn-save").html('Submit');
           //          $("#btn-save"). attr("disabled", false);
           //      })
           //   }

            ,error: function(data){
           var errors = [];
            $.each( data.responseJSON['errors'], function( key, value ) {
                errors += value+'<br>';
            });
            // alert(errors);
            swalWithBootstrapButtons.fire({
                  title: 'Error!',
                  html: errors,
                  icon: 'warning',
                  confirmButtonText: 'Confirmed',
                }).then((result) => {
                 // window.location.reload();
                    $("#btn-save").html('Submit');
                    $("#btn-save"). attr("disabled", false);
                })
            
          }
          
        });

    });
     $('#modalCatcher').on('click', '.edit', function () {

        var id = $(this).data('id');
                var base_path = $("#url").val();
        // ajax
        //   alert( base_path );
        $.ajax({
            type:"POST",
            url: base_path+"/edit-acctMaster",
            data: { id: id },
            dataType: 'json',
            success: function(res){
                // alert(JSON.stringify(res));
                // var date = res.sched_date->format('m-d-Y');
              $('#ajaxAccMasterModel').html("Edit AccMaster");
              $('#ajax-AccMaster-model').modal('show');
              $('#id').val(res.id);
              $('#acct_cd').val(res.acct_cd);
              $('#description').val(res.description);
           },
            error: function (data) {
                 
                 alert(data.responseText);
                 
             }
        });

    });


});
</script>
@endsection

   

