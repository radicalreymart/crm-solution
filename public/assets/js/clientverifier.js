window.addEventListener("load", function() {
$('#gender').on('change',function()
    {
        
        var fname = document.getElementById("fname").value;
        var mname = document.getElementById("mname").value;
        var lname = document.getElementById("lname").value;
        var bday = document.getElementById("datepicker").value;
        var date = new Date(bday);
        var utcString = date.toISOString().substring(0,19);
        var explodedate = utcString.split("T");
        var localDatetime = explodedate[0]+' '+explodedate[1];
        var fullname = fname+' '+mname+' '+lname;
        var oldclient = fullname+'|'+localDatetime;
        var gender = $(this).val();
        var id = $(this).data('id');
                var base_path = $("#url").val();
        if(gender){
            // alert(oldclient);
        $.ajax({
                            type:"GET",
                            url:base_path+'/getClient1/'+oldclient,
                            success:function(res){               
                            if(res){
                                // alert('client already registered');
                            $.each(res,function(key,value){
                                var explodedate1 = value['birthdate'].split(" ");
                                var agestring = value['contact_no'].substr(3);
                                 // alert(JSON.stringify(agestring));
                                 var of_tag1 = value['of_tag'][0];
                                 var of_tag2 = value['of_tag'][2];
                                 // alert(of_tag);
                                   $('#addEditPMESForm').trigger("reset");
                                   $('#ajaxPMESModel').html("Is this you?");
                                   $('#ajax-pmes-model').modal('show');
                                   $('#id').val(value['id']);
                                    if(of_tag1 == 0){                                    
                                   document.getElementById('1selfn').checked = true; 
                                   }else{
                                   document.getElementById('1selfy').checked = true;
                                   }
                                   if(of_tag2 == 0){                                    
                                   document.getElementById('1relativen').checked = true; 
                                   }else{
                                   document.getElementById('1relativey').checked = true;
                                   }
                                   $('#fname1').val(value['fname']);
                                   $('#mname1').val(value['mname']);
                                   $('#lname1').val(value['lname']);
                                   $('#fullname').val(value['fullname']);
                                   $('#datepicker1').val(explodedate1[0]);
                                   $('#agecal1').val(value['age']);
                                   $('#gender1').val(value['gender']);
                                   $('#contact_no1').val(agestring);
                                   if(value['occupation1'] != null)
                                   {
                                    $('#occupation1').val(value['occupation']);
                                   }else{
                                   $('#occupation1').val(0);
                                   }
                                   if(value['other_occupation'] != null)
                                   {
                                   $('#other_occupation1').attr('hidden', false);
                                   $('#other_occupation1').val(value['other_occupation']);
                                   }
                                   $('#region_id1').val(value['region_id']);
                                    var province_code = value['province_id'];
                                    var city_code = value['city_id'];
                                    var barangay_code = value['barangay_id'];
                                    // alert(barangay_code);
                                   $.ajax({
                                        type:"GET",
                                        // base_path+'getProvince/'+region_code,
                                        url:base_path+'/getProvince/'+value['region_id'],
                                        success:function(res){               
                                        if(res){
                                        $("#province_id1").empty();
                                        // $("#province_id1").attr('readonly', false);
                                        $("#province_id1").append('<option value="">Select Province</option>');                            
                                        $("#city_id1").append('<option value="">Select City</option>');
                                        $("#barangay_id1").empty();
                                        $("#barangay_id1").append('<option value="">Select Barangay</option>');
                                        $.each(res,function(key,value){

                                            $("#province_id1").append('<option class="form-control" value="'+ value['province_id'] +'">'+ value['name'] +'</option>');
                                            $('#province_id1').val(province_code);
                                        });
                                        }else{
                                           $("#province_id1").empty();
                                        }
                                        }
                                        });
                                   $.ajax({
                                       type:"GET",
                                       url:base_path+'/getCities/'+province_code,
                                       success:function(res){  
                                        if(res){
                                            $("#city_id1").empty();
                                            $("#city_id1").append('<option value="">Select City</option>');
                                            $("#barangay_id1").empty();
                                            $("#barangay_id1").append('<option value="">Select Barangay</option>');
                                            $.each(res,function(key,value){
                                                $("#city_id1").append('<option class="form-control" value="'+ value['city_id'] +'">'+ value['name'] +'</option>');
                                               $('#city_id1').val(city_code);
                                            });
                                        }else{
                                           $("#city_id1").empty();
                                        }
                                       }
                                        });
                                   $.ajax({
                                           type:"GET",
                                           url:base_path+'/getBarangays/'+city_code,
                                           success:function(res){  
                                            if(res){
                                               $("#barangay_id1").empty();
                                                $("#barangay_id1").append('<option value="">Select Barangay</option>');
                                                $.each((res),function(key,value){
                                            // alert(JSON.stringify(value['code']));
                                                    $("#barangay_id1").append('<option class="form-control" value="'+ value['code'] +'">'+ value['name'] +'</option>');
                                                   $('#barangay_id1').val(barangay_code);
                                                });
                                            }else{
                                               $("#barangay_id1").empty();
                                            }
                                           }
                                        });
                                   $('#street1').val(value['street']);
                                   // alert(of_tag1);
                                   if(of_tag1 == 0){                                    
                                   document.getElementById('1selfn').checked = true; 
                                   }else{
                                   document.getElementById('1selfy').checked = true;
                                   }
                                   if(of_tag2 == 0){                                    
                                   document.getElementById('1relativen').checked = true; 
                                   }else{
                                   document.getElementById('1relativey').checked = true;
                                   }

                                   // $('.self').val(of_tag1);
                                   // $('.relative').val(of_tag2);
                                   $('#marketing_channel1').val(value['marketing_channel_id']);
                                   $('#purpose_tag1').val(value['purpose_tag']);
                                   $('#member_status1').val(value['member_status']);
                                   if(value['member_status'] == 0)
                                       {
                                       $('#div-staffcd1').attr('hidden', false);
                                       $('#staff_referral_cd1').attr('hidden', false);
                                       $('#staff_referral_cd1').val(value['staff_referral_cd']);
                                       }
                                    else
                                       {
                                       $('#div-memno1').attr('hidden', false);
                                       $('#member_no1').attr('hidden', false);
                                       $('#member_no1').val(value['member_no']);
                                       }
                                   $('#seminar_pref1').val(value['seminar_pref']);
                                   $('#batch_id1').val(value['batch_id']);
                                   $('#branch_id1').val(value['branch_id']);
                                   $('#gmail_address1').val(value['gmail_address']);
                                });
                                // $('#contact_no').val(value['contact_no']);
                                // $('#gmail_address').val(value['gmail_address']);
                                // // $('#region_id').empty();
                                // // $("#region_id").append('<option class="form-control" value="'+ value['region_id'] +'">'+ value['name'] +'</option>');
                                // $('#region_id').val(value['region_id']);
                                // $('#region_hidden').val(value['region_primary']);
                                // // $('#province_id').empty()
                                // $('#province_id').val(value['province_id']);
                                // $('#province_hidden').val(value['province_primary']);
                                // // $('#city_id').empty();
                                // $('#city_id').val(value['city_id']);
                                // $('#city_hidden').val(value['city_primary']);
                                // // $('#barangay_id').empty();
                                // $('#barangay_id').val(value['barangay_id']);
                                // $('#barangay_hidden').val(value['barangay_primary']);
                                // $('#street').val(value['street']);
                                // $('#occupation').val(value['occupation_id']);
                                // $('#other_occupation').val(value['other_occupation']);
                                // $('#of_tag').val(value['of_tag']);
                                // $('#marketing_channel').val(value['marketing_channel']);
                                // $('#purpose_tag').val(value['purpose_tag']);
                                // $('#member_status').val(value['member_status']);
                                // $('#member_no').val(value['member_no']);
                                // $('#staff_referral_cd').val(value['staff_referral_cd']);
                                // $('#seminar_pref').val(value['seminar_pref']);
                                // $('#batch_id').val(value['batch_id']);
                                // $('#branch_id').val(value['branch_id']);
                                // $('#data_consent_fl').val(value['data_consent_fl']);
                            // });
                            }else{
                            }
                            }
                            });
        }
    });
});