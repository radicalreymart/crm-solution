<!-- Modal -->
<div wire:ignore.self class="modal fade" id="updateModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog" role="document">
       <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Edit Batch</h5>
                <button type="button" class="close" wire:click="cancel" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group row">
                            <input type="hidden" wire:model="batch_id">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Batch Code') }}</label>

                            <div class="col-md-6">
                                <input id="batch_cd" type="text" wire:model="batch_cd" class="form-control @error('batch_cd') is-invalid @enderror" name="batch_cd" value="{{ old('batch_cd') }}" required autocomplete="batch_cd" autofocus />

                                @error('batch_cd')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">Schedule Date and Time</label>
                             <div class="col-md-6">
                                <input type="datetime" wire:model="sched_date" class="form-control"  name="sched_date" min="{{$mindate}}" id="dateInput2">
                                <span class="text-danger">
                                    <strong>{{$errors->first('sched_date') ?? null}}
                                    </strong>
                                </span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="employee_role" class="col-md-4 col-form-label text-md-right">{{ __('Preferred Application') }}</label>

                            <div class="col-md-6">
                                <select name="online_vid_tool" wire:model="online_vid_tool" class="form-control">
                                                <option value="">Select...</option>
                                                <option value = 0>Google Meet</option>
                                </select>
                                @error('online_vid_tool')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="meeting_cd" class="col-md-4 col-form-label text-md-right">{{ __('Meeting Code') }}</label>

                            <div class="col-md-6">
                                <input id="meeting_cd" wire:model="meeting_cd" type="text" class="form-control @error('meeting_cd') is-invalid @enderror" name="meeting_cd" value="{{ old('meeting_cd') }}" required autocomplete="meeting_cd" autofocus />

                                @error('meeting_cd')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        

                        <div class="form-group row">
                            <label for="meeting_pw" class="col-md-4 col-form-label text-md-right">{{ __('Meeting Password') }}</label>

                            <div class="col-md-6">
                                <input id="meeting_pw" wire:model="meeting_pw"  class="form-control @error('meeting_pw') is-invalid @enderror" name="meeting_pw" required autocomplete="new-meeting_pw">

                                @error('meeting_pw')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Reschedule Remarks') }}</label>

                            <div class="col-md-6">
                                
                                <textarea id="resched_remarks" wire:model="resched_remarks" class="form-control" name="resched_remarks"  required autocomplete="resched_remarks" autofocus></textarea>

                                @error('resched_remarks')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" wire:click.prevent="cancel()" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" wire:click.prevent="update()" class="btn btn-primary" data-dismiss="modal">Save</button>
            </div>
       </div>
    </div>
</div>