
@extends('layouts.app', [
    'class' => 'sidebar-mini ',
    'namePage' => 'Loan Type Term Create',
    'activePage' => 'loan-type-term-management',
    'activeNav' => '',
])

@section('content')
<div class="panel-header panel-header-sm">
  </div>
  <div class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="card-header">
                <div class="float-left">
                    <h4 class="card-title">New Loan Type-Payment Term</h4>
                </div>
                <div class="float-right">
                    <a class="btn btn-primary" href="{{ url('/loanTypeTerm/') }}"> Back</a>
                </div>
          
        <div class="card">
                        <div class="card-body">
                                @if(count($errors) > 0 )
                            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                            <ul class="p-0 m-0" style="list-style: none;">
                            @foreach($errors->all() as $error)
                            <li>{{$error}}</li>
                            @endforeach
                            </ul>
                            </div>
                            @endif
                                <form method="POST" action="{{ route('loanTypeTerm.store') }}">
                                    @csrf

                                    <div class="form-group row">    

                                        <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Loan Type') }}</label>

                                        <div class="col-md-5">
                                            
                                            <select id="loan_type_id" name="loan_type_id" class="form-control">
                                            <option value="" selected disabled>Select Loan Type</option>
                                            @foreach($loan_type as $item)
                                            <option value="{{$item->id}}">{{$item->name}}</option>
                                            @endforeach
                                            </select>

                                            @error('loan_type_id')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                        <div class="col-md-2" style="padding-top: 5px; "><a  href="{{ route('loanType.create') }}">
                                            <li style="color: green;" class="fas fa-plus-circle fa-lg" aria-hidden="true"></li>
                                        </a></div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="name" class="col-md-4 col-form-label text-md-right">{{ __(' Payment Term') }}</label>

                                        <div class="col-md-6">
                                            <select id="payterm_id" name="payterm_id" class="form-control">
                                            <option value="" selected disabled>Select Payment Terms</option>
                                            @foreach($payment_terms as $item)
                                            <option value="{{$item->id}}">{{$item->name}}</option>
                                            @endforeach
                                            </select>


                                            @error('payterm_id')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                        </div>
                                      <div class="form-group row mb-0">
                                      <div class="col-md-6 offset-md-4">

                                      <button type="submit" class="btn btn-primary"  >Submit</button>
                                      </div>
                                      </div>
                                  </form>

        </div>


        </div>
      </div>
    </div>
  </div>

@endsection
