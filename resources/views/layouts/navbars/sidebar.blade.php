<div class="sidebar" data-color="blue">
  <!--
    Tip 1: You can change the color of the sidebar using: data-color="blue | green | orange | red | yellow"
-->
  <div class="logo">
    <a href="/home" class="simple-text logo-normal">
      {{ __('CMSRS-Solutions') }}
    </a>
  </div>
  <div class="sidebar-wrapper" id="sidebar-wrapper">
    <ul class="nav">
      <li class="@if ($activePage == 'home') active @endif">
        <a href="{{ route('home') }}">
          <i class="now-ui-icons design_app"></i>
          <p>{{ __('Dashboard') }}</p>
        </a>
      </li>
      <li>
          @if(Auth::user()->department_id == 1 || Auth::user()->role_id === 0 )
        <a class="hvr-icon-bounce" data-toggle="collapse" href="#pmesScheduling">
            <i class="now-ui-icons design_bullet-list-67 hvr-icon"></i>
          <p>
            {{ __("PMES Scheduling") }}
            <b class="caret"></b>
          </p>
        </a>
        <div class="collapse" id="pmesScheduling">
          <ul class="nav">
            <li class="@if ($activePage == 'batch-management') active @endif">
              <a href="{{ url('batchv2') }}">
                <i class="far fa-calendar-alt"></i>
                <p> {{ __("Batch Schedules") }} </p>
              </a>
            </li>
            <li class="@if ($activePage == 'client-management') active @endif">
              <a href="{{ url('clientv2') }}">
                <i class="fas fa-user-friends"></i>
                <p> {{ __("Client Master") }} </p>
              </a>
            </li>
            <li class="@if ($activePage == 'pmes-management') active @endif">
              <a href="{{ route('pmes.index') }}">
                <i class="far fa-calendar-check"></i>
                <p> {{ __("PMES Attendance") }} </p>
              </a>
            </li>
          </ul>
        </div>
          @endif
            @if(Auth::user()->department_id == 2 || Auth::user()->role_id == 0 )
        <a class="hvr-icon-wobble-horizontal" data-toggle="collapse" href="#loanManagement">
            <i class="fas fa-hand-holding-usd hvr-icon"></i>
          <p>
            {{ __("Loan Management") }}
            <b class="caret"></b>
          </p>
        </a>
        <div class="collapse" id="loanManagement">
          <ul class="nav">
            <li class="@if ($activePage == 'preloan-management') active @endif">
              <a href="{{ url('preloan') }}">
                <i class="fas fa-coins"></i>
                <p> {{ __("New Loan Index") }} </p>
              </a>
            </li>
          </ul>
        </div>
          @endif

            @if(Auth::user()->role_id == 0 || Auth::user()->department_id == 3)
        <a class="hvr-icon-pulse-grow" data-toggle="collapse" href="#paymentManagement">
            <i class="fas fa-cash-register hvr-icon "></i>
          <p>
            {{ __("Payment Management") }}
            <b class="caret"></b>
          </p>
        </a>
        <div class="collapse" id="paymentManagement">
          <ul class="nav">
            <li class="@if ($activePage == 'optm-management') active @endif">
              <a class="shorten_text"  href="{{ url('optm') }}">
                <i class="fas fa-hands-helping" style="padding-top: 10px;"></i>
                 {{ __("Online Payment") }} <br> {{ __("Transaction Master") }}
              </a>
            </li>
          </ul>
          <ul class="nav">
            <li class="@if ($activePage == 'optd-management') active @endif">
              <a class="shorten_text"  href="{{ url('optd') }}">
                <i class="fas fa-donate" style="padding-top: 10px;"></i>
                 {{ __("Online Payment") }} <br> {{ __("Transaction Details") }}
              </a>
            </li>
          </ul>
          
        </div>
        @endif
        <a class="spinning" data-toggle="collapse" href="#Settings">
            <i class="now-ui-icons loader_gear "></i>
          <p>
            {{ __("Settings") }}
            <b class="caret"></b>
          </p>
        </a>
        <div class="collapse" id="Settings">
            @if(Auth::user()->role_id == 0)
          <ul class="nav">
            <li class="@if ($activePage == 'user-management') active @endif">
              <a href="{{ url('userv2') }}">
                <i class="far fa-id-card"></i>
                <p > {{ __("User Management") }} </p>
              </a>
            </li>
          </ul> 
          <ul class="nav">
            <li class="@if ($activePage == 'branch-management') active @endif">
              <a href="{{ route('branch.index') }}">
                <i class="fas fa-store"></i>
                <p> {{ __("Branch Management") }} </p>
              </a>
            </li>
          </ul>
          <ul class="nav">
            <li class="@if ($activePage == 'occupation-management') active @endif">
              <a href="{{ route('occupation.index') }}">
                <i class="fas fa-user-tie"></i>
                <p> {{ __("Occupation Management") }} </p>
              </a>
            </li>
          </ul>
          <ul class="nav">
            <li class="@if ($activePage == 'marketingChannel-management') active @endif justify-this">
              <a href="{{ route('marketingChannel.index') }}">
                <i class="fas fa-ad"></i>
                <p> {{ __("Marketing Channel Management") }} </p>
              </a>
            </li>
          </ul>
            @endif

            @if(Auth::user()->role_id == 0 || Auth::user()->department_id == 1)
            <ul class="nav">
              <li class="@if ($activePage == 'miuf-management') active @endif">
                <a href="{{ url('miuf') }}">
                  <i class="far fa-edit"></i>
                  <p > {{ __("Member to be Updated") }} </p>
                </a>
              </li>
            </ul>
            <ul class="nav">
              <li class="@if ($activePage == 'member-management') active @endif">
                <a   href="{{ url('member') }}">
                  <i class="fas fa-users"></i>
                   {{ __("Member") }}
                </a>
              </li>
            </ul>
            @endif
            @if(Auth::user()->role_id == 0 || Auth::user()->department_id == 1 || Auth::user()->department_id == 4 || Auth::user()->department_id == 5)
            <ul class="nav">
            <ul class="nav">
            <li class="@if ($activePage == 'feedback-management') active @endif">
              <a href="{{ url('feedback') }}">
                <i class="far fa-comments" aria-hidden="true"></i>
                <p > {{ __("Feedback Index") }} </p>
              </a>
            </li>
          </ul>
          @endif

            @if(Auth::user()->department_id == 3 || Auth::user()->role_id == 0 )
            <ul class="nav">
              <li class="@if ($activePage == 'acctMaster-management') active @endif">
                <a   href="{{ url('acctMaster') }}">
                  <i class="fas fa-file-invoice-dollar"></i>
                   {{ __("Accounts") }}
                </a>
              </li>
            </ul>
            <ul class="nav">
              <li class="@if ($activePage == 'bank-management') active @endif">
                <a   href="{{ url('bank') }}">
                  <i class="fas fa-piggy-bank"></i>
                   {{ __("Bank") }}
                </a>
              </li>
            </ul>
            <ul class="nav">
              <li class="@if ($activePage == 'trasactionType-management') active @endif">
                <a   href="{{ url('transactionType') }}">
                  <i class="fas fa-hand-holding-usd"></i>
                   {{ __("Transaction Type") }}
                </a>
              </li>
            </ul>
            @endif

            @if(Auth::user()->department_id == 2 || Auth::user()->role_id == 0 )
          <ul class="nav">
            <li class="@if ($activePage == 'loan-type-management') active @endif">
              <a   href="{{ url('loanType') }}">
                <i class="fas fa-hand-holding-usd"></i>
                 {{ __("Loan Type") }}
              </a>
            </li>
          </ul>
          @endif

         
            
        </div>
      <!-- <li class="@if ($activePage == 'icons') active @endif">
        <a href="{{ route('page.index','icons') }}">
          <i class="now-ui-icons education_atom"></i>
          <p>{{ __('Icons') }}</p>
        </a>
      </li> -->
      <!-- <li class = "@if ($activePage == 'maps') active @endif">
        <a href="{{ route('page.index','maps') }}">
          <i class="now-ui-icons location_map-big"></i>
          <p>{{ __('Maps') }}</p>
        </a>
      </li> -->
      <!-- <li class = " @if ($activePage == 'notifications') active @endif">
        <a href="{{ route('page.index','notifications') }}">
          <i class="now-ui-icons ui-1_bell-53"></i>
          <p>{{ __('Notifications') }}</p>
        </a>
      </li> -->
     <!--  <li class = " @if ($activePage == 'table') active @endif">
        <a href="{{ route('page.index','table') }}">
          <i class="now-ui-icons design_bullet-list-67"></i>
          <p>{{ __('Table List') }}</p>
        </a>
      </li> -->
      <li class = " @if ($activePage == 'table') active @endif">
            <a class="logout hvr-icon-push" data-url="{{ url('force-logout') }}">
             <i class="now-ui-icons media-1_button-power hvr-icon"></i>
              <p>   {{ __('Logout') }}</p>
            </a>
      </li>
      
    </ul>
  </div>
</div>
