<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use App\Models\LoanTypePayMode;
use App\Models\LoanType;

class LoanTypeController extends Controller
{
    //
    public function index()
    {
        $loanType = LoanType::whereNull('soft_deleted_at')->simplePaginate(10);
        // dd($loanType);
        return view('loanTypePayment.loanType.index', compact('loanType'));
        
    }

    public function create()
    {

        return view('loanTypePayment.loanType.create');
     
    }

    public function store(Request $request)
    {
        // dd($request->all());

        $request->validate([
            'cd' => 'required',
            'name' => 'required',
        ]);
        $loanType = [
             'cd' => $request['cd'],
             'name' => $request['name'],
            'crea_by' => auth()->user()->uname,
        ];
        $loanType_revive = [ 'id' => $request->id,
            'soft_deleted_at' => null,];
       
       LoanType::updateOrcreate ($loanType,$loanType_revive);
       
        return redirect()->route('loanType.index')
            ->with('success', 'Loan Type created successfully.');
    }

    public function edit($id)
    {
        $loanType = LoanType::find($id);
       
       return view('loanTypePayment.loanType.edit', [
        'loanType' => $loanType
        ]);

    }

    public function update($id, Request $request)
    {
        // dd($request->all());
        $rules = array(
            'cd' => 'required',
            'name' => 'required',
        );
         $validator = Validator::make($request->all(), $rules);
         $loanTypes = LoanType::find($id);
            $loanTypes->cd      = $request->get('cd');
            $loanTypes->name       = $request->get('name');
            $loanTypes->updated_at      = Carbon::now();
            $loanTypes->upd_by = auth()->user()->uname;
            $loanTypes->save();
       
        return redirect()->route('loanType.index')
            ->with('success', 'Loan Type updated successfully');
        
    }


    public function deleteAll(Request $request, loanType $loanType)
    {
        $ids = $request->ids;
        $loanTypes = LoanType::whereIn('id',explode(",",$ids))->whereNull('soft_deleted_at')->get();
        if($loanTypes->count())
        {
            $json = ['error' => 'Please delete Loan Payment Term/Mode corresponding to this entry.'];
        }
        else{
            foreach($loanTypes as $data)
            {
                $loanType = LoanType::find($data->id);
                $loanType->soft_deleted_at = Carbon::now();
                $loanType->save();
            }
        }
                return response()->json(['success'=>"LoanType deleted successfully."]);

    }

}
