<?php

namespace App\Http\Controllers;

use App\Models\PreLoanApplication;
use App\Models\Region;
use App\Models\MarketingChannel;
use App\Models\Occupation;
use App\Models\Branch;
use App\Models\LoanTypePayMode;
use App\Models\LoanTypePayTerm;
use App\Models\PaymentTerms;
use App\Models\LoanType;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use App\Models\Barangay;
use Yajra\Address\HasAddress;
use App\Models\City;
use App\Models\Province;
use App\Models\STM;
use App\Models\User;
use App\Models\UserMember;
use App\Models\Member;
use Yajra\Address\AddressServiceProvider;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendMail;
use App\Mail\LoanMail;
use Livewire\Component;
use Livewire\WithPagination;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\File; 
use Illuminate\Support\Facades\Storage; 
use Alert;
use App\Exports\LoanExport;
use Maatwebsite\Excel\Facades\Excel;

class PreLoanApplicationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $department = auth()->user()->department_id;
        $branch = auth()->user()->branch_id;
        $isAdmin = auth()->user()->role_id;
        $loan_type = LoanType::all();
        $payment_terms = PaymentTerms::all();
        if($department == 2 || $isAdmin == 0){
            $marketingChannel = MarketingChannel::all();
            $regions = Region::all();
            $occupation = Occupation::all();
           $preloan1 = PreLoanApplication::with('usermember','occupation', 'marketing','loan_type');
           if($branch == 9){
               $preloan1->whereNull('soft_deleted_at');
           }else{
               $preloan1->where('branch_id', $branch)
               ->whereNull('soft_deleted_at');
           }
             $preloan =   $preloan1->get();
            // dd();
           // dd($preloan);
             return view('preloan.index', compact('preloan','occupation', 'marketingChannel', 'regions', 'loan_type', 'payment_terms'));
        }else{
            Alert::error("You do not have the permission to access this page.");
            return redirect()->route('home');
        }

    }
    public function payment_modeAPI($id)
    {
        $LoanTypePayMode = LoanTypePayMode::where('loan_type_id', $id)->get(['paymode_id','paymode_name']);
        return response()->json($LoanTypePayMode);
    }

    public function payment_termAPI($id)
    {
        $loanTypePayTerm = LoanTypePayTerm::where('loan_type_id', $id)->get(['payterm_id','payterm_name']);
        return response()->json($loanTypePayTerm);
    }

    public function payment_termMonths($id)
    {
        $paymentTerm = PaymentTerms::where('id', $id)->get('months')->first();
        return response()->json($paymentTerm->months);
    }

    public function create(Request $request)
    {  
        $user_id = auth()->user()->id;
        $usermember_id = UserMember::where('user_id', $user_id)->pluck('id');
        $branch_iddef = UserMember::where('user_id', $user_id)->pluck('branch_id')->first();
        $preloan1 = PreLoanApplication::with('usermember.user')->where('usermember_id', $usermember_id[0])->get();
        // dd($preloan1[0]->usermember);
        // if(count($preloan1)){
        //     return redirect()->back();
        // }else{
        $branches  = Branch::whereNull('soft_deleted_at')
        ->Where('cd', '!=', 'TRD')
        ->Where('cd', '!=', 'FR')
        ->Where('cd', '!=', 'ADMIN')
        ->get();
        $regions = Region::all();
        $city = City::all();
        $province = Province::all();
        $barangay = Barangay::all();
        $occupation = Occupation::all();
        $marketingChannel = MarketingChannel::all();
        $loanType = LoanType::whereNull('soft_deleted_at')->get();
        $user_member = UserMember::with('member')->where('user_id', auth()->user()->id)->get();
        return view('enduser.preloan.create', compact('user_member','occupation','regions', 'city','province', 'barangay',
        'marketingChannel', 'branches', 'loanType', 'preloan1','branch_iddef'));
        // }
    }
    public function store(Request $request)
    {
        $sitefeedback = $request->sitefeedback;
        // dd($id_type);
        $validator = Validator::make($request->all(), [
             'attachment' => 'array|min:8'
            ,'attachment.*' => 'image|mimes:jpeg,png,jpg,gif,svg|max:5000'
            ,'amount_needed' => 'required'
            ,'branch_id1' => 'required'
            ,'loan_product' => 'required'
            ,'share_capital' => 'required'
            ,'loan_purpose' => 'required'
            ,'payment_terms' => 'required'
            ,'payment_mode' => 'required'
            ,'occupation_id' => 'required'
            ,'montly_income' => 'required'
            ,'montly_expense' => 'required'
            ,'marketing_channel_id' => 'required'
         ], $message = ['min' => 'All document must be filled',
                        'max' => 'Your image is to large']
         );

            // dd($validator);
        if ($validator->fails())
        {
            Alert::error('Validation Error: '.$validator->errors()->first())->autoClose(null);
            return redirect()->back();  
            // redirect()->back()->with('error', ['your message here']);            
        }

                     // dd($validator);
        if(isset($request->id)){
            $upd_by = auth()->user()->uname;
            $updated_at = Carbon::now();
        }else{
            $upd_by = null;
            $updated_at = null;
        }
        $user_id = auth()->user()->id;
        $usermember_id = UserMember::where('user_id', $user_id)->pluck('id');
        // dd($usermember_id[0]);
            $checked = $request->has('data_consent_fl') ? 1 : 0;
            $intChannel = intval($request['marketing_channel_id']);
            if(auth()->user()->role_id == 0 ||  auth()->user()->role_id == 1){
                $crea_by = auth()->user()->uname;
             }else{                
                $crea_by = 'enduser';
             }
         $id = $request['id'];
        $preloanfile  = PreLoanApplication::where('id',$id)->whereNull('soft_deleted_at')->where('loan_status', 1)
        ->first('file_path1');
        // dd(is_null($preloanfile));
            $randomStr = Str::random(11);
        $share_capital = $request['share_capital'];
        $amount_needed = $request['amount_needed'];
        $randInt = rand(100,999);
            if(is_null($preloanfile)){
                if(is_null($request['id'])){
                     $files = $request->file('attachment');
                     $i = 1;
                  foreach ($files as $file)
                    {
                        $id_type = ($request['id_type'] == 'Other') ?  $request['other_id_type'] : $request['id_type'];
                        if(!empty($file)){
                           if($i == 1){
                                $name = $id_type.'^'.'frontID';
                           }elseif($i == 2){
                                $name = $id_type.'^'.'backID';
                           }elseif($i == 3){
                                $name = 'SoAimg1';
                           }elseif($i == 4){
                                $name = 'SoAimg2';
                           }elseif($i == 5){
                                $name = 'SoAimg3';
                           }elseif($i == 6){
                                $name = 'CtSimg1';
                           }elseif($i == 7){
                                $name = 'CtSimg2';
                           }elseif($i == 8){
                                $name = 'CtSimg3';
                           }elseif($i == 9){
                                $name = 'Signature';
                           }
                            // $origname=$file->getClientOriginalName();
                            $extension=$file->getClientOriginalExtension();
                            $filename[$i] = $randInt.Carbon::now()->format('Ymd').('-').$name.'.'.$extension;
                            $file->move('assets/img/uploads/loans/'.$randInt.Carbon::now()->format('Ymd').'/', $filename[$i]);
                        }
                        $filearray[] = $filename[$i];
                        $i++;
                        // dd($filearray);
                    }
                    $str_json = $filearray[0] . '|' .  $filearray[1] . '|' . $filearray[2].'|'.$filearray[3].'|'.$filearray[4] . '|' . $filearray[5].'|'.$filearray[6].'|'.$filearray[7];
                }else{
                    $str_json = $request['file_path1'];
                }
            }else{
                $str_json = $preloanfile; 
            }
                 // dd($filearray[8]);
            if(isset($filearray[8])){
            $str_signiture = $filearray[8];
            }else{
            $folderPath =  str_replace('\\', '/', public_path('assets/img/uploads/loans/'.$randInt.Carbon::now()->format('Ymd').'/'));
                // mkdir($folderPath, 0777, true);
            
            $image_parts = explode(";base64,", $request->hidden_sig);
            $image_type_aux = explode("image/", $image_parts[0]);
            $image_type = $image_type_aux[1];
            $image_base64 = base64_decode($image_parts[1]);
            $file = $folderPath . $randInt.Carbon::now()->format('Ymd') .'-Signature'.'.'.$image_type;
            $str_signiture = $randInt.Carbon::now()->format('Ymd').'-Signature'. '.'.$image_type;
                    // dd($str_signiture);
            }
        if($request['occupation_id'] == 0){
            $occupation_id = null;
        }else{
            $occupation_id = $request['occupation_id'] ;
        }

        if($request['loan_purpose'] == 11){
            $loan_purpose = null;
        }else{
            $loan_purpose = $request['loan_purpose'] ;
        }

        $preloan_create = [
                 'file_path1' => $str_json
                 ,'signiture' => $str_signiture
                ,'branch_id' => $request['branch_id1']
                // ,'contact_no' => '+63'.$request['contact_no']
                ,'co_maker' => $request['co_maker']
                ,'other_occupation' => $request['other_occupation']
                // ,'age_group' => $request['age_group']
                // ,'region_id' => $request['region_id']
                // ,'province_id' => $request['province_id']
                // ,'city_id' => $request['city_id']
                // ,'barangay_id' => $request['barangay_id']
                // ,'street' => $request['street']
                // ,'fb_link' => $request['fb_link']
                ,'share_capital'        =>    $request['share_capital']
                ,'marketing_channel_id' =>    $intChannel
                ,'amount_needed'        => $request['amount_needed']
                ,'savings_salary_amount'=> $request['savings-salary-amount']
                ,'loanable_amount'      => $request['loanable_amount']
                ,'ref_number'           => $randomStr
                ,'loan_product'         => $request['loan_product']
                ,'loan_purpose'         => $loan_purpose
                ,'other_purpose'        => $request['other_purpose']
                ,'payment_terms'        => $request['payment_terms']
                ,'payment_mode'         => $request['payment_mode']
                ,'occupation_id'        => $occupation_id
                ,'montly_income'        => $request['montly_income']
                ,'montly_expense'       => $request['montly_expense']
                ,'usermember_id'        => $usermember_id[0]
                ,'data_consent_fl'      => $checked
                ,'crea_by'              => $crea_by
                ,'created_at'           => Carbon::now()
           ];
        $preloan = PreLoanApplication::create($preloan_create);
             if(isset($filearray[8])){

             }else{

                // $url = "/home3/mhrmpcoo/crm.mhrmpcoop.com.ph/assets/img/uploads/loans/".$randInt.Carbon::now()->format('Ymd').'/'.$randInt.Carbon::now()->format('Ymd').'-Signature'. '.'.$image_type;
                // file_put_contents($url, $image_base64);
                file_put_contents($file, $image_base64);
             }
        // // STM::create([
        // //             'preloan_id' => $preloan->id,
        // //             'service_details'=> '.',
        // //             'service_status' => 1,
        // //             'service_type_id' => 1,
        // //             'crea_by' => $crea_by,
        //             ]);
         // dd($request->all());
        if($sitefeedback == 'yes'){
        return redirect()->route('enduser.sitefeedback', 'non-logout');
        }else{
          if(auth()->user()->role_id == 3){
          // return response()->json($image_type_aux );
            Alert::success('Loan application complete', "Please don't forget to copy the reference number:".$randomStr)->autoClose(null);
            return redirect()->route('enduser.home')->with('success', 'Post created successfully');
            // return response()->json(['success' => $randomStr], 200);
          }elseif(auth()->user()->role_id == 0 || auth()->user()->role_id == 1 ){
            return response()->json(['success' => 'success'], 200);
          }else{
            return redirect()->back();
          }
        }
    }

    public function enduserpreloanindex()
    {
        $user_id = auth()->user()->id;
        $loan_type = LoanType::all();
        $payment_terms = PaymentTerms::all();
        $usermember_id = UserMember::where('user_id', $user_id)->pluck('id');
        $preloan  = PreLoanApplication::with('usermember','marketing','occupation', 'loan_type')->where('usermember_id', $usermember_id)->get();
        // dd($preloan);

        $preloan_pending = PreLoanApplication::with('usermember','marketing','occupation')->orwhere('usermember_id', $usermember_id)->where('loan_status', null)->orWhere('loan_status', 3)->first();

         $preloan_ongoing  = PreLoanApplication::with('usermember','marketing','occupation')->where('usermember_id', $usermember_id)->Where('loan_status', 1)->orWhere('loan_status', 5)->first();

         $preloan_paid_or_declined  = PreLoanApplication::with('usermember','marketing','occupation')->where('usermember_id', $usermember_id)->where('loan_status', 2)->orWhere('loan_status', 4)->get();
        // dd($preloan_paid_or_declined);

        $branch  = Branch::whereNull('soft_deleted_at')
        ->Where('cd', '!=', 'TRD')
        ->Where('cd', '!=', 'FR')
        ->Where('cd', '!=', 'ADMIN')
        ->get();
        $marketingChannel = MarketingChannel::all();
        $regions = Region::all();
        $occupation = Occupation::all();
        return view('enduser.preloan.index', compact('preloan_pending', 'preloan_paid_or_declined','preloan_ongoing','preloan','occupation', 'marketingChannel', 'branch' ,'regions', 'payment_terms', 'loan_type'));
    }
    public function editmodeterms(Request $request, PreLoanApplication $preloan)
    {
        // dd($request->all());
        $id = $request['id'];

        // dd($request['loanstatus']);
       $preloan = PreLoanApplication::find($id);
       $payment_terms = $preloan->payment_terms;
       $payment_mode = $preloan->payment_mode;
       $preloan->payment_terms = $request['payment_terms'];
       $preloan->payment_mode = $request['payment_mode'];
       $preloan->loan_status = null;
       if($request['payment_terms'] == $payment_terms){
        Alert::success('Payment Mode updated');
       }elseif($request['payment_mode'] == $payment_mode){
        Alert::success('Payment Term updated');
       }else{
        Alert::success('Payment Term and Mode updated');
       }
       $preloan->save();
       return redirect()->back();

    }

    public function loanstatus(Request $request, PreLoanApplication $preloan)
    {
        $validator = Validator::make($request->all(), [
            // 'decline' =>'required', 
            'approval_remarks' =>'required', 
        ]
        ,[
            // 'decline.required' => 'Decline type is required.',
            'approval_remarks.required' => 'Remark is required.'
        ]);
        if ($validator->fails())
        {
            $a = implode('<br>',$validator->errors()->all());
            Alert::error('Validation Error: <br>'.$a)->autoClose(null);
            return redirect()->back();        
        }

        $decline = 'temp';
        
        $approval_remarks = $request['approval_remarks'];
        $id = $request['iddecline'];

        // dd($request['loanstatus']);
       $preloan = PreLoanApplication::with('usermember.user')->find($id);
       $preloan->approval_remarks     =  $request['approval_remarks'];
       $preloan->loan_status = 1;
       $preloan->upd_by = auth()->user()->uname;
       $loaner_name = $preloan->usermember->user->uname;
       $email = $preloan->usermember->user->email;
       // dd($email);
       $preloan->save();

        $details = 
        [
            'decline_type' => $decline,
            // 'decline_type' = $decline,
            'remarks' => $request['approval_remarks'],
        ];

        Mail::to($email)->queue(new LoanMail($details));
        Alert::success('Info', $loaner_name.'Loan is pending for approval.');
       // return response()->json(['success' => 'success'], 200);
        return redirect()->route('preloan')->with('success', $loaner_name.' Loan is pending for approval.');
    }
    public function loanstatusmodal($id, Request $request, PreLoanApplication $preloan)
    {
        $loanstatus = $request['loanstatus'];
        // dd($request['loanstatus']);
       $preloan = PreLoanApplication::find($id);
       $preloan->loan_status     =  $request['loanstatus'];
       $preloan->upd_by = auth()->user()->uname;
       $preloan->save();
       return response()->json(['success' => 'success'], 200);
        // return redirect()->route('preloan')->with('success', 'Loan status edited successfully');
    }


    public function loanstatusAPI($id, Request $request, PreLoanApplication $preloan)
    {
        $preloan  = PreLoanApplication::where('id',$id)->first();

        $json = array(
            'loan_status'           => $preloan['loan_status'],
            'approval_remarks'      => $preloan['approval_remarks'],
        );
        return response()->json($json);
    }

    public function loanapproval(Request $request)
    {
        // dd($request->all());
        $validator = Validator::make($request->all(), [
            'amount_approved1' =>'required', 
            'approval_remarks' =>'required', 
        ]
        ,[
            'amount_approved1.required' => 'Amount approve is required.',
            'approval_remarks.required' => 'Remark is required.'
        ]);
        if ($validator->fails())
        {
            $a = implode('<br>',$validator->errors()->all());
            Alert::error('Validation Error: <br>'.$a)->autoClose(null);
            return redirect()->back();        
        }
        $id = $request['idapprove'];
        // dd($request->all());
        $preloan = PreLoanApplication::with('usermember.user')->find($id);
        $preloan->amount_approved   =  $request['amount_approved1'];
        $preloan->approval_remarks  =  $request['approval_remarks'];
        $preloan->loan_status       =  2;
        $preloan->upd_by            = auth()->user()->uname;
        $loaner_name = $preloan->usermember->user->uname;
        $email = $preloan->usermember->user->email;
        $preloan->save();
         $details = 
        [
            'decline_type' => 'approve',
            'remarks' => $request['approval_remarks'],
        ];

        Mail::to($email)->queue(new LoanMail($details));

        return redirect()->route('preloan')->with('success', $loaner_name.' Loan Approved successfully');
    }

    public function edit(PreLoanApplication $preLoanApplication, Request $request)
    {
        // $where = array('id' => $request->id);
        $preloan  = PreLoanApplication::where('id',$request->id)->first();
        // return response()->json($preloan);
        // dd($preloan);
        $json = array(
            'id'                    => $preloan['id']
            ,'file_path1'           => $preloan['file_path1']
            ,'branch_id'           => $preloan['branch_id']
            ,'birthdate'            => $preloan->usermember->birthdate
            ,'fullname'             => $preloan->usermember->crea_by
            ,'gender'               => $preloan->usermember->member->gender
            ,'email'                => $preloan->usermember->user->email
            ,'region_id'            => $preloan->usermember->region_id
            ,'province_id'          => $preloan->usermember->province_id
            ,'city_id'              => $preloan->usermember->city_id
            ,'barangay_id'          => $preloan->usermember->barangay_id
            ,'street'               => $preloan->usermember->street
            ,'fb_link'              => $preloan->usermember->fb_link
            ,'marketing_channel_id' => $preloan['marketing_channel_id']
            ,'co_maker'             => $preloan['co_maker']
            ,'ref_number'           => $preloan['ref_number']
            ,'amount_approved'      => $preloan['amount_approved']
            ,'share_capital'        => $preloan['share_capital']
            ,'approval_remarks'     => $preloan['approval_remarks']
            ,'amount_needed'        => $preloan['amount_needed']
            ,'loan_status'          => $preloan['loan_status']
            ,'loan_product'         => $preloan['loan_product']
            ,'savings_salary_amount'=> $preloan['savings_salary_amount']
            ,'loanable_amount'      => $preloan['loanable_amount']
            ,'loan_type'            => $preloan->loan_type->name
            ,'loan_purpose'         => $preloan['loan_purpose']
            ,'payment_terms'        => $preloan['payment_terms']
            ,'payment_mode'         => $preloan['payment_mode']
            ,'occupation_id'        => $preloan['occupation_id']
            ,'other_occupation'     => $preloan['other_occupation']
            ,'other_purpose'        => $preloan['other_purpose']
            ,'montly_income'        => $preloan['montly_income']
            ,'montly_expense'       => $preloan['montly_expense']
            ,'created_at'           => Carbon::parse($preloan['created_at'])->format('Y-m-d')
        );
        return response()->json($json);
    }

    public function getAttachment(Request $request)
    {
        $attachment = PreLoanApplication::where('id', $request->id)->whereNull('soft_deleted_at')->first('file_path1');
        $signiture = PreLoanApplication::where('id', $request->id)->whereNull('soft_deleted_at')->first('signiture');
        $json = array($attachment['file_path1'], $signiture); 
        // $explode = explode('|', $attachment);
       return response()->json($json);
    }
    public function enduserAttachment(Request $request)
    {
        $attachment = PreLoanApplication::where('id', $request->id)->first('file_path1');
        $signiture = PreLoanApplication::where('id', $request->id)->first('signiture');
        $json = array($attachment['file_path1'], $signiture); 
        // $explode = explode('|', $attachment);
       return response()->json($json);
    }


    public function preloancount(Request $request)
    {
        $preloancount = PreLoanApplication::whereNull('soft_deleted_at')->get('id');
        // $explode = explode('|', $attachment);
       return response()->json($preloancount);
    }

    public function deleteAll(Request $request, PreLoanApplication $preloan)
    {
        $ids = $request->ids;
        // dd($ids);
        // $path = explode("|",$request->ids);
        $preloan = PreLoanApplication::whereIn('id',explode(",",$ids))->whereNull('soft_deleted_at')->get();
        if($preloan->count())
        {
            foreach($preloan as $data)
            {
                $preloan = PreLoanApplication::find($data->id);
                $file_path = explode("|",$preloan->file_path1);
                $x = explode('-', $file_path[0]);
                $preloan->soft_deleted_at = Carbon::now();
                $preloan->save();
                // $image_path = "assets/uploads/id/".$x[0]."/";  // Value is not URL but directory file path
                // if(File::exists($image_path)) {
                //     File::deleteDirectory($image_path);
                //     // $nofile = 'thereis';
                // }else{
                //     // $nofile = 'nofile';
                // }
            }
        }
        $is_enduser = auth()->user()->role_id;
        if($is_enduser == 3){
            return back();
        }else{
            return response()->json(['success'=>"Loan deleted successfully."]);
        }

    }
    public function exportExcel(Request $request) 
    {
        $start = $request->start.' 00:00:00';
        $end = $request->end.' 23:59:59';
        $start_end = $start.', '.$end;
        $date = date('M-d-Y', strtotime($start)).'-'.date('M-d-Y', strtotime($end));
        return \Excel::download(new LoanExport($start, $end), 'Loan('.$date.').'.'xlsx');
    }
}
