@extends('layouts.app', [
    'class' => 'sidebar-mini ',
    'namePage' => 'Batch Index',
    'activePage' => 'batch-management',
    'activeNav' => '',
])

@section('indexdeleteall')
  <script src="{{ asset('assets') }}/js/indexdeleteall.js"></script>
@endsection
@section('content')
    @include('auth.passwords.confirmmodal')
    @include('batch.modal')
<div class="panel-header panel-header-sm">
  </div>
  <div class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header">
            @include('flash-message')
                <div class="float-left">
                    <h4 class="card-title">Batch</h4>
                </div>
                <div class="float-right">
                    <a href="javascript:void(0)" title="Add Batch" class="btn btn-success" id="addNewBatch">
                        <li class="fas fa-plus-circle fa-lg" aria-hidden="true"></li>
                      </a>
                    <button class="btn btn-danger delete_all" data-url="{{ url('DeleteAllBatch') }}">Delete All Selected</button>
                    <a class="btn btn-warning" href="{{ url('/batchv2History') }}">
                        Batch History
                    </a>
                    
                </div>
          </div>
          <div class="card-body">
            <div id="modalCatcher" class="table-responsive">
              <input type="hidden" value="{{url('/')}}" id="url" name="url">
              <table id="datatablesSimple" class="table">
                <thead class=" text-primary">
                    <th ><input type="checkbox" id="master"></th>
                    <th >Action</th>
                    <th >Batch Code</th>
                    <th>Schedule Date</th>
                    <th>Schedule Time</th>
                    <th>Batch Application</th>
                    <th class="meeting-col">Meeting Link</th>
                    <th>Participant Number</th>
                    <th>Batch Status</th>
                    <th class="shorten_text">Reschedule Remarks</th>
                    <th>Meeting Password</th>
                    <th>Date Created</th>
                    <th>Created By</th>
                    <th>Updated By</th>
                    <th>Date Updated</th>
                </thead>
                <tbody>
                   @if($batch->count())
                    @foreach ($batch as $data)
                    <tr>
                    <td ><input type="checkbox" class="sub_chk" data-id="{{$data->id}}"></td>
                    <td>
                    <a href="javascript:void(0)" title="Edit Batch" class="edit" data-id="{{ $data->id }}">
                      <i class="fas fa-edit  fa-lg"></i>
                    </a>
                    @csrf
                    @method('DELETE')
                    </td> 
                    
                    <td>{{ $data->batch_cd }}</td>
                    
                    <td>{{ \Carbon\Carbon::parse($data->sched_date)->format('m-d-Y')}}</td>
                    
                    <td>{{ \Carbon\Carbon::parse($data->sched_date)->format('h:i a')}}</td>
                    
                    @if($data->online_vid_tool == 0)
                    <td>Google Meet</td>
                    @elseif($data->online_vid_tool == 1)
                    <td>Zoom</td>
                    @else
                    <td>Walk-in</td>
                    @endif
                    
                    @if($data->online_vid_tool == 0)
                    <td class="meeting-col"><a href="https://meet.google.com/{{$data->meeting_cd}}">https://meet.google.com/{{$data->meeting_cd}}</a></td>
                    @elseif($data->online_vid_tool == 1)
                    <td class="meeting-col"><a href="httpshttps://us02web.zoom.us/j/7864183054?pwd=QmNwRzk3UWNocFM4RmZSekJEb3VyUT09">httpshttps://us02web.zoom.us/j/7864183054?pwd=QmNwRzk3UWNocFM4RmZSekJEb3VyUT09</a></td>
                    @else
                    <td>Walk-in</td>
                    @endif
                    
                    @if($data->participant != 0)
                    <td>
                      <a href="javascript:void(0)" data-id="{{$data->id}}" class="btn btn-info" id="participant" style="font-size: medium; font-weight:bold; ">
                     {{$data->participant}}
                      </a>
                    </td>
                    @else
                    <td></td>
                    @endif

                    @if(\Carbon\Carbon::parse($data->sched_date)->format('m/d/Y') == $now)
                    <td>Today</td>
                    @elseif(\Carbon\Carbon::parse($data->sched_date)->isPast())
                    <td>Expired</td>
                    @elseif($data->resched_remarks !== '.')
                    <td>Rescheduled</td>
                    @elseif($data->participant < 5)
                    <td>Insufficient Participant</td>
                    @else
                    <td>Pending</td>
                    @endif
                    
                    <td>{{$data->resched_remarks}}</td>
                    
                    @if(is_null($data->meeting_pw))
                    <td></td>
                    @else
                    <td>{{$data->meeting_pw}}</td>
                    @endif

                    <td>{{ date_format($data->created_at, 'm-d-Y') }}</td>
                    
                    <td>{{ $data->crea_by }}</td>

                    @if(isset($data->upd_by))
                    <td>{{ $data->upd_by }}</td>
                    @else
                    <td></td>
                    @endif
                    @if(isset($data->updated_at))
                    <td>{{ date_format($data->updated_at, 'jS M Y') }}</td>
                    @else
                    <td></td>
                    @endif
                    
                    </tr>
                    @endforeach
                    @endif
                </tbody>
              </table>

            </div>
          </div>
        </div>
      </div>
      
    </div>
  </div>
    
    <!-- boostrap model -->
    
<!-- end bootstrap model -->
<script src="{{ asset('assets') }}/js/batchdatepicker.js"></script>
<script src="{{ asset('assets') }}/js/extrajs.js"></script>
<script>
   // window.addEventListener("load", function() {
  //   window.addEventListener('DOMContentLoaded', event => {
  //   // Simple-DataTables
  //   // https://github.com/fiduswriter/Simple-DataTables/wiki
    
  //   const datatablesSimple = document.getElementById('datatablesSimple');
  //   if (datatablesSimple) {
  //       new simpleDatatables.DataTable(datatablesSimple, {
  //   fixedHeight: true
  //       });
  //   }
  // });
$(document).ready(function(){
  $('#datatablesSimple').DataTable({

  });
   const swalWithBootstrapButtons = Swal.mixin({
                  customClass: {
                    confirmButton: 'btn btn-success',
                    cancelButton: 'btn btn-danger'
                  },
                  buttonsStyling: false
                }) 
   
    $('.close-modal').click(function () {
     var modal_array = 
    [
      '#ajax-batch-model',
      '#ajax-participant-model'
    ];
        closeModal(modal_array);
        //    $('#ajax-batch-model').modal('hide');
        //    $('#ajax-participant-model').modal('hide'); 
    });

    $.ajaxSetup({
        headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $('#online_vid_tool').on("change", function(){
        var online_vid_tool = $(this).val();
        // alert(online_vid_tool);
        if(online_vid_tool == 1){
            $('#meeting_cd_div').attr('hidden', false);
            $('#meeting_pw_div').attr('hidden', true);
            $('#meeting_cd_lb').html('Zoom Link');
            $('#meeting_cd_textarea').attr('hidden', false);
            $('#meeting_cd_textarea').attr('disabled', false);
            $('#meeting_cd_text').attr('hidden', true);
            $('#meeting_cd_text').attr('disabled', true);
            $('#meeting_cd_textarea').val('');
            $('#meeting_cd_text').val('');
            $('#meeting_pw').val('');
        }else if(online_vid_tool == 0){
            $('#meeting_cd_div').attr('hidden', false);
            $('#meeting_pw_div').attr('hidden', false);
            $('#meeting_cd_lb').html('Meeting Code');
            $('#meeting_cd_textarea').attr('hidden', true);
            $('#meeting_cd_textarea').attr('disabled', true);
            $('#meeting_cd_text').attr('hidden', false);
            $('#meeting_cd_text').attr('disabled', false);
            $('#meeting_cd_textarea').val('');
            $('#meeting_cd_text').val('');
            $('#meeting_pw').val('');
        }else{
            $('#meeting_cd_div').attr('hidden', true);
            $('#meeting_pw_div').attr('hidden', true);
            $('#meeting_cd_textarea').attr('hidden', true);
            $('#meeting_cd_textarea').attr('disabled', true);
            $('#meeting_cd_text').attr('hidden', true);
            $('#meeting_cd_text').attr('disabled', true);
            $('#meeting_cd_textarea').val('');
            $('#meeting_cd_text').val('');
            $('#meeting_pw').val('');
        }
    });

    $('#addNewBatch').click(function () {
       $('#addEditbatchForm').trigger("reset");
       $('#ajaxBatchModel').html("Add Batch");
       $('#ajax-batch-model').modal('show');
       $('#remark_id').attr('hidden', true);
    });
      // $('#modalCatcher').on('click', '#btn-save', function () {
    $('#btn-save').click(function (event) {
         // alert('hello');
            try{
          var id = $("#id").val();
          var batch_cd = $("#batch_cd").val();
          var sched_date = $("#sched_date").val();
          var online_vid_tool = $("#online_vid_tool").val();
          var meeting_cd = $("#meeting_cd").val();
          var meeting_pw = $("#meeting_pw").val();
          var resched_remarks = $("#resched_remarks").val();
          $("#btn-save").html('Please Wait...');
          $("#btn-save").attr("disabled", true);
          $("#close-modal").html('Please Wait...');
          $("#close-modal").attr("disabled", true);           
        }
            catch(err) {
            swalWithBootstrapButtons.fire({
                  title: 'Error!',
                  text: err,
                  icon: 'warning',
                  confirmButtonText: 'Confirmed',
                }).then((result) => {
             // window.location.reload();
            $("#btn-save").html('Submit');
            $("#btn-save"). attr("disabled", false);
                })
        }
        // event.preventDefault();
        // ajax
        $.ajax({
            type:"POST",
            url: "{{ url('add-update-batchv2') }}",
            // headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            data: {
              id:id,
              batch_cd:batch_cd,
              sched_date:sched_date,
              online_vid_tool:online_vid_tool,
              meeting_cd:meeting_cd,
              meeting_pw:meeting_pw,
              resched_remarks:resched_remarks,
            },
            dataType: 'json',
            success: function(res){
              // alert('success');
             swalWithBootstrapButtons.fire({
                  title: 'Success!',
                  icon: 'success',
                  confirmButtonText: 'Confirmed',
                }).then((result) => {
             window.location.reload();})
              // alert(JSON.stringify(res));
            $("#btn-save").html('Submit');
            $("#btn-save"). attr("disabled", false);
           }
          //   ,error: function(xhr, status, error){
          //  var errorMessage = xhr.status + ': ' + xhr.statusText
          //  alert('Error? - ' + errorMessage);
           
          // }
          
           ,error: function (data) {
            // alert(JSON.stringify(data.responseJSON['errors']))
            var errors = [];
            $.each( data.responseJSON['errors'], function( key, value ) {
                errors += value+'<br>';
            });
            // alert(errors);
            swalWithBootstrapButtons.fire({
                  title: 'Error!',
                  html: errors,
                  icon: 'warning',
                  confirmButtonText: 'Confirmed',
                }).then((result) => {
                 // window.location.reload();
                    $("#btn-save").html('Submit');
                    $("#btn-save"). attr("disabled", false);
                })
            }
        });

    });
     $('#modalCatcher').on('click', '.edit', function () {

        var id = $(this).data('id');
                var base_path = $("#url").val();
        // ajax
        //   alert( base_path );
        $.ajax({
            type:"POST",
            url: base_path+"/edit-batchv2",
            data: { id: id },
            dataType: 'json',
            success: function(res){
                // alert(JSON.stringify(res.sched_date));
                // var date = res.sched_date->format('m-d-Y');
              $('#remark_id').attr('hidden', false);
              $('#ajaxBatchModel').html("Edit Batch");
              $('#ajax-batch-model').modal('show');
              $('#id').val(res.id);
              $('#batch_cd').val(res.batch_cd);
              $('#sched_date').val(res.sched_date);
              $('#online_vid_tool').val(res.online_vid_tool);
              $('#meeting_cd').val(res.meeting_cd);
              $('#meeting_pw').val(res.meeting_pw);
              $('#resched_remarks').val(res.resched_remarks);
           },
            error: function (data) {
                 var err = JSON.parse(data.responseText);
                 if(err['message'] == 'Password confirmation required.'){   
                 alert(err['message']);
                 window.location.href = "password/confirm";
                 }else{
                 // alert('please delete the batch connected to this client/batch');
                 alert(data.responseText);
                 }
             }
        });

    });
    $.clearInput = function () {
        $('insert_form').find('input[type=text], input[type=password], input[type=number], input[type=email], select, textarea').val('');
    };
     $('.modal').on('hidden.bs.modal', function(){
    $.clearInput();
    $(this).find('form')[0].reset();
    $('#participant-table').find("#tbody").empty();
    });

     $('#modalCatcher').on('click', '#participant', function () {

        var id = $(this).data('id');
                var base_path = $("#url").val();
        // ajax
          // alert( id );
        $.ajax({
            type:"POST",
            url: base_path+"/participant",
            data: { id: id },
            dataType: 'json',
            success: function(res){
                // alert(JSON.stringify(res));
                // var date = res.sched_date->format('m-d-Y');
              $('#ajaxParticipantModel').html("Batch Participant");
              $('#ajax-participant-model').modal('show');
            $.each( res, function( key, value ) {
              var table = document.getElementById("tbody");
              var row = table.insertRow(0);
              var cell1 = row.insertCell(0);
              var cell2 = row.insertCell(1);
              var cell3 = row.insertCell(2);
              var cell4 = row.insertCell(3);
              cell1.innerHTML = '<td>'+value.client.fullname+'</td>';
              cell2.innerHTML = '<td>'+value.client.contact_no+'</td>';
              cell3.innerHTML = '<td>'+value.client.gmail_address+'</td>';
              cell4.innerHTML = '@if('+value.client.attendance+' == 1)<td>Present</td>@elseif('+value.client.attendance+' == 2)<td>Absent</td>@else<td>Pending</td>@endif';
            });
           },
            error: function (data) {
                 alert(data.responseText);
                 
             }
        });
    });
 });
</script>
@endsection


