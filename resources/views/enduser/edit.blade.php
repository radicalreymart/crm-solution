@extends('layouts.app1')
@section('content')
  <style>
    .alert{
      color: black !important;
    }
  </style>
    <!-- <nav class="navbar navbar-expand-lg navbar-light bg-white">
      <div class="container-fluid">
        <a class="navbar-brand" href="#">
          <img src="{{asset('assets/img/logo.jpg')}}" alt="" width="180" class="d-inline-block align-text-top">
        </a>
      </div>
    </nav> -->
    <!-- Masthead-->
    <header class="masthead mb-1">
      <div class="container mt-1 pt-1">
            @include('flash-message')
          
      </div>
    </header>

    <div class="container mt-5" id="sign_up">
      <div class="card p-2 pt-3 bg-light border-0">
        <div class="row"> 
          <div class="col-md-12 order-md-1">
            <form method="POST" name='formname' action="{{ route('enduser.update') }}">
              <input type="hidden" id="enduser" name="enduser" value="enduser" >
              <input type="hidden" value="{{url('/')}}" id="url" name="url">
              <input type="hidden" id="sitefeedback" name="sitefeedback">
                                        @csrf
              <div class="card mx-3 shadow-lg border-0">
                <div class="card-header bg-light text-primary py-2 pl-3 border-0">
                  <h6 class="m-0 default-primary">Personal Information <a class=" hvr-icon-push float-right" onClick="window.location.href = '/enduser/home';" data-url="{{ url('/enduser/home') }}">{{ __('Back') }}<i class="far fa-caret-square-left hvr-icon"></i></a></h6>
                </div>
                <div class="card-body px-3">
                  <div class="row px-3">
                    <div class="col-md-4 mb-3">
                      <label for="fname">First Name</label>
                      <input type="text" class="form-control form-control-sm  " id="fname" name="fname" placeholder="First Name"   autocomplete="fname">
                    </div>
                    <div class="col-md-4 mb-3">
                      <label for="address2">Middle Name  <span class="text-muted">(Optional)</span></label>
                      <input type="text" value="{{ old('mname') }}" name="mname" class="form-control form-control-sm" id="mname" placeholder="Middle Name">
                    </div>
                    <div class="col-md-4 mb-3">
                      <label for="lname">Last Name</label>
                      <input type="text" value="{{ old('lname') }}" name="lname" class="form-control form-control-sm " id="lname" placeholder="Last Name"  autocomplete="lname" >
                    </div>
                  </div> <!-- Long Name -->
                  
                  <div class="row px-3">
                    <div class="col-md-6 mb-3">
                      <label for="contact_no">Facebook</label>
                        <input type="text" class="form-control form-control-sm " id="fb_link"   name="fb_link" autocomplete="fb_link">
                    </div>
          
                    <div class="col-md-6 mb-3 mt-0">
                      <label for="email">Email Address</label>
                        <input type="email" class="form-control form-control-sm" id="email" placeholder="example@gmail.com"  name="email" autocomplete="email">
                    </div>  
                  </div> <!-- Contact Information -->

                  <div class="row px-3">
                    <div class="col-md-6 mb-3 mt-0">
                      <label for="of_offm">Birth-date </label>
                        <input type="date" class="form-control form-control-sm " id="birthdate" value="{{$birthdate}}"  name="birthdate"  autocomplete="birthdate">
                    </div>
                    <div class="col-md-6 mb-3 mt-0">
                      <label for="of_offm">OFFM  <span class="text-muted">Overseas Filipino Family Member</span></label>
                        <input type="text" class="form-control form-control-sm " id="of_offm"   name="of_offm" placeholder="Full Name" autocomplete="of_offm">
                    </div>  
                  </div>
                </div>
              </div>
      
              <div class="card mt-4 mx-3 shadow-lg border-0">
                <div class="card-header bg-light text-primary pt-2 pl-3 border-0">
                  <h6 class="m-0 default-primary">Present Address </h6>
                </div>
                <div class="card-body">
                  <div class="row px-3">
                    <div class="col-md-4 mb-3">
                      <label for="region_code">Region</label>
                      <select class="form-control custom-select custom-select-sm " id="region_id"  type="text" name="region_id" >
                        <option value="">Select Region</option>
                         @foreach($regions as $item)
                        <option id="region_code" value="{{$item->region_id}}">{{$item->name}}</option>
                        @endforeach
                      </select>
                      <input type="text" id="region_hidden" name="region_hidden" hidden readonly  class="form-control form-control-sm">
                    </div>
                    <div class="col-md-4 mb-3">
                      <label for="province_id">Province</label>
                      <select readonly class="form-control  custom-select custom-select-sm" id="province_id"  readonly name="province_id" autocomplete="province_id" >
                        <option value="">Select Province</option>
                      </select>
                      <input type="text" id="province_hidden" name="province_hidden" hidden readonly  class="form-control form-control-sm">
                    </div>
                    <div class="col-md-4 mb-3">
                      <label for="city_id">City/Municipality</label>
                      <select  class="form-control  custom-select custom-select-sm" id="city_id"   name="city_id" autocomplete="city_id" readonly>
                        <option value="">Select City/Municipality</option>
                      </select>
                      <input type="text" id="city_hidden" name="city_hidden" hidden readonly  class="form-control form-control-sm">
                    </div>
                  </div> <!-- Region, Province, Municiaplity -->
                  <div class="row px-3">
                    <div class="col-md-4 mb-3">
                      <label for="barangay_id">Barangay</label>
                      <select readonly class="form-control custom-select custom-select-sm" readonly id="barangay_id"  type="text" name="barangay_id" autocomplete="barangay_id" >
                        <option value="">Select Barangay</option>
                      </select>
                      <input type="text" id="barangay_hidden" name="barangay_hidden" hidden readonly  class="form-control form-control-sm">
                    </div>
                    <div class="col-md-6 mb-3">
                      <label for="street">Bldg. No./ Street / Subdivision</label>
                      <input type="text" class="form-control form-control-sm " id="street" placeholder="Bldg. No./ Street / Subdivision" name="street">
                    </div>
                    <div class="col-md-2 mb-3">
                      <label for="zipcode">Zip Code</label>
                      <input type="text" class="form-control form-control-sm " id="zipcode" placeholder="Ex: 1860" name="zipcode">
                    </div>
                  </div> <!-- Barangay, Street, zipcode -->
                </div>
              </div>

              

              <div class="card mt-4 mx-3 shadow-lg border-0">
                <div class="card-header bg-light text-primary pt-2 pl-3 border-0">
                  <h6 class="m-0 default-primary">Contact Information  </h6>
                </div>
                <div class="card-body">
                  <div class="row px-3">
                    <div class="col-md-6 mb-3">
                      <label for="barangay_id">Mobile Number</label>
                      <div class="input-group input-group-sm">
                        <div class="input-group-prepend">
                          <span class="input-group-text">+63</span>
                        </div>
                        <input type="text" class="form-control form-control-sm " id="mobile_no1" placeholder="10-digit Mobile Number"  minlength="10" maxlength="10" onkeydown="return ( event.ctrlKey || event.altKey 
                    || (47<event.keyCode && event.keyCode<58 && event.shiftKey==false) 
                    || (95<event.keyCode && event.keyCode<106)
                    || (event.keyCode==8) || (event.keyCode==9) 
                    || (event.keyCode>34 && event.keyCode<40) 
                    || (event.keyCode==46))" name="mobile_no1" autocomplete="mobile_no1" title="" style="border-radius: 0;" >
                      </div>
                    </div>
                    <div class="col-md-6 mb-3">
                      <label for="barangay_id">Landline Number</label>
                      <input type="text" class="form-control form-control-sm" id="landline_no1" placeholder="7-digit Landline Number"  minlength="7" maxlength="7" onkeydown="return ( event.ctrlKey || event.altKey 
                    || (47<event.keyCode && event.keyCode<58 && event.shiftKey==false) 
                    || (95<event.keyCode && event.keyCode<106)
                    || (event.keyCode==8) || (event.keyCode==9) 
                    || (event.keyCode>34 && event.keyCode<40) 
                    || (event.keyCode==46))" name="landline_no1" autocomplete="landline_no1" title="" style="border-radius: 0;" >
                    </div>
                  </div> <!-- Barangay, Street -->
                  <div class="row px-3">
                    <div class="col-md-6 mb-3">
                      <div class="input-group input-group-sm">
                        <div class="input-group-prepend">
                          <span class="input-group-text">+63</span>
                        </div>
                        <input type="text" class="form-control form-control-sm " id="mobile_no2" placeholder="10-digit Mobile Number"  minlength="10" maxlength="10" onkeydown="return ( event.ctrlKey || event.altKey 
                    || (47<event.keyCode && event.keyCode<58 && event.shiftKey==false) 
                    || (95<event.keyCode && event.keyCode<106)
                    || (event.keyCode==8) || (event.keyCode==9) 
                    || (event.keyCode>34 && event.keyCode<40) 
                    || (event.keyCode==46))" name="mobile_no2" autocomplete="mobile_no2" title="" style="border-radius: 0;" >
                      </div>
                    </div>
                    <div class="col-md-6 mb-3">
                      <input type="text" class="form-control form-control-sm" id="landline_no2" placeholder="7-digit Landline Number"  minlength="7" maxlength="7" onkeydown="return ( event.ctrlKey || event.altKey 
                    || (47<event.keyCode && event.keyCode<58 && event.shiftKey==false) 
                    || (95<event.keyCode && event.keyCode<106)
                    || (event.keyCode==8) || (event.keyCode==9) 
                    || (event.keyCode>34 && event.keyCode<40) 
                    || (event.keyCode==46))" name="landline_no2" autocomplete="landline_no2" title="" style="border-radius: 0;" >
                    </div>
                  </div> <!-- Barangay, Street -->
                  <div class="row px-3">
                    <div class="col-md-6 mb-3">
                      <div class="input-group input-group-sm">
                        <div class="input-group-prepend">
                          <span class="input-group-text">+63</span>
                        </div>
                        <input type="text" class="form-control form-control-sm " id="mobile_no3" placeholder="10-digit Mobile Number"  minlength="10" maxlength="10" onkeydown="return ( event.ctrlKey || event.altKey 
                    || (47<event.keyCode && event.keyCode<58 && event.shiftKey==false) 
                    || (95<event.keyCode && event.keyCode<106)
                    || (event.keyCode==8) || (event.keyCode==9) 
                    || (event.keyCode>34 && event.keyCode<40) 
                    || (event.keyCode==46))" name="mobile_no3" autocomplete="mobile_no3" title="" style="border-radius: 0;" >
                      </div>
                    </div>
                    <div class="col-md-6 mb-3">
                      <input type="text" class="form-control form-control-sm" id="landline_no3" placeholder="7-digit Landline Number"  minlength="7" maxlength="7" onkeydown="return ( event.ctrlKey || event.altKey 
                    || (47<event.keyCode && event.keyCode<58 && event.shiftKey==false) 
                    || (95<event.keyCode && event.keyCode<106)
                    || (event.keyCode==8) || (event.keyCode==9) 
                    || (event.keyCode>34 && event.keyCode<40) 
                    || (event.keyCode==46))" name="landline_no3" autocomplete="landline_no3" title="" style="border-radius: 0;" >
                    </div>
                  </div> <!-- Barangay, Street -->
                </div>
              </div>

              <div class="card mt-4 mx-3 shadow-lg border-0">
                <div class="card-header bg-light text-primary pt-2 pl-3 border-0">
                  <h6 class="m-0 default-primary">Source of Income  </h6>
                </div>
                <div class="card-body">
                  <div class="row px-3">
                    <div class="col-md-6 mb-3">
                      <label for="occupation">Occupation</label>
                      <select class="custom-select custom-select-sm  custom-select custom-select-sm -sm  d-block w-100" id="occupation"  type="text" name="occupation" autocomplete="occupation">
                        <option value="">Select Occupation</option>
                         @foreach($occupations as $item)
                          <option id="occupation" value="{{$item->id}}">{{$item->name}}</option>
                          @endforeach
                          <option value="0">Other</option>
                      </select>
                    </div>
                    <div style="padding-top: 5px;" id="div-occu" class="col-md-6 mb-3">
                      <label for="occupation"></label>
                      <input type="text" id="other_occupation" placeholder="Other Occupation" hidden name="other_occupation"  class="form-control form-control-sm">
                    </div>
                  </div> <!-- Occupation -->
                  <div class="row px-3">
                    <div class="col-md mb-3">
                      <label for="emp_bus_name">Employer/Business Name</label>
                      <input type="text" class="form-control form-control-sm " id="emp_bus_name"  name="emp_bus_name">
                    </div>
                  </div>
                  <div class="row px-3">
                    <div class="col-md mb-3">
                      <label for="emp_bus_address">Employer/Business Address</label>
                      <input type="text" class="form-control form-control-sm " id="emp_bus_address"  name="emp_bus_address">
                    </div>
                  </div>
                  <div class="row px-3">
                    <div class="col-md-6 mb-3">
                      <label for="montly_income">Monthly Income</label>
                      <input type="number" class="form-control form-control-sm @error('montly_income') is-invalid @enderror" onchange="(function(el){el.value=parseFloat(el.value).toFixed(2);})(this)"
                     oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"
                       maxlength="7" min="0.00" max="9999999.00" step="0.05"   value="{{ old('montly_income') }}"   id="montly_income"  name="montly_income">                    
                    </div>
                    <div class="col-md-6 mb-3">
                      <label for="emp_bus_startdate">Start Date</label>
                      <input type="date" class="form-control form-control-sm " id="emp_bus_startdate"  name="emp_bus_startdate">
                    </div>
                  </div>

                </div>
              </div>


              <div class="card mt-4 mx-3 shadow-lg border-0">
                <div class="card-header bg-light text-primary pt-2 pl-3 border-0">
                  <h6 class="m-0 default-primary">Beneficiaries  </h6>
                </div>
                <div class="card-body">
                  <div class="row px-3">
                    <div class="col-md-6 mb-3">
                      <label for="relative1">Full Name</label>
                      <input type="text" class="form-control form-control-sm " id="relative1"  name="relative1">                    
                    </div>
                    <div class="col-md-6 mb-3">
                      <label for="relationship1">Relationship</label>
                      <input type="text" class="form-control form-control-sm " id="relationship1" name="relationship1">                    
                    </div>
                  </div>
                  <div class="row px-3">
                    <div class="col-md-6 mb-3">
                      <input type="text" class="form-control form-control-sm " id="relative2"  name="relative2">                    
                    </div>
                    <div class="col-md-6 mb-3">
                      <input type="text" class="form-control form-control-sm " id="relationship2"  name="relationship2">                    
                    </div>
                  </div>
                  <div class="row px-3">
                    <div class="col-md-6 mb-3">
                      <input type="text" class="form-control form-control-sm " id="relative3"  name="relative3">                    
                    </div>
                    <div class="col-md-6 mb-3">
                      <input type="text" class="form-control form-control-sm " id="relationship3"  name="relationship3">                    
                    </div>
                  </div>
                </div>
              </div>

                <div class=" card-header bg-light text-primary pt-2 pl-3 border-0">
                  <h6 class="m-0 default-primary">Signature  
                    <a id="clear" class="btn btn-danger btn-sm" type="button"> Clear</a>
                  </h6>
                </div>
                <div  id="manual-sig" class="float-right col-md">
                    <canvas style="background-color: lightgrey;" id="signature"></canvas>
                    <input type="hidden" id="hidden_sig" name="hidden_sig">
                </div>
                <div>&nbsp;</div>
              <button class="btn btn-secondary mt-5 mb-5 btn-block" style="background-color: rgb(7,17,174);" id="btn-save-miuf" type="button">Submit</button>
            </form>
          </div>
      </div>
  </div>
  </div>

<script src="https://cdn.jsdelivr.net/npm/signature_pad@2.3.2/dist/signature_pad.min.js"></script>
<script>
  const swalWithBootstrapButtons = Swal.mixin({
    customClass: {
      confirmButton: 'btn btn-success',
      cancelButton: 'btn btn-danger'
    },
    buttonsStyling: false
  }) 
  var canvas = document.getElementById ("signature");
  var signaturePad = new SignaturePad(canvas);
  $('#clear').click(function (e) {
            e.preventDefault();
            signaturePad.clear();
            $("#signature").val('');
        });
  $('#btn-save-miuf').click(function (event) {
            // event.preventDefault();
  // var sig_val = signaturePad.value();
  var data = signaturePad.toDataURL('image/png');
  $('#hidden_sig').val(data);
  // alert(data);

        let $form = $(this).closest('form');
        var sitefeedback = $('#sitefeedback');
        swalWithBootstrapButtons.fire({
                    title: 'Magandang Buhay!',
                    icon: 'question',
                    text:" Thank you for using our website, Kamay-ari! We would like to know how we could improve our website through this survey. This will take only a few minutes.",
                    showCancelButton: true,
                    confirmButtonText: 'Yes',
                    cancelButtonText: 'No',
                  }).then((result) => {
                    if (result.isConfirmed) {
                      // alert(base_path);
                        sitefeedback.val('yes');
                      $form.submit();
                      // event.preventDefault();
                      // window.location.href = $(this).data('feedback');
                    }else if (
                        /* Read more about handling dismissals below */
                        result.dismiss === Swal.DismissReason.cancel
                      ) {
                         
                        sitefeedback.val('no');
                        $form.submit();
                        }
                  })
  });
</script>
 <script>
  // window.addEventListener("load", function() {
$(document).ready(function($){
    $.ajaxSetup({
        headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
   
}); 
</script>
  <script src="{{ asset('assets') }}/js/core/jquery.min.js"></script>
  <script src="{{ asset('assets') }}/js/core/popper.min.js"></script>
  <script src="{{ asset('assets') }}/js/core/bootstrap.min.js"></script>
  <script src="{{ asset('assets') }}/js/plugins/perfect-scrollbar.jquery.min.js"></script>
  <script src="{{ asset('assets') }}/js/addressDropdown.js"></script>
  <script src="{{ asset('assets') }}/js/extrajs.js"></script>
  <script src="{{ asset('assets') }}/js/clientotheroccu.js"></script>
@endsection

