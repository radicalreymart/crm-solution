<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use Storage;
use File;
use Excel;
use App\Exports\MIUFExport;
use App\Imports\MIUFImport;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\UserRequest;
use App\Models\Client;
use App\Models\UserMember;
use App\Models\MIUF;
use App\Models\Region;
use App\Models\Occupation;
use Yajra\Address\AddressServiceProvider;
use Alert;

class MIUFController extends Controller
{
    protected $isVerified;

    public function __construct() 
    {
        $this->middleware(function ($request, $next) {
            $this->isVerified = Auth::user()->email_verified_at;
            return $next($request);
        });
    }
    public function index()
    {
        $department = auth()->user()->department_id;
        $isAdmin = auth()->user()->role_id;
        if($department == 1 || $isAdmin == 0){
        $branch = auth()->user()->branch_id;
            $miuf  = MIUF::with('member','region', 'province', 'city', 'barangay');
            // dd($miuf->get());
            if($branch == 9){
                $miuf->orderByRaw('-status ASC');
            }else{
                $miuf->whereRelation('member', 'branch_id', $branch)
                ->orderByRaw('-status ASC');
                // ->orderBy('serv_status');
            }
            $miuf = $miuf->get();
            return view('miuf.index', compact('miuf'));
        }else{
            Alert::error("You do not have the permission to access this page.");
            return redirect()->route('home');
        }
    }
    //
    public function edit()
    {
        $miuf  = MIUF::all();
        $regions = Region::all();
        $occupations = Occupation::all();
        $user_id = auth()->user()->id;
        $member_no = UserMember::where('user_id', $user_id)->pluck('member_no');
        $birthdate = UserMember::where('user_id', $user_id)->pluck('birthdate')->first();
        // dd($birthdate);
        $miuf = MIUF::where('member_no', $member_no)->get();
        // dd($miuf);
        if($miuf->count()){
            $editable_at = Carbon::parse($miuf[0]->editable_at)->format('d/m/Y');
            if($editable_at >= Carbon::now()->format('d/m/Y')){
                if(isset($this->isVerified)){
                    Alert::info('Reminder', 'Member Info updatable only on/after: '.$editable_at)->autoClose(null);
                return redirect()->route('enduser.home');
                }else{
                    Alert::info('Reminder', 'Member Info updatable only on/after: '.$editable_at)->autoClose(null);
                    return redirect()->route('verification.notice');
                }
            }else{
            return view('enduser.edit', compact('miuf', 'regions', 'occupations', 'birthdate'));
            }
        }else{
            return view('enduser.edit', compact('miuf', 'regions', 'occupations', 'birthdate'));
            
        }
        // dd($miuf[0]->editable_at);
    }
    public function update(Request $request)
    {
        $sitefeedback = $request->sitefeedback;
        if($request->region_id){
        $validator = Validator::make($request->all(), [
            'region_id' => 'required',
            'province_id' => 'required',
            'city_id' => 'required',
            'barangay_id' => 'required',
                ]);
            if ($validator->fails())
            {
                Alert::error('Validation Error: '.$validator->errors()->first())->autoClose(null);
                return redirect()->back();  
                // redirect()->back()->with('error', ['your message here']);            
            }
        }

        $user_id = auth()->user()->id;
        $member_no = UserMember::where('user_id', $user_id)->pluck('member_no');
        $update_miuf = MIUF::where('member_no', $member_no)->get();
        // dd($update_miuf);
        if($update_miuf->count()){
            $str_signiture = $update_miuf[0]->signiture;
            // dd('null');
        }else{

            // dd('one');
        $randInt = rand(100,999);
        $folderPath = str_replace('\\', '/', public_path('assets/img/uploads/member_upd/'.$randInt.Carbon::now()->format('Ymd').'/'));

        // $folderPath = str_replace('\\', '/', '/home3/mhrmpcoo/crm.mhrmpcoop.com.ph/assets/img/uploads/member_upd/'.$randInt.Carbon::now()->format('Ymd').'/');
                mkdir($folderPath, 0777, true);
        
        // dd($folderPath);
        // File::makeDirectory($folderPath);
        $image_parts = explode(";base64,", $request->hidden_sig);
        $image_type_aux = explode("image/", $image_parts[0]);
        $image_type = $image_type_aux[1];
        $image_base64 = base64_decode($image_parts[1]);
        $file = $folderPath . $randInt.Carbon::now()->format('Ymd') .'-Signiture'. '.'.$image_type;
        $str_signiture = $randInt.Carbon::now()->format('Ymd').'-Signiture'. '.'.$image_type;
        file_put_contents($file, $image_base64);
        }

        if($request['occupation'] == 0){
            $occupation_id = null;
        }else{
            $occupation_id = $request['occupation'];
        }
                $mobile_no = array(
                  "mobile_no1"      => $request->mobile_no1,
                  "mobile_no2"      => $request->mobile_no2,
                  "mobile_no3"      => $request->mobile_no3,
                );
                $landline_no = array(
                  "landline_no1"    => $request->landline_no1,
                  "landline_no2"    => $request->landline_no2,
                  "landline_no3"    => $request->landline_no3,
                );
                $benificiaries = array(
                    'relative1'     => $request->relative1, 
                    'relationship1' => $request->relationship1,
                    'relative2'     => $request->relative2, 
                    'relationship2' => $request->relationship2,
                    'relative3'     => $request->relative3, 
                    'relationship3' => $request->relationship3,
                );
        $miuf_create = 
        [   
              "member_no"           => $member_no[0],
              "fname"               => $request->fname,
              "mname"               => $request->mname,
              "lname"               => $request->lname,
              "fullname"            => $request->fname.' '.$request->mname.' '.$request->lname,
              "fb_link"             => $request->fb_link,
              "email"               => $request->email,
              "birthdate"           => $request->birthdate,
              "region_id"           => $request->region_id,
              "province_id"         => $request->province_id,
              "city_id"             => $request->city_id,
              "barangay_id"         => $request->barangay_id,
              "street"              => $request->street,
              "zipcode"             => $request->zipcode,
              'mobile_no'           => $mobile_no,
              'landline_no'         => $landline_no,
              'of_offm'             => $request->of_offm,
              "occupation_id"       => $occupation_id,
              "other_occupation"    => $request->other_occupation,
              "emp_bus_name"        => $request->emp_bus_name,
              "emp_bus_address"     => $request->emp_bus_address,
              "montly_income"       => $request->montly_income,
              "emp_bus_startdate"   => $request->emp_bus_startdate,
              'benificiaries'       => $benificiaries,
              'editable_at'         => Carbon::now()->addMonth(6),
              "crea_by"             => auth()->user()->uname,
              "signiture"           => $str_signiture,
              "status"              => null,
        ];
        if($update_miuf->count()){
        $miuf_update = 
        [
              'id'                 => $update_miuf[0]->id,
              'upd_by'             => $update_miuf[0]->full_name,
        ];  
        }else
        {
            $miuf_update = [];
        }
        MIUF::updateOrcreate($miuf_update, $miuf_create);
        if($sitefeedback == 'yes'){
            return redirect()->route('enduser.sitefeedback', 'non-logout');
        }else{
            if(isset($this->isVerified)){
                Alert::success('Success', 'Member Info to be updated');
                return redirect()->route('enduser.home');                
            }else{
                Alert::success('Success', 'Member Info to be updated');
                return redirect()->route('verification.notice');
            }
        }
        // $request->session()->flash('success', 'Member Info to be updated');
    }
    public function show(Request $request)
    {
        $miuf = MIUF::with('region', 'province', 'city', 'barangay', 'occupation')->where('id', $request->id)->get();
        // dd($miuf);
        return response()->json($miuf);
    }
    
    public function updateMIUF($array, $from, $to) 
    {
        $miuf = $array->whereNull('status')
        ->whereBetween('created_at', [$from, $to])->get(); 
        // dd($miuf);
        foreach($miuf as $data){
            $miufupdate = MIUF::find($data->id);
            $miufupdate->status = 1;
            $miufupdate->encode_by = auth()->user()->uname;
            $miufupdate->encode_date = Carbon::now();  
            $miufupdate->save();
        }
        // return response()->json('update');
    }
    public function exportExcel(Request $request) 
    {
        $start = $request->start.' 00:00:00';
        $end = $request->end.' 23:59:59';
        $start_end = $start.', '.$end;
        $date = date('M-d-Y', strtotime($start)).'-'.date('M-d-Y', strtotime($end));
        

        return \Excel::download(new MIUFExport($start, $end), 'MIUF('.$date.').'.'xlsx');
    }
    

}