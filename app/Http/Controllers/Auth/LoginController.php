<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;
use Alert;
use App\Models\User;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
        // $this->middleware('guest:member')->except('logout');
    }
    public function login(Request $request)
    {   
        $input = $request->all();
        // dd($input);
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required',
        ]);
        if (User::where('email', '=',  $input['email'])->exists()) {
           // user found
            if(Auth::attempt(['email' => $input['email'], 'password' => $input['password']], true))
            {
                if (auth()->user()->role_id == 3) {
                    return redirect()->route('enduser.home');
                }else{
                    return redirect()->route('home');
                }
            }else{
                Alert::error('Error', '"Password is invalid. Please try again.".');
                return redirect()->route('login')
                    ->with('error','"Password is invalid. Please try again.".');
            }
        }else{
                Alert::error('Error', 'Email entered does not exist in the system.');
                return redirect()->route('login')
                    ->with('error','Email entered does not exist in the system.');
        }
          
    }
}
