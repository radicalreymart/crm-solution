@extends('layouts.app', [
    'class' => 'sidebar-mini ',
    'namePage' => 'MIUF Index',
    'activePage' => 'miuf-management',
    'activeNav' => 'settings',
])

@section('content')

<div class="panel-header panel-header-sm">
  </div>
  <div class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header">
            @include('flash-message')
                <div class="float-left">
                    <h4 class="card-title">MIUF Index</h4>
                    <span class="text-muted">Member Information Update Form</span>
                </div>
                <div class="float-right">
                    <form action="{{ route('exportMiuf') }}"  >
                        {{ csrf_field() }}
                        Start Date
                    <input style="right: 5px;" class="form-control  start" max="{{ Carbon\Carbon::now()->format('Y-m-d'); }}" value="{{Carbon\Carbon::now()->format('Y-m-d'); }}" type="date" id="start" name="start">
                        End Date
                    <input style="right: 5px;" class="form-control  start" max="{{ Carbon\Carbon::now()->format('Y-m-d'); }}" value="{{Carbon\Carbon::now()->format('Y-m-d'); }}" type="date" id="end" name="end">
                        <button class="btn btn-primary" id="start"   >Download Excel </button>
                    </form>
                </div>
          </div>
          <div class="card-body">
            <div id="modalCatcher" class="table-responsive">
              <input type="hidden" value="{{url('/')}}" id="url" name="url">
                <table id="datatablesSimple" class="table">
                <thead class=" text-primary">
                    <th >Action</th>
                    <th >Status</th>
                    <th>Editable On/After</th>
                    <th>Created By</th>
                    <th>Date Created</th>
                    <th>Date Updated</th>
                </thead>
                <tbody>
                    @if($miuf->count())
                    @foreach ($miuf as $data)
                    <tr>
                    <td>
                      <a href="javascript:void(0)" title="Show Member Update Info" class="show" data-id="{{ $data->id }}">
                        <i class="fas fa-eye  fa-lg"></i>
                      </a>
                    @csrf
                    @method('DELETE')
                    </td>
                    @if(isset($data->status))
                    <td>Uploaded</td>
                    @else
                    <td>Pending for upload</td>
                    @endif
                    <td>{{ Carbon\Carbon::parse($data->editable_at)->format('m-d-Y'); }}</td>
                    <td>{{ $data->crea_by }}</td>
                    <td>{{ date_format($data->created_at, 'm-d-Y') }}</td>

                    @if(isset($data->updated_at))
                    <td>{{ date_format($data->updated_at, 'jS M Y') }}</td>
                    @else
                    <td></td>
                    @endif

                    </tr>
                    @endforeach
                    @endif
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      
    </div>
  </div>
    <!-- boostrap model -->

<div class="modal fade" id="ajax-miuf-model" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
            <h4 class="modal-title" id="ajaxMIUFModel"></h4>
            <button type="button" class="btn btn-default"  id="close-modal" data-dismiss="modal">Close</button>
            </div>
            <div class="modal-body">
                <table class="table" id="dynamicAddRemove">
                    <tr>
                        <td>Member Number</td>
                        <td id="member_no"></td>
                    </tr>
                    <tr>
                        <td>First Name </td>
                        <td id="fname"></td>
                    </tr>
                    <tr>
                        <td>Middle Name</td>
                        <td id="mname"></td>
                    </tr>
                    <tr>
                        <td>Last Name</td>
                        <td id="lname"></td>
                    </tr>
                    <tr>
                        <td>Email</td>
                        <td id="email"></td>
                    </tr>
                    <tr>
                        <td>Address</td>
                        <td id="region_id"></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td id="province_id"></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td id="city_id"></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td id="barangay_id"></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td id="street"></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td id="zipcode"></td>
                    </tr>
                    <tr>
                        <td>Facebook</td>
                        <td id="fb_link"></td>
                    </tr>
                    <tr>
                        <td>Mobile Number</td>
                        <td id="mobile_no1"></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td id="mobile_no2"></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td id="mobile_no3"></td>
                    </tr>
                    <tr>
                        <td>Landline Number</td>
                        <td id="landline_no1"></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td id="landline_no2"></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td id="landline_no3"></td>
                    </tr>
                    <tr>
                        <td>OF/OFFM</td>
                        <td id="of_offm"></td>
                    </tr>
                    <tr>
                        <td>Occupation</td>
                        <td  id="occupation"></td>
                    </tr>
                    <tr>
                        <td>Monthly Income</td>
                        <td id="montly_income"></td>
                    </tr>
                    <tr>
                        <td>Employer/<br>Business Name</td>
                        <td id="emp_bus_name"></td>
                    </tr>
                    <tr>
                        <td>Employer/<br>Business Address</td>
                        <td id="emp_bus_address"></td>
                    </tr>
                    <tr>
                        <td>Start Date</td>
                        <td id="emp_bus_startdate"></td>
                    </tr>
                    <tr>
                        <td>Beneficiaries</td>
                        <td id="benificiaries1"></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td id="benificiaries2"></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td id="benificiaries3"></td>
                    </tr>
                    <tr>
                        <td>Signature</td>
                        <td id="signiture">
                            <a id="href" target="_blank" rel="noopener noreferrer">
                                <img id="sigimg" height="100" width="200">
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <td>Editable On/After</td>
                        <td id="editable_at"></td>
                    </tr>
                    <tr>
                        <td>Uploaded By</td>
                        <td id="upload_by"></td>
                    </tr>
                    <tr>
                        <td>Date Uploaded</td>
                        <td id="upload_date"></td>
                    </tr>
                    <tr>
                        <td>Date Created</td>
                        <td id="created_at"></td>
                    </tr>
                    <tr>
                        <td>Created By</td>
                        <td id="crea_by"></td>
                    </tr>
                    <tr>
                        <td>Date Updated</td>
                        <td id="updated_at"></td>
                    </tr>
                    <tr>
                        <td>Updated By</td>
                        <td id="upd_by"></td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- end bootstrap model -->

<script>
  // window.addEventListener("load", function() {
  //   window.addEventListener('DOMContentLoaded', event => {
  //   // Simple-DataTables
  //   // https://github.com/fiduswriter/Simple-DataTables/wiki
    
  //   const datatablesSimple = document.getElementById('datatablesSimple');
  //   if (datatablesSimple) {
  //       new simpleDatatables.DataTable(datatablesSimple, {
  //   fixedHeight: true
  //       });
  //   }
  // });
$(document).ready(function(){
    $('#datatablesSimple').DataTable({

  });
  const swalWithBootstrapButtons = Swal.mixin({
                  customClass: {
                    confirmButton: 'btn btn-success',
                    cancelButton: 'btn btn-danger'
                  },
                  buttonsStyling: false
                })
  $('#close-modal').click(function () {
       $('#ajax-user-model').modal('hide');
    });
 $.clearInput = function () {
        $('insert_form').find('input[type=text], input[type=password], input[type=number], input[type=email], select, textarea').val('');
    };
     $('.modal').on('hidden.bs.modal', function(){
             // window.location.reload();
             // $('#insert_form').removeData('bs.modal')
    $.clearInput();
    // $(this).find('form')[0].reset();
    });

    $.ajaxSetup({
        headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

      $('#modalCatcher').on('click', '.show', function () 
    {
        $('#ajaxMIUFModel').html("MIUF Details");
        $('#ajax-miuf-model').modal('show');
        var id = $(this).data('id');
        var base_path = $("#url").val();
        $.ajax({
                type:"get",
                url: base_path+"/miuf/show",
                data: { id: id },
                dataType: 'json',
                success: function(res){
                  // console.log(res.regions);
                  // alert(JSON.stringify(res[0].province));
                  $('#member_no').html(res[0].member_no);
                  $('#fname').html(res[0].fname);
                  $('#mname').html(res[0].mname);
                  $('#lname').html(res[0].lname);
                  $('#email').html(res[0].email);
                  $('#region_id').html(res[0].region.name);
                  $('#province_id').html(res[0].province.name);
                  $('#city_id').html(res[0].city.name);
                  $('#barangay_id').html(res[0].barangay.name);
                  $('#street').html(res[0].street);
                  $('#zipcode').html(res[0].zipcode);
                  $('#fb_link').html(res[0].fb_link);
                  if(res[0].mobile_no['mobile_no1'] != null){
                  $('#mobile_no1').html('+63'+res[0].mobile_no['mobile_no1']);
                  }
                  if(res[0].mobile_no['mobile_no2'] != null){
                  $('#mobile_no2').html('+63'+res[0].mobile_no['mobile_no2']);
                  }if(res[0].mobile_no['mobile_no3'] != null){
                  $('#mobile_no3').html('+63'+res[0].mobile_no['mobile_no3']);
                  }
                  $('#landline_no1').html(res[0].landline_no['landline_no1']);
                  $('#landline_no2').html(res[0].landline_no['landline_no2']);
                  $('#landline_no3').html(res[0].landline_no['landline_no3']);
                  $('#of_offm').html(res[0].of_offm);
                  if(res[0].occupation_id == null){
                  $('#occupation').html(res[0].other_occupation);
                  }else{
                  $('#occupation').html(res[0].occupation.name);  
                  }
                  $('#montly_income').html(res[0].montly_income);
                  $('#emp_bus_name').html(res[0].emp_bus_name);
                  $('#emp_bus_address').html(res[0].emp_bus_address);
                  $('#emp_bus_startdate').html(res[0].emp_bus_startdate);
                  if(res[0].benificiaries['relative1'] != null){
                  $('#benificiaries1').html(res[0].benificiaries['relative1']+'<br>'+res[0].benificiaries['relationship1']);
                  }
                  if(res[0].benificiaries['relative2'] != null){
                  $('#benificiaries2').html(res[0].benificiaries['relative2']+'<br>'+res[0].benificiaries['relationship2']);
                  }
                  if(res[0].benificiaries['relative3'] != null){
                  $('#benificiaries3').html(res[0].benificiaries['relative3']+'<br>'+res[0].benificiaries['relationship3']);
                  }

                  // $('#signiture').html(res[0].signiture);
                  var src = res[0].signiture;
                    var splitfile = res[0].signiture.split('-');
                    var imgsrc1 = base_path+'/assets/img/uploads/member_upd/'+splitfile[0]+'/'+src;
                    var img1 = document.getElementById("sigimg");
                    var aid1 = document.getElementById("href");
                    var src1 = document.createAttribute("src");
                    var href1 = document.createAttribute("href");
                    src1.value = imgsrc1;
                    img1.setAttributeNode(src1);  
                    href1.value = imgsrc1;
                    aid1.setAttributeNode(href1);
                  $('#editable_at').html(res[0].editable_at.split(' ')[0]);
                    // date_format(, 'jS M Y'));
                  $('#upload_by').html(res[0].upload_by);
                  $('#upload_date').html(res[0].upload_date);
                  $('#crea_by').html(res[0].crea_by);
                  $('#upd_by').html(res[0].upd_by);
                  $('#created_at').html(res[0].created_at.split('T')[0]);
                  $('#updated_at').html(res[0].updated_at.split('T')[0]);
                }
                ,error: function (data) {
                     // alert('please delete the preloan connected to this preloan/preloan');
                     alert(data.responseText);  
                }
            });
    });
});
</script>
@endsection



   

