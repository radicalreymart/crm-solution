<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class CreateUserMemberTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_member', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('user_id')->nullable();
            $table->foreign('user_id', 'fk_user_id')->references('id')->on('users')->onUpdate('set null')->onDelete('set null');

            $table->string('member_no', 10)->index();
            $table->foreign('member_no', 'user_fk_member_no')->references('member_no')->on('master_file');

            $table->string('contact_no', 11);
            $table->date('birthdate');
            $table->string('fb_link')->nullable();
            $table->address();
            $table->string('crea_by');
            $table->string('upd_by')->nullable();
            $table->datetime('soft_deleted_at')->nullable();
            $table->timestamps();
            $table->rememberToken();
        });

        // DB::statement('ALTER TABLE `user_member` ADD CONSTRAINT `fk_member_no` FOREIGN KEY (`member_no`) REFERENCES`member_master`(`member_no`) ON DELETE RESTRICT ON UPDATE RESTRICT;');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_member');
    }
}
