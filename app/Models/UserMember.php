<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Models\Member;
use App\Models\User;
use Yajra\Address\HasAddress;


class UserMember extends Authenticatable
{

    use HasAddress;
        protected $guard = 'member';
    use HasFactory;
    protected $table = 'user_member';
    protected $fillable = [
    'username',
    'user_id',
    'password', 
    'member_no', 
    'long_name', 
    'contact_no',
    'branch_id',
    'email_address', 
    'birthdate', 
    'gender', 
    'fb_link', 
    'street', 
    'barangay_id', 
    'city_id', 
    'province_id', 
    'region_id', 
    'role_id', 
    'crea_by', 
    'upd_by', 
    'soft_deleted_at'];

    public function member()
    {
        return $this->belongsTo('App\Models\Member','member_no','member_no');
    }
    public function user()
    {
        return $this->belongsTo('App\Models\User','user_id','id');
    }
}
