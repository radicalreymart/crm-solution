<!-- @extends('layouts.app1') -->

@section('loanstyle')
<style>
.step {
  position: relative;
  min-height: 1em;
  color: gray;
}
.title {
  line-height: 1.5em;
  font-weight: bold;
}
.caption {
  font-size: 0.8em;
}
.step + .step {
  margin-top: 1.5em
}
.step > div:not(:first-child) {
  margin-left: 1.5em;
  padding-left: 1em;
}
/* Circle */
.circle {
  background: gray;
  position: relative;
  width: 1.5em;
  height: 1.5em;
  line-height: 1.5em;
  border-radius: 100%;
  color: #fff;
  text-align: center;
  box-shadow: 0 0 0 3px #fff;
}
/* Vertical Line */
.circle:after {
  content: ' ';
  position: absolute;
  display: block;
  top: 1px;
  right: 50%;
  bottom: 1px;
  left: 50%;
  height: 100%;
  width: 1px;
  transform: scale(1, 2);
  transform-origin: 50% -100%;
  background-color: rgba(0, 0, 0, 0.25);
  z-index: -1;
}
.step:last-child .circle:after {
  display: none
}
.step.step-active {
  color: #4285f4
}
.step.step-active .circle {
  background-color: #4285f4;
}


	td.shorten_text {
/*max-width: 100px;
 overflow: hidden;
 text-overflow: ellipsis;
 white-space: nowrap;*/
 max-width: 100px;
  padding: 5px;
  text-overflow: ellipsis;
  white-space: nowrap;
  /*overflow: hidden;*/
}

td.shorten_text:hover {
  overflow: visible;
}
td.shorten_text:before{
  content: attr(title);
}
.alert{
	color: black !important;
}
.form-control{
	line-height: 1 !important;
}
[type=file] {
		position: absolute;
		filter: alpha(opacity=0);
		opacity: 0;
}
input,
[type=file] + label {
	border: 1px solid black;
	border-radius: 3px;
	text-align: left;
	padding: 0px;
	width: 100%;
	height: 30px;
	margin: 0;
	left: 0;
	position: relative;
}
[type=file] + label {
	text-align: center;
	left: 0em;
	top: 0.5em;
	/* Decorative */
	background: blue;
	color: #fff;
	border: black;
	cursor: pointer;
}
[type=file] + label:hover {
	background: #3399ff;
}
label.shorten_text1 {
 max-width: 100%;
	/*padding: px;*/
	text-overflow: ellipsis;
	white-space: nowrap;
	overflow: hidden;
}

label.shorten_text1:hover {
	overflow: visible;
}
label.shorten_text1:before{
	content: attr(title);
}
button::-moz-focus-inner {border: 0;}
/* ----------------------- */
/* Basic page layout */
/*body {
	max-width:600px;
	margin:auto;
	border:1px solid #aaa;
	padding:1em;
	font-size:1em;
	font-family:Arial,Helvetica,sans-serif;
}*/

</style>
@endsection

@section('content')
<!-- Masthead-->
<header class="masthead mb-1">
	
	
</header>

	 @include('sweetalert::alert')
<div class="container mt-5 col-md-8" id="sign_up">
	<div class="card p-2 pt-3 bg-light border-0">
		<div class="row"> 
			<div class="col-md-12 order-md-1">
				<div class="card mx-3 shadow-lg border-0">
					<div class="card-header bg-light text-primary py-2 pl-3 border-0">
						<h6 class="m-0 default-primary">Loan Transaction Index<a class=" hvr-icon-push float-right" onClick="window.location.href = '/enduser/home';" data-url="{{ url('/enduser/home') }}">{{ __('Back') }}<i class="far fa-caret-square-left hvr-icon"></i></a></h6>
						
					</div>
					<div class="card-body px-3">
								<a class="btn btn-outline-light btn-xl text-uppercase " style="color: black; border-color: black; " type="button" href="{{route('preloan.create')}}">New Loan</a>
						    
						<div class="justify-content">
						<input type="hidden" value="{{url('/')}}" id="url" name="url">
			              <div id="modalCatcher" class="table-responsive">
				              <table id="datatablesSimple" class="table">
				                <thead  class=" text-primary">
				                    <th  style=" z-index: 500; width: 30px;" class="sticky-col first-col"></th>
				                    <th  style="z-index: 500; width: 30px; color: #8ebf42;" class="sticky-col second-col"><i class="fa fa-paperclip" aria-hidden="true"></i></th>
				                    <th  style=" z-index: 500; width: 30px;" class="sticky-col third-col" >Amount Applied/<br>Approved</th>
				                    <th style=" z-index: 500; width: 30px;" class="sticky-col fourth-col" >Reference Code

				                    </th>
				                    <th style=" z-index: 500;" class="sticky-col fifth-col">Loan Status</th>
				                    <th >Share Capital</th>
				                    <th >Member Code/Name </th>
				                    <th >Loan Product</th>
									<!-- <th >Loan Purpose</th>
				                    <th >Payment</th>
				                    <th >Income Source</th> -->
				                    <!-- <th >Monthly Expense</th>
				                    <th >Monthly Income</th>
				                    <th >Email Address</th>
				                    <th >Gender</th>
				                    <th >Age Group</th>
				                    <th >Address</th>
				                    <th >Facebook</th>
				                    <th >Marketing Channel</th>
				                    <th>Updated By</th>
				                    <th>Date Updated</th> -->
				                </thead>
				                <tbody>
				                    @if($preloan->count())
				                    @foreach ($preloan as  $key => $data)    
				                    <tr>

				                    <td class="sticky-col first-col">
				                    </td>
				                    <td class="sticky-col second-col ">
				                      <a href="javascript:void(0)" title="Loan Details" class="edit" data-id="{{ $data->id }}">
				                        <i class="fas fa-eye  fa-lg"></i>
				                      </a>
				                      @if($data->file_path1 != null)
				                      <a href="javascript:void(0)" title="Show Requirement" class="attachment" data-id="{{ $data->id }}">
				                        <i  class="fas fa-file-image hvr-pulse fa-lg" aria-hidden="true"></i>
				                      </a>
				                      @endif
				                    
				                       
				                    </td>
				                    <td style="z-index: 1 ;" id="sticky-col{{$key}}" class="sticky-col third-col">
				                    	{{ number_format($data->amount_needed, 2)}}/
				                    	@if(isset($data->amount_approved))
				                    	{{ number_format($data->amount_approved, 2)}}
				                    	@else
				                    	Null
				                    	@endif
				                    </td>
				                    <td id="sticky-col"  class="sticky-col fourth-col ref_number"> 
				                    @if(isset($data->ref_number))
							        <a href="javascript:void(0)" title="." class="copyNonInputToClipboard"  data-id='{{$key}}'>
								        <p id="p-to-copy{{$key}}">
					                    {{$data->ref_number}} 
								        </p>
							        </a>
				                    @endif</td>
				                    <td style="display: flex; justify-content: center !important;">
				                    	@if($data->loan_status == 1) Pending For Approval &nbsp; 

				                    	@elseif($data->loan_status == 2) Approved
				                    	
				                    	@elseif($data->loan_status == 3) Declined &nbsp; 

				                    	@else For Assesment @endif
				                    	<a href="javascript:void(0)" title="Loan Status" class="status"  data-id="{{ $data->id }}">
				                        <i class="fas fa-search  fa-lg"></i>
				                      	</a>
				                	</td>
				                    <td >{{number_format($data->share_capital, 2)}}</td>
				                    <td >{{$data->usermember->member->member_no}}<br>
				                    	 {{$data->usermember->member->long_name}}
				                    </td>
				                    <td>
				                    	{{$data->loan_type->name}}
				                    </td>
					                 
				                    @endforeach
				                    @endif
				                </tbody>
				              </table>
			              </div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- Modal -->
<div class="modal fade" id="ajax-attachment-model" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
            <h4 class="modal-title" id="ajaxAttachmentModel"></h4>
            <button type="button" class="btn btn-default" onClick="window.location.reload();" id="close-modal" data-dismiss="modal">Close</button>
            </div>
            <div class="modal-body">
                <div class="card-body" >
                   
                    <div class="">
                      <div class="slide s01">
                        <a id="href0" target="_blank" rel="noopener noreferrer">
                            <h4 id="name0"></h4>
                        <img id="img0" height="300" width="400">
                        </a>
                      </div>
                      <div class="slide s02">
                        <a id="href1" target="_blank" rel="noopener noreferrer">
                            <h4 id="name1"></h4>
                         <img id="img1" height="300" width="400">
                        </a>
                      </div>
                      <div class="slide s03">
                        <a id="href2" target="_blank" rel="noopener noreferrer">
                            <h4 id="name2"></h4>
                        <img id="img2" height="300" width="400">
                        </a>
                      </div>
                      <div class="slide s04">
                        <a id="href3" target="_blank" rel="noopener noreferrer">
                            <h4 id="name3"></h4>
                        <img id="img3" height="300" width="400">
                        </a>
                      </div>
                      <div class="slide s05">
                        <a id="href4" target="_blank" rel="noopener noreferrer">
                            <h4 id="name4"></h4>
                        <img id="img4" height="300" width="400">
                        </a>
                      </div>
                      <div class="slide s06">
                        <a id="href5" target="_blank" rel="noopener noreferrer">
                            <h4 id="name5"></h4>
                         <img id="img5" height="300" width="400">
                        </a>
                      </div>
                      <div class="slide s07">
                        <a id="href6" target="_blank" rel="noopener noreferrer">
                            <h4 id="name6"></h4>
                        <img id="img6" height="300" width="400">
                        </a>
                      </div>
                      <div class="slide s08">
                        <a id="href7" target="_blank" rel="noopener noreferrer">
                            <h4 id="name7"></h4>
                        <img id="img7" height="300" width="400">
                        </a>
                      </div>
                      <div class="slide s09">
                        <a id="href8" target="_blank" rel="noopener noreferrer">
                            <h4 id="name8"></h4>
                        <img id="img8" height="300" width="400">
                        </a>
                      </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="ajax-tracking-model" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
            <h4 class="modal-title" id="ajaxTrackingModel"></h4>
            <button type="button" class="btn btn-default" id="close-modal" onClick="window.location.reload();" data-dismiss="modal">Close</button>
            </div>
            <div class="modal-body">
					<div id="step-1" class="step">
					   <div>
					      <div id="circle1" class="circle"><i class="fa fa-check"></i></div>
					   </div>
					   <div>
					      <div id='title1' class="title">Loan Application</div>
					      <div id='caption1' class="caption">For Assesment</div>
					   </div>
					</div>
					<div id="step-2" class="step">
					   <div>
					      <div id="circle2" class="circle">2</div>
					   </div>
					   <div>
					      <div id='title2' class="title"></div>
					      <div id='caption2' class="caption"></div>
					   </div>
					</div>
					<div  id="step-3" class="step">
					   <div>
					      <div id="circle3" class="circle">3</div>
					   </div>
					   <div>
					      <div id='title3' class="title"></div>
					      <div id='caption3' class="caption"> </div>
					   </div>
					</div>
            </div>
        </div>
    </div>
</div>
<!-- Modal -->
 <div class="modal fade" id="ajax-preloan-model" aria-hidden="true" data-backdrop="static" data-keyboard="false">
      <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
            <h4 class="modal-title" id="ajaxPreloanModel"></h4>
            <button type="button" class="btn btn-default" id="close-modal" onClick="window.location.reload();" data-dismiss="modal">Close</button>
            </div>
            <div class="col-md-8">
            <p id="created_at"></p>
            </div>
            <div class="modal-body">
                    <form action="{{ route('enduser-edit-mode-terms') }}" method="POST" id="insert_form" name='formname' >
                                        @csrf
                            <input type="hidden" name="id" id="id">
                            <input type="hidden" value="{{url('/')}}" id="url" name="url">
                            <input type="hidden" disabled id="file_path1" name="file_path1">
                       
	                           
                            <div class="form-group row">
                               <div class="card-header bg-light text-primary py-2 pl-3 border-0">
                                  <h6 class="m-0 default-primary">LOAN ASSESSMENT
                                  </h6>
                                </div>
                            </div>
							<div class="form-group row">
                                <label  for="name" class="col-md-6 col-form-label float-left">{{ __('Reference Code') }}</label>
                                <div  class="col-md-6" >
                                    <input type="text" disabled  class="form-control form-control-sm @error('ref_number') is-invalid @enderror" readonly id="ref_number" name="ref_number">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label  for="name" class="col-md-6 col-form-label float-left">{{ __('Share Capital') }}</label>
                                <div  class="col-md-6" >
                                    <input type="number" disabled class="form-control form-control-sm @error('share_capital') is-invalid @enderror" onchange="(function(el){el.value=parseFloat(el.value).toFixed(2);})(this)" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"value="0.00" maxlength="6" min="0.00" readonly max="9999999.00" step="0.05" id="share_capital" name="share_capital">
                                </div>
                            </div>
                            <div id="comakerdiv" hidden class="form-group row">
                                <label  for="name" class="col-md-6 col-form-label float-left">{{ __('Co-Maker') }}</label>
                                <div  class="col-md-6" >
                                    <input type="text"  disabled  required  class="form-control m @error('co_maker') is-invalid @enderror" value="{{ old('co_maker') }}" id="co_maker"  name="co_maker">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label  for="name" class="col-md-6 col-form-label float-left">{{ __('Branch to Loan to') }}</label>
                                <div  class="col-md-6" >
                                    <select disabled  class="  form-control @error('branch_id') is-invalid @enderror" id="branch_id"  required  type="text" name="branch_id" autocomplete="branch_id" >
                                       	@foreach($branch as $item)
                                        <option value="{{$item->id}}">{{$item->name}}</option>
                                        @endforeach
                                      </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label  for="name" class="col-md-6 col-form-label float-left">{{ __('Amount Applied') }}</label>
                                <div  class="col-md-6" >
                                    <input type="number" disabled class="form-control form-control-sm @error('amount_needed') is-invalid @enderror" onchange="(function(el){el.value=parseFloat(el.value).toFixed(2);})(this)" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"value="0.00" maxlength="6" min="0.00" readonly max="9999999.00" step="0.05" id="amount_needed" name="amount_needed">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label  for="name" class="col-md-6 col-form-label float-left">{{ __('Amount Approved') }}</label>
                                <div  class="col-md-6" >
                                    <input type="number" disabled class="form-control form-control-sm @error('amount_approved') is-invalid @enderror" onchange="(function(el){el.value=parseFloat(el.value).toFixed(2);})(this)" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"value="0.00" maxlength="6" min="0.00" readonly max="9999999.00" step="0.05" id="amount_approved" name="amount_approved">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label  for="name" class="col-md-6 col-form-label float-left">{{ __('What loan product do you want to avail?') }}</label>
                                <div  class="col-md-6" >
                                    <select disabled  class="  form-control @error('loan_product') is-invalid @enderror" id="loan_product"  required  type="text" name="loan_product" autocomplete="loan_product" >
                                       	@foreach($loan_type as $item)
                                        <option value="{{$item->id}}">{{$item->name}}</option>
                                        @endforeach
                                      </select>
                                </div>
                            </div>
                            <div id="savings_salary_div" hidden class="form-group row">
                                <label  for="name" class="col-md-6 col-form-label float-left" id="savings_salary_label"></label>
                                <div  class="col-md-6" >
                                    <input type="number" disabled class="form-control form-control-sm @error('savings_salary_amount') is-invalid @enderror" onchange="(function(el){el.value=parseFloat(el.value).toFixed(2);})(this)" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"value="0.00" maxlength="6" min="0.00" readonly max="9999999.00" step="0.05" id="savings_salary_amount" name="savings_salary_amount">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="name" class="col-md-6 col-form-label float-left">{{ __('Purpose of Loan') }}</label>
                                <div  class="col-md-6" >
                                    <select disabled hidden class="  form-control @error('loan_purpose') is-invalid @enderror" id="loan_purpose"  required   type="text" name="loan_purpose" autocomplete="loan_purpose" >
                                        <option value="" selected disabled>Select Status</option>
                                        <option value='0' @if (old('loan_purpose') == '0') selected="selected" @endif>Business-Related Expense</option>
                                        <option value='1' @if (old('loan_purpose') == '1') selected="selected" @endif>Debt Consolidation</option>
                                        <option value='2' @if (old('loan_purpose') == '2') selected="selected" @endif>Education  / Tuition fees</option>
                                        <option value='3' @if (old('loan_purpose') == '3') selected="selected" @endif>Health / Medical fees</option>
                                        <option value='4' @if (old('loan_purpose') == '4') selected="selected" @endif>Home Renovation / Improvements</option>
                                        <option value='5' @if (old('loan_purpose') == '5') selected="selected" @endif>Life Events / Celebration</option>
                                        <option value='6' @if (old('loan_purpose') == '6') selected="selected" @endif>Personal Consumption</option>
                                        <option value='7' @if (old('loan_purpose') == '7') selected="selected" @endif>Purchase of Appliance / Gadget / Furniture</option>
                                        <option value='8' @if (old('loan_purpose') == '8') selected="selected" @endif>Start-up Business</option>
                                        <option value='9' @if (old('loan_purpose') == '9') selected="selected" @endif>Vacation / Travel Expense</option>
                                        <option value='10' @if (old('loan_purpose') == '10') selected="selected" @endif>Vehicle Repair / Purchase</option>
                                      </select>
	                               <input type="text" hidden disabled required  class="form-control m @error('other_purpose') is-invalid @enderror" value="{{ old('other_purpose') }}" id="other_purpose"  name="other_purpose">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label  for="name" class="col-md-6 col-form-label float-left">{{ __('Payment Terms') }}</label>
                                <div  class="col-md-6" >
                                    <select  class="  form-control custom-select @error('payment_terms') is-invalid @enderror" id="payment_terms"  required  type="text" name="payment_terms" autocomplete="payment_terms" >
                                     	@foreach($payment_terms as $item)
                                        <option value="{{$item->id}}">{{$item->name}}</option>
                                        @endforeach
                                      </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label  for="name" class="col-md-6 col-form-label float-left">{{ __('Mode of Payment') }}</label>
                                <div  class="col-md-6" >
                                    <select  class="form-control custom-select @error('payment_mode') is-invalid @enderror" id="payment_mode"  required  type="text" name="payment_mode" autocomplete="payment_mode" >
                                        
                                      </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label  for="name" class="col-md-6 col-form-label float-left">{{ __('Source of Income') }}</label>
                                <div  class="col-md-6" >
                                    <select disabled hidden class="form-control m @error('occupation_id') is-invalid @enderror"   id="occupation_id"  required  type="text" name="occupation_id" autocomplete="occupation_id" >
                                        <option value="" selected disabled>Select</option>
                                         @foreach($occupation as $item)
                                        <option  value="{{$item->id}}" @if (old('occupation_id') == '{{$item->id}}') selected="selected" @endif>{{$item->name}}</option>
                                        @endforeach
                                    </select>
                                    <input type="text" hidden disabled required  class="form-control m @error('other_occupation') is-invalid @enderror" value="{{ old('other_occupation') }}" id="other_occupation"  name="other_occupation">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="name" class="col-md-6 col-form-label float-left">{{ __('How much is your monthly gross income?') }}</label>
                                <div  class="col-md-6" >
                                    <input type="text"  required  class="form-control m @error('montly_income') is-invalid @enderror" value="{{ old('montly_income') }}" id="montly_income"  name="montly_income">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label  for="name" class="col-md-6 col-form-label float-left">{{ __('How much are your monthly expenses?') }}</label>
                                <div  class="col-md-6" >
                                    <input type="text" required class="form-control m @error('montly_expense') is-invalid @enderror" value="{{ old('montly_expense') }}" id="montly_expense"  name="montly_expense">
                                </div>
                            </div>

            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary btn-save" id="btn-save">Save
                </button>
            </div>
                    </form>  
        </div>
    </div>
</div>


@endsection

@section('preloan')

<script>


$(document).ready(function(){

const swalWithBootstrapButtons = Swal.mixin({
  customClass: {
    confirmButton: 'btn btn-success',
    cancelButton: 'btn btn-danger'
  },
  buttonsStyling: false
})
	$('#ajax-tracking-model').modal({backdrop: 'static', keyboard: false})  
 	$.clearInput = function () {
        $('insert_form').find('input[type=text], input[type=password], input[type=number], input[type=email], input[type=file], label, select, textarea').val('');
    };
     $('.modal').on('hidden.bs.modal', function(){
             // window.location.reload();
             // $('#insert_form').removeData('bs.modal')
    $.clearInput();
    $(this).find('form')[0].reset();
    });
	$.ajaxSetup({
			headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
	});


// document.addEventListener('DOMContentLoaded', function() {
$('#modalCatcher').on('click', '.copyNonInputToClipboard', function () 
{
        var id = $(this).data('id');
  // function copyNonInputToClipboard(id) {
    /* Text can only be copied to the clipboard from an input. This function creates a temporary textArea, fills it with the intended text, positions it to not be noticed, selects & copies the text, and then deletes the temporary textArea */
    const text = document.getElementById('p-to-copy'+id).innerText;
	// alert(text);
    const textArea = document.createElement("textarea");
    textArea.value = text;
    textArea.setAttribute("readonly", "");
    textArea.style.position = "absolute";
    textArea.style.left = "-9999px";
    document.body.appendChild(textArea);
    textArea.select();
    document.execCommand("copy");
    document.body.removeChild(textArea);
    Swal.fire("The following text has been copied to the clipboard:" + "\n\n" + text);
    // alert(
    //     "The following text has been copied to the clipboard:" + "\n\n" + text
    // );
// }
});
    $('.status').click(function () {
       $('#ajaxTrackingModel').html("Loan Status");
       $('#ajax-tracking-model').modal('show');
       var id = $(this).data('id');
        var base_path = $("#url").val();
        // alert(id+base_path);
        $.ajax({
                type:"get",
                url: base_path+"/loanstatusAPI/"+id,
                data: { id: id },
                dataType: 'json',
                success: function(res){
                	// alert(res.loan_status);
				      // $('#step-2').addClass('step-active');
				      $('#step-3').attr('hidden', true);
				      $('#step-2').attr('hidden', true);
				      // $('#title2').html('Pending for Approval');
				      // $('#caption2').html('Please wait while your Loan is being assessed');
                	if(res.loan_status == 1){
                		$('#step-2').removeClass('step-active');
				      $('#step-2').attr('hidden', false);
				      $('#step-2').addClass('step-active');
				      $('#circle2').html('<i class="fa fa-check"></i>');
				      $('#title2').html('Pending for Approval');
				      $('#caption2').html(res.approval_remarks);
                	}else if(res.loan_status == 2){
				      $('#step-2').removeClass('step-active');
				      $('#step-3').attr('hidden', false);
				      $('#step-3').addClass('step-active');
				      $('#circle2').html('<i class="fa fa-check"></i>');
				      $('#title3').html('Approved');
				      if(res.approval_remarks === null){
				      	// console.log(res.approval_remarks);
					      $('#caption3').html('Your Loan have been approve please go to the corresponding branch for disbursement');
				      }else{
				      	  $('#caption3').html(res.approval_remarks);
				      }
                	}else if(res.loan_status == 3){
                		$('#step-2').removeClass('step-active');
				      $('#step-3').addClass('step-active');
				      $('#circle2').html('<i class="fa fa-check"></i>');
				      $('#title3').html('Declined');
				      $('#caption3').html(res.approval_remarks);

                	}

                }
                ,error: function (data) {
                     // alert('please delete the preloan connected to this preloan/preloan');
                     console.log(data.responseText);  
                }
            });

    });

	$('#modalCatcher').on('click', '.attachment', function () 
    {
      $('#ajaxAttachmentModel').html("Requirements");
      $('#ajax-attachment-model').modal('show');
        var id = $(this).data('id');
        var base_path = $("#url").val();
        // alert(id+base_path);
        $('#attachmentID').val(id);
        $.ajax({
                type:"get",
                url: base_path+"/enduser-attachment",
                data: { id: id },
                dataType: 'json',
                success: function(res){
                    // alert(JSON.stringify(res));
                    var frmcontroler = JSON.stringify(res).split(',');
                    var expAttacment = res[0].split('|');
                    var basesigniture = res[1].signiture;
                    var splitsigniture = res[1].signiture.split('-');
                    var imgsrc1 = base_path+'/assets/img/uploads/loans/'+splitsigniture[0]+'/'+basesigniture;
                    // alert(JSON.stringify(splitsigniture[1]));
                    document.getElementById("name"+8).innerHTML = splitsigniture[1];
                    var img1 = document.getElementById("img"+8);
                    var aid1 = document.getElementById("href"+8);
                    var src1 = document.createAttribute("src");
                    var href1 = document.createAttribute("href");
                    src1.value = imgsrc1;
                    img1.setAttributeNode(src1);  
                    href1.value = imgsrc1;
                    aid1.setAttributeNode(href1);
                    //   // $('#id').val(res.id);
                    $.each( expAttacment, function( key, value ) {
                        var prefix = value.split('-');
                        // var array
                        var imgsrc = base_path+'/assets/img/uploads/loans/'+prefix[0]+'/'+value;
                        // alert(prefix[1]);
                        document.getElementById("name"+key).innerHTML = prefix[1];
                        var img = document.getElementById("img"+key);
                        var aid = document.getElementById("href"+key);
                        var src = document.createAttribute("src");
                        var href = document.createAttribute("href");
                        src.value = imgsrc;
                        img.setAttributeNode(src);  
                        href.value = imgsrc;
                        aid.setAttributeNode(href);
                      // var row = document.getElementById("tablerow");
                      // var cell = row.insertCell(0);
                      // cell.innerHTML = '<img src='+imgsrc+' height=100 width=300>';
                    });

                }
                ,error: function (data) {
                     // alert('please delete the preloan connected to this preloan/preloan');
                     console.log(data.responseText);  
                }
            });
    });


  $('#modalCatcher').on('click', '.edit', function () 
    {
        var id = $(this).data('id');
        var base_path = $("#url").val();
                $.ajax({
                        type:"POST",
                        url: base_path+"/enduser-edit-preloan",
                        data: { id: id },
                        dataType: 'json',
                        success: function(res){
                        if(res.gender == "F"){
                            console.log('female');
                            var gender = 1;
                        }else{
                            console.log('male');
                            var gender = 0;
                        }
                          $('#ajaxPreloanModel').html("Loan Details");
                          $('#ajax-preloan-model').modal('show');
                          $('#created_at').html("Date Created: "+res.created_at);
                          $('#id').val(res.id);
                          $("#fullname").val(res.fullname);
                          $("#age_group").val(res.age_group);
                          $("#age_group").attr('disabled', true);
                          $("#amount_needed").val(res.amount_needed);
                          $("#amount_approved").val(res.amount_approved);
                          $("#share_capital").val(res.share_capital);
                          $("#loan_product").val(res.loan_product);
                          switch(res.loan_product){
				            case 18:
				              $('#savings_salary_div').attr('hidden', false);
				              $('#savings_salary_label').html('Salary');
	                          $('#savings_salary_amount').val(res.savings_salary_amount);
				              break;
				            case 19:
				              $('#savings_salary_div').attr('hidden', false);
				              $('#savings_salary_label').html('Salary');
	                          $('#savings_salary_amount').val(res.savings_salary_amount);
				              break;
				            case 20:
				              $('#savings_salary_div').attr('hidden', false);
				              $('#savings_salary_label').html('Mid-Year Bonus');
	                          $('#savings_salary_amount').val(res.savings_salary_amount);
				              break;
				            case 21:
				              $('#savings_salary_div').attr('hidden', false);
				              $('#savings_salary_label').html('Year-End Bonus');
	                          $('#savings_salary_amount').val(res.savings_salary_amount);
				              break;
				            case 22:
				              $('#savings_salary_div').attr('hidden', false);
				              $('#savings_salary_label').html('Performance Evaluation Incentives');
	                          $('#savings_salary_amount').val(res.savings_salary_amount);
				              break;
				            case 23:
				              $('#savings_salary_div').attr('hidden', false);
				              $('#savings_salary_label').html('Performance Based Bonus');
	                          $('#savings_salary_amount').val(res.savings_salary_amount);
				              break;
				            case 24:
				              $('#savings_salary_div').attr('hidden', false);
				              $('#savings_salary_label').html('Salary');
	                          $('#savings_salary_amount').val(res.savings_salary_amount);
				              break;
				            case 25:
				              $('#savings_salary_div').attr('hidden', false);
				              $('#savings_salary_label').html('Mid-Year Bonus');
	                          $('#savings_salary_amount').val(res.savings_salary_amount);
	                          break;
				            case 26:
				              $('#savings_salary_div').attr('hidden', false);
				              $('#savings_salary_label').html('Year-End Bonus');
	                          $('#savings_salary_amount').val(res.savings_salary_amount);
				              break;
				            default:
				              var result = null;
				              break;  
                          }
                          $("#ref_number").val(res.ref_number);
                          $("#payment_terms").val(res.payment_terms);
                          $.ajax({
					          type:"get",
					          url: base_path+"/payment_modeAPI/"+res.loan_product,
					          success: function(data){
					            var payment_mode = document.getElementById("payment_mode");
					            for (var i = 0; i < data.length; ++i) {
					                // Append the element to the end of Array list
					              payment_mode[payment_mode.length] = new Option(data[i]['paymode_name'], data[i]['paymode_id']);
					            }
	                          $("#payment_mode").val(res.payment_mode);
					          }
					        });
                          if(res.co_maker != null){
	                          $("#co_maker").val(res.co_maker);
	                          $("#comakerdiv").attr('hidden', false);
                          }else{
	                          $("#comakerdiv").attr('hidden', true);
                          }
                          if(res.occupation_id != null){
	                          $("#occupation_id").val(res.occupation_id);
	                          $("#occupation_id").attr('hidden', false);
                          }else{
	                          $("#other_occupation").val(res.other_occupation);
	                          $("#other_occupation").attr('hidden', false);
                          }
                          if(res.loan_purpose != null){
	                          $("#loan_purpose").val(res.loan_purpose);
	                          $("#loan_purpose").attr('hidden', false);
                          }else{
	                          $("#other_purpose").val(res.other_purpose);
	                          $("#other_purpose").attr('hidden', false);
                          }
                          $("#montly_income").val(res.montly_income);
                          $("#montly_expense").val(res.montly_expense);
                          $("#email").val(res.email);
                          $("#gender").val(gender);
                          $("#fb_link").val(res.fb_link);
                          $("#montly_income").attr('disabled', true);
                          $("#montly_expense").attr('disabled', true);
                          $("#email").attr('disabled', true);
                          $("#gender").attr('disabled', true);
                          $("#fb_link").attr('disabled', true);
                          $("#contact_no").val(res.contact_no);
                          $("#occupation").val(res.occupation_id);
                          $("#region_id").val(res.region_id);
                          $("#occupation").attr('disabled', true);
                          $("#region_id").attr('disabled', true);
                          var province_id1 = res.province_id;
                          $.ajax({
                                        type:"GET",
                                        // base_path+'getProvince/'+region_code,
                                        url:base_path+'/getProvince/'+res.region_id,
                                        success:function(res){               
                                        if(res){
                                        $("#province_id").empty();
                                        $("#province_id").attr('readonly', false);
                                        $("#province_id").append('<option value="">Select Province</option>');                            
                                        $("#city_id").append('<option value="">Select City</option>');
                                        $("#barangay_id").empty();
                                        $("#barangay_id").append('<option value="">Select Barangay</option>');
                                        $.each(res,function(key,value){
                                            $("#province_id").append('<option class="form-control" value="'+ value['province_id'] +'">'+ value['name'] +'</option>');
                                        });
                                          $("#province_id").val(province_id1);
                                          $("#province_id").attr('disabled', true);
                                        }else{
                                           $("#province_id").empty();
                                        }
                                        }
                                        });
                          var city_id1 = res.city_id;
                          $.ajax({
                                       type:"GET",
                                       url:base_path+'/getCities/'+province_id1,
                                       success:function(res){  
                                        if(res){
                                            $("#city_id").attr('readonly', false);
                                            $("#city_id").empty();
                                            $("#city_id").append('<option value="">Select City</option>');
                                            $("#barangay_id").empty();
                                            $("#barangay_id").append('<option value="">Select Barangay</option>');
                                            $.each(res,function(key,value){
                                                $("#city_id").append('<option class="form-control" value="'+ value['city_id'] +'">'+ value['name'] +'</option>');
                                            });
                                            $("#city_id").val(city_id1);
                                            $("#city_id").attr('disabled', true);
                                        }else{
                                           $("#city_id").empty();
                                        }
                                       }
                                        });
                          var barangay_id1 = res.barangay_id;
                          $.ajax({
                                       type:"GET",
                                       url:base_path+'/getBarangays/'+city_id1,
                                       success:function(res){  
                                        if(res){
                                           $("#barangay_id").attr('readonly', false);
                                           $("#barangay_id").empty();
                                            $("#barangay_id").append('<option value="">Select Barangay</option>');
                                            $.each((res),function(key,value){
                                                $("#barangay_id").append('<option class="form-control" value="'+ value['code'] +'">'+ value['name'] +'</option>');
                                            });
                                          $("#barangay_id").val(barangay_id1);
                                          $("#barangay_id").attr('disabled', true);
                                        }else{
                                           $("#barangay_id").empty();
                                        }
                                       }
                                    });
                          $("#street").val(res.street);
                          $("#street").attr('disabled', true);
                          $("#marketing_channel_id").val(res.marketing_channel_id);
                          $("#member_status").val(res.member_status);
                       }
                        ,error: function (data) {
                             var err = JSON.parse(data.responseText);
                             if(err['message'] == 'Password confirmation required.'){   
                             alert(err['message']);
                             window.location.href = "password/confirm";
                             }else{
                             // alert('please delete the preloan connected to this preloan/preloan');
                             alert(data.responseText);
                             }
                         }
                    });
    });
	});

// $('#modalCatcher').on('click', '#newloan', function () 
// {
//         var id = $(this).data('id');
//         var base_path = $("#url").val();
//         var loanstatus = 3;
//         // alert(id+base_path);

// 	 swalWithBootstrapButtons.fire({
//           title: 'New Loan Application',
//           text: "Are you sure your loan is fully paid?",
//           icon: 'warning',
//           showCancelButton: true,
//           confirmButtonText: 'Yes',
//           cancelButtonText: 'No',
//           reverseButtons: true
//         }).then((result) => {
//           if (result.isConfirmed) {

//           	$.ajax({
//             type:"put",
//             url: base_path+"/loanstatus2/"+id,
//             dataType: 'json',
//             data: {
//               loanstatus:loanstatus,
//             },
//             success: function(res){
//             swalWithBootstrapButtons.fire({
//                   title: 'Success!',
//                   icon: 'success',
//                   confirmButtonText: 'Confirmed',
//                 }).then((result) => {
//              window.location.reload();})
//               // alert(JSON.stringify(res));
//             // $("#btn-save").html('Submit');
//             // $("#btn-save"). attr("disabled", false);
//             }
//             ,error: function (data) {
//                  // alert('please delete the preloan connected to this preloan/preloan');
//                  alert(data.responseText);  
//             }

//          });
//             // $.ajax({
//             //     url: $(this).data('url'),
//             //     type: 'POST',
//             //     headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
//             //     data: 'ids='+join_selected_values,
//             //     success: function (data) {
//             //         if (data['success']) {
//             //             $(".sub_chk:checked").each(function() {  
//             //                 $(this).parents("tr").remove();
//             //             });
//             //             swalWithBootstrapButtons.fire(
//             //               'Deleted!',
//             //               'Your file has been deleted.',
//             //               'success'
//             //             )
//             //             // var err = JSON.stringify(data);
//             //             // alert(err);
//             //             // alert(data['success']);
//             //         // } else if (data['message']) {
//             //         // alert(data['message']);
//             //         // window.location.href = "password/confirm";
//             //         } else if (data['error']) {
//             //             alert(data['error']);
//             //         } else {
//             //             // alert(JSON.stringify(data));
//             //             alert('Whoops Something went wrong!!');
//             //         }
//             //     },
//             //     error: function (data) {
//             //         var err = JSON.parse(data.responseText);
//             //         if(err['message'] == 'Password confirmation required.'){   
//             //         alert(err['message']);

//             //        $('#verifyModalLabel').html("Delete Data");
//             //        $('#verifyModal').modal('show');
//             //         // window.location.href = "password/confirm";
//             //         }else{
//             //         alert(data.responseText);
//             //         // alert('please delete the pmes connected to this client/batch');
//             //         }
//             //     }
//             // });   
//           } else if (
//             /* Read more about handling dismissals below */
//             result.dismiss === Swal.DismissReason.cancel
//           ) {
//             swalWithBootstrapButtons.fire(
//               'Cancelled',
//               ' ',
//               'error'
//             )
//           }
//         }) 
// });

</script>
@endsection

