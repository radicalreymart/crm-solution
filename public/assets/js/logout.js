
$(function() {
  $.ajaxSetup({
    headers: {
      'X-CSRF-Token': $('meta[name="_token"]').attr('content')
    }
  });
        
                var base_path = $("#url").val();
        $('.logout').on('click', function(e) {
          var role = $('#role').val();
            const swalWithBootstrapButtons = Swal.mixin({
              customClass: {
                confirmButton: 'btn btn-success',
                cancelButton: 'btn btn-danger'
              },
              buttonsStyling: false
            })
            if(role == 3){
              swalWithBootstrapButtons.fire({
                    title: 'Magandang Buhay!',
                    icon: 'question',
                    text:" Thank you for using our website, Kamay-ari! We would like to know how we could improve our website through this survey. This will take only a few minutes.",
                    showCancelButton: true,
                    confirmButtonText: 'Yes',
                    cancelButtonText: 'Logout',
                    focusCancel: true
                  }).then((result) => {
                    if (result.isConfirmed) {
                      // alert(base_path);
                      window.location.href = $(this).data('feedback');
                    }else if (
                        /* Read more about handling dismissals below */
                        result.dismiss === Swal.DismissReason.cancel
                      ) {
                          swalWithBootstrapButtons.fire(
                            'Logout Successful'
                          ).then((result) => {
                            // window.location.reload();
                          window.location.href = $(this).data('url');
                          })
                        
                      // alert($(this).data('url'));
                          // $.ajax({
                          //     headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                          //     type: 'get',
                          //     url: $(this).data('url'),
                          //     // data: 'ids='+join_selected_values,
                          //     success: function (data) {
                          //         alert(JSON.stringify(data));
                          //        // window.location.reload();
                          //       }
                          //   });
                        }
                  })
                
            }else{

              swalWithBootstrapButtons.fire({
                title: 'Are you sure you want to Logout?',
                icon: 'question',
                showCancelButton: true,
                confirmButtonText: 'Yes',
                cancelButtonText: 'No',
                reverseButtons: true
              }).then((result) => {
                if (result.isConfirmed) {
                  // alert(role);
                  swalWithBootstrapButtons.fire(
                      'Logout Successful'
                    ).then((result) => {
                      // window.location.reload();
                    window.location.href = $(this).data('url');
                    })
                        
                   // $.ajax({
                   //        url: $(this).data('url'),
                   //        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                   //        type: 'POST',
                   //        success: function (data) {
                   //            swalWithBootstrapButtons.fire(
                   //              'Logout Successful'
                   //            ).then((result) => {
                   //              window.location.reload();
                   //            })
                   //          }
                   //          ,error: function (data) {
                   //              console.log(data.responseText);
                   //            }
                   //      });
                  } else if (
                    /* Read more about handling dismissals below */
                    result.dismiss === Swal.DismissReason.cancel
                  ) {
                  
                    }
              })
            }
                
                
        });

});

