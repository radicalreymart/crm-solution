<?php

namespace App\Http\Controllers;

use App\Models\BankMaster;
use App\Models\OnlinePymntTrxnMaster;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\UserRequest;
use Alert;

class BankMasterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $department = auth()->user()->department_id;
        $isAdmin = auth()->user()->role_id;
        $branch = auth()->user()->branch_id;
        // dd(auth()->user()->uname);
        if($department == 3 || $isAdmin == 0){
            $bank  = BankMaster::whereNull('soft_deleted_at')->get();
            return view('bank.index')
                ->with('bank', $bank );
        }else{
            Alert::error("You do not have the permission to access this page.");
            return redirect()->route('home');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('bank.create');
     
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate([
            'cd' => 'required',
            'description' => 'required',
        ]);
       BankMaster::create([
             'cd' => $request['cd'],
             'description' => $request['description'],
            'crea_by' => auth()->user()->uname,
        ]);
       
       
        return redirect()->route('bank.index')
            ->with('success', 'bank created successfully.');
    }

    public function storebank(Request $request)
    {
        $request->validate([
            'cd' =>'required',
            'description' =>'required',  
        ],
        ['cd.required' => 'Code is required',
        'description.required' => 'Description is required',
        ]);
        if(isset($request->id)){
            $upd_by = auth()->user()->uname;
            $updated_at = Carbon::now();
        }else{
            $upd_by = null;
            $updated_at = null;
        }
        $bank_upd = 
        [
            'id' => $request->id,
        ];
        $bank_create = 
        [
            'cd' => $request->cd,
            'description' => $request->description,
            'crea_by'     => auth()->user()->uname,
            'upd_by'         => $upd_by,
            'updated_at'     => $updated_at,
        ];
        BankMaster::updateOrcreate($bank_upd, $bank_create);
        if($request->id != null){
        $request->session()->flash('success', 'Bank updated successfully!');
        }else{
        $request->session()->flash('success', 'Bank created successfully!');    
        }
                return response()->json(['success' => 'success'], 200);
        // return redirect()->route('pmes.index')->with('success', 'PMES created successfully.');
      
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Project  $project
     * @return \Illuminate\Http\Response
     */
    // public function show($id, User $user)
    // {
    //     $user = User::find($id);
        
        
       
    //       return view('userTable.show', [
    //     'user' => $user
    //     ]);
    // }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $bank  = BankMaster::find($id);
       
       return view('bank.edit', [
        'bank' => $bank 
        ]);

    }
    public function editmodal(Request $request, BankMaster $bank)
    {
        //
        $where = array('id' => $request->id);
        $bank  = BankMaster::where($where)->first();
        $json = array('id' => $request->id
            , 'cd' => $bank->cd
            , 'description' => $bank->description);
         // $request->session()->flash('success', 'bank updated successfully!');
        return response()->json($json);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request)
    {
        $rules = array(
            'cd' => 'required',
            'description' => 'required',
        );
         $validator = Validator::make($request->all(), $rules);
         $bank  = BankMaster::find($id);
            $bank ->cd      = $request->get('cd');
            $bank ->description       = $request->get('description');
            $bank ->updated_at      = Carbon::now();
            $bank ->upd_by = auth()->user()->uname;
            $bank ->save();
       
        return redirect()->route('bank.index')
            ->with('success', 'bank updated successfully');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Project  $project
     * @return \Illuminate\Http\Response
     */
    
    public function destroy($id)
    {
        $bank  = BankMaster::find($id);
        $bank ->delete();

        return response()->json(['success'=>"bank deleted successfully.", 'tr'=>'tr_'.$id]);

    }
    public function deleteAll(Request $request)
    {
        $ids = $request->ids;
        $bank = BankMaster::whereIn('id',explode(",",$ids))->get();
        $optm = OnlinePymntTrxnMaster::whereIn('bank_id',explode(",",$ids))->whereNull('soft_deleted_at')->get();
        if($optm->count())
        {
            $json = ['error' => 'Please delete Online Payment Transaction  corresponding to this entry.'];
        }else
        {
            if($bank->count())
            {
                foreach($bank as $data)
                {
                    $bank = BankMaster::find($data->id);
                    $bank->soft_deleted_at = Carbon::now();
                    $bank->save();
                }
            }
            $json = ['success'=>"Bank Deleted successfully."];
        }
        return response()->json($json);
    }
}
