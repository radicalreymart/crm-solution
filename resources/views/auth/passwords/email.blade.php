@extends('layouts.app', [
    'namePage' => 'Reset Password',
    'class' => 'login-page sidebar-mini ',
    'activePage' => '',
    'backgroundImage' => asset('assets') . "/img/bg14.jpg",
])

@section('content')
    <div class="content">
        <div class="container">
            <div  class="col-md-4 ml-auto mr-auto">
                    <form role="form" method="POST" action="{{ route('password.email') }}">
                        @csrf
                    <div style='background-image: linear-gradient(rgb(51, 133, 255), white, white);
                    background-size: cover; border-radius: 200px; height: 1000px;' class="card card-login card-plain">
                        <div class="card-header ">
                        <div align="center" class="logo-container">
                            <img  src="{{ asset('assets/img/mhrlarge-removebg.png') }}" alt="">
                        </div>
                        </div>
                        <div class="card-body ">
                        <div class="no-border form-control-lg {{ $errors->has('email') ? ' has-danger' : '' }}">
                            <input class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" placeholder="{{ __('Email') }}" type="email" name="email" value="{{ old('email') }}" style="color: inherit;" required autofocus>
                        </div>
                        @if ($errors->has('email'))
                            <span class="invalid-feedback" style="display: block;" role="alert">
                            <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                        </div>
                        <div class="card-footer ">
                            <button  type = "submit" class="btn btn-primary btn-round btn-lg btn-block mb-3">{{ __('Send Password Reset Link') }}</button>
                        </div>
                    </div>
                    </form>
                </div>
        </div>
    </div>
@endsection


@push('js')
<script>
    $(document).ready(function() {
      demo.checkFullPageBackgroundImage();
    });
  </script>
@endpush