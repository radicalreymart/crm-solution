@extends('layouts.app1')
@section('loanstyle')
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.3/css/bootstrap.css">
<style>
    .kbw-signature {
        width: 100%;
        height: 200px;
    }
    #sig canvas {
        width: 100% !important;
        height: auto;
    }
</style>
<style>
  .alert{
    color: black !important;
  }
  [type=file] {
      position: absolute;
      filter: alpha(opacity=0);
      opacity: 0;
  }
  input,
  [type=file] + label {
    border: 1px solid black;
    border-radius: 3px;
    text-align: left;
    padding: 0px;
    width: 100%;
    height: 30px;
    margin: 0;
    left: 0;
    position: relative;
  }
  [type=file] + label {
    text-align: center;
    left: 0em;
    top: 0.5em;
    /* Decorative */
    background: blue;
    color: #fff;
    border: black;
    cursor: pointer;
  }
  [type=file] + label:hover {
    background: #3399ff;
  }
  label.shorten_text1 {
   max-width: 100%;
    /*padding: px;*/
    text-overflow: ellipsis;
    white-space: nowrap;
    overflow: hidden;
  }

  label.shorten_text1:hover {
    overflow: visible;
  }
  label.shorten_text1:before{
    content: attr(title);
  }
</style>
@endsection

@section('content')
    <!-- Masthead-->
    <header class="masthead mb-1">
      <div class="container mt-1 pt-1">
       @include('sweetalert::alert')
     
         
      </div>
    </header>
    
    <div class="container mt-5 col-md-8" id="sign_up">
      <div class="card p-2 pt-3 bg-light border-0">
        <div class="row"> 
          <div class="col-md-12 order-md-1">
            <form id="insert_form" name='formname' enctype="multipart/form-data"  method="post" action="{{ route('preloan.store') }}"> 
            
              <input type="hidden" value="{{url('/')}}" id="url" name="url">
              <input type="hidden"  id="sitefeedback" name="sitefeedback">
              <input type="hidden"  id="loanable_amount" name="loanable_amount">
              <input type="hidden"  id="less_loan" name="less_loan">
                                        @csrf
              @foreach ($user_member as $data)

              <div class="card mx-3 shadow-lg border-0">
                <div class="card-header bg-light text-primary py-2 pl-3 border-0">
                  
                  @if(!empty($preloan1))
                  <h6 class="m-0 default-primary">Personal Information<a class=" hvr-icon-push float-right" onClick="window.location.href = '/enduser/preloan/index';" >{{ __('Back') }}<i class="far fa-caret-square-left hvr-icon"></i></a></h6>
                  @else
                  <h6 class="m-0 default-primary">Personal Information<a class=" hvr-icon-push float-right" onClick="window.location.href = '/enduser/home';" >{{ __('Back') }}<i class="far fa-caret-square-left hvr-icon"></i></a></h6>
                  @endif
                </div>
                <div class="card-body px-3">
                  <div class="row px-3">
                    <div class="col-md-6 mb-3">
                      <label for="fullname">Full Name</label>
                      <input readonly type="text"  tabindex="-1" class="form-control form-control-sm  @error('fullname') is-invalid @enderror" id="fullname"name="fullname" placeholder="Full Name" value="{{ $data->member->long_name }}"  autocomplete="fullname">
                      <div class="invalid-feedback">
                        Full Name is required.
                      </div>
                    </div>
                    <div class="col-md-6 mb-3">
                      <label for="contact_no">Contact Number</label>
                      <div class="input-group input-group-sm">
                        <div style="border: dimgrey !important;" class="input-group-prepend">
                          <span class="input-group-text">+63</span>
                        </div>
                        <input type="text" tabindex="-1"  class="form-control form-control-sm @error('contact_no') is-invalid @enderror" id="contact_no" placeholder="10-digit Mobile Number"  minlength="0" maxlength="10" value="{{ $data->contact_no }}" readonly onkeydown="return ( event.ctrlKey || event.altKey 
                    || (47<event.keyCode && event.keyCode<58 && event.shiftKey==false) 
                    || (95<event.keyCode && event.keyCode<106)
                    || (event.keyCode==8) || (event.keyCode==9) 
                    || (event.keyCode>34 && event.keyCode<40) 
                    || (event.keyCode==46))" name="contact_no" style="border-radius: 0;" autocomplete="contact_no"  >
                        <div class="invalid-feedback" style="width: 100%;">
                          Contact Number is required.
                        </div>
                      </div>
                    </div>
                  </div> <!-- Long Name -->
                  <div class="row px-3">
                    <div class="col-md-6 mb-3">
                      <label for="birthdate">Birthday</label>
                          <input readonly type="date" class="form-control form-control-sm  @error('birthdate') is-invalid @enderror" id="birthdate" tabindex="-1" required name="birthdate"  value="{{ $data->birthdate }}"  autocomplete="birthdate">
                          <div class="invalid-feedback">
                            Brithday is required.
                          </div>
                    </div>
                    <div class="col-md-6 mb-3">
                      <label for="gender">Gender</label>
                      <select disabled  tabindex="-1"  class="form-control form-control-sm d-block w-100" id="gender1"  type="text" name="gender1" autocomplete="gender1" >
                        <option value="M" @if($data->member->gender == "M") selected @endif>Male</option>
                        <option value="F" @if($data->member->gender == "F") selected @endif>Female</option>
                        <option value="1" @if($data->member->gender == "1") selected @endif}}>Rather not say</option>
                      </select>
                      <div class="invalid-feedback">
                        Please select your gender preference.
                      </div>
                    </div>
                  </div> <!-- Birthdate, Age and Gender -->
                  <div class="row px-3">
                    
          
                    <div class="col-md-6 mb-3 mt-0">
                      <label  for="gmail_address">Email Address</label>
                        <input tabindex="-1"  type="email" class="form-control-sm form-control @error('email') is-invalid @enderror" id="email" value="{{$data->user->email}}"  readonly name="email" autocomplete="email">
                        <div class="invalid-feedback" >
                          Your Email Address is required.
                        </div>
                    </div>
                    <div class="col-md-6 mb-3">
                      <label for="gmail_address">Facebook Link</label>
                            <i class="fa-brands fa-facebook-messenger"></i>
                      <input tabindex="-1"  type="text"  class="form-control form-control-sm @error('fb_link') is-invalid @enderror" id="fb_link" readonly value="{{$data->fb_link}}" name="fb_link" autocomplete="fb_link">
                      <div class="invalid-feedback">
                        This field is required.
                      </div>
                      </div>  
                  </div> <!-- Contact Information -->
                    <div class="row px-3">
                      
                      
                   </div>
                  <div class="row px-3">
                    <div class="col-md-12 mb-3">
                      
                    </div>
                  </div>
                </div>
              </div>
      
              <div class="card mt-4 mx-3 shadow-lg border-0">
                <div class="card-header bg-light text-primary pt-2 pl-3 border-0">
                  <h6 class="m-0 default-primary">Present Address</h6>
                </div>
                <div class="card-body">
                  <div class="row px-3">
                    <div class="col-md-4 mb-3">
                      <label for="region_code">Region</label>
                      <select tabindex="-1"  class="form-control form-control-sm @error('region_id') is-invalid @enderror" id="region_id" disabled type="text" name="region_id" >
                        <option value="">Select Region</option>
                         @foreach($regions as $item)
                         <option value="{{ $data->region_id }}" {{ $data->region_id == $item->id ? 'selected="selected"' : '' }}>{{ $item->name }}</option> 
                        @endforeach
                      </select>
                      <div class="invalid-feedback">
                        Region is required.
                      </div>
                    </div>
                    <div class="col-md-4 mb-3">
                      <label for="province_id">Province</label>
                      <select  tabindex="-1" disabled class="form-control  form-control-sm" id="province_id"   name="province_id" autocomplete="province_id" >
                        <option value="">Select Province</option>
                      </select>
                      <input type="text" value="{{$data->province_id}}" id="hidden_province_id" hidden>
                      <div class="invalid-feedback">
                        Province is required.
                      </div>
                    </div>
                    <div class="col-md-4 mb-3">
                      <label for="city_id">City/Municipality</label>
                      <select   tabindex="-1" class="form-control  form-control-sm" id="city_id"   name="city_id" autocomplete="city_id" disabled>
                      </select>
                      <input type="text" value="{{$data->city_id}}" id="hidden_city_id" hidden>
                      <div class="invalid-feedback">
                        City/Municipality is required.
                      </div>
                    </div>
                  </div> <!-- Region, Province, Municiaplity -->
                  <div class="row px-3">
                    <div class="col-md-4 mb-3">
                      <label for="barangay_id">Barangay</label>
                      <select  tabindex="-1" disabled class="form-control form-control-sm"  id="barangay_id"  type="text" name="barangay_id" autocomplete="barangay_id" >
                      </select>
                      <input type="text" value="{{$data->barangay_id}}" id="hidden_barangay_id" hidden>
                      <div class="invalid-feedback">
                        Barangay is required.
                      </div>
                    </div>
                    <div class="col-md-8 mb-3">
                      <label for="street">Bldg. No./Street/Apartment No.</label>
                      <input  tabindex="-1" type="text" class="form-control form-control-sm @error('street1') is-invalid @enderror" id="street1" value="{{$data->street}}" placeholder="Bldg. No./Street/Apartment No." name="street1" disabled>
                    </div>
                  </div> <!-- Barangay, Street -->
                </div>
              </div>
              @endforeach

              <div class="card mx-3 shadow-lg border-0">
                <div class="card-header bg-light text-primary py-2 pl-3 border-0">
                  <h6 class="m-0 default-primary">Loan Details</h6>
                </div>
                <div class="card-body px-3">
                  <div class="row px-3">
                    <div class="col-md-4 mb-3"> 
                      <label for="share_capital">Share Capital<span class="text-danger"> *</span>
                        </label>
                        <input type="number" class="form-control form-control-sm @error('share_capital') is-invalid @enderror" onchange="(function(el){el.value=parseFloat(el.value).toFixed(2);})(this)"
                     oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" value="{{ old('share_capital') }}" maxlength="10" min="0.00"  step="0.05" id="share_capital" name="share_capital">
                    </div>
                    <div class="col-md-4 mb-3"> 
                      <label for="share_capital">Co-Maker<span class="text-muted"> (Optional)</span>
                        </label>
                        <input type="text" oninput="this.value = this.value.toUpperCase()"  class="form-control form-control-sm @error('share_capital') is-invalid @enderror"  id="co_maker" name="co_maker" placeholder="Member Number" >
                        <p id="result"></p>
                    </div>
                    <div class="col-md-4 mb-3"> 
                      <label for="share_capital">Branch to Loan in
                        <span class="text-danger"> *</span></label>
                        <select class="custom-select-sm custom-select  " required  id="branch_id1" name="branch_id1">
                          @if ($branches->count())
                          <option value="" >Select Branch</option> 
                          @foreach($branches as $data)
                          <option value="{{ $data->id }}"
                            @if($branch_iddef === $data->id) selected='true' @endif >{{ $data->name }}</option> 
                          @endforeach   
                          @endif
                          </select>
                    </div>
                  </div>

                  <div class="row px-3">
                    <div id="gross" class="col-md-6 mb-3">
                      <label  >How much is your monthly gross income? <span class="text-danger"> *</span></label>
                      <input type="number" class="form-control form-control-sm @error('montly_income') is-invalid @enderror" onchange="(function(el){el.value=parseFloat(el.value).toFixed(2);})(this)"
                     oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"
                         maxlength="10" min="0.00"  step="0.05"   value="{{ old('montly_income') }}"   id="montly_income"  name="montly_income">
                    </div>
                    <div id='expenses' class="col-md-6 mb-3">
                      <label >How much is your monthly expenses?<span class="text-danger"> *</span></label>
                      <input type="number" class="form-control form-control-sm @error('montly_expense') is-invalid @enderror" onchange="(function(el){el.value=parseFloat(el.value).toFixed(2);})(this)"
                     oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"
                          maxlength="10" min="0.00"  step="0.05"  value="{{ old('montly_expense') }}"   id="montly_expense"  name="montly_expense">
                    </div>
                  </div>

                  <div class="row px-3">

                    <div id="product" class="col-md-6 mb-3">
                      <label>Loan Product <span class="text-danger"> *</span></label>
                      <select class=" custom-select-sm custom-select @error('loan_product') is-invalid @enderror" id="loan_product"  type="text" name="loan_product"   autocomplete="loan_product" >
                        <option value="" selected disabled>Select Loan Product</option>
                        @foreach($loanType as $item)
                        <option  value="{{$item->id}}" >{{$item->name}}</option>
                        @endforeach
                      </select>
                    </div>
                    <div hidden id="savings-salary-div" class="col-md-4 mb-3">
                      <label id="savings-salary-label"><span class="text-danger"> *</span></label>
                      <input type="number" class="form-control form-control-sm" onchange="(function(el){el.value=parseFloat(el.value).toFixed(2);})(this)"
                     oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"
                         maxlength="10" min="0.00"  step="0.05" name="savings-salary-amount" id="savings-salary-amount">
                    </div>
                    <div id="term_div" class="col-md-6 mb-3">
                      <label  >Loan Payment Terms<span class="text-danger"> *</span></label>
                      <select  class=" custom-select-sm custom-select @error('payment_terms') is-invalid @enderror" id="payment_terms"  type="text" name="payment_terms"   autocomplete="payment_terms" >
                        <option value="" selected disabled>Select</option>

                        <!-- <option value='0' @if (old('payment_terms') == '0') selected="selected" @endif>1 Month</option>
                        <option value='1' @if (old('payment_terms') == '1') selected="selected" @endif>3 Months</option>
                        <option value='2' @if (old('payment_terms') == '2') selected="selected" @endif>6 Months</option>
                        <option value='3' @if (old('payment_terms') == '3') selected="selected" @endif>9 Months</option>
                        <option value='4' @if (old('payment_terms') == '4') selected="selected" @endif>12 Months</option> -->
                      </select>
                    </div>
                  </div>

                  <div class="row px-3">
                    <div id="mode_div" class="col-md-6 mb-3">
                      <label>Loan Payment Mode <span class="text-danger"> *</span></label>
                      <select  class=" custom-select-sm custom-select @error('payment_mode') is-invalid @enderror" id="payment_mode"  type="text" name="payment_mode"   autocomplete="payment_mode" >
                        <option value="" selected disabled>Select</option>
                      </select>
                    </div>
                    <div id="amount" class="col-md-6 mb-3">  
                      <label for="amount_needed">Amount Applied
                      <span class="text-danger"> *</span></label>
                     <input type="number" class="form-control form-control-sm @error('amount_needed') is-invalid @enderror" onchange="(function(el){el.value=parseFloat(el.value).toFixed(2);})(this)"
                     oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" value="{{ old('amount_needed') }}" maxlength="10" min="0.00"  step="0.05" id="amount_needed" name="amount_needed">
                    <!-- <p id="loanable_amountLabel"></p> -->
                    </div>
                  </div>

                  <div class="row px-3">
                    <div id="income" class="col-md-6 mb-3">
                      <label >Source of Income<span class="text-danger"> *</span></label>
                      <select class="custom-select-sm custom-select @error('occupation_id') is-invalid @enderror"   id="occupation_id"  type="text" name="occupation_id"   autocomplete="occupation_id" >
                        <option value="" selected disabled>Select</option>
                         @foreach($occupation as $item)
                        <option  value="{{$item->id}}" @if (old('occupation_id') == '{{$item->id}}') selected="selected" @endif>{{$item->name}}</option>
                        @endforeach
                        <option value="0">Other Occupation</option>
                      </select>
                    </div>

                    <div id='purpose' class="col-md-6 mb-3">
                      <label >Purpose of Loan<span class="text-danger"> *</span></label>
                      <select  class=" custom-select-sm custom-select @error('loan_purpose') is-invalid @enderror" id="loan_purpose"  type="text" name="loan_purpose"   autocomplete="loan_purpose" >
                        <option value="" selected disabled>Select</option>
                        <option value='0' @if (old('loan_purpose') == '0') selected="selected" @endif>Business-Related Expense</option>
                        <option value='1' @if (old('loan_purpose') == '1') selected="selected" @endif>Debt Consolidation</option>
                        <option value='2' @if (old('loan_purpose') == '2') selected="selected" @endif>Education  / Tuition fees</option>
                        <option value='3' @if (old('loan_purpose') == '3') selected="selected" @endif>Health / Medical fees</option>
                        <option value='4' @if (old('loan_purpose') == '4') selected="selected" @endif>Home Renovation / Improvements</option>
                        <option value='5' @if (old('loan_purpose') == '5') selected="selected" @endif>Life Events / Celebration</option>
                        <option value='6' @if (old('loan_purpose') == '6') selected="selected" @endif>Personal Consumption</option>
                        <option value='7' @if (old('loan_purpose') == '7') selected="selected" @endif>Purchase of Appliance / Gadget / Furniture</option>
                        <option value='8' @if (old('loan_purpose') == '8') selected="selected" @endif>Start-up Business</option>
                        <option value='9' @if (old('loan_purpose') == '9') selected="selected" @endif>Vacation / Travel Expense</option>
                        <option value='10' @if (old('loan_purpose') == '10') selected="selected" @endif>Vehicle Repair / Purchase</option>
                        <option value='11' @if (old('loan_purpose') == '11') selected="selected" @endif>Other Purposes</option>
                      </select>
                    </div>

                  </div>
                  <div class="row px-3">
                    <div  class="col-md-6 mb-3">
                      <label hidden id="other_occupation_label">Other Occupation</label>
                      <input hidden class="form-control form-control-sm @error('other_occupation') is-invalid @enderror" type="text" name="other_occupation" id="other_occupation">
                    </div>
                    <div hidden id="other_purpose_label" class="col-md-6 mb-3">
                      <label>Other Loan Purpose</label>
                      <input type="text" class="form-control form-control-sm @error('share_capital') is-invalid @enderror" name="other_purpose" id="other_purpose">
                    </div>
                  </div>
                </div>
              </div>
              <div class="card mt-4 mx-3 shadow-lg border-0">
                <div class="card-header bg-light text-primary py-2 pl-3 border-0">
                  <h6 class="m-0 default-primary">Document Upload<span class="text-danger"> *</span></h6><p class="text-muted" id="uploadtext"></p>
                </div>
                <div id="document" class="card-body">
                  <div class="row px-3">
                     <div  class="col-md ">
                      <label >Government Issued ID: </label>
                      <p class="text-muted"> Government Issued ID (At least 1 valid government ID like Passport, GSIS, UMID, PRC and others)</p>
                     </div>
                  </div>
                  <div class="row px-3">
                     <div id="id_type_div" style="padding-top: 8px;" class="col-md-4 mb-3">
                      <select  class=" custom-select-sm custom-select @error('id_type') is-invalid @enderror" id="id_type"  type="text" name="id_type"   autocomplete="id_type" >
                        <option value="" selected disabled>Select ID</option>
                        <option value='UMID' >UMID</option>
                        <option value='GSIS'>GSIS</option>
                        <option value='Passport'>Passport</option>
                        <option value='PRC'>PRC</option>
                        <option value='Other'>Other</option>
                      </select>
                      </div>

                     <div id="id_other_div" style="padding-top: 8px;" hidden class="col-md-4 mb-3">
                      <input type="text" class='form-control form-control-sm' name="other_id_type" id="other_id_type">
                     </div>

                     <div id="id_front_div" class="col-md-4">
                      <!-- <input type="file" accept="image/*" id="frontID"class="form-control"  name="frontID"/> -->
                      <input class=" @error('attachment[]') is-invalid @enderror"  id="f01"  type="file" accept="image/*" placeholder="" name="attachment[]"  />
                      <label style="color: #fff" for="f01">ID Front Side</label>
                     </div>
                     <div id="id_back_div" class="col-md-4">
                      <input class=" @error('attachment[]') is-invalid @enderror" id="f02" type="file" accept="image/*" placeholder="" name="attachment[]"  />
                      <label style="color: #fff" for="f02">ID Back Side</label>
                     </div>
                      <div class="invalid-feedback">
                        This field is required.
                     </div>
                  </div>
                   <div class="row px-3">
                     <div class="col-md-3 ">
                      <label >Statement of Account: </label>
                     </div>
                     <div  class="col-md-3" >
                      <input class=" @error('attachment[]') is-invalid @enderror" id="f03" title="Any of the following: SOA, Ledger" type="file" accept="image/*" placeholder="" name="attachment[]"  />
                      <label style="color: #fff" title="Any of the following: SOA, Ledger" for="f03">1st</label>
                     </div>
                     <div  class="col-md-3" >
                      <input class=" @error('attachment[]') is-invalid @enderror" id="f04" title="Any of the following: SOA, Ledger" type="file" accept="image/*" placeholder="" name="attachment[]"  />
                      <label style="color: #fff" title="Any of the following: SOA, Ledger" for="f04">2nd</label>
                     </div>
                     <div  class="col-md-3" >
                      <input class=" @error('attachment[]') is-invalid @enderror" id="f05" type="file" accept="image/*" placeholder="" name="attachment[]" title="Any of the following: SOA, Ledger"  />
                      <label style="color: #fff" title="Any of the following: SOA, Ledger" for="f05">3rd</label>
                      </div>

                      <div class="invalid-feedback">
                        This field is required.
                      </div>
                   </div>
                    <div class="row px-3">
                      <div  class="col-md-3 ">
                      <label >Proof of Billing Address</label>
                     </div>
                      <div  class="col-md-3 " >
                      <input class=" @error('attachment[]') is-invalid @enderror" id="f06" type="file" accept="image/*" placeholder="" name="attachment[]"  />
                      <label title="Any of the following: Company Payslip, Billing Statement" style="color: #fff" for="f06">1st</label>
                      </div>
                      <div  class="col-md-3 " >
                      <input class=" @error('attachment[]') is-invalid @enderror" id="f07" type="file" accept="image/*" placeholder="" name="attachment[]"  />
                      <label title="Any of the following: Company Payslip, Billing Statement" style="color: #fff" for="f07">2nd</label>
                      </div>
                      <div  class="col-md-3 " >
                      <input class=" @error('attachment[]') is-invalid @enderror" id="f08" type="file" accept="image/*" placeholder="" name="attachment[]"  />
                      <label title="Any of the following: Company Payslip, Billing Statement" style="color: #fff" for="f08">3rd</label>
                      </div>
                      <div class="invalid-feedback">
                        This field is required.
                      </div>
                    </div>
                    <div class="row px-3 mb-3">
                      <div class="col-md-4 mb-3">
                          <label>Signature:</label>
                          <a id="upload-sig-btn" class="btn btn-danger btn-sm" type="button"> Upload Signature</a>
                          <a  hidden id="manual-sig-btn" class="btn btn-danger btn-sm" type="button"> Manual Signature</a>
                          <input hidden type="text" id="manualOrUpload" value="manual">
                      </div>
                        <div  id="manual-sig" >
                            <canvas style="background-color: lightgrey;" id="signature"></canvas>
                            <input type="hidden" id="hidden_sig" name="hidden_sig">
                            <a id="clear" class="btn btn-danger btn-sm" type="button"> Clear</a>
                        </div>
                        <div hidden id="upload-sig" class="col-md-8 mb-3">
                        <input disabled id="f09" type="file" accept="image/*" placeholder="" name="attachment[]"  />
                        <label title="Upload signature" id="labelf09" style="color: #fff" for="f09"> Upload signature</label>
                        </div>
                    </div>
                </div>
              </div>
              <div class="card mt-4 mx-3 shadow-lg border-0">
                <div class="card-header bg-light text-primary py-2 pl-3 border-0">
                  <h6 class="m-0 default-primary">Please answer the survey to improve our services.</h6>
                </div>
                <div class="card-body">
                  <div class="row px-3">
                      <label for="marketing_channel">Where did you hear about us?</label>
                      <select class="custom-select-sm custom-select @error('marketing_channel_id') is-invalid @enderror"   id="marketing_channel_id"  type="text"   name="marketing_channel_id" autocomplete="marketing_channel" >
                        <option value="" selected disabled>Select</option>
                         @foreach($marketingChannel as $item)
                        <option  value="{{$item->id}}" @if (old('marketing_channel_id') == '{{$item->id}}') selected="selected" @endif>{{$item->name}}</option>
                        @endforeach
                      </select>
                      <div class="invalid-feedback">
                        This field is required.
                      </div>
                  </div> 
                </div>
              </div>
      
              <button class="btn btn-secondary mt-5 mb-5 btn-block"  style="background-color: rgb(7,17,174);" type="button" id="btn-save-loan"  >Submit</button>
            </form>
          </div>
      </div>
  </div>
  </div>

  
@endsection

@section('preloan')

<script src="https://cdn.jsdelivr.net/npm/signature_pad@2.3.2/dist/signature_pad.min.js"></script>
<script>
  const swalWithBootstrapButtons = Swal.mixin({
        customClass: {
          confirmButton: 'btn btn-success',
          cancelButton: 'btn btn-danger'
        },
        buttonsStyling: false
      }) 
  var canvas = document.getElementById ("signature");
  var signaturePad = new SignaturePad(canvas);
  $('#clear').click(function (e) {
            e.preventDefault();
            signaturePad.clear();
            $("#signature").val('');
        });
  function validateForm() {
    // alert(branch_id);
  
    var share_capital = $("#share_capital").val();
    var loan_product = $("#loan_product").val();
    var savings_salary_amount = $("#savings-salary-amount").val();
    var amount_needed = $("#amount_needed").val();
    var branch_id = $("#branch_id1").val();
    var loan_purpose = $("#loan_purpose").val();
    var other_purpose = $("#other_purpose").val();
    var payment_terms = $("#payment_terms").val();
    var payment_mode = $("#payment_mode").val();
    var occupation_id = $("#occupation_id").val();
    var montly_income = $("#montly_income").val();
    var montly_expense = $("#montly_expense").val();
    var id_type = $("#id_type").val();
    var other_id_type = $("#other_id_type").val();
    var other_occupation = $("#other_occupation").val();
      var less_loan = $('#less_loan').val();

    var f01 = $("#f01").get(0).files.length;
    var f02 = $("#f02").get(0).files.length;
    var f03 = $("#f03").get(0).files.length;
    var f04 = $("#f04").get(0).files.length;
    var f05 = $("#f05").get(0).files.length;
    var f06 = $("#f06").get(0).files.length;
    var f07 = $("#f07").get(0).files.length;
    var f08 = $("#f08").get(0).files.length;
    var f09 = $("#f09").get(0).files.length;

    var f01size = f01 == 1 ? document.getElementById('f01').files[0].size : 0;
    var f02size = f02 == 1 ? document.getElementById('f02').files[0].size : 0;
    var f03size = f03 == 1 ? document.getElementById('f03').files[0].size : 0;
    var f04size = f04 == 1 ? document.getElementById('f04').files[0].size : 0;
    var f05size = f05 == 1 ? document.getElementById('f05').files[0].size : 0;
    var f06size = f06 == 1 ? document.getElementById('f06').files[0].size : 0;
    var f07size = f07 == 1 ? document.getElementById('f07').files[0].size : 0;
    var f08size = f08 == 1 ? document.getElementById('f08').files[0].size : 0;
    var f09size = f09 == 1 ? document.getElementById('f09').files[0].size : 0;
    
    var manualOrUpload = $("#manualOrUpload").val();

    if(manualOrUpload === 'manual'){
      var signatureType = signaturePad.isEmpty();
    }else{
      var manualIfTrue = f09 == 0 ? true : false;
      var signatureType = manualIfTrue;
    }
    if(id_type == 'Other'){
      var other_id_typeIfTrue = other_id_type == '' ? true : false;
    }
    switch(loan_product){
            case '4': // asset
              var loanProdPrompt = '<br>Time Deposit must be filled<br>';
              var loanProdPromptIfTrue = savings_salary_amount == '' ? loanProdPrompt : '';
              break;
            case '5': // asset
              var loanProdPrompt = '<br>Wealth Building Program must be filled<br>';
              var loanProdPromptIfTrue = savings_salary_amount == '' ? loanProdPrompt : '';
              break;
            case '6': // asset
              var loanProdPrompt = '<br>Lockin Savings must be filled<br>';
              var loanProdPromptIfTrue = savings_salary_amount == '' ? loanProdPrompt : '';
              break;
            case '18':
            case '19':
            case '24':
              var loanProdPrompt = '<br>Salary must be filled<br>';
              var loanProdPromptIfTrue = savings_salary_amount == '' ? loanProdPrompt : '';
              break;
            case '20':
            case '25':
              var loanProdPrompt = '<br>Mid-Year must be filled<br>';
              var loanProdPromptIfTrue = savings_salary_amount == '' ? loanProdPrompt : '';
              break;
            case '21':
            case '26':
              var loanProdPrompt = '<br>Year-End must be filled<br>';
              var loanProdPromptIfTrue = savings_salary_amount == '' ? loanProdPrompt : '';
              break;
            case '22':
              var loanProdPrompt = '<br>Performance Evaluation Incentive must be filled<br>';
              var loanProdPromptIfTrue = savings_salary_amount == '' ? loanProdPrompt : '';
              break;
            case '23':
              var loanProdPrompt = '<br>Performance Based Bonus must be filled<br>';
              var loanProdPromptIfTrue = savings_salary_amount == '' ? loanProdPrompt : '';
              break;
            // case '27':
            //   var result = vehiceLoan(montly_income, montly_expense, data);
            //   if(checkLoanableAmmount(amount_needed, result)){
            //     checkVehicleLoan(parseInt(amount_needed))
            //   }
            //   break;
            default:
              var loanProdPromptIfTrue = '';
              break;             
          }
    if(occupation_id == 0){
      var occupationIfTrue = other_occupation == '' ? '<br>Other occupation must be filled<br>' : '';
    }else{
      var occupationIfTrue = ''
    }
    if(loan_purpose == 11){
      var loan_purposeIfTrue = other_purpose == '' ? '<br>Other purpose must be filled<br>' : '';
    }else{
      var loan_purposeIfTrue = ''
    }
    var fiveMil = 5000001;
    // alert(other_id_typeIfTrue);
    var valmsg =  
        (share_capital === '' ? "<br>Share capital must be filled out<br>" : "") 
      + (loan_product === null ? "<br>Loan product for must be filled out<br>" : "") 
      + (loanProdPromptIfTrue) 
      + (amount_needed === '' ? "<br>Amount applied must be filled out<br>" : "") 
      + (branch_id === '' ? "<br>Branch to loan in must be filled out<br>" : "") 
      + (payment_terms === null ? "<br>Loan payment terms must be filled out<br>" : "")
      + (payment_mode == null ? "<br>Loan payment mode must be filled out<br>" : "")
      + (montly_income === '' ? "<br>Monthly gross income must be filled out<br>" : "")
      + (montly_expense === '' ? "<br>Monthly expenses must be filled out<br>" : "") 
      + (loan_purpose === null ? "<br>Loan purpose must be filled out<br>" : "") 
      + (loan_purposeIfTrue)
      + (occupation_id == null ? "<br>Source of income must be filled out<br>" : "")
      + (occupationIfTrue)
      + (id_type == null ? "<br>ID type must be filled out<br>" : "")
      + (other_id_typeIfTrue ? "<br>Please specify what type of ID<br>" : "")
      + (f01 == 0 ? "<br>Front side ID must be filled out<br>" : "")
      + (f02 == 0 ? "<br>Back side ID must be filled out<br>" : "")
      + (f03 == 0 ? "<br>Statement of account 1st must be filled out<br>" : "")
      + (f04 == 0 ? "<br>Statement of account 2nd must be filled out<br>" : "")
      + (f05 == 0 ? "<br>Statement of account 3rd must be filled out<br>" : "")
      + (f06 == 0 ? "<br>Proof of Billing Address 1st must be filled out<br>" : "")
      + (f07 == 0 ? "<br>Proof of Billing Address 2nd must be filled out<br>" : "")
      + (f08 == 0 ? "<br>Proof of Billing Address 3rd must be filled out<br>" : "")

      + (f01size > fiveMil ? "<br>Front side ID size must be less than 5mb(5000kb)<br>" : "")
      + (f02size > fiveMil ? "<br>Back side ID size must be less than 5mb(5000kb)<br>" : "")
      + (f03size > fiveMil ? "<br>Statement of account 1st size must be less than 5mb(5000kb)<br>" : "")
      + (f04size > fiveMil ? "<br>Statement of account 2nd size must be less than 5mb(5000kb)<br>" : "")
      + (f05size > fiveMil ? "<br>Statement of account 3rd size must be less than 5mb(5000kb)<br>" : "")
      + (f06size > fiveMil ? "<br>Proof of Billing Address 1st size must be less than 5mb(5000kb)<br>" : "")
      + (f07size > fiveMil ? "<br>Proof of Billing Address 2nd size must be less than 5mb(5000kb)<br>" : "")
      + (f08size > fiveMil ? "<br>Proof of Billing Address 3rd size must be less than 5mb(5000kb)<br>" : "")
      + (f09size > fiveMil ? "<br>Signiture size must be less than 5mb(5000kb)<br>" : "")

      + (less_loan != '' ? "<br>Your loan amount must be less than "+less_loan+"<br>" : "")

      + (signatureType ? "<br>Signature must be filled out<br>" : "");

      if (share_capital === '' ||
       loan_product === null || 
       loanProdPromptIfTrue ||
       amount_needed === '' ||  
       branch_id === ''  ||  
       payment_terms === null ||
       payment_mode === null||
       montly_income === '' ||
       montly_expense === '' ||
       loan_purpose === null|| 
       id_type === null||  
       f01 === 0||  
       f02 === 0||  
       f03 === 0||  
       f04 === 0||  
       f05 === 0||  
       f06 === 0||  
       f07 === 0||  
       f08 === 0||
       occupationIfTrue || 
       loan_purposeIfTrue ||
       signaturePad.isEmpty() ||
       less_loan != '' ||
       occupation_id === null   
       ) {
      // alert(fullname);
        swal.fire({
          icon: 'warning',
          html:valmsg
        });
        return false;
      }else{
        return true;
      }
  }

  $('form #btn-save-loan').click(function (event) {
  
    var share_capital = $("#share_capital").val();
    var loan_product = $("#loan_product").val();
    var amount_needed = $("#amount_needed").val();
    var loanable_amount = $("#loanable_amount").val();
    var branch_id = $("#branch_id1").val();
    var loan_purpose = $("#loan_purpose").val();
    var payment_terms = $("#payment_terms").val();
    var payment_mode = $("#payment_mode").val();
    var occupation_id = $("#occupation_id").val();
    var montly_income = $("#montly_income").val();
    var montly_expense = $("#montly_expense").val();
    var data = signaturePad.toDataURL('image/png');
    var hidden_sig = $('#hidden_sig').val(data);
    var isNeg = loanable_amount - amount_needed;
  let $form = $(this).closest('form');
  var sitefeedback = $('#sitefeedback');
  // alert(checkLessLoan());
    if(validateForm())
    {

       if(isNeg > 0)
       {
        // alert(loanable_amount-amount_needed);
                     // alert(canLoan(loanable_amount,amount_needed)+'can loan '+isNeg);
          swalWithBootstrapButtons.fire
                ({
                title: 'Magandang Buhay!',
                icon: 'question',
                text:" Thank you for using our website, Kamay-ari! We would like to know how we could improve our website through this survey. This will take only a few minutes.",
                showCancelButton: true,
                confirmButtonText: 'Yes',
                cancelButtonText: 'No',
                }).then((result) => {
                  if (result.isConfirmed) {
                      sitefeedback.val('yes');
                    $form.submit();
                    // event.preventDefault();
                  }else if (
                      /* Read more about handling dismissals below */
                      result.dismiss === Swal.DismissReason.cancel
                    ) {
                       
                      sitefeedback.val('no');
                      $form.submit();
                      }
                })
       }else
       {
                     // alert('cannot loan '+loanable_amount+','+amount_needed);
          swal.fire('Your amount applied is greater than your loanable amount, either adjust your amount applied or other details(terms, monthly income, monthly expense, salary, etc.) in form.');
          $("#btn-save-loan").html('Submit');
          $("#btn-save-loan"). attr("disabled", false);
       }
    }else{
                     // alert(loanable_amount);
      $("#btn-save-loan").html('Submit');
      $("#btn-save-loan"). attr("disabled", false);
    }
  });
$(document).ready(function(){
    function removeOptions(selectElement) {
       var i, L = selectElement.options.length - 1;
       for(i = L; i >= 0; i--) {
          selectElement.remove(i);
       }
       selectElement[selectElement.length] = new Option('Select ', '');
    }
    function assetLoan (montly_income,montly_expense,payment_terms){
      var answer = ((montly_income-montly_expense)*.50*payment_terms)/1+(.02*payment_terms)+0.03;
       $('#loanable_amount').val('');
      $('#loanable_amount').val(answer);
      return answer;
      // alert(formula);
    }
    function multiPLoan(montly_income,montly_expense,payment_terms){
      var answer = ((montly_income-montly_expense)*.35*payment_terms)/(1+(.02 *payment_terms)+0.03+.01);
       $('#loanable_amount').val('');
      $('#loanable_amount').val(answer);
      return answer;
    }
    function regLoan(montly_income,montly_expense,payment_terms){
      var answer = ((montly_income-montly_expense)*.3*payment_terms)/(1+(.01*payment_terms));
       $('#loanable_amount').val('');
      $('#loanable_amount').val(answer);
      return answer;
    }
    function vehiceLoan(montly_income,montly_expense,payment_terms){
      // var ndi = ;
      var answer = ((montly_income-montly_expense)*.45*payment_terms)/(1+(.01*payment_terms)+.03);
       $('#loanable_amount').val('');
      $('#loanable_amount').val(answer);
      return answer;
    }
    function houseLoanB(montly_income,montly_expense,payment_terms){
      var answer = ((montly_income-montly_expense)*.5*payment_terms)/(1+(.01*payment_terms)+.03+.01);
       $('#loanable_amount').val('');
      $('#loanable_amount').val(answer);
      return answer;
      // alert(answer);
    }
    // 5,612,000/3.48
    function houseLoanA(montly_income,montly_expense,payment_terms){
      var answer = ((montly_income-montly_expense)*.5*payment_terms)/(1+(.02*payment_terms)+.03);
       $('#loanable_amount').val('');
      $('#loanable_amount').val(answer);
      return answer;
    }
    function businessLoan(montly_income,montly_expense,payment_terms){
      var answer = ((montly_income-montly_expense)*.35*payment_terms)/(1+(.02*payment_terms)+.03);
       $('#loanable_amount').val('');
      $('#loanable_amount').val(answer);
      return answer;
    }
    function salaryLoanA(salary,payment_terms){
      var answer = (salary*.3*payment_terms)/(1+(.02*payment_terms)+.03);
       $('#loanable_amount').val('');
      $('#loanable_amount').val(answer);
      return answer;
    }
    function salaryLoanB(salary,payment_terms){
      var answer = (salary*.45*payment_terms)/(1+(.02*payment_terms)+.03);
       $('#loanable_amount').val('');
      $('#loanable_amount').val(answer);
      return answer;
    }
    function salaryLoanBBonus(payment_terms, bonus){
      var answer = bonus/(1+(.02*payment_terms));
       $('#loanable_amount').val('');
      $('#loanable_amount').val(answer);
      return answer;
    }
    function salaryLoanC(salary,payment_terms){
      var answer = (salary*.60*payment_terms)/(1+(.05*payment_terms)+.03+.01);
       $('#loanable_amount').val('');
      $('#loanable_amount').val(answer);
      return answer;
    }
    function salaryLoanCBonus(payment_terms, bonus){
      var answer = bonus/(1+(.05*payment_terms));
       $('#loanable_amount').val('');
      $('#loanable_amount').val(answer);
      return answer;
    }
    function b2bLoan(savings, payment_terms){
      var answer = (savings/(1+(payment_terms*.01)));
       $('#loanable_amount').val('');
      $('#loanable_amount').val(answer);
      return answer;
    }

    function checkHousingLoanA(ammount_needed){
      var value = '';
      if(ammount_needed < 100000){
        swalFire('Loan Amount must be greater than 100k for Housing Loan A');
      }else if(ammount_needed > 300000){
        swalFire('Loan Amount must be less than 300k for Housing Loan A');
        value = '300k';
      }
      return value;
    }
    function checkHousingLoanB(ammount_needed){
      var value = '';
      if(ammount_needed > 5000000){
        swalFire('Loan Amount must be less than 5m for Housing Loan B');
        value = '5m';
      }
      return value;
    }
    function checkAssetLoan(ammount_needed){
      var value = '';
      if(ammount_needed < 100000){
        swalFire('Loan Amount must be greater than 100k for MyAsset Loan');
      }else if(ammount_needed > 3000000){
        swalFire('Loan Amount must be less than 3m for MyAsset Loan');
        value = '3m';
      }
      return value;
    }
    function checkSalaryLoanAB(ammount_needed){
      var value = '';
      if(ammount_needed < 20000){
        swalFire('Loan Amount must be greater than 20k for Salary Loan A/B');
      }else if(ammount_needed > 100000){
        swalFire('Loan Amount must be less than 100k for Salary Loan A/B');
        value = '100k';
      }
      return value;
    }
    function checkSalaryLoanC(ammount_needed){
      var value = '';
      if(ammount_needed > 200000){
        swalFire('Loan Amount must be less than 200k for Salary Loan C');
        value = '200k';
      }
      return value;
    }
    function checkBusinessLoan(ammount_needed){
      var value = '';
      if(ammount_needed < 50000){
        swalFire('Loan Amount must be greater than 50k for Business Loan');
      }else if(ammount_needed > 5000000){
        swalFire('Loan Amount must be less than 5m for Business Loan');
        value = '5m';
      }
      return value;
    }
    function checkVehicleLoan(ammount_needed){
      var value = '';
      if(ammount_needed > 3000000){
        swalFire('Loan Amount must be less than 3m for Vehicle Loan');
        value = '3m';
      }
      return value;
    }
    function checkMultiPLoan(ammount_needed){
      var value = '';
      if(ammount_needed > 5000000){
        swalFire('Loan Amount must be less than 5m for Multi-Purpose Loan');
        value = '5m';
      }
      return value;
    }
    function checkRegularLoan(ammount_needed){
      var value = '';
      if(ammount_needed < 50000){
        swalFire('Loan Amount must be greater than 50k for Regular Loan');
      }else if(ammount_needed > 300000){
        swalFire('Loan Amount must be less than 300k for Regular Loan');
        value = '300k';
      }
      return value;
    }

    function checkB2BLoan(ammount_needed, savings){
      var value = '';
      if(ammount_needed > savings){
        swalFire('Loan Amount must be less than '+savings+' for Back-to-Back Loan');
        value = savings;
      }
      return value;
    }
    function swalFire(data) {
      swalWithBootstrapButtons.fire({
                    title: 'Info',
                    icon: 'info',
                    html:data,
            });
    }
    function checkLoanableAmmount(ammount_needed, loanableAmmount){
      var loanable = loanableAmmount.toFixed(0).toString().replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ",");
      if(ammount_needed > loanableAmmount){
       swalFire('Your loanable amount is: '+loanable+'<br> it might be better to change your payment term or lower the amount you applied')
       // swalFire('Your loanable amount is: <br> it might be better to change your payment term')
        return false;
      }else{
        return true;
      }
    }
    
    // Deposit - Total Interest = Loan Amount
    // "NTHP X .60 X Term  / 1+ (.05 X term)+.03+.01 
    // BONUS  / 1+ (Int X Term)"
// wealth builtin program
  $('#montly_expense').on('focusout', function(){
        $('#amount_needed').val('');
  });
  $('#montly_income').on('focusout', function(){
        $('#amount_needed').val('');
  });
  $('#payment_terms').change(function(){
        $('#amount_needed').val('');
  });
  $('#amount_needed').on('focusout', function(){

      var loan_product = $('#loan_product').val();
      var montly_income = $('#montly_income').val();
      var montly_expense = $('#montly_expense').val();
      var payment_terms = $('#payment_terms').val();
      var amount_needed = $(this).val();
      var loanable_amount = $('#loanable_amount').val();
      var salaryDeposit = $('#savings-salary-amount').val();
      // var less_loan = $('#less_loan').val();

      

      // alert(parseInt(amount_needed));
      $.ajax({
        type:"get",
        url: base_path+"/payment_termMonths/"+payment_terms,
        success: function(data){
          // if(loan_product == 1){
          // var result = assetLoan(montly_income, montly_expense, data);
          // }
          switch(loan_product){
            case '1': // asset
            case '2': // asset
            case '3': // asset
              var result = assetLoan(montly_income, montly_expense, data);
              if(checkLoanableAmmount(amount_needed, result)){
              var value = checkAssetLoan(parseInt(amount_needed))
                $('#less_loan').val(value);
              }
              break;
            case '4': // asset
            case '5': // asset
            case '6': // asset
              var result = b2bLoan(salaryDeposit, data);
              if(checkLoanableAmmount(amount_needed, result)){
                var value = checkB2BLoan(parseInt(amount_needed, salaryDeposit))
                $('#less_loan').val(value);
              }
              break;
            case '7': // businessLoan
              var result = businessLoan(montly_income, montly_expense, data);
              if(checkLoanableAmmount(amount_needed, result)){
                var value = checkBusinessLoan(parseInt(amount_needed))
                $('#less_loan').val(value);
              }
              break;
            case '8': // houseLoanA
              // var result = houseLoanA(montly_income, montly_expense, data);
              // if(checkLoanableAmmount(amount_needed, result)){
              //   var value = checkHousingLoanA(parseInt(amount_needed));
              //   $('#less_loan').val(value);
              // }
              break;
            case '9': // houseLoanB
              var result = houseLoanB(montly_income, montly_expense, data);
              if(checkLoanableAmmount(amount_needed, result)){
                var value = checkHousingLoanB(parseInt(amount_needed))
                $('#less_loan').val(value);
              }
              break;
            case '10':
            case '11':
            case '12':
            case '13':
            case '14':
            case '15':
            case '16':
              var result = multiPLoan(montly_income, montly_expense, data);
              if(checkLoanableAmmount(amount_needed, result)){
                var value = checkMultiPLoan(parseInt(amount_needed))
                $('#less_loan').val(value);
              }
              break;
            case '17':
              var result = regLoan(montly_income, montly_expense, data);
              if(checkLoanableAmmount(amount_needed, result)){
                var value = checkRegularLoan(parseInt(amount_needed))
                $('#less_loan').val(value);
              }
              break;
            case '18':
              var result = salaryLoanA(salaryDeposit, data);
              if(checkLoanableAmmount(amount_needed, result)){
                var value = checkSalaryLoanAB(parseInt(amount_needed))
                $('#less_loan').val(value);
              }
              break;
            case '19':
              var result = salaryLoanB(salaryDeposit, data);
              if(checkLoanableAmmount(amount_needed, result)){
                var value = checkSalaryLoanAB(parseInt(amount_needed))
                $('#less_loan').val(value);
              }
              break;
            case '20':
            case '21':
            case '22':
            case '23':
              var result = salaryLoanBBonus(data, salaryDeposit);
              if(checkLoanableAmmount(amount_needed, result)){
                var value = checkSalaryLoanAB(parseInt(amount_needed))
                $('#less_loan').val(value);
              }
              break;
            case '24':
              var result = salaryLoanC(salaryDeposit, data);
              if(checkLoanableAmmount(amount_needed, result)){
                var value = checkSalaryLoanC(parseInt(amount_needed))
                $('#less_loan').val(value);
              }
              break;
            case '25':
            case '26':
              var result = salaryLoanCBonus(data, salaryDeposit);
              if(checkLoanableAmmount(amount_needed, result)){
                var value = checkSalaryLoanC(parseInt(amount_needed))
                $('#less_loan').val(value);
              }
              break;
            case '27':
              var result = vehiceLoan(montly_income, montly_expense, data);
              if(checkLoanableAmmount(amount_needed, result)){
                var value = checkVehicleLoan(parseInt(amount_needed))
                $('#less_loan').val(value);
              }
              break;
            default:
              var result = null;
              break;             
          }
         // alert(result+' '+loan_product);
          // if(parseInt(amount_needed) >= result.toFixed(0))
          // {
          //   alert('amount to loan to high');
          // }
          // var results = result.toFixed(0).toString().replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ",");
            // $('#loanable_amountLabel').html(result.toFixed(0)+' '+parseInt(amount_needed));
        }
      });
    // alert('hello');
  });
  function addSavingsDeposit(html){
    $('#product').removeClass('col-md-6');
    $('#term_div').removeClass('col-md-6');
    $('#product').addClass('col-md-4');
    $('#term_div').addClass('col-md-4');
    $('#savings-salary-div').attr('hidden', false);
    $('#savings-salary-label').html(html);
    $('#savings-salary-amount').val('');
  }

  function removeSavingsDeposit(){
    $('#product').removeClass('col-md-4');
    $('#term_div').removeClass('col-md-4');
    $('#product').addClass('col-md-6');
    $('#term_div').addClass('col-md-6');
    $('#savings-salary-div').attr('hidden', true);
    $('#savings-salary-label').html('');
    $('#savings-salary-amount').val('');
  }

  $('#id_type').change(function(){
    var id_type = $(this).val();
    if(id_type == 'Other'){
      $('#id_type_div').removeClass('col-md-4');
      $('#id_front_div').removeClass('col-md-4');
      $('#id_back_div').removeClass('col-md-4');
      $('#id_type_div').addClass('col-md-2');
      $('#id_front_div').addClass('col-md-3');
      $('#id_back_div').addClass('col-md-3');
      $('#id_other_div').attr('hidden', false);
      $('#other_id_type').val('');
    }else{
      $('#id_type_div').removeClass('col-md-2');
      $('#id_front_div').removeClass('col-md-3');
      $('#id_back_div').removeClass('col-md-3');
      $('#id_type_div').addClass('col-md-4');
      $('#id_front_div').addClass('col-md-4');
      $('#id_back_div').addClass('col-md-4');
      $('#id_other_div').attr('hidden', true);
      $('#other_id_type').val('');

    }
  });

  $('#loan_product').change(function(){
        var loan_type_id = $(this).val();
        var deposit = $('#savings-salary-amount').val();
        $('#amount_needed').val('');

          switch(loan_type_id){
            case '1': // asset
            removeSavingsDeposit();
            break;
            case '2': // asset
            removeSavingsDeposit();
            break;
            case '3': // asset
            removeSavingsDeposit();
            break;
            case '4': // asset
            addSavingsDeposit('Time Deposit');
              break;
            case '5': // asset
            addSavingsDeposit('Wealth Building Program');
              // var result = b2bLoan(deposit, data);
              break;
            case '6': // asset
            addSavingsDeposit('Lockin Savings');
              // var result = b2bLoan(deposit, data);
              break;
            case '7': // businessLoan
            removeSavingsDeposit();
            break;
            case '8': // houseLoanA
            removeSavingsDeposit();
            break;
            case '9': // houseLoanB
            removeSavingsDeposit();
            break;
            case '10':
            removeSavingsDeposit();
            break;
            case '11':
            removeSavingsDeposit();
            break;
            case '12':
            removeSavingsDeposit();
            break;
            case '13':
            removeSavingsDeposit();
            break;
            case '14':
            removeSavingsDeposit();
            break;
            case '15':
            removeSavingsDeposit();
            break;
            case '16':
            removeSavingsDeposit();
            break;
            case '17':
            removeSavingsDeposit();
            break;
            case '18':
            case '19':
            addSavingsDeposit('Salary');
              break;
            case '20':
            addSavingsDeposit('Mid-Year');
              break;
            case '21':
            addSavingsDeposit('Year-End');
              break;
            case '22':
            addSavingsDeposit('Performance Evaluation Incentive');
              break;
            case '23':
            addSavingsDeposit('Performance Based Bonus');
              break;
            case '24':
            addSavingsDeposit('Salary');
              break;
            case '25':
            addSavingsDeposit('Mid-Year');
              break;
            case '26':
            addSavingsDeposit('Year-End');
              break;
            case '27':
            removeSavingsDeposit();
            break;
            default:
              var result = null;
              break;             
          }

        $.ajax({
          type:"get",
          url: base_path+"/payment_modeAPI/"+loan_type_id,
          success: function(data){

          var payment_mode1 = document.getElementById("payment_mode");
          removeOptions(payment_mode1);
          // alert(JSON.stringify(data));
            var payment_mode = document.getElementById("payment_mode");
              for (var i = 0; i < data.length; ++i) {
                  // Append the element to the end of Array list
                payment_mode[payment_mode.length] = new Option(data[i]['paymode_name'], data[i]['paymode_id']);
              }
            }
             ,error: function (data) {
                 // alert('please delete the preloan connected to this preloan/preloan');
                 console.log(data.responseText);  
                 alert(data.responseText);  
            }
        });
        $.ajax({
          type:"get",
          url: base_path+"/payment_termAPI/"+loan_type_id,
          success: function(data){
          // alert(JSON.stringify(data));

            var payment_term1 = document.getElementById("payment_terms");
            removeOptions(payment_term1);
            var payment_term = document.getElementById("payment_terms");
            for (var i = 0; i < data.length; ++i) {
                // Append the element to the end of Array list
              payment_term[payment_term.length] = new Option(data[i]['payterm_name'], data[i]['payterm_id']);
            }
          }
        });
    });

  $('#loan_purpose').change(function(){
    var loan_purpose = $(this).val();
    if(loan_purpose == 11){
      $('#other_purpose_label').attr('hidden', false);
      $('#other_purpose').focus();
    }else{
      $('#other_purpose_label').attr('hidden', true);
      $('#other_purpose').val('');
    }
  });
  $('#occupation_id').change(function(){
        var occu = $(this).val();
        if(occu == 0){
          // alert('other_occupation');
          $('#other_occupation').attr('hidden', false);
          $('#other_occupation_label').attr('hidden', false);
        }else{
          $('#other_occupation').attr('hidden', true);
          $('#other_occupation').val('');
          $('#other_occupation_label').attr('hidden', true);
        }
  });
  // $('#age_group').attr('readonly', true);
        var base_path = $("#url").val();

         const validate = () => {
      const $result = $('#result');
      const member_no = $('#co_maker').val();
      
      $result.text('');
      $.ajax({
                type:"get",
                url: base_path+"/validateCoMaker/",
                data: {member_no:member_no},
                // dataType: 'json',
                success: function(data){
                  // alert(data);
                  var member_no = data;
                  if(member_no == 'nottaken'){
                    $result.text('Member number is not in our database.');
                    $result.css('color', 'red');
                  }
                  else{
                    $result.text('Member number verified.');
                    $result.css('color', 'green');
                  }

                }
                // ,error: function (data) {
                //      // alert('please delete the preloan connected to this preloan/preloan');
                //      console.log(data.responseText);  
                //      alert(data.responseText);  
                // }
              });

      return false;
    }
          $('#co_maker').on('input', validate);
  var region = $("#region_id").val();
  var hidden_province_id = $("#hidden_province_id").val();
  var hidden_city_id = $("#hidden_city_id").val();
  var hidden_barangay_id = $("#hidden_barangay_id").val();
  // alert(region);
  $.ajax({
                type:"GET",
                // base_path+'getProvince/'+region_code,
                url:base_path+'/getProvince/'+region,
                success:function(res){               
                if(res){
                $("#province_id").append('<option value="">Select Province</option>'); 
                $.each(res,function(key,value){
                    // var province_id2 = value['province_id'];
                    // alert(JSON.stringify(province_id2));
                    $("#province_id").append('<option  value="'+ value['province_id'] +'" {{ $data->province_id == $item->id ? "selected='selected'" : '' }}>'+ value['name'] +'</option>');
                });
                  $("#province_id").val(hidden_province_id);
                }else{
                   $("#province_id").empty();
                }
                }
                });
  $.ajax({
               type:"GET",
               url:base_path+'/getCities/'+hidden_province_id,
               success:function(res){  
                if(res){
                    $.each(res,function(key,value){
                        $("#city_id").append('<option class="form-control" value="'+ value['city_id'] +'">'+ value['name'] +'</option>');
                    });
                    // $("#city_id").val(city_id);
                }else{
                   $("#city_id").empty();
                }
               }
                });
  $.ajax({
               type:"GET",
               url:base_path+'/getBarangays/'+hidden_city_id,
               success:function(res){  
                if(res){
                    $.each((res),function(key,value){
                        $("#barangay_id").append('<option class="form-control" value="'+ value['code'] +'">'+ value['name'] +'</option>');
                    });
                  // $("#barangay_id").val(barangay_id1);
                }else{
                   $("#barangay_id").empty();
                }
               }
            });

});
      
  $('#upload-sig-btn').click(function (event) {
            event.preventDefault();
            // signaturePad.signature('clear');
            signaturePad.clear();
            // $("#signature64").val('');
    $('#manual-sig').attr('hidden', true);
    $('#manual-sig-btn').attr('hidden', false);
    $('#upload-sig-btn').attr('hidden', true);
    $('#upload-sig').attr('hidden', false);
    $('#f09').attr('disabled', false);
    $('#signature').attr('disabled', true);
    $('#manualOrUpload').val('upload');
  });
  $('#manual-sig-btn').click(function (event) {
            event.preventDefault();
  // var upload_sig = document.getElementById ("f09");
            $('#f09').val(null);
            $('#labelf09').html('Upload signature');
    $('#manual-sig').attr('hidden', false);
    $('#signature').attr('disabled', false);
    $('#f09').attr('disabled', true);
    // $('#manual-sig').attr('disabled', false);
    $('#manual-sig-btn').attr('hidden', true);
    $('#upload-sig-btn').attr('hidden', false);
    $('#upload-sig').attr('hidden', true);
    $('#manualOrUpload').val('manual');
  });


  $("[type=file]").on("change", function(){
  // Name of file and placeholder
    var file = this.files[0].name;
    var dflt = $(this).attr("placeholder");
    if($(this).val()!=""){
      $(this).next().text(file);
    } else {
      $(this).next().text(dflt);
    }
  });
  $(document).on('keydown', 'input[pattern]', function(e){
    var input = $(this);
    var oldVal = input.val();
    var regex = new RegExp(input.attr('pattern'), 'g');

    setTimeout(function(){
      var newVal = input.val();
      if(!regex.test(newVal)){
        input.val(oldVal); 
      }
    }, 1);
  });
 
</script>
  <script src="{{ asset('assets') }}/js/extrajs.js"></script>
@endsection

