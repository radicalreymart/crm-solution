@extends('layouts.app', [
    'class' => 'sidebar-mini ',
    'namePage' => 'Batch Index',
    'activePage' => 'batch-management',
    'activeNav' => '',
])

@section('indexdeleteall')
  <script src="{{ asset('assets') }}/js/indexdeleteall.js"></script>
@endsection
@section('content')
<div>
    @livewire('search-batch-history')
</div>
@endsection