<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Livewire\WithPagination;
use Carbon\Carbon;
use App\Models\Client;


class SearchClient extends Component
{
    use WithPagination;
    public $searchTerm;

    public function render()
    {

        
        $searchTerm = '%'.$this->searchTerm.'%';
        return view('livewire.client.search-client',
        ['client' => Client::where('fullname','like', $searchTerm)
        ->orWhere('gmail_address','like', $searchTerm)
        ->orWhere('contact_no','like', $searchTerm)
        ->paginate(10)]);
    }
}
