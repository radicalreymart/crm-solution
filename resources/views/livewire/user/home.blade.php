@extends('layouts.app', [
    'class' => 'sidebar-mini ',
    'namePage' => 'User Index',
    'activePage' => 'user-management',
    'activeNav' => 'settings',
])

@section('indexdeleteall')
  <script src="{{ asset('assets') }}/js/indexdeleteall.js"></script>
@endsection
@section('content')

<div>
    @livewire('users')
</div>

    
@endsection
@section('userscript')
<script type="text/javascript">
        window.livewire.on('userStore', () => {
            $('#exampleModal').modal('hide');
            $('.modal-backdrop').remove();
            setTimeout(function(){
   // alert('hei');
       $("div.alert").remove();
    }, 5000);
        });
</script>
@endsection