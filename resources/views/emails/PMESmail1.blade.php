

@component('mail::layout')
{{-- Header --}}

@slot('header')
@component('mail::header', ['url' => config('app.url')])

@endcomponent
@endslot
<body style="margin:0;padding:0;word-spacing:normal;background-color:#939297;">
  <div role="article" aria-roledescription="email" lang="en" style="text-size-adjust:100%;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;background-color:lightgrey;">
    <table role="presentation" style="width:100%;border:none;border-spacing:0;">
      <tr>
        <td align="center" style="padding:0;">
          <!--[if mso]>
          <table role="presentation" align="center" style="width:600px;">
          <tr>
          <td>
          <![endif]-->
          <table role="presentation" style="width:94%;max-width:600px;border:none;border-spacing:0;text-align:left;font-family:Arial,sans-serif;font-size:16px;line-height:22px;color:#363636;">
            <tr>
              <td style="padding:40px 30px 30px 30px;text-align:center;font-size:24px;font-weight:bold;">
                <a href="" style="text-decoration:none;"><img src="{{ asset('assets/img/mhrlarge-removebg.png') }}" width="115" alt="" style="width:115px;max-width:80%;margin-bottom:20px;"></a>
              </td>
            </tr>
            <tr>
              <td style="padding:30px;background-color:#ffffff;">
                <h1 style="margin-top:0;margin-bottom:16px;font-size:26px;line-height:32px;font-weight:bold;letter-spacing:-0.02em;">
                    MAGANDANG BUHAY!<br>
                </h1>
                <p style="margin:0;">Future Kamay-ari! Maraming Salamat sa inyong pag-register! Ito ang magsisilbing imbitasyon para sa ating PRE-MEMBERSHIP EDUCATION @if(isset($details['link'])) WEBINAR thru Google Meet App. @else SEMINAR. @endif </p>
              </td>
            </tr>
            <tr>
              <td style="padding:0;font-size:24px;line-height:28px;font-weight:bold;">
                <a href="" style="text-decoration:none;"></a>
              </td>
            </tr>
            <tr>
              <td style="padding:35px 30px 11px 30px;font-size:0;background-color:#ffffff;border-bottom:1px solid #f0f0f5;border-color:rgba(201,201,207,.35);">
                
                
                  <p style="margin-top:0;margin-bottom:12px;">

 @if(isset($details['link']))
LINK: <a href="{{ $details['link'] }}">{{ $details['link'] }}</a> <br>
  @if(isset($details['meeting_cd']))
    MEETING CODE: <b>{{ $details['meeting_cd'] }}</b><br>
  @endif
@endif
DATE & TIME: <b>{{ \Carbon\Carbon::parse($details['sched_date'])->format('M d, Y')}} {{ \Carbon\Carbon::parse($details['sched_date'])->format('H:i a')}}</b><br>

</p>
                  <p style="margin-top:0;margin-bottom:18px;">
                    @if($details['resched_remarks'] == '.')

                    @else
                    Reschedule Remarks: <b>{{ $details['resched_remarks'] }}</b>
                    @endif
                  </p>
<p style="margin:0; text-align: center;"><a href="https://calendar.google.com/calendar/u/0/r/eventedit?text={{ $details['text'] }}&dates={{ $details['startdate'] }}/{{ $details['enddate'] }}&details={{ $details['description'] }}&location=MHRMPC+Main+Branch&sf=true&output=xml"target="_blank" rel="nofollow"style="background: blue; text-decoration: none; padding: 10px 25px; color: #ffffff; border-radius: 4px; display:inline-block; mso-padding-alt:0;text-underline-color:#ff3884">Add to my calendar</span><!--[if mso]><i style="letter-spacing: 25px;mso-font-width:-100%">&nbsp;</i><![endif]--></a></p>
                  <br><br>
@if($details['vid_tool'] == 0)
<p class="screen-1__paragraph-2" style=" font-family: 'Montserrat', sans-serif;
    font-weight: 700;
    text-align: center;">Para sa mga mobile users:</p>
<p class="screen-1__paragraph-5" style="font-family: 'Montserrat', sans-serif;
    font-weight: 500;
    text-align: center;">
I-download lamang ang 'Google Meet' App sa Play Store or Apple Store
at i-copy lang ang <b>MEETING LINK</b> at <b>MEETING CODE</b> na makikita sa itaas.
</p><br>
<p class="screen-1__paragraph-2" style=" font-family: 'Montserrat', sans-serif;
    font-weight: 700;
    text-align: center;">
Para sa mga desktop o laptop users:
</p>

<p class="screen-1__paragraph-5" style="font-family: 'Montserrat', sans-serif;
    font-weight: 500;
    text-align: center;">
Hindi na kailangan pang mag-download ng app, i-click lamang ang
<b>MEETING LINK</b> sa inyong browser at i-paste ang <b>MEETING CODE</b>.
</p><br>
<p class="screen-1__paragraph-5" style="font-family: 'Montserrat', sans-serif;
    font-weight: 500;
    text-align: center;">
At hintayin lamang na i-accept ng ating Host ang inyong request para
maka-join sa ating webinar. Magkita-kita tayo sa nakatakdang araw at
oras ng ating webinar! <br> Maraming Salamat!
</p>
@elseif($details['vid_tool'] == 1)
<p class="screen-1__paragraph-2" style=" font-family: 'Montserrat', sans-serif;
    font-weight: 700;
    text-align: center;">Para sa mga mobile users:</p>
<p class="screen-1__paragraph-5" style="font-family: 'Montserrat', sans-serif;
    font-weight: 500;
    text-align: center;">
I-download lamang ang 'Zoom' App sa Play Store or Apple Store
at i-click lang ang <b> LINK</b>  na makikita sa itaas.
</p><br>
<p class="screen-1__paragraph-2" style=" font-family: 'Montserrat', sans-serif;
    font-weight: 700;
    text-align: center;">
Para sa mga desktop o laptop users:
</p>

<p class="screen-1__paragraph-5" style="font-family: 'Montserrat', sans-serif;
    font-weight: 500;
    text-align: center;">
Hindi na kailangan pang mag-download ng app, i-click lamang ang
<b>LINK</b> sa inyong browser.
</p><br>
<p class="screen-1__paragraph-5" style="font-family: 'Montserrat', sans-serif;
    font-weight: 500;
    text-align: center;">
At hintayin lamang na i-accept ng ating Host ang inyong request para
maka-join sa ating webinar. Magkita-kita tayo sa nakatakdang araw at
oras ng ating webinar! <br> Maraming Salamat!
</p>
@else
<p class="screen-1__paragraph-2" style=" font-family: 'Montserrat', sans-serif;
    font-weight: 700;
    text-align: center;"></p>
<p class="screen-1__paragraph-5" style="font-family: 'Montserrat', sans-serif;
    font-weight: 500;
    text-align: center;">
    Maaari lamang na pumunta sa isinaad na MHRMPC Branch . Magkita-kita tayo sa nakatakdang araw at oras ng ating seminar! <br> Maraming Salamat!
</p><br>
<p class="screen-1__paragraph-2" style=" font-family: 'Montserrat', sans-serif;
    font-weight: 700;
    text-align: center;">
</p>

<p class="screen-1__paragraph-5" style="font-family: 'Montserrat', sans-serif;
    font-weight: 500;
    text-align: center;">
</p><br>
<p class="screen-1__paragraph-5" style="font-family: 'Montserrat', sans-serif;
    font-weight: 500;
    text-align: center;">
</p>
@endif
                  <p style="margin:0;"></p>
                <!--[if mso]>
                </td>
                </tr>
                </table>
                <![endif]-->
              </td>
            </tr>
            <tr>
              <td style="padding:30px;font-size:24px;line-height:28px;font-weight:bold;background-color:#ffffff;border-bottom:1px solid #f0f0f5;border-color:rgba(201,201,207,.35);">
                <a href="" style="text-decoration:none;"></a>
              </td>
            </tr>
            <tr>
              <td style="padding:30px;background-color:#ffffff;">
                <p style="margin:0;"></p>
              </td>
            </tr>
            <tr>
              <td style="padding:30px;text-align:center;font-size:12px;background-color:#404040;color:#cccccc;">
              </td>
            </tr>
          </table>
          <!--[if mso]>
          </td>
          </tr>
          </table>
          <![endif]-->
        </td>
      </tr>
    </table>
  </div>
</body>

{{-- Footer --}}
@slot('footer')
@component('mail::footer')

@endcomponent
@endslot
@endcomponent
