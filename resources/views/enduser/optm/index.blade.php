<!-- @extends('layouts.app1') -->

@section('loanstyle')
<style>
.alert{
	color: black !important;
}
[type=file] {
		position: absolute;
		filter: alpha(opacity=0);
		opacity: 0;
}
input,
[type=file] + label {
	border: 1px solid black;
	border-radius: 3px;
	text-align: left;
	padding: 0px;
	width: 100%;
	height: 30px;
	margin: 0;
	left: 0;
	position: relative;
}
[type=file] + label {
	text-align: center;
	left: 0em;
	top: 0.5em;
	/* Decorative */
	background: blue;
	color: #fff;
	border: black;
	cursor: pointer;
}
[type=file] + label:hover {
	background: #3399ff;
}
label.shorten_text1 {
 max-width: 100%;
	/*padding: px;*/
	text-overflow: ellipsis;
	white-space: nowrap;
	overflow: hidden;
}

label.shorten_text1:hover {
	overflow: visible;
}
label.shorten_text1:before{
	content: attr(title);
}
button::-moz-focus-inner {border: 0;}
/* ----------------------- */
/* Basic page layout */
/*body {
	max-width:600px;
	margin:auto;
	border:1px solid #aaa;
	padding:1em;
	font-size:1em;
	font-family:Arial,Helvetica,sans-serif;
}*/

</style>
@endsection

@section('content')
<!-- Masthead-->
<header class="masthead mb-1">
	
</header>

	 @include('sweetalert::alert')
<div class="container mt-5 col-md-8" id="optm-details">
	<div class="card p-2 pt-3 bg-light border-0">
		<div class="row"> 
			<div class="col-md-12 order-md-1">
				<div class="card mx-3 shadow-lg border-0">
					<div class="card-header bg-light text-primary py-2 pl-3 border-0">
						<h6 class="m-0 default-primary">Online Payment Transaction Index
							<a class=" hvr-icon-push float-right" onClick="window.location.href = '/enduser/home';" data-url="{{ url('/enduser/home') }}">{{ __('Back') }}<i class="far fa-caret-square-left hvr-icon"></i></a>
						</h6>
						
					</div>
					<div class="card-body px-3">
						<div class="justify-content">
						<input type="hidden" value="{{url('/')}}" id="url" name="url">
			              <div id="modalCatcher" class="table-responsive">
							<a class="btn btn-outline-light btn-xl text-uppercase " style="color: black; border-color: black; " type="button" href="{{route('enduser.optm.create')}}">New transaction</a>
							<table class="table">
							  <thead>
							    <tr>
							      <th scope="col">Action</th>
							      <th scope="col">Total Amount</th>
							      <th scope="col">Official Receipt</th>
							      <th scope="col">Transaction Date</th>
							    </tr>
							  </thead>
							  <tbody>
							      @if($optm->count())
				                    @foreach ($optm as $data)
							    <tr>
				                    <td>
				                    @if($data->file_path != null)
				                      <a href="javascript:void(0)" title="Show Attachment" class="attachment" data-id="{{ $data->id }}">
				                        <i  class="fas fa-file-image hvr-pulse fa-lg" aria-hidden="true"></i>
				                      </a>
				                     @endif
				                     @if($data->status != 1 || $data->status != null)
				                      <a href="javascript:void(0)" title="Show OPTD" class="OPTD" data-id="{{ $data->id }}">
				                        <i  class="fas fa-dollar-sign hvr-pulse fa-lg" aria-hidden="true"></i>
				                      </a>
				                     @endif
				                     
				                    </td>
				                    <td>{{ number_format($data->total_amt,2) }}</td>
				                    @if($data->status == 2)
				                    <td>Pending OR</td>
				                    @elseif($data->status == 3)
				                    <td>{{ $data->or_ref_no }}
				                    	<a href="{{ route('filedoc',  $data->file_path['optmor'])}}" target="_blank"  title="OR PDF"  data-id="{{ $data->file_path['optmor']}}">
				                          <i class="fas fa-receipt fa-lg"></i>
				                        </a>
				                    </td>
				                    @else
				                    <td>
										<a  style="color: black; border-color: black;" href="{{route('enduser.optd.create')}}">Payment details not complete click here to finish it</a></td>
				                    @endif

				                    <td>{{ date_format($data->created_at, 'jS M Y') }}</td>
							    </tr>
				                    @endforeach
				                    @endif
							  </tbody>
							</table>
			              </div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- Modal -->
<div class="modal fade" id="ajax-attachment-model" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
            <h4 class="modal-title" id="ajaxAttachmentModel"></h4>
            <button type="button" class="btn btn-default" onClick="window.location.reload();" id="close-modal" data-dismiss="modal">Close</button>
            </div>
            <div class="modal-body">
                <div class="card-body" >
                    <div class="">
                      <div class="slide s01">
                        <a id="href" target="_blank" rel="noopener noreferrer">
                            <h4 id="name"></h4>
                        <img id="img" height="300" width="400">
                        </a>
                      </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
	
<!-- Modal -->
<div class="modal fade" id="ajax-OPTD-model" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class=" modal-dialog">
        <div class=" modal-content">
            <div class="modal-header">
            <h4 class="modal-title" id="ajaxOPTDModel"></h4>
            <button type="button" class="btn btn-default" onClick="window.location.reload();" id="close-modal" data-dismiss="modal">Close</button>
            </div>
            <div class="modal-body">
            <div class="card-header">
	            <p id="ajaxOPTDModel1" class='text-muted'>Online Payment Transaction Detail</p>
            </div>
	            <div class="card-body" >
		            <table class="table" id="dynamicAddRemove">
		                <tr>
		                    <!-- <th>OPTM ID</th> -->
		                    <th>Amount</th>
		                    <th>Account</th>
		                </tr>
		                <tbody id="tbody">
		                  
		                </tbody>
		            </table>
	            </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('preloan')
<script>


$(document).ready(function(){

	$('#ajax-OPTD-model').modal({backdrop: 'static', keyboard: false})  
 	$.clearInput = function () {
        $('insert_form').find('input[type=text], input[type=password], input[type=number], input[type=email], input[type=file], label, select, textarea').val('');
    };
     $('.modal').on('hidden.bs.modal', function(){
             // window.location.reload();
             // $('#insert_form').removeData('bs.modal')
    $.clearInput();
    $(this).find('form')[0].reset();
    });

	$.ajaxSetup({
			headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
	});

	 $('#modalCatcher').on('click', '.OPTD', function () 
    {
        $('#ajaxOPTDModel').html("OPTD");
        $('#ajax-OPTD-model').modal('show');
        var id = $(this).data('id');
        var base_path = $("#url").val();
        $.ajax({
                type:"post",
                url: base_path+"/OPTDmodal",
                data: { id: id },
                dataType: 'json',
                success: function(res){
                	// console.log(res.acount);
                	$.each( res, function( key, value ) {
		              var table = document.getElementById("tbody");
		              var one = 1;
		              $("#tbody").append('');
		              var row = table.insertRow(0);
		              var cell1 = row.insertCell(0);
		              var cell2 = row.insertCell(1);
		              // console.log(value.amount);
		              cell1.innerHTML = value.amount;
		              cell2.innerHTML = value.description;
		            });
                }
                ,error: function (data) {
                     // alert('please delete the preloan connected to this preloan/preloan');
                     alert(data.responseText);  
                }
            });
    });

	 $('#modalCatcher').on('click', '.attachment', function () 
    {
        $('#ajaxAttachmentModel').html("Image Attach");
        $('#ajax-attachment-model').modal('show');
        var id = $(this).data('id');
        var base_path = $("#url").val();
        // alert(id+base_path);
        // $('#attachmentID').val(id);
        $.ajax({
                type:"post",
                url: base_path+"/enduserattachment",
                data: { id: id },
                dataType: 'json',
                success: function(res){
	            	var src = res.file_path['optmdoc'];
	                var splitfile = res.file_path['optmdoc'].split('-');
	                var imgsrc1 = base_path+'/assets/img/uploads/payments/'+splitfile[0]+'/'+src;
	                var img1 = document.getElementById("img");
	                var aid1 = document.getElementById("href");
	                var src1 = document.createAttribute("src");
	                var href1 = document.createAttribute("href");
	                document.getElementById("name").innerHTML = splitfile[1];
	                src1.value = imgsrc1;
	                img1.setAttributeNode(src1);  
	                href1.value = imgsrc1;
	                aid1.setAttributeNode(href1);
                }
                ,error: function (data) {
                     alert(data.responseText);  
                }
            });
    });
});
</script>
@endsection

