<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Branch;

class MemberMaster extends Model
{
    use HasFactory;
    public $incrementing = false;
    protected $primaryKey = 'member_no';
    protected $table = 'member_master';
    // use HasFactory;
    protected $fillable = [
        'member_no',
        'long_name',
        'branch_id',
        'gender',
        'birthdate',
        'email_address',
        'contact_no',
        'crea_by',
        'created_at',
        'upd_by',
        'updated_at',
        'soft_deleted_at',
    ];
    public function branch()
    {
        // return $this->belongsTo('App\Models\Branch','id');
        return $this->belongsTo(Branch::class);
    }
}
