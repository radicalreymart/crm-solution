<?php
namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Models\Batch;
use App\Models\Client;
use Illuminate\Support\Str;
use Carbon\Carbon;

class DatabaseSeeder extends Seeder
{
    /**  
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // DB::statement('SET FOREIGN_KEY_CHECKS=0');

        // DB::table('users')->truncate();

        // $this->call([ UsersTableSeeder::class]);

        // DB::statement('SET FOREIGN_KEY_CHECKS=1');

        // DB::table('branch')->insert(
        //     [
        //     [
        //        'cd'=>'main',
        //        'name'=> 'Main Branch',
        //        'crea_by'=>'admin',
        //        'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        //     ],
        //     [
        //        'cd'=>'kbo',
        //        'name'=> 'Kasiglahan Branch',
        //        'crea_by'=>'admin',
        //        'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        //     ],
        //     [
        //        'cd'=>'sro',
        //        'name'=> 'San Rafael Branch',
        //        'crea_by'=>'admin',
        //        'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        //     ],
        //     [
        //        'cd'=>'smo',
        //        'name'=> 'San Mateo Branch',
        //        'crea_by'=>'admin',
        //        'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        //     ]
        //     ]);
        //  DB::table('users')->insert(
        //     [
        //     [
        //        'uname'=>'admin',
        //        'password'=> bcrypt('admin123'),
        //        'email'=>'admin@admin.com',
        //        'branch_id'=>1,
        //        'role_id'=>0,
        //        'crea_by'=>'admin',
        //        'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        //     ]
        //     ,[
        //        'uname'=>'user',
        //        'password'=> bcrypt('users123'),
        //        'email'=>'user@user.com',
        //        'branch_id'=>2,
        //        'role_id'=>1,
        //        'crea_by'=>'admin',
        //        'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        //     ]
        //  ]);

        //   DB::table('service_type')->insert(
        //     [
        //     [
        //        'service_type'=>'PMES',
        //        'initial_recipient_id'=>1,
        //        'verifier_id'=>1,
        //        'crea_by'=>'admin',
        //        'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        //     ],
        //     [
        //        'service_type'=>'Payment',
        //        'initial_recipient_id'=>2,
        //        'verifier_id'=>2,
        //        'crea_by'=>'admin',
        //        'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        //     ],[
        //        'service_type'=>'Loan',
        //        'initial_recipient_id'=>3,
        //        'verifier_id'=>3,
        //        'crea_by'=>'admin',
        //        'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        //     ],
        //     ]);
        //     DB::table('service_status')->insert(
        //     [
        //     [
        //        'status_name'=>'Waiting',
        //        'crea_by'=>'admin',
        //        'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        //     ],
        //     [
        //        'status_name'=>'Ongoing',
        //        'crea_by'=>'admin',
        //        'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        //     ],
        //     [
        //        'status_name'=>'Done',
        //        'crea_by'=>'admin',
        //        'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        //     ],
        //     [
        //        'status_name'=>'Expired',
        //        'crea_by'=>'admin',
        //        'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        //     ],
        //     ]);
            
            
            
        //     DB::table('marketing_channel')->insert(
        //     [
        //     [
        //        'cd'=>'mc1',
        //        'name'=>'Co-workers/Co-employees/Manager',
        //        'crea_by'=>'admin',
        //        'upd_by'=>'admin',
        //        'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        //     ],
        //     [
        //        'cd'=>'mc2',
        //        'name'=>'Email',
        //        'crea_by'=>'admin',
        //        'upd_by'=>'admin',
        //        'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        //     ],[
        //        'cd'=>'mc3',
        //        'name'=>'Facebook Post/Ads',
        //        'crea_by'=>'admin',
        //        'upd_by'=>'admin',
        //        'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        //     ],[
        //        'cd'=>'mc4',
        //        'name'=>'Family: Relatives/Spouse/Live-in Partner/Siblings',
        //        'crea_by'=>'admin',
        //        'upd_by'=>'admin',
        //        'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        //     ],[
        //        'cd'=>'mc5',
        //        'name'=>'Friend/s',
        //        'crea_by'=>'admin',
        //        'upd_by'=>'admin',
        //        'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        //     ],[
        //        'cd'=>'mc6',
        //        'name'=>'MHRMPC Members',
        //        'crea_by'=>'admin',
        //        'upd_by'=>'admin',
        //        'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        //     ],[
        //        'cd'=>'mc7',
        //        'name'=>'Print Ads/Poster/Tarpaulin/Flyers',
        //        'crea_by'=>'admin',
        //        'upd_by'=>'admin',
        //        'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        //     ],[
        //        'cd'=>'mc8',
        //        'name'=>'Radio/Other Media',
        //        'crea_by'=>'admin',
        //        'upd_by'=>'admin',
        //        'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        //     ],[
        //        'cd'=>'mc9',
        //        'name'=>'SMS/Text',
        //        'crea_by'=>'admin',
        //        'upd_by'=>'admin',
        //        'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        //     ],[
        //        'cd'=>'mc10',
        //        'name'=>'Youtube/Instagram/Twitter',
        //        'crea_by'=>'admin',
        //        'upd_by'=>'admin',
        //        'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        //     ],[
        //        'cd'=>'mc11',
        //        'name'=>'Walk-in',
        //        'crea_by'=>'admin',
        //        'upd_by'=>'admin',
        //        'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        //     ],
        //     ]);
        //     DB::table('occupation')->insert(
        //     [
        //     [
        //        'cd'=>'ocu1',
        //        'name'=>'occupation 1',
        //        'crea_by'=>'admin',
        //        'upd_by'=>'admin',
        //        'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        //     ],
        //     [
        //        'cd'=>'ocu2',
        //        'name'=>'occupation 2',
        //        'crea_by'=>'admin',
        //        'upd_by'=>'admin',
        //        'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        //     ],[
        //        'cd'=>'ocu3',
        //        'name'=>'occupation 3',
        //        'crea_by'=>'admin',
        //        'upd_by'=>'admin',
        //        'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        //     ],
        //     ]);

      // DB::table('client_masters')->insert(
      //       [
      //       [
      //          'fname'=>'admin',
      //          'mname'=>'reymart',
      //          'lname'=>'san',
      //          'fullname'=>'admin reymart san',
      //          'contact_no'=>'+639386166598',
      //          'member_status'=>0,
      //          'member_no'=>'2021-0260',
      //          'seminar_pref'=>0,
      //          'gmail_address'=>'admin@gmail.com',
      //          'branch_id'=> 1,
      //          'gender'=>0,
      //          'birthdate'=>Carbon::parse('1990-07-13'),
      //          'pref_date'=>Carbon::now()->addDay(1),
      //          'age'=>31,
      //          'street'=>'178 M H del pilar street ',
      //          'barangay_primary'=>12633,
      //          'city_primary'=>484,
      //          'province_primary'=>21,
      //          'region_primary'=>4,
      //          'barangay_id'=>'045808011',
      //          'city_id'=>'045808',
      //          'province_id'=>'0458',
      //          'region_id'=>'04',
      //          'occupation_id'=>1,
      //          'of_tag'=> 1,
      //          'purpose_tag'=> 1,
      //          'marketing_channel_id'=>4,
      //          'staff_referral_cd'=>' 123qwe1',
      //          'data_consent_fl'=>1,
      //          'sched_stat'=>0,
      //          'crea_by'=>'admin'
      //       ]
      //       ]);

      //   DB::table('batch')->insert(
      //       [
      //       [
      //          'batch_cd'=>123,
      //          'batch_stat'=>0,
      //          'sched_date'=>Carbon::now()->addDay(1),
      //          'online_vid_tool'=>0,
      //          'meeting_cd'=>'zds-zda-asd',
      //          'meeting_pw'=>null,
      //          'crea_by'=>'admin',
      //          'upd_by'=>'null',
      //          'created_at' => Carbon::now()->format('Y-m-d H:i:s')
      //       ],
      //       ]);
         
      //    DB::table('pmesMasters')->insert(
      //       [
      //       [
      //          'client_id'=> 1,
      //             'batch_id'=> null,
      //          'serv_status'=>0,
      //          'crea_by'=>'admin',
      //          'created_at' => Carbon::now()->format('Y-m-d H:i:s')
      //       ],
      //       [
      //            'client_id'=> 1,
      //             'batch_id'=> 1,
      //             'serv_status'=>1,
      //          'crea_by'=>'admin',
      //          'created_at' => Carbon::now()->format('Y-m-d H:i:s')
      //       ],
      //       ]);
      //    DB::table('service_trxn_master')->insert(
      //       [
      //       [
      //          'log_date'=>Carbon::now()->format('Y-m-d H:i:s'),
      //          'client_id'=>1,
      //          'service_status_id'=>1,
      //          'service_type_id'=>1,
      //          'service_details'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce quis lacus in eros hendrerit laoreet sit amet hendrerit turpis. Integer in venenatis orci, at congue augue. Fusce tellus mauris, vulputate a pulvinar eget, auctor vitae mauris. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Sed ex nulla, auctor sit amet libero vel, aliquam interdum sem. Nulla lorem odio, ullamcorper in ornare nec, congue a mi. Aenean consectetur, mi vel pharetra dignissim, eros orci fermentum leo, nec blandit purus urna eget felis. Nunc sit amet dui dapibus, malesuada risus non, pulvinar nulla. Suspendisse sit amet odio ac eros ultrices placerat sed quis nisi. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Proin condimentum, erat at condimentum tincidunt, arcu massa sollicitudin lorem, nec consectetur massa diam in est. Nam ac mi dolor. Nulla posuere dignissim tellus ut sodales. Integer scelerisque tortor at diam euismod, eu ornare arcu vulputate.',
      //          'crea_by'=>'admin',
      //          'service_duration' => Carbon::now()->format('H:i:s'),
      //          'svc_start' => '2021-08-27 04:06:41.000',
      //          'svc_end' => Carbon::now()->format('Y-m-d H:i:s'),
      //          'created_at' => Carbon::now()->format('Y-m-d H:i:s')
      //       ],
      //       [
      //          'log_date'=>Carbon::now()->format('Y-m-d H:i:s'),
      //          'client_id'=>1,
      //          'service_status_id'=>2,
      //          'service_type_id'=>2,
      //          'service_details'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce quis lacus in eros hendrerit laoreet sit amet hendrerit turpis. Integer in venenatis orci, at congue augue. Fusce tellus mauris, vulputate a pulvinar eget, auctor vitae mauris. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Sed ex nulla, auctor sit amet libero vel, aliquam interdum sem. Nulla lorem odio, ullamcorper in ornare nec, congue a mi. Aenean consectetur, mi vel pharetra dignissim, eros orci fermentum leo, nec blandit purus urna eget felis. Nunc sit amet dui dapibus, malesuada risus non, pulvinar nulla. Suspendisse sit amet odio ac eros ultrices placerat sed quis nisi. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Proin condimentum, erat at condimentum tincidunt, arcu massa sollicitudin lorem, nec consectetur massa diam in est. Nam ac mi dolor. Nulla posuere dignissim tellus ut sodales. Integer scelerisque tortor at diam euismod, eu ornare arcu vulputate.',
      //          'crea_by'=>'admin',
      //          'service_duration' => null,
      //          'svc_end' => null,
      //          'svc_start' => Carbon::now()->format('Y-m-d H:i:s'),
      //          'created_at' => Carbon::now()->format('Y-m-d H:i:s')
      //       ],[
      //          'log_date'=>Carbon::now()->format('Y-m-d H:i:s'),
      //          'client_id'=>1,
      //          'service_status_id'=>3,
      //          'service_type_id'=>3,
      //          'service_details'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce quis lacus in eros hendrerit laoreet sit amet hendrerit turpis. Integer in venenatis orci, at congue augue. Fusce tellus mauris, vulputate a pulvinar eget, auctor vitae mauris. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Sed ex nulla, auctor sit amet libero vel, aliquam interdum sem. Nulla lorem odio, ullamcorper in ornare nec, congue a mi. Aenean consectetur, mi vel pharetra dignissim, eros orci fermentum leo, nec blandit purus urna eget felis. Nunc sit amet dui dapibus, malesuada risus non, pulvinar nulla. Suspendisse sit amet odio ac eros ultrices placerat sed quis nisi. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Proin condimentum, erat at condimentum tincidunt, arcu massa sollicitudin lorem, nec consectetur massa diam in est. Nam ac mi dolor. Nulla posuere dignissim tellus ut sodales. Integer scelerisque tortor at diam euismod, eu ornare arcu vulputate.',
      //          'crea_by'=>'admin',
      //          'service_duration' => null,
      //          'svc_end' => null,
      //          'svc_start' => Carbon::now()->format('Y-m-d H:i:s'),
      //          'created_at' => Carbon::now()->format('Y-m-d H:i:s')
      //       ],
      //       ]);


        $arrayFname = array('Edwin','Clancy','Shelia','Joye','Lilly','Dani','Cassidy','Damian','Liddy','Elliot','Brooke','Willow');
        $arrayMname = array( 'Wade','Foster','Atkinson','Wickham','Winchester','Waters','Pitts','Paxton','Williams','Victors');
        $arrayLname = array('Wade','Foster','Atkinson','Wickham','Winchester','Waters','Pitts','Paxton','Williams','Victors');

        $now = Carbon::now();
        $batches = Batch::whereDate('sched_date', '>=' , $now)->pluck('id')->all();
        // $count = range(1, 5);
        $count = 3;
        for ($i = 0; $i < $count; $i++) {
            $fname = $arrayFname[array_rand($arrayFname)];
            $mname = $arrayMname[array_rand($arrayMname)];
            $lname = $arrayLname[array_rand($arrayLname)];
            $rand_bath = $batches[array_rand($batches)];
            //Generate a random year using mt_rand.
            $year= mt_rand(1000, date("Y"));
            //Generate a random month.
            $month= mt_rand(1, 12);
            //Generate a random day.
            $day= mt_rand(1, 28);
            //Using the Y-M-D format.
            $randomDate = $year . "-" . $month . "-" . $day;
            
            $client = Client::create([
                           'fname'=>$fname,
                           'mname'=>$mname,
                           'lname'=>$lname,
                           'fullname'=>$fname." ".$mname." ".$lname,
                           'contact_no'=>'+639'.rand(1,1000000000),
                           'member_status'=>0,
                           'member_no'=>Str::random(3).'-'.Str::random(3).'-'.Str::random(3).'-'.Str::random(3),
                           'seminar_pref'=>0,
                           'gmail_address'=>Str::random(10).'@gmail.com',
                           'branch_id'=> mt_rand(1, 4),
                           'gender'=>0,
                           'birthdate'=>Carbon::parse($randomDate),
                           'batch_id'=>$rand_bath,
                           'age'=>31,
                           'street'=>'178 M H del pilar street ',
                           'barangay_primary'=>12633,
                           'city_primary'=>484,
                           'province_primary'=>21,
                           'region_primary'=>4,
                           'barangay_id'=>'045808011',
                           'city_id'=>'045808',
                           'province_id'=>'0458',
                           'region_id'=>'04',
                           'occupation_id'=>1,
                           'of_tag'=> 1,
                           'purpose_tag'=> 1,
                           'marketing_channel_id'=>4,
                           'staff_referral_cd'=>Str::random(10),
                           'data_consent_fl'=>1,
                           'sched_stat'=>0,
                           'crea_by'=>'admin'
                        ]);
            $pmes =     DB::table('pmesmasters')->insert(
                        [
                        [
                            'client_id' => $client->id,
                            'batch_id' => $rand_bath,
                            'serv_status' => 1,
                            'crea_by'=>'admin',
                            'created_at' => Carbon::now()
                        ]
                        ]);
            $curparticipant = Batch::where('id', $rand_bath)->first()->participant;
            Batch::whereId($rand_bath)->update([
                'participant' => $curparticipant+1,
            ]);
        }
    }
}
