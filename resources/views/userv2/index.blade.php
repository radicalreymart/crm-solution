@extends('layouts.app', [
    'class' => 'sidebar-mini ',
    'namePage' => 'User Index',
    'activePage' => 'user-management',
    'activeNav' => 'settings',
])

@section('indexdeleteall')
  <script src="{{ asset('assets') }}/js/indexdeleteall.js"></script>
@endsection
@section('content')
    @include('auth.passwords.confirmmodal')

<div class="panel-header panel-header-sm">
  </div>
  <div class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header">
            @include('flash-message')
                <div class="float-left">
                    <h4 class="card-title">User Master</h4>
                </div>
                <div class="float-right">
                      <a href="javascript:void(0)" title="Add User" class="btn btn-success" id="addNewUser">
                        <li class="fas fa-plus-circle fa-lg" aria-hidden="true"></li>
                      </a>
                    <button class="btn btn-danger delete_all" data-url="{{ url('DeleteAllUser') }}">Delete All Selected</button>
                </div>
          </div>
          <div class="card-body">
            <div id="modalCatcher" class="table-responsive">
              <input type="hidden" value="{{url('/')}}" id="url" name="url">
                <table id="datatablesSimple" class="table">
                <thead class=" text-primary">
                    <th><input type="checkbox" id="master"></th>
                    <th >Action</th>
                    <th>User Name</th>
                    <th>Email</th>
                    <th>Branch</th>
                    <th>Department</th>
                    <th>Role</th>
                    <th>Created By</th>
                    <th>Date Created</th>
                    <th>Updated By</th>
                    <th>Date Updated</th>
                </thead>
                <tbody>
                    @if($users->count())
                    @foreach ($users as $data)
                    <tr>
                    <td ><input type="checkbox" class="sub_chk" data-id="{{$data->id}}"></td>
                    <td>
                      <a href="javascript:void(0)" title="Edit User" class="edit" data-id="{{ $data->id }}">
                        <i class="fas fa-edit  fa-lg"></i>
                      </a>
                    @csrf
                    @method('DELETE')
                    </td>
                    <td>{{ $data->uname }}</td>
                    <td>{{ $data->email }}</td>
                    @if(isset($data->branch->name))
                    <td>{{ $data->branch->name }}</td>
                    @else
                    <td>Enduser</td>
                    @endif
                    @if(isset($data->department->cd))
                    <td>{{ $data->department->cd }}</td>
                    @else
                    <td>Enduser</td>
                    @endif
                    @if($data->role_id == 1 && $data->employee_role == 0)
                    <td>User</td>
                    @elseif($data->employee_role == 1 && $data->role_id == 1)
                    <td>User BM</td>
                    @else
                    <td>Admin</td>
                    @endif
                    <td>{{ $data->crea_by }}</td>
                    <td>{{ date_format($data->created_at, 'm-d-Y') }}</td>

                    @if(isset($data->upd_by))
                    <td>{{ $data->upd_by }}</td>
                    @else
                    <td></td>
                    @endif
                    @if(isset($data->updated_at))
                    <td>{{ date_format($data->updated_at, 'jS M Y') }}</td>
                    @else
                    <td></td>
                    @endif

                    </tr>
                    @endforeach
                    @endif
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      
    </div>
  </div>
    <!-- boostrap model -->
    <div class="modal fade" id="ajax-user-model" aria-hidden="true" data-backdrop="static" data-keyboard="false">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title" id="ajaxUserModel"></h4>
            <button type="button" class="btn btn-default close-modal" id="close-modal" >Close</button>
          </div>
          <div class="modal-body">
            <form action="javascript:void(0)" class="form-horizontal" method="POST">
              <!-- {{ csrf_field() }} -->
              <!-- <input type="hidden" name="_method" value="POST"> -->
              <input type="hidden" name="id" id="id">
              
              <div class="form-group row">
                  <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('User Name') }}</label>

                  <div class="col-md-6">
                      <input id="uname" type="text" wire:model="uname" class="form-control @error('uname') is-invalid @enderror" name="uname" value="{{ old('uname') }}" required autocomplete="uname" autofocus />

                      @error('uname')
                          <span class="invalid-feedback" role="alert">
                              <strong>{{ $message }}</strong>
                          </span>
                      @enderror
                  </div>
              </div>

              <div class="form-group row">
                  <label for="name" class="col-md-4 col-form-label text-md-right">Branch</label>
                  <div class="col-md-6">
                    <select id="branch_id" name="branch_id" class="form-control">
                         <option value="">Select Branch</option>
                    @foreach($branch as $item)
                    <option value="{{$item->id}}">{{$item->name}}</option>
                    @endforeach
                    </select>
                     <span class="text-danger">
                        <strong>{{$errors->first('branch_id') ?? null}}
                        </strong>
                    </span>
                  </div>
              </div>  
              <div class="form-group row">
                  <label for="name" class="col-md-4 col-form-label text-md-right">Department</label>
                  <div class="col-md-6">
                    <select id="department_id" name="department_id" class="form-control">
                         <option value="">Select Department</option>
                    @foreach($department as $item)
                    <option value="{{$item->id}}">{{$item->name}}</option>
                    @endforeach
                    </select>
                     <span class="text-danger">
                        <strong>{{$errors->first('department_id') ?? null}}
                        </strong>
                    </span>
                  </div>
              </div>  

              <div class="form-group row">
                  <label for="employee_role" class="col-md-4 col-form-label text-md-right">{{ __('Employee Designation') }}</label>

                  <div class="col-md-6">
                      <select id="employee_role" name="employee_role" class="form-control">
                          <option value="">Select Designation</option>
                          <option value="0">Branch Employee</option>
                          <option value="1">Branch Manager</option>
                      </select>
                      @error('employee_role')
                          <span class="invalid-feedback" role="alert">
                              <strong>{{ $message }}</strong>
                          </span>
                      @enderror
                  </div>
              </div>

              <div class="form-group row">
                  <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('Email') }}</label>

                  <div class="col-md-6">
                      <input id="email" wire:model="email" type="text" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus />

                      @error('email')
                          <span class="invalid-feedback" role="alert">
                              <strong>{{ $message }}</strong>
                          </span>
                      @enderror
                  </div>
              </div>
              <div class="form-group row">
                  <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                  <div class="col-md-6">
                      <input id="password_reg"  type="password" class="form-control @error('password') is-invalid @enderror" name="password_reg" required autocomplete="new-password">

                      @error('password_reg')
                          <span class="invalid-feedback" role="alert">
                              <strong>{{ $message }}</strong>
                          </span>
                      @enderror
                  </div>
              </div>

              <div class="form-group row">
                  <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                  <div class="col-md-6">
                      <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                  </div>
              </div>

              <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class=" btn btn-primary btn-save" id="btn-save">{{ __('Save')}}
                  <i hidden id="loading" class="fas fa-spinner fa-spin"></i>
                </button>
              </div>
            </form>
          </div>
          <div class="modal-footer">
            
          </div>
        </div>
      </div>
    </div>
<!-- end bootstrap model -->

<script>
  // // window.addEventListener("load", function() {
  //   window.addEventListener('DOMContentLoaded', event => {
  //   // Simple-DataTables
  //   // https://github.com/fiduswriter/Simple-DataTables/wiki
    
  //   const datatablesSimple = document.getElementById('datatablesSimple');
  //   if (datatablesSimple) {
  //       new simpleDatatables.DataTable(datatablesSimple, {
  //   fixedHeight: true
  //       });
  //   }
  // });
$(document).ready(function(){
  $('#datatablesSimple').DataTable({

  });
  const swalWithBootstrapButtons = Swal.mixin({
                  customClass: {
                    confirmButton: 'btn btn-success',
                    cancelButton: 'btn btn-danger'
                  },
                  buttonsStyling: false
                })
  $('.close-modal').click(function () {
    var modal_array = 
    [
      '#ajax-user-model'
    ];
       closeModal(modal_array);
       // $('#ajax-user-model').modal('hide');
    });
 $.clearInput = function () {
        $('insert_form').find('input[type=text], input[type=password], input[type=number], input[type=email], select, textarea').val('');
    };
     $('.modal').on('hidden.bs.modal', function(){
             // window.location.reload();
             // $('#insert_form').removeData('bs.modal')
    $.clearInput();
    $(this).find('form')[0].reset();
    });

    $.ajaxSetup({
        headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('#addNewUser').click(function () {
       $('#addEditUserForm').trigger("reset");
       $('#ajaxUserModel').html("Add User");
       $('#ajax-user-model').modal('show');
    });
      // $('#modalCatcher').on('click', '#btn-save', function () {
    $('#btn-save').click(function (event) {
         // alert('hello');
          try{
          var id = $("#id").val();
          var uname = $("#uname").val();
          var branch_id = $("#branch_id").val();
          var department_id = $("#department_id").val();
          var employee_role = $("#employee_role").val();
          var email = $("#email").val();
          var password = $("#password_reg").val();
          var password_confirm = $("#password-confirm").val();
          // alert(id+cd+description+trxn_type_id);
          $("#btn-save").html('Please Wait...');
          $("#btn-save"). attr("disabled", true);           
              }
            catch(err) {
            swalWithBootstrapButtons.fire({
                  title: 'Error!',
                  text: err,
                  icon: 'warning',
                  confirmButtonText: 'Confirmed',
                }).then((result) => {
             // window.location.reload();
            $("#btn-save").html('Submit');
            $("#btn-save"). attr("disabled", false);
                })
            }

        // event.preventDefault();
        // ajax
        $.ajax({
            type:"POST",
            url: "{{ url('add-update-userv2') }}",
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            data: {
              id:id,
              uname:uname,
              branch_id:branch_id,
              department_id:department_id,
              employee_role:employee_role,
              email:email,
              password:password,
              password_confirm:password_confirm,
            },
            dataType: 'json',
            success: function(res){
              // alert('success');
             swalWithBootstrapButtons.fire({
                  title: 'Success!',
                  icon: 'success',
                  confirmButtonText: 'Confirmed',
                }).then((result) => {
             window.location.reload();})
              // alert(JSON.stringify(res));
            $("#btn-save").html('Submit');
            $("#loading").attr("hidden", true);
            $("#btn-save").attr("disabled", false);
           }
          //   ,error: function(xhr, status, error){
          //  var errorMessage = xhr.status + ': ' + xhr.statusText
          //  alert('Error? - ' + errorMessage);
          // }          
           ,error: function (data) {
             var errors = [];
           $.each( data.responseJSON['errors'], function( key, value ) {
                errors += value+'<br>';
            });
            swalWithBootstrapButtons.fire({
                  title: 'Error!',
                  html: errors,
                  icon: 'warning',
                  confirmButtonText: 'Confirmed',
                }).then((result) => {
                 // window.location.reload();
                    $("#btn-save").html('Submit');
                    $("#btn-save"). attr("disabled", false);
                })
           //   swalWithBootstrapButtons.fire({
           //        title: 'Error!',
           //        text: data.responseText,
           //        icon: 'warning',
           //        confirmButtonText: 'Confirmed',
           //      }).then((result) => {
           //   // window.location.reload();
           //  $("#btn-save").html('Submit');
           //  $("#loading").attr("hidden", true);
           //  $("#btn-save").attr("disabled", false);
           // })
            }
        });

    });
     $('#modalCatcher').on('click', '.edit', function () {

        var id = $(this).data('id');
                var base_path = $("#url").val();
        // ajax
        //   alert( base_path );
        $.ajax({
            type:"POST",
            url: base_path+"/edit-userv2",
            data: { id: id },
            dataType: 'json',
            success: function(res){
                // alert(JSON.stringify(res));
                // var date = res.sched_date->format('m-d-Y');
              $('#ajaxUserModel').html("Edit user");
              $('#ajax-user-model').modal('show');
              $('#id').val(res.id);
              $("#uname").val(res.uname);
              $("#branch_id").val(res.branch_id);
              $("#department_id").val(res.department_id);
              $("#employee_role").val(res.employee_role);
              $("#email").val(res.email);
              $("#password_reg").val(res.password);
              $("#password-confirm").val(res.password);
           },
            error: function (data) {
                 alert(data.responseText);
             }
        });

    });


});
</script>
@endsection

   

