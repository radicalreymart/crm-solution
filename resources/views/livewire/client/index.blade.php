  <div class="content">
<div class="panel-header panel-header-sm">
  </div>
    <div class="row">
      @include('livewire.client.update')
    @include('auth.passwords.confirmmodal')
      <div class="col-md-12">
        <div class="card">
          <div class="card-header">
              @include('flash-message')
                <div class="float-left">
                    <h4 class="card-title">Client</h4>
                </div>
                <div class="float-right">
                  @include('livewire.client.create')
                    <button class="btn btn-danger delete_all" data-url="{{ url('DeleteAllClient') }}">Delete All Selected</button>
                </div>
          </div>
          <div class="card-body">
            <div class="table-responsive wrapper1">
                        <input type="hidden" value="{{url('/')}}" id="url" name="url">
              <table id="datatablesSimple" class="table">
            <input type="text"  class="form-control" placeholder="Search Client Name or Email or Contact Number" wire:model="searchTerm" />
                <thead class=" text-primary">
                    <th class="sticky-col first-col"><input type="checkbox" id="master"></th>
                    <th class="sticky-col second-col">Action</th>
                    <!-- <th class="sticky-col third-col"  >Client ID</th> -->
                    <th class="sticky-col third-col" >Full Name</th>
                    <th class="sticky-col fourth-col" >Contact Number</th>
                    <th class="sticky-col fifth-col">Member Status</th>
                    <th >Service Status</th>
                    <th >Preferred Schedule</th>
                    <th >Seminar Preference</th>
                    <th >Email Address</th>
                    <th >Office</th>
                    <th >Gender</th>
                    <th >Age</th>
                    <th >Barangay</th>
                    <th >City</th>
                    <th >Province</th>
                </thead>
                <tbody>
                    @if($client->count())
                    @foreach ($client as $data)
                    <tr>
                    <td class="sticky-col first-col"><input type="checkbox" class="sub_chk" data-id="{{$data->id}}"></td>
                    <td class="sticky-col second-col">
                    <a href=""  data-toggle="modal" data-target="#updateModal" wire:click="edit({{ $data->id }})" >
                      <i class="fas fa-edit  fa-lg"></i> 
                    </a>
                    </td>
                    <!-- <td  class=" sticky-col third-col" >{{$data->id}}</td> -->
                    <td class="sticky-col third-col">{{$data->fullname}}</td>
                    <td class="sticky-col fourth-col">{{$data->contact_no}}</td>
                    @if($data->member_status == 0)
                    <td class="sticky-col fifth-col">New Member</td>
                    @elseif($data->member_status == 1)
                    <td class="sticky-col fifth-col">Returning Member</td>
                    @else
                    <td class="sticky-col fifth-col">From Associate Member</td>
                    @endif
                    @if($data->sched_stat == 0)
                    <td>Batch Pending</td>
                    @else
                    <td>Email Sent</td>
                    @endif
                    @if(is_null($data->batch_id))
                    <td></td>
                    @else
                    <td>{{\Carbon\Carbon::parse($data->batch->sched_date)->format('m-d-Y')}}</td>
                    @endif
                    @if($data->seminar_pref == 0)
                    <td>Online Video Conference</td>
                    @else
                    <td>Walk-in / Physical Appearance</td>
                    @endif
                    <td>{{$data->gmail_address}}</td>
                    <td>{{$data->branch->name}}</td>
                    @if($data->gender == 0)
                    <td>Male</td>
                    @else
                    <td>Female</td>
                    @endif
                    <td>{{$data->age}}</td>
                    <td>{{$data->barangay->name}}</td>
                    <td>{{$data->city->name}}</td>
                    <td>{{$data->province->name}}</td>
                   
                    @endforeach
                    @endif
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      
    </div>
  </div>