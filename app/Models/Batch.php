<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\pmes;


class Batch extends Model
{
    protected $table = 'batch';
    use HasFactory;
    protected $fillable = [
        'batch_cd',
        'batch_stat',
        'sched_date',
        'online_vid_tool',
        'meeting_cd',
        'resched_remarks',
        'meeting_pw',
        'crea_by',
        'created_at',
        'upd_by',
        'updated_at',
        'soft_deleted_at',
    ];
    // public function pmes()
    // {
    //     return $this->hasOne(pmes::class);
    // }
}
