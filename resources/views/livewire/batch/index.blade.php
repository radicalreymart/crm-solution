  <div class="content">
<div class="panel-header panel-header-sm">

</div>
    @include('livewire.batch.update')
    @include('auth.passwords.confirmmodal')
    @include('livewire.batch.participant')
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header">
            @include('flash-message')
                <div class="float-left">
                    <h4 class="card-title">Batch</h4>
                </div>
                <div class="float-right">
                    <a class="btn btn-warning" href="{{ url('/batch/history') }}">
                        Batch History
                    </a>

                    @include('livewire.batch.create')
                    
                    
                    @if(Auth::user()->employee_role == 0)
                    <button  class="btn btn-danger delete_all"  data-url="{{ url('DeleteAllBatch') }}">Delete All Selected</button>
                    @endif
                </div>
          </div>
            <div class="card-body">
            <div class="table-responsive">
              <table id="datatablesSimple"  class="table">
            <input type="text"  class="form-control" placeholder="Search Batch Code" wire:model="searchTerm" />
                <thead class=" text-primary">
                    <th ><input type="checkbox" id="master"></th>
                    <th >Action</th>
                    <th >Batch Code</th>
                    <th>Schedule Date</th>
                    <th>Schedule Time</th>
                    <th>Pref. Application</th>
                    <th class="meeting-col">Meeting Link</th>
                    <th class="shorten_text">Reschedule Remarks</th>
                    <th>Meeting Password</th>
                    <th>Participant Number</th>
                    <th>Batch Status</th>
                    <th>Date Created</th>
                    <th>Created By</th>
                </thead>
                <tbody>
                    @if($batch1->count())
                    @foreach ($batch1 as $data)
                    <tr>
                    <td ><input type="checkbox" class="sub_chk" data-id="{{$data->id}}"></td>
                    <td>
                    <a href="" data-toggle="modal"  data-target="#updateModal" wire:click="edit({{ $data->id }})" >
                      <i class="fas fa-edit  fa-lg"></i> 
                    </a>
                    
                    <!-- @csrf
                    @method('DELETE') -->
                    </td> 
                    <td>{{ $data->batch_cd }}</td>
                    
                    <td>{{ \Carbon\Carbon::parse($data->sched_date)->format('m-d-Y')}}</td>
                    
                    <td>{{ \Carbon\Carbon::parse($data->sched_date)->format('h:i a')}}</td>
                    
                    @if($data->online_vid_tool == 0)
                    <td>Google Meet</td>
                    @endif
                    
                    <td class="meeting-col"><a href="https://meet.google.com/{{$data->meeting_cd}}">https://meet.google.com/{{$data->meeting_cd}}</a></td>

                    <td>{{$data->resched_remarks}}</td>
                    
                    @if(is_null($data->meeting_pw))
                    <td></td>
                    @else
                    <td>{{$data->meeting_pw}}</td>
                    @endif
                    
                    @if($data->participant != 0)
                    <td><button type="button" class="btn btn-info" value="{{$data->id}}" data-toggle="modal" data-target="#participantModal" wire:click="participant({{ $data->id }})">
                        {{$data->participant}}
                    </button></td>
                    @else
                    <td></td>
                    @endif

                    @if(\Carbon\Carbon::parse($data->sched_date)->format('m/d/Y') == $now)
                    <td>Today</td>
                    @elseif(\Carbon\Carbon::parse($data->sched_date)->format('m/d/Y') <= $now)
                    <td>Expired</td>
                    @elseif($data->resched_remarks !== '.')
                    <td>Rescheduled</td>
                    @elseif($data->participant < 5)
                    <td>Insufficient Participant</td>
                    @else
                    <td>Pending</td>
                    @endif
                    
                    <td>{{ date_format($data->created_at, 'm-d-Y') }}</td>
                    
                    <td>{{ $data->crea_by }}</td>
                    </tr>
                    @endforeach
                    @endif
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      
    </div>
  </div>


