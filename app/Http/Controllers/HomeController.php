<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\PreLoanApplication;
use App\Models\UserMember;
use App\Models\MIUF;
use App\Models\OnlinePymntTrxnMaster;
use App\Models\OnlinePymntTrxnDetail;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
    //     return view('home');

        if(!Auth::guest()){
            if(auth()->user()->role_id == 3){
                return redirect()->route('enduser.home');
            }
            else
            {
                return view('/home');
            }
        }else{
                return view('/auth/login');
        }
    }
    public function memberHome()
    {
        $user_id = auth()->user()->id;
        $usermember_id = UserMember::where('user_id', $user_id)->pluck('id');
        $preloan1 = PreLoanApplication::where('usermember_id', $usermember_id[0])->get()->toArray();
        $optm = OnlinePymntTrxnMaster::where('usermember_id', $usermember_id[0])->get()->toArray();
        // dd($optm);
        return view('enduser.home', compact('optm','preloan1'));
    }
}
