<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Client;
use App\Models\Batch;
use App\Models\Branch;
use App\Models\Region;
use App\Models\Province;
use App\Models\City;
use App\Models\Barangay;
use App\Models\Occupation;
use Yajra\Address\HasAddress;

class PMES extends Model
{
    use HasFactory;use HasFactory;
    protected $table = 'pmesmasters';
    protected $fillable = [
     'client_id'
    ,'client_branch'
    ,'batch_id'
    ,'batch_sched_date'
    ,'serv_status'
    ,'membership'
    ,'crea_by'
    ,'upd_by'
    ,'soft_deleted_at'
      
    ];
     public function client()
    {
        return $this->belongsTo('App\Models\Client','client_id','id');
    }
     public function batch()
    {
        return $this->belongsTo('App\Models\Batch','batch_id','id');
    }
    
    public function branch()
    {
        return $this->belongsTo('App\Models\Branch','client_branch','id');
    }
    public function region()
    {
        return $this->belongsTo('App\Models\Region','region_id','client.region_id');
    }
    public function province()
    {
        return $this->belongsTo('App\Models\Province','province_id','client.province_id');
    }
    public function city()
    {
        return $this->belongsTo('App\Models\City','city_id','client.city_id');
    }
    public function barangay()
    {
        return $this->belongsTo('App\Models\Barangay','barangay_id','client.code');
    }
     public function occupation()
    {
        return $this->belongsTo('App\Models\Occupation','occupation_id','id');
    }
       
}
