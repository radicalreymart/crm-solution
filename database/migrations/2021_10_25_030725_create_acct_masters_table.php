<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAcctMastersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('acct_masters', function (Blueprint $table) {
            $table->id();
            $table->string('acct_cd');
            $table->text('description');
            $table->unsignedBigInteger('trxn_type_id');
            $table->foreign('trxn_type_id', 'payment_fk_trxn_type_id')->references('id')->on('transaction_types');
            $table->string('crea_by');
            $table->string('upd_by')->nullable();
            $table->datetime('soft_deleted_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('acct_masters');
    }
}
