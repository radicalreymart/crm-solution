<?php

namespace App\Http\Controllers;

use App\Models\MarketingChannel;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\UserRequest;

class MarketingChannelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $marketingChannel  = MarketingChannel::whereNull('soft_deleted_at')->get();
        return view('marketingChannel.index')
            ->with('marketingChannel', $marketingChannel );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('marketingChannel.create');
     
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request);
        $request->validate([
            'cd' => 'required',
            'name' => 'required',
        ]);
       MarketingChannel::create([
             'cd' => $request['cd'],
             'name' => $request['name'],
            'crea_by' => auth()->user()->uname,
        ]);
       
       
        return redirect()->route('marketingChannel.index')
            ->with('success', 'Marketing Channel created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Project  $project
     * @return \Illuminate\Http\Response
     */
    // public function show($id, User $marketingChannel)
    // {
    //     $marketingChannel = User::find($id);
        
        
       
    //       return view('userTable.show', [
    //     'user' => $marketingChannel
    //     ]);
    // }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $marketingChannel  = MarketingChannel::find($id);
       
       return view('marketingChannel.edit', [
        'marketingChannel' => $marketingChannel 
        ]);

    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request)
    {
        $rules = array(
            'cd' => 'required',
            'name' => 'required',
        );
         $validator = Validator::make($request->all(), $rules);
         $marketingChannel  = MarketingChannel::find($id);
            $marketingChannel ->cd      = $request->get('cd');
            $marketingChannel ->name       = $request->get('name');
            $marketingChannel ->updated_at      = Carbon::now();
            $marketingChannel ->upd_by = auth()->user()->uname;
            $marketingChannel ->save();
       
        return redirect()->route('marketingChannel.index')
            ->with('success', 'Marketing Channel updated successfully');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Project  $project
     * @return \Illuminate\Http\Response
     */
    
    public function destroy($id)
    {
        $marketingChannel  = MarketingChannel::find($id);
        $marketingChannel ->delete();

        return response()->json(['success'=>"Marketing Channel deleted successfully.", 'tr'=>'tr_'.$id]);
 
    }
    public function deleteAll(Request $request)
    { 
        $ids = $request->ids;
        $mcs = MarketingChannel::whereIn('id',explode(",",$ids))->whereNull('soft_deleted_at')->get();
        if($mcs->count())
        {
            foreach($mcs as $data)
            {
                $mc = MarketingChannel::find($data->id);
                $mc->soft_deleted_at = Carbon::now();
                $mc->save();
            }
        }
        return response()->json(['success'=>"Marketing Channel deleted successfully."]);
    }
}
