<?php

namespace App\Http\Controllers;

use Alert;
use App\Models\Client;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Yajra\Address\HasAddress;
use App\Models\Barangay;
use App\Models\City;
use App\Models\Province;
use App\Models\Region;
use App\Models\Country;
use App\Models\Occupation;
use App\Models\MarketingChannel;
use App\Models\Branch;
use App\Models\Batch;
use App\Models\PMES;
use App\Models\STM;
use Yajra\Address\AddressServiceProvider;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendMail;
use App\Mail\VerificationMail;
use Livewire\Component;
use Livewire\WithPagination;
use Illuminate\Support\Str;

class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    // use HasAddress;

    
    public function index()
    {
        $department = auth()->user()->department_id;
        $isAdmin = auth()->user()->role_id;
        $branch = auth()->user()->branch_id;
        if($department == 1 || $isAdmin == 0){
            $occupations = Occupation::all();
            $branches  = Branch::whereNull('soft_deleted_at')
            ->Where('cd', '!=', 'TRD')
            ->Where('cd', '!=', 'FR')
            ->Where('cd', '!=', 'ADMIN')
            ->get();
            $marketingChannel = MarketingChannel::all();
            $now = Carbon::now();
            $regions = Region::all();
            $batch_sched = Batch::all();
            $batches = Batch::whereDate('sched_date', '>', $now)->whereNull('soft_deleted_at')->get();
           $client1 = Client::with( 'marketingChannel', 'branch', 'batch');
           if($branch == 9){
               $client1->whereNull('soft_deleted_at');
           }else{
               $client1->where('branch_id', $branch)
               ->whereNull('soft_deleted_at');
           }
             $client =   $client1->orderby('created_at', 'desc')->get();
             return view('clientv2.index', compact('client', 'occupations', 'branches', 'marketingChannel', 'now', 'regions', 'batches', 'batch_sched'));
        }else{
            Alert::error("You do not have the permission to access this page.");
            return redirect()->route('home');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function create(Request $request)
    {  
          $client = Client::all();
          // dd($client);
          $regions = Region::all();
          $occupations = Occupation::all();
          $branches  = Branch::whereNull('soft_deleted_at')
            ->Where('cd', '!=', 'TRD')
            ->Where('cd', '!=', 'FR')
            ->Where('cd', '!=', 'ADMIN')
            ->get();
          $marketingChannel = MarketingChannel::all();
          $now = Carbon::now();
          $batches = Batch::whereDate('sched_date', '>=', $now)->whereNull('soft_deleted_at')->get();
          // dd($batches);

      return view('client.create', compact(
        'occupations',
        'regions',
        'branches',
        'marketingChannel',
        'batches',
        'client'));
      
    }


    public function verifieduser(Request $request)
    {
        $of_tag = $request->self.",".$request->relative;
        $checked = $request->has('data_consent_fl') ? 1 : 0;
        $intChannel = intval($request['marketing_channel']);
        // print_r($request);
        if($request['occupation'] == 0){
            $occupation_id = null;
        }else{
            $occupation_id = $request['occupation'] ;
        }
         $client = Client::whereId($request['id'])->update([
             'fname' => $request['fname'],
             'mname' => $request['mname'],
             'lname' => $request['lname'],
             'fullname' => $request['fname'].' '. $request['mname'].' '.$request['lname'],
             'contact_no' => '+63'.$request['contact_no'],
             'gmail_address' => $request['gmail_address'],
             'gender' => $request['gender'],
             'birthdate' => $request['birthdate'],
             'age' => $request['age'],
             'region_id' => $request['region_id'],
             'province_id' => $request['province_id'],
             'city_id' => $request['city_id'],
             'barangay_id' => $request['barangay_id'],
             'street' => $request['street'],
             'occupation_id' => $occupation_id,
             'of_tag' => $of_tag,
            'marketing_channel_id' =>    $intChannel,
            'purpose_tag' =>  $request['purpose_tag'],
             'member_status' => $request['member_status'],
             'member_no' => $request['member_no'],
            'staff_referral_cd' =>    $request['staff_referral_cd'],
             'seminar_pref' => $request['seminar_pref'],
             'branch_id' => $request['branch_id'],
             'batch_id' => $request['batch_id'],
            // 'sched_stat' =>   $sched_stat,
            'data_consent_fl' =>   $checked,
            // 'country_id' =>    1,
            'upd_by' => 'enduser',
            'updated_at' => Carbon::now()
        ]);
         $request->session()->flash('success', 'Account updated successfully!');
         return response()->json(['success' => 'success'], 200);
    }
    public function endusercreate(Request $request)
   {
      $client = Client::all();
      $regions = Region::all();
      $occupations = Occupation::all();
      $branches  = Branch::whereNull('soft_deleted_at')
        ->Where('cd', '!=', 'TRD')
        ->Where('cd', '!=', 'FR')
        ->Where('cd', '!=', 'ADMIN')
        ->get();
      $marketingChannel = MarketingChannel::all();
      $now = Carbon::now();
      $batches = Batch::whereDate('sched_date', '>', $now)->whereNull('soft_deleted_at')->get();

      return view('enduser.end_user', compact(
        'occupations',
        'regions',
        'branches',
        'marketingChannel',
        'batches',
        'client'
    ));
   }
   public function storeclient(Request $request)
   {    
        if(isset($request->id)){
            $upd_by = auth()->user()->uname;
            $updated_at = Carbon::now();
        }else{
            $upd_by = null;
            $updated_at = null;
        }
            $of_tag = $request->self.",".$request->relative;
           // dd($of_tag);
           $birthdate = Carbon::parse($request['birthdate'])->format('Y-m-d H:i:s.v');
           $message = ['gmail_address.regex' => 'Please put gmail as email address only',
                    'fname.required' => 'First name is required',
                    'lname.required' => 'Last name is required',
                    'contact_no.required' => 'Contact Number is required',
                    'gender.required' => 'Gender is required',
                    'birthdate.required' => 'Birthdate is required',
                    'region_id.required' => 'Region is required',
                    'province_id.required' => 'Province is required',
                    'city_id.required' => 'City is required',
                    'street.required' => 'Street is required',
                    'occupation.required' => 'Occupation is required',
                    'self.required' => 'Overseas worker inforamtion is required',
                    'relative.required' => 'Overseas worker inforamtion is required',
                    'marketingChannel.required' => 'Survey is required',
                    'purpose_tag.required' => 'Survey is required',
                    'member_status.required' => 'Member status is required',
                    'seminar_pref.required' => 'Seminar preference is required',
                    'branch_id.required' => 'Branch is required',
                    'batch_id.required' => 'Prefered Schedule is required',
                    ];
           $request->validate([
               'fname' => 'required',
               'lname' => 'required',
               'contact_no' => 'required|digits_between:1,10',
               'gmail_address' => 'required|email|regex:/gmail/',
               'gender' => 'required',
               'birthdate' => 'required|date',
               'age' => 'required',
               'region_id' => 'required',
               'province_id' => 'required',
               'city_id' => 'required',
               'street' => 'required',
               'occupation' => 'required',
               'self' => 'required',
               'relative' => 'required',
               'marketing_channel' => 'required',
               'purpose_tag' => 'required',
               'member_status' => 'required',
               'seminar_pref' => 'required',
               'branch_id' => 'required',
               'batch_id' => 'required',
           ],  $message);
               $checked = $request->has('data_consent_fl') ? 1 : 0;
               $intChannel = intval($request['marketing_channel']);
               if($request['occupation'] == 0){
                    $occupation_id = null;
                }else{
                    $occupation_id = $request['occupation'] ;
                }
               if(!Auth::guest()){
                        $crea_by = auth()->user()->uname;
                     }else{
                        $crea_by = 'enduser';
                     }

        $token = Str::random(20);
        $client_upd = [
            'id' => $request['id'],
        ];
        $client_create = [
                'fname' => $request['fname'],
                'mname' => $request['mname'],
                'lname' => $request['lname'],
                'fullname' => $request['fname'].' '. $request['mname'].' '.$request['lname'],
                'contact_no' => '+63'.$request['contact_no'],
                'gmail_address' => $request['gmail_address'],
                'gender' => $request['gender'],
                'fb_link' => $request['fb_link'],
                'token' => $token,
                'birthdate' => $birthdate,
                'age' => $request['age'],
                'region_id' => $request['region_id'],
                'province_id' => $request['province_id'],
                'city_id' => $request['city_id'],
                'barangay_id' => $request['barangay_id'],
                'street' => $request['street'],
                'occupation_id' => $occupation_id,
                'other_occupation' => $request['other_occupation'],
                'of_tag' => $of_tag,
               'marketing_channel_id' =>    $intChannel,
               'purpose_tag' =>  $request['purpose_tag'],
                'member_status' => $request['member_status'],
                'member_no' => $request['member_no'],
               'staff_referral_cd' =>    $request['staff_referral_cd'],
                'seminar_pref' =>  $request['seminar_pref'],
                'batch_id' => $request['batch_id'],
                'branch_id' => $request['branch_id'],
               'data_consent_fl' =>   $checked,
               'sched_stat' =>   0,
               'crea_by' =>    $crea_by,
               'created_at' => Carbon::now(),
            'upd_by'         => $upd_by,
            'updated_at'     => $updated_at,
           ];
          $client = Client::updateOrcreate($client_upd, $client_create);
          if($client->wasRecentlyCreated)
          {
           if($request['batch_id1'] == null){
                $pmesnobatch = PMES::create([
                     'client_id' => $client->id,
                     'client_branch' => $client->branch_id,
                     'batch_id' => null,
                     'batch_sched_date' => null,
                     'serv_status' => 0,
                     'crea_by' => $crea_by,
                ]);

                $details = [
                'id' => $client->id,
                'token' => $token,
                ];
                $email = $request['gmail_address'];
                // dd($meeting_cd);
                // Mail::to($email)->queue(new VerificationMail($details));
                $servicestatus = 1;
            }else{
                $participant = Batch::where('id', $request['batch_id1'])->first()->participant;
                $sched_date = Batch::where('id', $request['batch_id1'])->first()->sched_date;
              $pmes =   PMES::create([
                'client_id' => $client->id,
                'client_branch' => $client->branch_id,
                'batch_id' => $request['batch_id1'],
                'batch_sched_date' => $sched_date,
                'serv_status' => 1,
               'crea_by' => 'enduser',
               ]);

                // $servicestatus = 2;
                Batch::whereId($request['batch_id1'])->update([
                    'participant' => $participant+1,
                ]);
                $meeting_cd = Batch::where('id', $request['batch_id1'])->first()->meeting_cd;
                $vid_tool = Batch::where('id', $request['batch_id1'])->first()->online_vid_tool;
                // return response()->json($vid_tool);
                if($vid_tool == 0){
                    $link = 'https://meet.google.com/'.$meeting_cd;
                    $description = 'Online Pre-Membership Education Webinar';
                }elseif($vid_tool == 1){
                    $link = 'https://us05web.zoom.us/j/86189281691?pwd=aWJSMVRDM2xRQURIcTNVcXRrOTMvQT09';
                    $description = 'Online Pre-Membership Education Webinar';
                }else{
                    $link = null;
                    $description = 'Online Pre-Membership Education Seminar';
                }
                $start_date = \Carbon\Carbon::parse($sched_date)->format('Ymd\THis');
                $end_date = \Carbon\Carbon::parse($sched_date)->addHour(2)->format('Ymd\THis');
                $email = $request['gmail_address'];
                $details = [
                'vid_tool' => $vid_tool,
                    'link'   => $link,
                'meeting_cd' => $meeting_cd,
                'sched_date' => $sched_date,
                'resched_remarks' => '.',
                'text' => 'OLPMEW'.\Carbon\Carbon::parse($sched_date)->format('Ymd'),
                'description' => $description,
                'startdate' => $start_date,
                'enddate' => $end_date,
                'email' => $email,
                ];

                $editclient = Client::find($client->id);
                $editclient->sched_date = $sched_date;
                $editclient->email_verified_at = Carbon::now();
                $editclient->sched_stat = 2;
                $editclient->save();
                // dd($meeting_cd);
                Mail::to($email)->queue(new SendMail($details));
                }
          } 

            // $checkstm = STM::where('id', $pmes->id)->first();

            // if($checkstm == null) {
            // STM::create([
            //          'pmes_id' => $pmes->id,
            //          'service_details'=> '.',
            //         'service_status' => $servicestatus,
            //         'service_type_id' => 1,
            //          'crea_by' => $crea_by,
            //     ]);
            // }
           if($request->id != null){
            $request->session()->flash('success', 'Client updated successfully!');
            }else{
            $request->session()->flash('success', 'Client created successfully!');    
            }
            // dd($request->enduser);
            if($request->enduser != null){
                $request->session()->flash('success', 'Please check the email you entered to proceed!'); 
                Alert::success('Success', 'Please check the email you entered to proceed!')->autoClose(null);
                return redirect()->back();
            }else{
                // Alert::success('Success', 'Registration successfully!')->autoClose(null);
                return response()->json(['success' => 'success'], 200);
            }
            // return response()->json('hey');
   }

   public function verifyEmail(Request $request, $id, $token)
    {
        // $input = $request->all();
        $clientToken = Client::where('id', $id)->pluck('token')->first();
        if($token === $clientToken){
            $client = Client::find($id);

            $client->sched_stat = 1;
            $client->email_verified_at = Carbon::now();
            $client->save();
            // dd('verified');
            Alert::info('Thank you', 'Your email address is now verified. Please wait for an email regarding your PMES Schedule. Thank you!')->autoClose(null);
            return redirect()->route('login');
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $now = Carbon::now();
        $client = Client::find($id);
        $region = Region::all();
        $province = Province::where('region_id',$client->region_id)->get();
        $city = City::where('province_id',$client->province_id)->get();
        $barangay = Barangay::where('city_id',$client->city_id)->get();
        $branches  = Branch::whereNull('soft_deleted_at')
        ->Where('cd', '!=', 'TRD')
        ->Where('cd', '!=', 'FR')
        ->Where('cd', '!=', 'ADMIN')
        ->get();
        $batches = Batch::whereDate('sched_date', '>', $now)->get();
        $occupation = Occupation::all();
        $marketingChannel = MarketingChannel::all();
        // $pmes = PMES::with('client','batch')->where('client_id',$id)->get();
        // $pmes_id = PMES::where('client_id', $id)->first()->id;
        // $pmes = PMES::find($pmes_id);
        // $batch_id = PMES::where('client_id', $id)->first()->batch_id;
        $client_pref = Client::where('id', $id)->value('pref_date');
        if($client_pref == null){
            $pref_date = null;
        }else{
        $pref_date = Carbon::parse($client_pref)->format('Y-m-d'.'\T'.'H:i:s.v');
        }
        // dd($pref_date);
        return view('client.edit', [
        'client' => $client,
        'branches' => $branches,
        'barangay' => $barangay,
        'city' => $city,
        'province' => $province,
        'region' => $region,
        'occupation' => $occupation,
        'batches' => $batches,
        'marketingChannel' => $marketingChannel,
        'pref_date' => $pref_date
        ]);

    }
    public function editmodal(Request $request, Batch $batch)
    {
        //
        $where = array('id' => $request->id);
        $client  = Client::where($where)->first();
        $sched_date = Carbon::parse($batch->sched_date)->format('Y-m-d'.'\T'.'H:i:s.v');
        $json = array(
            'id' => $client['id'],
            'fname' => $client['fname'],
            'mname' => $client['mname'],
            'lname' => $client['lname'],
            'contact_no' => substr($client['contact_no'], -9),
            'gmail_address' => $client['gmail_address'],
            'gender' => $client['gender'],
            'birthdate' => Carbon::parse($client->birthdate)->format('Y-m-d'),
            'age' => $client['age'],
            'region_id' => $client['region_id'],
            'province_id' => $client['province_id'],
            'city_id' => $client['city_id'],
            'barangay_id' => $client['barangay_id'],
            'street' => $client['street'],
            'occupation_id' => $client['occupation_id'],
            'other_occupation' => $client['other_occupation'],
            'of_tag' => $client['of_tag'],
           'marketing_channel_id' =>    $client['marketing_channel_id'],
           'purpose_tag' =>  $client['purpose_tag'],
            'member_status' => $client['member_status'],
            'member_no' => $client['member_no'],
           'staff_referral_cd' =>    $client['staff_referral_cd'],
            'seminar_pref' =>  $client['seminar_pref'],
            'batch_id' => $client['batch_id'],
            'branch_id' => $client['branch_id'],
            'sched_stat' => $client['sched_stat'],
           'data_consent_fl' =>   $client['data_consent_fl'],
        );
         // $request->session()->flash('success', 'member updated successfully!');
        return response()->json($json);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $pref_date = Carbon::parse($request['pref_date'])->format('Y-m-d H:i:s.v');
        $checked = $request->has('data_consent_fl') ? 1 : 0;
        $intChannel = intval($request['marketing_channel']);
        // dd($request);
        if($request['batch_id'] == null){
            $sched_stat = 0;
        }else{
            $sched_stat = 1;
        }
        $message = ['gmail_address.regex' => 'Please put gmail as email adress only'];
        $request->validate([
            'fname' => 'required',
            'lname' => 'required',
            'contact_no' => 'required|digits_between:1,10',
            'gmail_address' => 'required|email|regex:/gmail/',
            'gender' => 'required',
            'birthdate' => 'required|date',
            'age' => 'required',
            'region_id' => 'required',
            'province_id' => 'required',
            'city_id' => 'required',
            'street' => 'required',
            'occupation' => 'required',
            'of_tag' => 'required',
            'marketing_channel' => 'required',
            'purpose_tag' => 'required',
            'member_status1' => 'required',
            'seminar_pref' => 'required',
            'branch_id' => 'required',
        ],  $message);
        // dd($request);
        $client = Client::whereId($id)->update([
             'fname' => $request['fname'],
             'mname' => $request['mname'],
             'lname' => $request['lname'],
             'fullname' => $request['fname'].' '. $request['mname'].' '.$request['lname'],
             'contact_no' => '+63'.$request['contact_no'],
             'gmail_address' => $request['gmail_address'],
             'gender' => $request['gender'],
             'birthdate' => $request['birthdate'],
             'age' => $request['age'],
             'region_id' => $request['region_id'],
             'province_id' => $request['province_id'],
             'city_id' => $request['city_id'],
             'barangay_id' => $request['barangay_id'],
             'street' => $request['street'],
             'occupation_id' => $request['occupation'],
             'of_tag' => $request['of_tag'],
            'marketing_channel_id' =>    $intChannel,
            'purpose_tag' =>  $request['purpose_tag'],
             'member_status' => $request['member_status1'],
             'member_no' => $request['member_no1'],
            'staff_referral_cd' =>    $request['staff_referral_cd1'],
             'seminar_pref' => $request['seminar_pref'],
             'pref_date' => $pref_date,
             'branch_id' => $request['branch_id'],
            'sched_stat' =>   $sched_stat,
            'data_consent_fl' =>   $checked,
            // 'country_id' =>    1,
            'upd_by' => auth()->user()->uname,
            'updated_at' => Carbon::now()
        ]);

        return redirect()->route('clientv2.index')
            ->with('success', 'Client updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $clientmaster = Client::find($id);
        $clientmaster->delete();

        return response()->json(['success'=>"Client Deleted successfully.", 'tr'=>'tr_'.$id]);
        // return redirect()->url('/search-boxClient/')
            // ->with('success', 'Service Transaction deleted successfully');
    }
    public function deleteAll(Request $request)
    {
         $ids = $request->ids;
        $clients = Client::whereIn('id',explode(",",$ids))->get();
        $pmesClients = PMES::wherein('client_id',explode(",",$ids))->whereNull('soft_deleted_at')->get();
        if($pmesClients->count())
        {
            $json = ['error' => 'Please delete PMES corresponding to this entry.'];
        }else
        {
            if($clients->count())
            {
                foreach($clients as $data)
                {
                    $client = Client::find($data->id);
                    $client->soft_deleted_at = Carbon::now();
                    $client->save();
                }
            }
            $json = ['success'=>"Client Deleted successfully."];
        }
        return response()->json($json);
    }
    public function getHiddenRegion($region_code)
    {
        $region =Region::where("region_id",$region_code)->get(["id"]);
        
        return response()->json($region);
    }
    public function getHiddenProvince($province_code)
    {
        $province_id =Province::where("province_id",$province_code)->get(["id"]);
        
        return response()->json($province_id);
    }
    public function getHiddenCities($city_code)
    {
        $city_id =City::where("city_id",$city_code)->get(["id"]);
        
        return response()->json($city_id);
    }
    public function getHiddenBarangays($barangay_code)
    {
        $barangay_id =Barangay::where("code",$barangay_code)->get(["id", "code"]);
        
        return response()->json($barangay_id);
    }
    public function getProvince($region_code)
    {
        $province =Province::where("region_id",$region_code)->get(["name","province_id","id"]);
        
        return response()->json($province);
    }
    public function getCities($province_code)
    {
        $cities = City::where("province_id",$province_code)->get(["name","id","city_id"]);
        return response()->json($cities);
    }
    public function getBarangays($city_code)
    {

        $barangay = Barangay::where("city_id",$city_code)->get(["name","code"]);
        return response()->json($barangay);
    }
    
    public function getClient1($localDatetime)
    {
        $localDatetime = explode('|', $localDatetime);
        // dd($localDatetime[1]);
        $client = Client::where("fullname",$localDatetime[0])->whereDate("birthdate",$localDatetime[1])->get();
        // $client = Client::whereDate("birthdate",$localDatetime)->first();
        return response()->json($client);
    }
}
