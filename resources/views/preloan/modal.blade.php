
<!-- Modal -->
 <div class="modal fade" id="ajax-preloan-model" aria-hidden="true" data-backdrop="static" data-keyboard="false">
      <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
            <h4 class="modal-title" id="ajaxPreloanModel"></h4>
            <button type="button" class="btn btn-default " id="close-modal" data-dismiss="modal">Close</button>
            </div>
            <div class="col-md-8">
            <p id="created_at"></p>
            </div>
            <div class="modal-body">
                    <form id="insert_form" name='formname' >
                            <input type="hidden" name="id" id="id">
                            <input type="hidden" value="{{url('/')}}" id="url" name="url">
                            <input type="hidden" id="file_path1" name="file_path1">
                       
                       <div class="form-group row">
                               <label for="name" class="col-md-4 col-form-label text-md-left">{{ __('Full Name') }}</label>
                           <div class="col-md-8">
                               <input id="fullname" type="text"  class="form-control @error('fullname') is-invalid @enderror" name="fullname" value="{{ old('fullname') }}" placeholder="Full Name" readonly autocomplete="fullname"  required autofocus />
                               @error('fullname')
                                   <span class="invalid-feedback" role="alert">
                                       <strong>{{ $message }}</strong>
                                   </span>
                               @enderror
                           </div>
                       </div> 
                       <!-- bday-age-gender -->
                            <div class="form-group row">
                                <label for="name" class="col-md-4 col-form-label float-left">{{ __('Birthdate') }}</label>
                                <div class="col-md-8">
                                <!--- date picker --->
                                      <input readonly type="date" class="form-control form-control-sm  @error('birthdate') is-invalid @enderror" id="birthdate" tabindex="-1" required name="birthdate"  autocomplete="birthdate">
                                    @error('birthdate')
                                      
                                    <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                
                                <label for="name" class="col-md-4 col-form-label float-left">{{ __('Gender') }}</label>
                                <div class="col-md-8">
                                    <select class="form-control @error('gender') is-invalid @enderror" id="gender"  type="text" name="gender" required  autocomplete="gender">
                                        <option value="">Select Gender</option>
                                        <option value="0" @if (old('gender') == '0') selected="selected" @endif>Male</option>
                                        <option value="1" @if (old('gender') == '1') selected="selected" @endif>Female</option>
                                        <option value="2" @if (old('gender') == '2') selected="selected" @endif>Rather not to say</option>
                                    </select>
                                    @error('gender')
                                    <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div> 
                            <!-- contact no-gmail address -->
                            <div class="form-group row">
                                <label for="name" class="col-md-4 col-form-label text-md-left">{{ __('Contact Number') }}</label>
                                <div class="col-md-8 form-cotrol input-group">
                                    <div class="input-group-prepend form-cotrol">
                                        <div style="background-color: #E3E3E3; width: 50px; color: black;" class="input-group-text form-cotrol"><i>{{ __('+63') }}</i></div>
                                    </div>
                                    <input id="contact_no" readonly wire:model="contact_no" type="text" onkeydown="return ( event.ctrlKey || event.altKey 
                                    || (47<event.keyCode && event.keyCode<58 && event.shiftKey==false) 
                                    || (95<event.keyCode && event.keyCode<106)
                                    || (event.keyCode==8) || (event.keyCode==9) 
                                    || (event.keyCode>34 && event.keyCode<40) 
                                    || (event.keyCode==46))" minlength="0"  required maxlength="10"  class="input-group form-control @error('contact_no') is-invalid @enderror" name="contact_no" value="{{ old('contact_no') }}" placeholder="9386166587 "  autocomplete="contact_no" title="Ex: 9386166587" />
                                    @error('contact_no')
                                    <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror    
                                </div>
                                
                            </div>
                            <div class="form-group row">
                                <label for="name" class="col-md-4 col-form-label text-md-left">{{ __('Gmail Address') }}</label>
                                <div class="col-md-8">
                                    <input id="email" type="text"  required class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" placeholder="Gmail Email Address"  autocomplete="email" autofocus />

                                    @error('email')
                                    <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>


                            <!-- present address -->
                            <div class="form-group row">
                                <label for="name" class="col-md col-form-label float-left">{{ __('Present Address') }}</label>
                            </div>
                            <div class="form-group row">
                                <div style="padding-top: 10px;" class="col-md-6">
                                <select id="region_id" wire:model="selectedRegion" name="region_id" class="form-control">
                                <option selected value="0">Select Region</option>
                                 @foreach($regions as $item)
                                <option id="region_code" value="{{$item->region_id}}">{{$item->name}}</option>
                                @endforeach
                                </select>
                                <div id='response'></div>
                                @error('region_id')
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                                </div>
                                <div style="padding-top: 10px;" class="col-md-6">
                                    <select id="province_id" wire:model="selectedProvince" name="province_id" class="form-control">
                                        <option selected value="0">Select Province</option>
                                    </select>
                                <div id='response'></div>
                                @error('province_id')
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                                </div>
                                
                            </div>
                            <div  class="form-group row">
                                <div style="padding-top: 10px;" class="col-md-6">
                                    <select id="city_id" wire:model="selectedCity" name="city_id" class="form-control">
                                    <option selected value="0">Select City</option>
                                    </select>
                                <div id='response'></div>
                                @error('city_id')
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                                </div>
                                <div style="padding-top: 10px;" class="col-md-6">
                                <select id="barangay_id" wire:model="selectedBarangay" name="barangay_id" class="form-control">
                                        <option selected value="0">Select Barangay</option>
                                    </select>
                                <div id='response'></div>
                                @error('barangay_id')
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                                </div>
                            </div>
                            <div  class="form-group row">
                                <div style="padding-top: 10px;" class="col-md">
                                    <input id="street" type="text" wire:model="street" class="form-control @error('street') is-invalid @enderror" name="street" value="{{ old('street') }}" placeholder="Bldg. No./ Street / Subdivision"  autocomplete="street" autofocus />
                                    @error('street')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <!-- occu-oftag-marChanel-purpose -->
                            

                            
                            <div class="form-group row">
                                <label for="name" class="col-md-4 col-form-label float-left">{{ __('Facebook Link') }}</label>
                                <div class="col-md-8">
                                    <input type="text"  required class="form-control  @error('fb_link') is-invalid @enderror" value="{{ old('fb_link') }}" id="fb_link" name="fb_link">
                                    @error('fb_link')
                                    <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <!-- <div id="app" class="form-group row">
                            </div> -->
                            <div class="form-group row">
                               <div class="card-header bg-light text-primary py-2 pl-3 border-0">
                                  <h6 class="m-0 default-primary">LOAN ASSESSMENT
                                  </h6>
                                </div>
                            </div>

                            <div id="comakerdiv" hidden class="form-group row">
                                <label  for="name" class="col-md-6 col-form-label float-left">{{ __('Co-Maker') }}</label>
                                <div  class="col-md-6" >
                                    <input type="text"  disabled  required  class="form-control m @error('co_maker') is-invalid @enderror" value="{{ old('co_maker') }}" id="co_maker"  name="co_maker">
                                </div>
                            </div>
                             <div class="form-group row">
                                <label  for="name" class="col-md-6 col-form-label float-left">{{ __('Share Capital') }}</label>
                                <div  class="col-md-6" >
                                    <input type="number" class="form-control form-control-sm @error('share_capital') is-invalid @enderror" onchange="(function(el){el.value=parseFloat(el.value).toFixed(2);})(this)" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"value="0.00" maxlength="6" min="0.00" max="9999999.00" readonly step="0.05" id="share_capital" name="share_capital">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label  for="name" class="col-md-6 col-form-label float-left">{{ __('Amount Applied') }}</label>
                                <div  class="col-md-6" >
                                    <input type="number" class="form-control form-control-sm @error('amount_needed') is-invalid @enderror" onchange="(function(el){el.value=parseFloat(el.value).toFixed(2);})(this)" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"value="0.00" maxlength="6" min="0.00" max="9999999.00" readonly step="0.05" id="amount_needed" name="amount_needed">
                                </div>

                            </div>
                             <div class="form-group row">
                                <label  for="name" class="col-md-6 col-form-label float-left">{{ __('Amount Approved') }}</label>
                                <div  class="col-md-6" >
                                    <input type="number" class="form-control form-control-sm @error('amount_approved') is-invalid @enderror" onchange="(function(el){el.value=parseFloat(el.value).toFixed(2);})(this)" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"value="0.00" maxlength="6" min="0.00" max="9999999.00" readonly step="0.05" id="amount_approved" name="amount_approved">
                                </div>

                            </div>
                            <div class="form-group row">
                                <label  for="name" class="col-md-6 col-form-label float-left">{{ __('What loan product do you want to avail?') }}</label>
                                <div  class="col-md-6" >
                                    <select disabled  class="  form-control @error('loan_product') is-invalid @enderror" id="loan_product"  required  type="text" name="loan_product" title="" autocomplete="loan_product" >
                                        @foreach($loan_type as $item)
                                        <option value="{{$item->id}}">{{$item->name}}</option>
                                        @endforeach
                                      </select>
                                </div>
                            </div>
                            <div id="savings_salary_div" hidden class="form-group row">
                                <label  for="name" class="col-md-6 col-form-label float-left" id="savings_salary_label"></label>
                                <div  class="col-md-6" >
                                    <input type="number" disabled class="form-control form-control-sm @error('savings_salary_amount') is-invalid @enderror" onchange="(function(el){el.value=parseFloat(el.value).toFixed(2);})(this)" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"value="0.00" maxlength="6" min="0.00" readonly max="9999999.00" step="0.05" id="savings_salary_amount" name="savings_salary_amount">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="name" class="col-md-6 col-form-label float-left">{{ __('Purpose of Loan') }}</label>
                                <div  class="col-md-6" >
                                    <select disabled hidden class="  form-control @error('loan_purpose') is-invalid @enderror" id="loan_purpose"  required   type="text" name="loan_purpose" autocomplete="loan_purpose" >
                                        <option value="" selected disabled>Select Status</option>
                                        <option value='0' @if (old('loan_purpose') == '0') selected="selected" @endif>Business-Related Expense</option>
                                        <option value='1' @if (old('loan_purpose') == '1') selected="selected" @endif>Debt Consolidation</option>
                                        <option value='2' @if (old('loan_purpose') == '2') selected="selected" @endif>Education  / Tuition fees</option>
                                        <option value='3' @if (old('loan_purpose') == '3') selected="selected" @endif>Health / Medical fees</option>
                                        <option value='4' @if (old('loan_purpose') == '4') selected="selected" @endif>Home Renovation / Improvements</option>
                                        <option value='5' @if (old('loan_purpose') == '5') selected="selected" @endif>Life Events / Celebration</option>
                                        <option value='6' @if (old('loan_purpose') == '6') selected="selected" @endif>Personal Consumption</option>
                                        <option value='7' @if (old('loan_purpose') == '7') selected="selected" @endif>Purchase of Appliance / Gadget / Furniture</option>
                                        <option value='8' @if (old('loan_purpose') == '8') selected="selected" @endif>Start-up Business</option>
                                        <option value='9' @if (old('loan_purpose') == '9') selected="selected" @endif>Vacation / Travel Expense</option>
                                        <option value='10' @if (old('loan_purpose') == '10') selected="selected" @endif>Vehicle Repair / Purchase</option>
                                      </select>
                                      <input type="text" hidden disabled required  class="form-control m @error('other_purpose') is-invalid @enderror" value="{{ old('other_purpose') }}" id="other_purpose"  name="other_purpose">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label  for="name" class="col-md-6 col-form-label float-left">{{ __('Payment Terms') }}</label>
                                <div  class="col-md-6" >
                                    <select disabled class="  form-control @error('payment_terms') is-invalid @enderror" id="payment_terms"  required  type="text" name="payment_terms" autocomplete="payment_terms" >
                                         @foreach($payment_terms as $item)
                                        <option value="{{$item->id}}">{{$item->name}}</option>
                                        @endforeach
                                      </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label  for="name" class="col-md-6 col-form-label float-left">{{ __('Mode of Payment') }}</label>
                                <div  class="col-md-6" >
                                    <select disabled class="form-control @error('payment_mode') is-invalid @enderror" id="payment_mode"  required  type="text" name="payment_mode" autocomplete="payment_mode" >
                                        <option value="1" >Daily</option>
                                            <option value="2" >Weekly</option>
                                            <option value="3" >Semi-Monthly</option>
                                            <option value="4" >Monthly</option>
                                            <option value="5" >Lumpsum</option>
                                            <option value="6" >Lumpsum (for 1 month only)</option>
                                      </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label  for="name" class="col-md-6 col-form-label float-left">{{ __('Source of Income') }}</label>
                                <div  class="col-md-6" >
                                    <select disabled hidden class="form-control m @error('occupation_id') is-invalid @enderror"   id="occupation_id"  required  type="text" name="occupation_id" autocomplete="occupation_id" >
                                        <option value="" selected disabled>Select</option>
                                         @foreach($occupation as $item)
                                        <option  value="{{$item->id}}" @if (old('occupation_id') == '{{$item->id}}') selected="selected" @endif>{{$item->name}}</option>
                                        @endforeach
                                      </select>
                                    <input type="text" hidden disabled required  class="form-control m @error('other_occupation') is-invalid @enderror" value="{{ old('other_occupation') }}" id="other_occupation"  name="other_occupation">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="name" class="col-md-6 col-form-label float-left">{{ __('How much is your monthly gross income?') }}</label>
                                <div  class="col-md-6" >
                                    <input type="text"  required  class="form-control m @error('montly_income') is-invalid @enderror" value="{{ old('montly_income') }}" id="montly_income"  name="montly_income">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label  for="name" class="col-md-6 col-form-label float-left">{{ __('How much are your monthly expenses?') }}</label>
                                <div  class="col-md-6" >
                                    <input type="text" required class="form-control m @error('montly_expense') is-invalid @enderror" value="{{ old('montly_expense') }}" id="montly_expense"  name="montly_expense">
                                </div>
                            </div>

                           <!--  <div class="form-group row">
                               <div class="card-header bg-light text-primary py-2 pl-3 border-0">
                                  <h6 class="m-0 default-primary">PLEASE ANSWER THE SURVEY TO IMPROVE OUR SYSTEM IN LOAN PRODUCT ASSESSMENT.</h6>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="name" class="col-md col-form-label float-left">{{ __('Where did you hear about Most Holy Rosary Multi-Purpose Cooperative?') }}</label>
                            </div>
                            <div class="form-group row">
                                <div  class="col-md" >
                                    <select id="marketing_channel_id" required  name="marketing_channel_id" class="form-control">
                                    <option value="">Select</option>
                                    @foreach($marketingChannel as $item)
                                    <option  value="{{$item->id}}">{{$item->name}}</option>
                                    @endforeach
                                    </select>
                                </div>
                            </div>
                        --> <!--
                            -->
                    </form>  
            </div>
            <!-- <div class="modal-footer">
                <button type="button" tabindex="-1" class="btn btn-secondary close-btn" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary btn-save" id="btn-save">Save
                </button>
            </div> -->
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="ajax-decline-model" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
            <h4 class="modal-title" id="ajaxDeclineModel"></h4>
            <!-- <button type="button" class="btn btn-default" onClick="window.location.reload();" id="close-modal" data-dismiss="modal">Close</button> -->
            </div>
            <div class="modal-body">
                <div class="card-body" >
                <form action="{{ route('loanstatus1') }}" method="post">
                           @method('post')
                           @csrf
                    <div class="form-group row">
                        <input type="hidden" name="iddecline" id="iddecline">
                        <div  class="col-md-4" >
                        <label  for="name" class="col-md-6 col-form-label float-left">{{ __('Remarks') }}
                        </label>
                        </div>
                        <div  class="col-md-8" >
                            <textarea class="form-control textarea @error('approval_remarks') is-invalid @enderror" id="approval_remarks" name="approval_remarks" style="border-color: black; background-color:lightgrey;"></textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" tabindex="-1"   class="btn btn-secondary close-modal" >Close</button>
                        <button type="submit" class="btn btn-primary btn-save" id="btn-save-decline">Save
                        </button>
                    </div>
                </form>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="ajax-approve-model" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
            <h4 class="modal-title" id="ajaxApproveModel"></h4>
            <!-- <button type="button" class="btn btn-default" onClick="window.location.reload();" id="close-modal" data-dismiss="modal">Close</button> -->
            </div>
            <div class="modal-body">
                <div class="card-body" >
                <!-- <form action="" id="insert_form1" name='formname1' > -->
                <form action="{{ route('loanapproval') }}" method="post">
                           @method('post')
                           @csrf
                    <div class="form-group row">
                        <label  for="name" class="col-md-6 col-form-label float-left">{{ __('Amount Needed') }}</label>
                        <input type="hidden" name="idapprove" id="idapprove">
                        <div  class="col-md-6" >
                            <input type="number" class="form-control form-control-sm @error('amou
                            nt_needed1') is-invalid @enderror" onchange="(function(el){el.value=parseFloat(el.value).toFixed(2);})(this)" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"value="0.00" readonly maxlength="6" min="0.00" max="9999999.00" step="0.05" id="amount_needed1" name="amount_needed1">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label  for="name" class="col-md-6 col-form-label float-left">{{ __('Amount Approved') }}</label>
                        <!-- <input type="hidden" name="idapprove" id="idapprove"> -->
                        <div  class="col-md-6" >
                            <input type="number" class="form-control form-control-sm @error('amount_approved1') is-invalid @enderror" onchange="(function(el){el.value=parseFloat(el.value).toFixed(2);})(this)" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"value="0.00" maxlength="6" min="0.00" max="9999999.00" step="0.05" id="amount_approved1" name="amount_approved1">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label  for="name" class="col-md-6 col-form-label float-left">{{ __('Remarks') }}</label>
                        <div  class="col-md-6" >
                            <textarea class="form-control textarea @error('approval_remarks') is-invalid @enderror"  name="approval_remarks" style="border-color: black; background-color:lightgrey;"></textarea>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" tabindex="-1" id="close-modal" class="btn btn-secondary close-modal" >Close</button>
                        <button type="submit" class="btn btn-primary btn-save">Save
                        </button>
                    </div>
                </form>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="ajax-attachment-model" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
            <h4 class="modal-title" id="ajaxAttachmentModel"></h4>
            <button type="button" class="btn btn-default"  id="close-modal" data-dismiss="modal">Close</button>
            </div>
            <div class="modal-body">
                <div class="card-body" >
                   
                    <div class="">
                      <div class="slide s01">
                        <a id="href0" target="_blank" rel="noopener noreferrer">
                            <h4 id="name0"></h4>
                        <img id="img0" height="300" width="400">
                        </a>
                      </div>
                      <div class="slide s02">
                        <a id="href1" target="_blank" rel="noopener noreferrer">
                            <h4 id="name1"></h4>
                         <img id="img1" height="300" width="400">
                        </a>
                      </div>
                      <div class="slide s03">
                        <a id="href2" target="_blank" rel="noopener noreferrer">
                            <h4 id="name2"></h4>
                        <img id="img2" height="300" width="400">
                        </a>
                      </div>
                      <div class="slide s04">
                        <a id="href3" target="_blank" rel="noopener noreferrer">
                            <h4 id="name3"></h4>
                        <img id="img3" height="300" width="400">
                        </a>
                      </div>
                      <div class="slide s05">
                        <a id="href4" target="_blank" rel="noopener noreferrer">
                            <h4 id="name4"></h4>
                        <img id="img4" height="300" width="400">
                        </a>
                      </div>
                      <div class="slide s06">
                        <a id="href5" target="_blank" rel="noopener noreferrer">
                            <h4 id="name5"></h4>
                         <img id="img5" height="300" width="400">
                        </a>
                      </div>
                      <div class="slide s07">
                        <a id="href6" target="_blank" rel="noopener noreferrer">
                            <h4 id="name6"></h4>
                        <img id="img6" height="300" width="400">
                        </a>
                      </div>
                      <div class="slide s08">
                        <a id="href7" target="_blank" rel="noopener noreferrer">
                            <h4 id="name7"></h4>
                        <img id="img7" height="300" width="400">
                        </a>
                      </div>
                      <div class="slide s09">
                        <a id="href8" target="_blank" rel="noopener noreferrer">
                            <h4 id="name8"></h4>
                        <img id="img8" height="300" width="400">
                        </a>
                      </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@section('clientjs')
  <script src="{{ asset('assets') }}/js/addressDropdown.js"></script>
  <script src="{{ asset('assets') }}/js/extrajs.js"></script>
@endsection