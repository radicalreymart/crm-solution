@extends('layouts.app1')
@section('content')
  <style>
    .alert{
      color: black !important;
    }
    .rate {
    text-align: center ;
    }
    .radio-lm{  
    transform: scale(1.5);
    }
    .comment{
      border-color: black !important; 
    }
    .comment:hover{
     border-color: black !important;
    }
  </style>
    <!-- <nav class="navbar navbar-expand-lg navbar-light bg-white">
      <div class="container-fluid">
        <a class="navbar-brand" href="#">
          <img src="{{asset('assets/img/logo.jpg')}}" alt="" width="180" class="d-inline-block align-text-top">
        </a>
      </div>
    </nav> -->
    <!-- Masthead-->
    <header class="masthead mb-1">
      <div class="container mt-1 pt-1">
            @include('flash-message')
            @include('sweetalert::alert')
      </div>
    </header>

    <div class="container mt-5" id="sign_up">
      <div class="card p-2 pt-3 bg-light border-0">
        <div class="row"> 
          <!-- <div class="col-md-12 order-md-1">

          </div> -->
          <div class="col-md-12 order-md-1">
            <form method="POST" name='formname' action="{{ route('sitefeedback-store') }}" onsubmit="return validateForm()">
              @if(Auth::check())
              <input type="hidden" id="regmem" name="regmem"  >
              <input type="hidden" id="regmem_num" name="regmem_num" value="{{$user_info->member_no}}" >
              @else
              <input type="hidden" id="nonregmem" name="nonregmem"  >
              <input type="hidden" id="nonregmem_num" name="nonregmem_num"  >
              @endif
              <input type="hidden" value="{{url('/')}}" id="url" name="url">
              <input type="hidden" value="{{$logout}}" id="logout" name="logout">
                                        @csrf
              <div class="card mx-3 shadow-lg border-0">
                <div class="card-header bg-light text-primary py-2 pl-3 border-0">
                  <h6 class="m-0 default-primary">Personal Information 
                    @if(!Auth::guest())
                    <a class="justify-content"> | Member No: {{$user_info->member_no}}</a>
                      @if( $logout == 'logout')
                           <a class="hvr-icon-push float-right" id="logout-button" data-url="{{ url('force-logout') }}" >{{ __('Logout') }}<i class="now-ui-icons media-1_button-power hvr-icon"></i></a>
                      @else
                        <a class=" hvr-icon-push float-right" onClick="window.location.href = '/enduser/home';" data-url="{{ url('/enduser/home') }}">{{ __('Back') }}<i class="far fa-caret-square-left hvr-icon"></i></a>
                      @endif
                    @endif
                  </h6>
                </div>
                <div class="card-body px-3">
                    <!-- <div class="row px-3">
                      <div class="col-md-4 mb-3">
                        <label for="fname">First Name</label>
                        <input type="text" class="form-control form-control-sm  @error('fname') is-invalid @enderror " id="fname" name="fname" placeholder="First Name" @if(!Auth::guest())value="{{$fname}}" readonly @endif  autocomplete="fname">
                        <div class="invalid-feedback">
                          First Name is required.
                        </div>
                      </div>
                      <div class="col-md-4 mb-3">
                        <label for="address2">Middle Name  <span class="text-muted">(Optional)</span></label>
                        <input type="text" class="form-control form-control-sm  " id="mname" name="mname" placeholder="Middle Name"  @if(!Auth::guest())value="{{$mname}}" readonly @endif  autocomplete="mname">
                      </div>
                      <div class="col-md-4 mb-3">
                        <label for="lname">Last Name</label>
                        <input type="text" class="form-control form-control-sm  @error('lname') is-invalid @enderror " id="lname" name="lname" placeholder="Last Name"  @if(!Auth::guest())value="{{$lname}}" tabindex="-1" disabled @endif  autocomplete="lname" >
                        <div class="invalid-feedback">
                          Last Name is required.
                        </div>
                      </div>
                    </div> -->
                    <div class="row px-3">
                      <div class="col-md mb-3">
                        <label >Full Name <span class="text-danger">*</span></label>
                        <input type="text" class="form-control form-control-sm  @error('fname') is-invalid @enderror " readonly id="fullname" name="fullname" placeholder="Full Name" @if(!Auth::guest())value="{{$fullname}}" readonly @endif  autocomplete="fname">
                        <div class="invalid-feedback">
                          Full Name is required.
                        </div>
                      </div>
                    </div> <!-- Long Name -->
                    <div class="row px-3">
                      <div class="col-md-4 mb-3">
                          <label for="fname">Gender</label>
                          <select id="gender" class="form-control custom-select custom-select-sm @error('gender') is-invalid @enderror" name="gender" @if(!Auth::guest()) readonly @endif>
                            <option value="" selected disabled>Select Gender</option>
                            <option value="1" @if($gender == 'M') selected @endif>Male</option>
                            <option value="2" @if($gender == "F") selected @endif>Female</option>
                          </select>
                          <input hidden readonly type="text" class="form-control form-control-sm  " id="gender1" name="gender1" >
                          <div class="invalid-feedback">
                            Gender is required.
                          </div>
                        </div>
                        <div class="col-md-4 mb-3">
                          <label for="branch_id">Branch  </label>
                          <select class="form-control custom-select custom-select-sm @error('branch_id') is-invalid @enderror" id="branch_id"  type="text" name="branch_id" autocomplete="branch_id" @if(!Auth::guest()) disabled @endif>
                            <option value="" selected disabled>Select Branch</option>
                            @foreach($branches as $item)
                            <option id="branch" value="{{$item->id}}" {{ $branch == $item->id ? 'selected="selected"' : '' }}>{{$item->name}}</option>
                            @endforeach
                          </select>
                          <input hidden readonly type="text" class="form-control form-control-sm  " id="branch_id1" name="branch_id1" >
                          <div class="invalid-feedback">
                            Branch is required.
                          </div>
                        </div>
                        <div class="col-md-4 mb-3">
                          <label for="lname">Membership Type</label>
                          <select id="member_type" class="form-control custom-select custom-select-sm @error('gender') is-invalid @enderror" name="member_type" @if(!Auth::guest()) disabled @endif>
                            <option value="" selected disabled>Select Type</option>
                            <option value="1" @if($type == 1) selected @endif>Associate</option>
                            <option value="2" @if($type == 2) selected @endif>Regular</option>
                          </select>
                          <input hidden readonly type="text" class="form-control form-control-sm  " id="member_type1" name="member_type1" >
                          <div class="invalid-feedback">
                            Membership Type is required.
                          </div>
                        </div>
                      </div>
                </div>
              </div>

              <div class="card mt-4 mx-3 shadow-lg border-0">
                <div class="card-header bg-light text-primary pt-2 pl-3 border-0">
                  <h6 class="m-0 default-primary">Please rate below your transaction experience  </h6>
                </div>
                <div class="card-body">
                  <table class="table">
                    <thead>
                      <tr>
                        <th scope="col">Rating Scale:<span class="text-danger"> *</span><br>1 - Needs Improvement <br> 2 - Fair <br> 3 - Satisfactory <br> 4 - Very Satisfactory <br> 5 - Outstanding</th>
                        <th class="rate" scope="col">★</th>
                        <th class="rate" scope="col">★★</th>
                        <th class="rate" scope="col">★★★</th>
                        <th class="rate" scope="col">★★<br>★★</th>
                        <th class="rate" scope="col">★★★<br>★★</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>  

                        <th scope="row">How was the experience with our website?<p class="small font-italic">Kumusta ang karanasan mo sa aming website?</p></th>
                        <td class="rate radio-lm"><input   type="radio" value=1 name="rate1" id="1rate1"></td>
                        <td class="rate radio-lm"><input  type="radio" value=2 name="rate1" id="1rate2"></td>
                        <td class="rate radio-lm"><input  type="radio" value=3 name="rate1" id="1rate3"></td>
                        <td class="rate radio-lm"><input  type="radio" value=4 name="rate1" id="1rate4"></td>
                        <td class="rate radio-lm"><input  type="radio" value=5 name="rate1" id="1rate5"></td>
                      </tr>
                      <tr>
                        <th scope="row">Did this page help you?<p class="small font-italic">Nakatulong ba sa'yo ang page na ito?</p></th>
                        <td class="rate radio-lm"><input   type="radio" value=1 name="rate2" id="2rate1"></td>
                        <td class="rate radio-lm"><input  type="radio" value=2 name="rate2" id="2rate2"></td>
                        <td class="rate radio-lm"><input  type="radio" value=3 name="rate2" id="2rate3"></td>
                        <td class="rate radio-lm"><input  type="radio" value=4 name="rate2" id="2rate4"></td>
                        <td class="rate radio-lm"><input  type="radio" value=5 name="rate2" id="2rate5"></td>
                      </tr>
                      <tr>
                        <th scope="row">How easy was it to find the information you were looking for?<p class="small font-italic">Gaano kadaling mahanap ang impormasyong hinahanap mo?</p></th>
                        <td class="rate radio-lm"><input  type="radio" value=1 name="rate3" id="3rate1"></td>
                        <td class="rate radio-lm"><input  type="radio" value=2 name="rate3" id="3rate2"></td>
                        <td class="rate radio-lm"><input  type="radio" value=3 name="rate3" id="3rate3"></td>
                        <td class="rate radio-lm"><input  type="radio" value=4 name="rate3" id="3rate4"></td>
                        <td class="rate radio-lm"><input  type="radio" value=5 name="rate3" id="3rate5"></td>
                      </tr>
                      <tr>
                        <th scope="row">Have you got the necessary information that you want?<p class="small font-italic">Nakuha mo ba ang kinakailangang impormasyon na gusto mo?</p></th>
                        <td class="rate radio-lm"><input  type="radio" value=1 name="rate4" id="4rate1"></td>
                        <td class="rate radio-lm"><input  type="radio" value=2 name="rate4" id="4rate2"></td>
                        <td class="rate radio-lm"><input  type="radio" value=3 name="rate4" id="4rate3"></td>
                        <td class="rate radio-lm"><input  type="radio" value=4 name="rate4" id="4rate4"></td>
                        <td class="rate radio-lm"><input  type="radio" value=5 name="rate4" id="4rate5"></td>
                      </tr>
                      <tr>
                        <th scope="row">Overall, how happy are you with the website?<p class="small font-italic">Sa pangkalahatan, gaano ka kasaya sa website?</p></th>
                        <td class="rate radio-lm"><input  type="radio" value=1 name="rate5" id="5rate1"></td>
                        <td class="rate radio-lm"><input  type="radio" value=2 name="rate5" id="5rate2"></td>
                        <td class="rate radio-lm"><input  type="radio" value=3 name="rate5" id="5rate3"></td>
                        <td class="rate radio-lm"><input  type="radio" value=4 name="rate5" id="5rate4"></td>
                        <td class="rate radio-lm"><input  type="radio" value=5 name="rate5" id="5rate5"></td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>

              <div class="card mt-4 mx-3 shadow-lg border-0">
                <div class="card-header bg-light text-primary pt-2 pl-3 border-0">
                  <h6 class="m-0 default-primary">Feedback: Comments/Suggestions<span class="text-danger"> *</span></h6>
                </div>
                <div class="card-body">
                  <textarea class="comment form-control" name="comment" id="comment"></textarea>
                </div>
              </div>


                <div class=" card-header bg-light text-primary pt-2 pl-3 border-0">
                  <h6 class="m-0 default-primary">Signature  
                    <a id="clear" class="btn btn-danger btn-sm" type="button"> Clear</a>
                  </h6>
                </div>
                <div  id="manual-sig" class="float-right col-md">
                    <canvas style="background-color: lightgrey;" id="signature"></canvas>
                    <input type="hidden" id="hidden_sig" name="hidden_sig">
                </div>
                <div>&nbsp;</div>
              <button class="btn btn-secondary mt-5 mb-5 btn-block" style="background-color: rgb(7,17,174);" id="btn-save-feedback" type="submit">Submit</button>
            </form>
          </div>
        </div>
      </div>
    </div>

<script src="https://cdn.jsdelivr.net/npm/signature_pad@2.3.2/dist/signature_pad.min.js"></script>
<script>
  $('#logout-button').on('click', function(e) {
          var role = $('#role').val();
            const swalWithBootstrapButtons = Swal.mixin({
              customClass: {
                confirmButton: 'btn btn-success',
                cancelButton: 'btn btn-danger'
              },
              buttonsStyling: false
            })
    swalWithBootstrapButtons.fire({
      title: 'Are you sure you want to Logout?',
      icon: 'question',
      showCancelButton: true,
      confirmButtonText: 'Yes',
      cancelButtonText: 'No',
      reverseButtons: true
    }).then((result) => {
      if (result.isConfirmed) {
        // alert(role);
        swalWithBootstrapButtons.fire(
            'Logout Successful'
          ).then((result) => {
            // window.location.reload();
          window.location.href = $(this).data('url');
          })
        } else if (
          /* Read more about handling dismissals below */
          result.dismiss === Swal.DismissReason.cancel
        ) {
        
          }
    })
  });
  function validateForm() {
    let comment = document.getElementById("comment").value;
      let rate1 = $("input[type='radio'][name='rate1']:checked").val();
      let rate2 = $("input[type='radio'][name='rate2']:checked").val();
      let rate3 = $("input[type='radio'][name='rate3']:checked").val();
      let rate4 = $("input[type='radio'][name='rate4']:checked").val();
      let rate5 = $("input[type='radio'][name='rate5']:checked").val();
      let rateArray = [];
      rateArray += rate1+',';
      rateArray += rate2+',';
      rateArray += rate3+',';
      rateArray += rate4+',';
      rateArray += rate5+',';
      var ratesplit =  rateArray.includes(undefined);
      var signatureType = signaturePad.isEmpty(); 
      var valmsg =  (ratesplit == true ? "<br>Please  complete all of the survey rating<br>" : "") 
      + (comment === '' ? "<br>Comment/Suggestions must be filled out<br>" : "") 
      + (signatureType ? "<br>Signature must be filled out<br>" : "");

      if (ratesplit == true ||
       comment === ''||
       signaturePad.isEmpty()
       ) {
      // alert(fullname);
        swal.fire({
          icon: 'warning',
          html:valmsg
        });
        return false;
      }
  }
  var canvas = document.getElementById ("signature");
  var signaturePad = new SignaturePad(canvas);
  $('#clear').click(function (e) {
            e.preventDefault();
            signaturePad.clear();
            $("#signature").val('');
        });
  $('#btn-save-feedback').click(function (event) {
            // event.preventDefault();
  // var sig_val = signaturePad.value();
  var data = signaturePad.toDataURL('image/png');
  $('#hidden_sig').val(data);
  $("#gender"). attr("disabled", false);
  $("#branch_id"). attr("disabled", false);
  $("#member_type"). attr("disabled", false);
  $("#lname"). attr("disabled", false);
  // alert(data);
  });
</script>
 <script>
  // window.addEventListener("load", function() {

  var base_path = $("#url").val();
  var nonregmem_num = $("#nonregmem_num").val();
  var regmem_num = $("#regmem_num").val();
    $.lastFeedback = function (nonregmem_num) {
     $.ajax({
      type:"GET",
      url:base_path+'/lastsiteFeedback',
      data: {nonregmem_num:nonregmem_num},
      success:function(res){
        // alert(JSON.stringify(res));
        if(res.status == 1){

        }else{
          Swal.fire({
            title: 'This member already did a feedback last: '+res.date,
            text: 'Create another feedback?',
            showDenyButton: true,
            confirmButtonText: 'Yes',
            denyButtonText: `No`,
          }).then((result) => {
              if (result.isConfirmed) {
                
              } else if (
                /* Read more about handling dismissals below */
                result.dismiss === Swal.DismissReason.deny
              ) {
                 window.location.href = base_path+'/';
              }
            })
        }
      },
      error: function (data) {
          alert(data.responseText);
      }              
     });
};

$('#lname').focusout(function(event){
  var fname = $('#fname').val();
  var lname = $('#lname').val();
  $.ajax({
      type:"GET",
      url:base_path+'/lastsiteFeedbackNonRegNonMem',
      data: {fname:fname,
        lname:lname
      },
      success:function(res){
        // alert(JSON.stringify(res));
        if(res.status == 1){

        }else{
          Swal.fire({
            title: 'This member already did a feedback last: '+res.date,
            text: 'Create another feedback?',
            allowOutsideClick: false,
            showDenyButton: true,
            confirmButtonText: 'Yes',
            denyButtonText: `No`,
          }).then((result) => {
              if (result.isConfirmed) {
                
              } else if (
                /* Read more about handling dismissals below */
                result.dismiss === Swal.DismissReason.deny
              ) {
                 window.location.href = base_path+'/';
              }
            })
        }
      },
      error: function (data) {
          alert(data.responseText);
      } 

     });
});

$(document).ready(function($){
 // $.lastFeedback();
// console.log(data);
  if(regmem_num == null){
      Swal.fire({
        title: 'Are you a MHRMPC Member?',
        text: 'Please input you member number',
        input: `text`,
        inputAttributes: {
          autocapitalize: 'on',
          placeholder:"Ex:SRR-0001, SMR-0002, 0001",
        },
        showCancelButton: true,
        confirmButtonText: 'Look up',
        showLoaderOnConfirm: true,
        allowOutsideClick: false,
        preConfirm: (input) => {
          return fetch(base_path+`/sitefeedbackAPI/`+input)
        .then(response => {
          // alert(JSON.stringify(input));
          if (!response.ok) {
            throw new Error(response.statusText)
          }
          return response.json()
        })
        .catch(error => {
          Swal.showValidationMessage(
            `Member Number provided does not match our entry`
          )
        })
        },
        // allowOutsideClick: () => !Swal.isLoading()
      }).then((result) => {
        if (result.isConfirmed) {
        $("#fname"). attr("readonly", true);
        $("#fname"). val(result.value[0].fname);
        $("#mname"). attr("readonly", true);
        $("#mname"). val(result.value[0].mname);
        // $("#lname"). attr("readonly", true);
        $("#lname"). attr("disabled", true);
        $("#lname"). attr("tabindex", -1);
        $("#lname"). val(result.value[0].lname);
        $("#gender"). attr("hidden", true);
        $("#gender1"). attr("hidden", false);
        $("#gender1"). val(result.value[0].gender);
        $("#gender"). val(1);
        $("#branch_id"). attr("hidden", true);
        $("#branch_id1"). attr("hidden", false);
        $("#branch_id1"). val(result.value[0].branch);
        $("#branch_id"). val(1);
        $("#member_type"). attr("hidden", true);
        $("#member_type1"). attr("hidden", false);
        $("#member_type1"). val(result.value[0].type);
        $("#member_type"). val(1);
        $("#mem_num").html('| Member No: '+result.value[0].member_no);
         $("#mem_num").attr("hidden", false);
         $("#nonregmem").val(1);
         $("#nonregmem_num").val(result.value[0].member_no);
         $.lastFeedback(result.value[0].member_no);

        }else if(
                /* Read more about handling dismissals below */
                result.dismiss === Swal.DismissReason.cancel){
        }
      })
    }else{
   $.lastFeedback();
        
  }
    $.ajaxSetup({
        headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    

}); 
</script>
@endsection

