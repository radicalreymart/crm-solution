 
  <div class="content">
<div class="panel-header panel-header-sm">
  </div>
    @include('livewire.user.update')
    @include('auth.passwords.confirmmodal')
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header">
            @include('flash-message')
                <div class="float-left">
                    <h4 class="card-title">Users</h4>
                </div>
                <div class="float-right">
                    @include('livewire.user.create')
                    <button class="btn btn-danger delete_all" data-url="{{ url('DeleteAllUser') }}">Delete All Selected</button>
                </div>
          </div>
          <div class="card-body">
            <input type="text"  class="form-control" placeholder="Search User Name or Email " wire:model="searchTerm" />
            <div class="table-responsive">
              <table class="table">
                <thead class=" text-primary">
                    <th><input type="checkbox" id="master"></th>
                    <th >Action</th>
                    <th>User Name</th>
                    <th>Email</th>
                    <th>Branch</th>
                    <th>Role</th>
                    <th>Created By</th>
                    <th>Date Created</th>
                </thead>
                <tbody>
                    @if($users1->count())
                    @foreach ($users1 as $user)
                    <tr>
                    <td><input type="checkbox" class="sub_chk" data-id="{{$user->id}}"></td>
                    <td>
                    <a href="" data-toggle="modal" data-target="#updateModal" wire:click="edit({{ $user->id }})" >
                      <i class="fas fa-edit  fa-lg"></i> 
                    </a>
                    </td>
                    <td>{{ $user->uname }}</td>
                    <td>{{ $user->email }}</td>
                    <td>{{ $user->branch->name }}</td>
                    @if($user->role_id == 1 && $user->employee_role == 0)
                    <td>User</td>
                    @elseif($user->employee_role == 1 && $user->role_id == 1)
                    <td>User BM</td>
                    @else
                    <td>Admin</td>
                    @endif
                    <td>{{ $user->crea_by }}</td>
                    <td>{{ date_format($user->created_at, 'm-d-Y') }}</td>

                    </tr>
                    @endforeach
                    @endif
                </tbody>
              </table>
            </div>
             {{ $users1->links() }}
          </div>
        </div>
      </div>
      
    </div>
  </div>