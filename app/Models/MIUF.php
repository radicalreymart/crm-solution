<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Member;
use App\Models\Region;
use App\Models\Province;
use App\Models\City;
use App\Models\Barangay;
use App\Models\Occupation;
use Yajra\Address\HasAddress;

class MIUF extends Model
{
    use HasFactory;
    use HasAddress;

    protected $table = 'miuf';
    protected $casts = [
    'mobile_no' => 'array', // Will convarted to (Array)
    'landline_no' => 'array', // Will convarted to (Array)
    'benificiaries' => 'array', // Will convarted to (Array)
    ];
    protected $fillable = [
              'member_no'
            , 'fname'
            , 'mname'
            , 'lname'
            , 'fullname'
            , 'email'
            , 'birthdate'
            , 'street'
            , 'barangay_id'
            , 'city_id'
            , 'province_id'
            , 'region_id'
            , 'zipcode'
            , 'fb_link'
            , 'mobile_no'
            , 'landline_no'
            , 'of_offm'
            , 'occupation_id'
            , 'other_occupation'
            , 'montly_income'
            , 'emp_bus_name'
            , 'emp_bus_address'
            , 'emp_bus_startdate'
            , 'benificiaries'
            , 'signiture'
            , 'status'
            , 'editable_at'
            , 'encode_by'
            , 'encode_date'
            , 'crea_by'
            , 'upd_by'
            ];

    public function member()
    {
        return $this->belongsTo('App\Models\Member','member_no','member_no');
    }
    public function region()
    {
        return $this->belongsTo('App\Models\Region','region_id','region_id');
    }
    public function province()
    {
        return $this->belongsTo('App\Models\Province','province_id','province_id');
    }
    public function city()
    {
        return $this->belongsTo('App\Models\City','city_id','city_id');
    }
    public function barangay()
    {
        return $this->belongsTo('App\Models\Barangay','barangay_id','code');
    }
     public function occupation()
    {
        return $this->belongsTo('App\Models\Occupation','occupation_id','id');
    }
    // public function region()
    // {
    //     return $this->belongsTo(Region::class);
    // }

}
