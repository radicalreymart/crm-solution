<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBatchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('batch', function (Blueprint $table) {
            $table->id();
            $table->string('batch_cd', 100);
            $table->smallInteger('batch_stat');
            $table->Integer('participant')->default('0');
            $table->dateTime('sched_date');
            $table->smallInteger('online_vid_tool');
            $table->string('resched_remarks');
            $table->string('meeting_cd')->nullable();
            $table->string('meeting_pw')->nullable();
            $table->string('crea_by');
            $table->string('upd_by')->nullable();
            $table->datetime('soft_deleted_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('batch');
    }
}
