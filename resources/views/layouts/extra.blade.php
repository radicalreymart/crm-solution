@extends('layouts.app1')
@section('loanstyle')

<!--<link rel="stylesheet" type="text/css" href="http://keith-wood.name/css/jquery.signature.css">-->
<style>
 .kbw-signature {
	display: inline-block;
	border: 1px solid #a0a0a0;
	-ms-touch-action: none;
}
.kbw-signature-disabled {
	opacity: 0.35;
}
    .kbw-signature {
        width: 100%;
        height: 200px;
    }
    #sig canvas {
        width: 100% !important;
        height: auto;
    }
.alert{
  color: black !important;
}
[type=file] {
    position: absolute;
    filter: alpha(opacity=0);
    opacity: 0;
}
input,
[type=file] + label {
  border: 1px solid black;
  border-radius: 3px;
  text-align: left;
  padding: 0px;
  width: 100%;
  height: 30px;
  margin: 0;
  left: 0;
  position: relative;
}
[type=file] + label {
  text-align: center;
  left: 0em;
  top: 0.5em;
  /* Decorative */
  background: blue;
  color: #fff;
  border: black;
  cursor: pointer;
}
[type=file] + label:hover {
  background: #3399ff;
}
label.shorten_text1 {
 max-width: 100%;
  /*padding: px;*/
  text-overflow: ellipsis;
  white-space: nowrap;
  overflow: hidden;
}

label.shorten_text1:hover {
  overflow: visible;
}
label.shorten_text1:before{
  content: attr(title);
}
</style>
@endsection

@section('content')
    <!-- Masthead-->
    <header class="masthead mb-1">
      <div class="container mt-1 pt-1">
       @include('sweetalert::alert')
     
          <div class="masthead-subheading m-3">Magandang Buhay!</div>
          <div class="masthead-heading text-uppercase m-3">Be a member! <br> Be one of us!</div>
          <p class="">Thank you for having interesting membership with us. Please fill up the necessary fields so we can reach you as soon as possible.</p>
          <a class="btn btn-outline-light btn-xl text-uppercase" tabindex="-1" type="button" href="#sign_up">Proceed to Loan Registration</a><br>
          <a class="btn btn-outline-light btn-xl text-uppercase" tabindex="-1"  type="button" href="{{url('/enduser/create')}}">Sign-up Now!</a>
      </div>
    </header>
    
    <div class="container mt-5 col-md-8" id="sign_up">
      <div class="card p-2 pt-3 bg-light border-0">
        <div class="row"> 
          <div class="col-md-12 order-md-1">
            <form id="insert_form" name='formname' enctype="multipart/form-data"  method="post" action="{{ route('preloan.store') }}"> 
            
              <input type="hidden" value="{{url('/')}}" id="url" name="url">
                                        @csrf
              @foreach ($user_member as $data)

              <div class="card mx-3 shadow-lg border-0">
                <div class="card-header bg-light text-primary py-2 pl-3 border-0">
                  <h6 class="m-0 default-primary">Personal Information</h6>
                </div>
                <div class="card-body px-3">
                  <div class="row px-3">
                    <div class="col-md-6 mb-3">
                      <label for="fullname">Full Name</label>
                      <input readonly type="text"  tabindex="-1" class="form-control form-control-sm  @error('fullname') is-invalid @enderror" id="fullname" required name="fullname" placeholder="Full Name" value="{{ $data->member->long_name }}"  autocomplete="fullname">
                      <div class="invalid-feedback">
                        Full Name is required.
                      </div>
                    </div>
                    <div class="col-md-6 mb-3">
                      <label for="contact_no">Contact Number</label>
                      <div class="input-group input-group-sm">
                        <div style="border: dimgrey !important;" class="input-group-prepend">
                          <span class="input-group-text">+63</span>
                        </div>
                        <input type="text" tabindex="-1"  class="form-control form-control-sm @error('contact_no') is-invalid @enderror" id="contact_no" placeholder="10-digit Mobile Number"  minlength="0" maxlength="10" value="{{ $data->contact_no }}" readonly onkeydown="return ( event.ctrlKey || event.altKey 
                    || (47<event.keyCode && event.keyCode<58 && event.shiftKey==false) 
                    || (95<event.keyCode && event.keyCode<106)
                    || (event.keyCode==8) || (event.keyCode==9) 
                    || (event.keyCode>34 && event.keyCode<40) 
                    || (event.keyCode==46))" name="contact_no" style="border-radius: 0;"  required autocomplete="contact_no"  >
                        <div class="invalid-feedback" style="width: 100%;">
                          Contact Number is required.
                        </div>
                      </div>
                    </div>
                  </div> <!-- Long Name -->
                  <div class="row px-3">
                    <div class="col-md-6 mb-3">
                      <label for="birthdate">Age Group</label>
                      <select disabled  tabindex="-1"  class="custom-select custom-select-sm  custom-select custom-select-sm -sm d-block w-100" id="age_group"  type="text" name="age_group" autocomplete="age_group" >
                        <option value="">Select Age Group</option>
                        <option value="0" {{ $data->age_group == 0 ? 'selected' : '' }}>18 - 24</option>
                        <option value="1" {{ $data->age_group == 1 ? 'selected' : '' }}>25 - 34</option>
                        <option value="2" {{ $data->age_group == 2 ? 'selected' : '' }}>35 - 44</option>
                        <option value="3" {{ $data->age_group == 3 ? 'selected' : '' }}>45 - 54</option>
                        <option value="4" {{ $data->age_group == 4 ? 'selected' : '' }}>55 - 64</option>
                        <option value="5" {{ $data->age_group == 5 ? 'selected' : '' }}>65 - Up</option>
                      </select>
                      <div class="invalid-feedback">
                        Age is required.
                      </div>
                    </div>
                    <div class="col-md-6 mb-3">
                      <label for="gender">Gender</label>
                      <select disabled  tabindex="-1"  class="custom-select custom-select-sm  custom-select custom-select-sm -sm d-block w-100" id="gender1"  type="text" name="gender1" autocomplete="gender1" >
                        <option value="M" @if($data->member->gender == "M") selected @endif>Male</option>
                        <option value="F" @if($data->member->gender == "F") selected @endif>Female</option>
                        <option value="1" @if($data->member->gender == "1") selected @endif}}>Rather not say</option>
                      </select>
                      <div class="invalid-feedback">
                        Please select your gender preference.
                      </div>
                    </div>
                  </div> <!-- Birthdate, Age and Gender -->
                  <div class="row px-3">
                    
          
                    <div class="col-md-6 mb-3 mt-0">
                      <label style="padding-bottom: 10px;" for="gmail_address">Email Address</label>
                        <input tabindex="-1"  type="email" class="form-control-sm form-control @error('email') is-invalid @enderror" id="email" value="{{$data->user->email}}"  readonly required name="email" autocomplete="email">
                        <div class="invalid-feedback" >
                          Your Email Address is required.
                        </div>
                    </div>
                    <div class="col-md-6 mb-3">
                      <label for="gmail_address">Facebook Link 
                            <i class="fa-brands fa-facebook-messenger"></i>
                      <input tabindex="-1"  type="text"  class="form-control form-control-sm @error('fb_link') is-invalid @enderror" id="fb_link" readonly value="{{$data->fb_link}}"  required name="fb_link" autocomplete="fb_link">
                      <div class="invalid-feedback">
                        This field is required.
                      </div>
                      </div>  
                  </div> <!-- Contact Information -->
                    <div class="row px-3">
                      
                      
                   </div>
                  <div class="row px-3">
                    <div class="col-md-12 mb-3">
                      
                    </div>
                  </div>
                </div>
              </div>
      
              <div class="card mt-4 mx-3 shadow-lg border-0">
                <div class="card-header bg-light text-primary pt-2 pl-3 border-0">
                  <h6 class="m-0 default-primary">Present Address</h6>
                </div>
                <div class="card-body">
                  <div class="row px-3">
                    <div class="col-md-4 mb-3">
                      <label for="region_code">Region</label>
                      <select tabindex="-1"  class="form-control form-control-sm @error('region_id') is-invalid @enderror" id="region_id" disabled type="text" name="region_id"  required >
                        <option value="">Select Region</option>
                         @foreach($regions as $item)
                         <option value="{{ $data->region_id }}" {{ $data->region_id == $item->id ? 'selected="selected"' : '' }}>{{ $item->name }}</option> 


                        @endforeach
                      </select>
                      <div class="invalid-feedback">
                        Region is required.
                      </div>
                    </div>
                    <div class="col-md-4 mb-3">
                      <label for="province_id">Province</label>
                      <select  tabindex="-1" disabled class="form-control  form-control-sm"  required id="province_id"   name="province_id" autocomplete="province_id" >
                        <option value="">Select Province</option>
                      </select>
                      <input type="text" value="{{$data->province_id}}" id="hidden_province_id" hidden>
                      <div class="invalid-feedback">
                        Province is required.
                      </div>
                    </div>
                    <div class="col-md-4 mb-3">
                      <label for="city_id">City/Municipality</label>
                      <select   tabindex="-1" class="form-control  form-control-sm" id="city_id"  required   name="city_id" autocomplete="city_id" disabled>
                      </select>
                      <input type="text" value="{{$data->city_id}}" id="hidden_city_id" hidden>
                      <div class="invalid-feedback">
                        City/Municipality is required.
                      </div>
                    </div>
                  </div> <!-- Region, Province, Municiaplity -->
                  <div class="row px-3">
                    <div class="col-md-4 mb-3">
                      <label for="barangay_id">Barangay</label>
                      <select  tabindex="-1" disabled class="form-control form-control-sm"  required  id="barangay_id"  type="text" name="barangay_id" autocomplete="barangay_id" >
                      </select>
                      <input type="text" value="{{$data->barangay_id}}" id="hidden_barangay_id" hidden>
                      <div class="invalid-feedback">
                        Barangay is required.
                      </div>
                    </div>
                    <div class="col-md-8 mb-3">
                      <label for="street">Address</label>
                      <input  tabindex="-1" type="text" class="form-control form-control-sm @error('street1') is-invalid @enderror" id="street1" value="{{$data->street}}"  required placeholder="Bldg. No./Street/Apartment No." name="street1" disabled>
                    </div>
                  </div> <!-- Barangay, Street -->
                </div>
              </div>
              @endforeach

              <div class="card mx-3 shadow-lg border-0">
                <div class="card-header bg-light text-primary py-2 pl-3 border-0">
                  <h6 class="m-0 default-primary">Loan Assessment</h6>
                </div>
                <div class="card-body px-3">
                  <div class="row px-3">
                    <div class="col-md mb-3"> 
                      <label for="share_capital">Share Capital<p class="text-muted">If your share capital are larger than you loan amount, requirements will not be needed</p>
                        </label>
                       <input type="number" class="form-control form-control-sm @error('share_capital') is-invalid @enderror" onchange="(function(el){el.value=parseFloat(el.value).toFixed(2);})(this)"
                       oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" value="0.00" maxlength="6" min="0.00" max="9999999.00" step="0.05" id="share_capital" name="share_capital">
                    </div>
                  </div>
                  <div class="row px-3">
                    <div class="col-md-6 mb-3">  
                      <label for="amount_needed">Amount Needed
                      </label>
                     <input type="number" class="form-control form-control-sm @error('amount_needed') is-invalid @enderror" onchange="(function(el){el.value=parseFloat(el.value).toFixed(2);})(this)"
                     oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" value="0.00" maxlength="6" min="0.00" max="9999999.00" step="0.05" id="amount_needed" name="amount_needed">
                    </div>
                    <div  class="col-md-6 mb-3">
                      <label>What loan product do you want to avail?</label>
                      <select required class=" form-control-sm form-control @error('loan_product') is-invalid @enderror" id="loan_product"  type="text" name="loan_product"   autocomplete="loan_product" >
                        <option value="" selected disabled>Select Loan Product</option>
                        <option value='0' @if (old('loan_product') == '0') selected="selected" @endif>Regular Loan</option>
                        <option value='1' @if (old('loan_product') == '1') selected="selected" @endif>Salary Loan</option>
                        <option value='3' @if (old('loan_product') == '3') selected="selected" @endif>Back-2-Back Loan</option>
                        <option value='4' @if (old('loan_product') == '4') selected="selected" @endif>Housing Loan</option>
                        <option value='5' @if (old('loan_product') == '5') selected="selected" @endif>Multi-Purpose Loan</option>

                      </select>
                    </div>
                  </div>
                  <div class="row px-3">
                    <div  class="col-md-6 mb-3">
                      <label >Purpose of Loan</label>
                      <select  required  class=" form-control-sm form-control @error('loan_purpose') is-invalid @enderror" id="loan_purpose"  type="text" name="loan_purpose"   autocomplete="loan_purpose" >
                        <option value="" selected disabled>Select</option>
                        <option value='0' @if (old('loan_purpose') == '0') selected="selected" @endif>Business-Related Expense</option>
                        <option value='1' @if (old('loan_purpose') == '1') selected="selected" @endif>Debt Consolidation</option>
                        <option value='2' @if (old('loan_purpose') == '2') selected="selected" @endif>Education  / Tuition fees</option>
                        <option value='3' @if (old('loan_purpose') == '3') selected="selected" @endif>Health / Medical fees</option>
                        <option value='4' @if (old('loan_purpose') == '4') selected="selected" @endif>Home Renovation / Improvements</option>
                        <option value='5' @if (old('loan_purpose') == '5') selected="selected" @endif>Life Events / Celebration</option>
                        <option value='6' @if (old('loan_purpose') == '6') selected="selected" @endif>Personal Consumption</option>
                        <option value='7' @if (old('loan_purpose') == '7') selected="selected" @endif>Purchase of Appliance / Gadget / Furniture</option>
                        <option value='8' @if (old('loan_purpose') == '8') selected="selected" @endif>Start-up Business</option>
                        <option value='9' @if (old('loan_purpose') == '9') selected="selected" @endif>Vacation / Travel Expense</option>
                        <option value='10' @if (old('loan_purpose') == '10') selected="selected" @endif>Vehicle Repair / Purchase</option>
                      </select>
                    </div>
                    <div  class="col-md-6 mb-3">
                      <label  >Loan Payment Terms</label>
                      <select  required  class=" form-control-sm form-control @error('payment_terms') is-invalid @enderror" id="payment_terms"  type="text" name="payment_terms"   autocomplete="payment_terms" >
                        <option value="" selected disabled>Select</option>
                        <option value='0' @if (old('payment_terms') == '0') selected="selected" @endif>1 Month</option>
                        <option value='1' @if (old('payment_terms') == '1') selected="selected" @endif>3 Months</option>
                        <option value='2' @if (old('payment_terms') == '2') selected="selected" @endif>6 Months</option>
                        <option value='3' @if (old('payment_terms') == '3') selected="selected" @endif>9 Months</option>
                        <option value='4' @if (old('payment_terms') == '4') selected="selected" @endif>12 Months</option>
                      </select>
                    </div>
                  </div>
                  <div class="row px-3">
                    <div  class="col-md-6 mb-3">
                      <label>Loan Payment Mode </label>
                      <select  required  class=" form-control-sm form-control @error('payment_mode') is-invalid @enderror" id="payment_mode"  type="text" name="payment_mode"   autocomplete="payment_mode" >
                        <option value="" selected disabled>Select Status</option>
                        <option value='0' @if (old('payment_mode') == '0') selected="selected" @endif>Daily</option>
                        <option value='1' @if (old('payment_mode') == '1') selected="selected" @endif>Weekly</option>
                        <option value='2' @if (old('payment_mode') == '2') selected="selected" @endif>Semi-monthly</option>
                        <option value='3' @if (old('payment_mode') == '3') selected="selected" @endif>Monthly</option>
                      </select>
                    </div>
                    <div  class="col-md-6 mb-3">
                      <label >Source of Income</label>
                      <select required  class="form-control form-control-sm @error('occupation_id') is-invalid @enderror"   id="occupation_id"  type="text" name="occupation_id"   autocomplete="occupation_id" >
                        <option value="" selected disabled>Select</option>
                         @foreach($occupation as $item)
                        <option  value="{{$item->id}}" @if (old('occupation_id') == '{{$item->id}}') selected="selected" @endif>{{$item->name}}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                  <div class="row px-3">
                    <div  class="col-md-6 mb-3">
                      <label  >How much is your monthly gross income? </label>
                      <input type="number" class="form-control form-control-sm @error('montly_income') is-invalid @enderror" onchange="(function(el){el.value=parseFloat(el.value).toFixed(2);})(this)"
                     oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"
                      value="0.00" maxlength="7" min="0.00" max="9999999.00" step="0.05"   value="{{ old('montly_income') }}"   id="montly_income"  name="montly_income">
                    </div>
                    <div  class="col-md-6 mb-3">
                      <label >How much is your monthly expenses?</label>
                      <input type="number" class="form-control form-control-sm @error('montly_income') is-invalid @enderror" onchange="(function(el){el.value=parseFloat(el.value).toFixed(2);})(this)"
                     oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"
                      value="0.00" maxlength="7" min="0.00" max="9999999.00" step="0.05"  value="{{ old('montly_expense') }}"   id="montly_expense"  name="montly_expense">
                    </div>
                  </div>
                </div>
              </div>
              <div class="card mt-4 mx-3 shadow-lg border-0">
                <div class="card-header bg-light text-primary py-2 pl-3 border-0">
                  <h6 class="m-0 default-primary">Document Upload</h6><p class="text-muted" id="uploadtext"></p>
                </div>
                <div id="document" class="card-body">
                  <div class="row px-3">
                     <div  class="col-md-9 ">
                      <label >Government Issued ID: </label>
                      <p class="text-muted"> Government Issued ID (At least 1 valid government ID like Passport, GSIS, UMID, PRC and others)</p>
                     </div>
                     <div style="padding-top: 8px;" class="col-md-4 mb-3">
                      <select  required  class=" form-control-sm form-control @error('id_type') is-invalid @enderror" id="id_type"  type="text" name="id_type"   autocomplete="id_type" >
                        <option value="" selected disabled>Select Status</option>
                        <option value='UMID' >UMID</option>
                        <option value='GSIS'>GSIS</option>
                        <option value='Passport'>Passport</option>
                        <option value='PRC'>PRC</option>
                        <option value='Other'>Other</option>
                      </select>
                    </div>
                     <div  class="col-md-4 mb-3">
                      <!-- <input type="file" id="frontID" required class="form-control"  name="frontID"/> -->
                      <input  required  id="f01" type="file" placeholder="" name="attachment[]" multiple />
                      <label style="color: #fff" for="f01">ID Front Side</label>
                     </div>
                     <div class="col-md-4 mb-3">
                      <input  required  id="f02" type="file" placeholder="" name="attachment[]" multiple />
                      <label style="color: #fff" for="f02">ID Back Side</label>
                     </div>
                      <div class="invalid-feedback">
                        This field is required.
                     </div>
                  </div>
                   <div class="row px-3">
                     <div class="col-md-3 ">
                      <label >Statement of Account: </label>
                     </div>
                     <div  class="col-md-3" style="padding-right: 5px;">
                      <input  required  id="f03" title="Any of the following: SOA, Ledger" type="file" placeholder="" name="attachment[]" multiple />
                      <label style="color: #fff" title="Any of the following: SOA, Ledger" for="f03">1st</label>
                     </div>
                     <div  class="col-md-3" style="padding-right: 5px; padding-left: 5px;">
                      <input  required  id="f04" title="Any of the following: SOA, Ledger" type="file" placeholder="" name="attachment[]" multiple />
                      <label style="color: #fff" title="Any of the following: SOA, Ledger" for="f04">2nd</label>
                     </div>
                     <div  class="col-md-3" style="padding-left: 5px;">
                      <input  required  id="f05" type="file" placeholder="" name="attachment[]" title="Any of the following: SOA, Ledger" multiple />
                      <label style="color: #fff" title="Any of the following: SOA, Ledger" for="f05">3rd</label>
                      </div>
                   </div>
                    <div class="row px-3">
                      <div  class="col-md-3 ">
                      <label >Proof of Billing Address</label>
                     </div>
                      <div  class="col-md-3 mb-3" style="padding-right: 5px;">
                      <input  required  id="f06" type="file" placeholder="" name="attachment[]" multiple />
                      <label title="Any of the following: Company Payslip, Billing Statement" style="color: #fff" for="f06">1st</label>
                      </div>
                      <div  class="col-md-3 mb-3" style="padding-right: 5px; padding-left: 5px;">
                      <input  required  id="f07" type="file" placeholder="" name="attachment[]" multiple />
                      <label title="Any of the following: Company Payslip, Billing Statement" style="color: #fff" for="f07">2nd.</label>
                      </div>
                      <div  class="col-md-3 mb-3" style=" padding-left: 5px;">
                      <input  required  id="f08" type="file" placeholder="" name="attachment[]" multiple />
                      <label title="Any of the following: Company Payslip, Billing Statement" style="color: #fff" for="f08">3rd</label>
                      </div>
                      <div class="invalid-feedback">
                        This field is required.
                     </div>
                    </div>
                    <div class="row px-3">
                      <div class="col-md-4 ">
                          <label>Signature:</label>
                          <a id="upload-sig-btn" class="btn btn-danger btn-sm" type="button"> Upload Signiture</a>
                          <a  hidden id="manual-sig-btn" class="btn btn-danger btn-sm" type="button"> Manual Signiture</a>
                      </div>
                        <div  id="manual-sig" class="col-md-8 mb-3">
                            <br/>
                            <div id="sig"></div>
                            <br/><br/>
                            <a id="clear" class="btn btn-danger btn-sm" type="button"> Clear</a>
                            <textarea  required  id="signature" name="signed" style="display: none"></textarea>
                        </div>
                        <div hidden id="upload-sig" class="col-md-8 mb-3">
                        <input disabled id="f09" type="file" placeholder="" name="attachment[]" multiple />
                        <label title="Upload signiture" style="color: #fff" for="f09"> Upload signiture</label>
                        </div>
                    </div>
                </div>
              </div>
              <div class="card mt-4 mx-3 shadow-lg border-0">
                <div class="card-header bg-light text-primary py-2 pl-3 border-0">
                  <h6 class="m-0 default-primary">Please answer the survey to improve our services.</h6>
                </div>
                <div class="card-body">
                  <div class="row px-3">
                      <label for="marketing_channel">Where did you hear about us?</label>
                      <select class="form-control form-control-sm @error('marketing_channel_id') is-invalid @enderror"   id="marketing_channel_id"  type="text"   name="marketing_channel_id" autocomplete="marketing_channel" >
                        <option value="" selected disabled>Select</option>
                         @foreach($marketingChannel as $item)
                        <option  value="{{$item->id}}" @if (old('marketing_channel_id') == '{{$item->id}}') selected="selected" @endif>{{$item->name}}</option>
                        @endforeach
                      </select>
                      <div class="invalid-feedback">
                        This field is required.
                      </div>
                  </div> 
                </div>
              </div>
      
              <div class="card border-none mt-4 mx-3 shadow-lg">
                <div class="card-body">
                  <h6 class="card-title">Data Privacy Consent</h6>
                    <p class="card-text">In compliance with the Data Privacy Act of 2012 and Credit Information Corporation (CIC), and its Implementing Rules and Regulations, I agree and authorize the MHRMPC to use my personal information to process any transaction related in the availment of MHRMPC products and services and administer the benefits as stated in the policy and other service agreements and inform me of future customer campaign using the personal information I shared with the coop.</p>
                    <div class="custom-control custom-checkbox">
                      <input type="checkbox" name="data_consent_fl" value="on"  class="custom-control-input" id="same-address">
                      <label class="custom-control-label" for="same-address">I agree</label>
                    </div>
                </div>
              </div>
              <button class="btn btn-secondary mt-5 mb-5 btn-block" style="background-color: rgb(7,17,174);" type="submit" id="btn-save-loan"  >Submit</button>
            </form>
          </div>
      </div>
  </div>
  </div>
  <script>
 $(document).ready(function(){
      var sig = $('#sig').signature({syncField: '#signature', syncFormat: 'PNG'});
        $('#clear').click(function (e) {
            e.preventDefault();
            sig.signature('clear');
            $("#signature64").val('');
        });
 });
        </script>
@endsection

@section('preloan')

<script>
 
$(document).ready(function(){
            // event.preventDefault();
    
        });
  // $('#age_group').attr('readonly', true);
        var base_path = $("#url").val();
  var region = $("#region_id").val();
  var hidden_province_id = $("#hidden_province_id").val();
  var hidden_city_id = $("#hidden_city_id").val();
  var hidden_barangay_id = $("#hidden_barangay_id").val();
  // alert(region);
  $.ajax({
                type:"GET",
                // base_path+'getProvince/'+region_code,
                url:base_path+'/getProvince/'+region,
                success:function(res){               
                if(res){
                $("#province_id").append('<option value="">Select Province</option>'); 
                $.each(res,function(key,value){
                    // var province_id2 = value['province_id'];
                    // alert(JSON.stringify(province_id2));
                    $("#province_id").append('<option  value="'+ value['province_id'] +'" {{ $data->province_id == $item->id ? "selected='selected'" : '' }}>'+ value['name'] +'</option>');
                });
                  $("#province_id").val(hidden_province_id);
                }else{
                   $("#province_id").empty();
                }
                }
                });
  $.ajax({
               type:"GET",
               url:base_path+'/getCities/'+hidden_province_id,
               success:function(res){  
                if(res){
                    $.each(res,function(key,value){
                        $("#city_id").append('<option class="form-control" value="'+ value['city_id'] +'">'+ value['name'] +'</option>');
                    });
                    // $("#city_id").val(city_id);
                }else{
                   $("#city_id").empty();
                }
               }
                });
  $.ajax({
               type:"GET",
               url:base_path+'/getBarangays/'+hidden_city_id,
               success:function(res){  
                if(res){
                    $.each((res),function(key,value){
                        $("#barangay_id").append('<option class="form-control" value="'+ value['code'] +'">'+ value['name'] +'</option>');
                    });
                  // $("#barangay_id").val(barangay_id1);
                }else{
                   $("#barangay_id").empty();
                }
               }
            });

});
  $('#upload-sig-btn').click(function (event) {
            event.preventDefault();
            sig.signature('clear');
            $("#signature64").val('');
    $('#manual-sig').attr('hidden', true);
    $('#manual-sig-btn').attr('hidden', false);
    $('#upload-sig-btn').attr('hidden', true);
    $('#upload-sig').attr('hidden', false);
    $('#f09').attr('disabled', false);
    $('#signature').attr('disabled', true);
  });
  $('#manual-sig-btn').click(function (event) {
    $('#manual-sig').attr('hidden', false);
    $('#signature').attr('disabled', false);
    $('#f09').attr('disabled', true);
    $('#manual-sig').attr('dia', false);
    $('#manual-sig-btn').attr('hidden', true);
    $('#upload-sig-btn').attr('hidden', false);
    $('#upload-sig').attr('hidden', true);
  });


  $("[type=file]").on("change", function(){
  // Name of file and placeholder
  var file = this.files[0].name;
  var dflt = $(this).attr("placeholder");
  if($(this).val()!=""){
    $(this).next().text(file);
  } else {
    $(this).next().text(dflt);
  }
});
  $(document).on('keydown', 'input[pattern]', function(e){
  var input = $(this);
  var oldVal = input.val();
  var regex = new RegExp(input.attr('pattern'), 'g');

  setTimeout(function(){
    var newVal = input.val();
    if(!regex.test(newVal)){
      input.val(oldVal); 
    }
  }, 1);
});
  $("#amount_needed").focusout(function(event){
    var share_capital = $('#share_capital').val();
    var amount_needed = $('#amount_needed').val();
    if(share_capital > amount_needed){
      // alert('docu disabled');
      $('#uploadtext').html("Your share capital is greater than the amount needed so we dont need required documents");
      $('#f01').attr('disabled', true);
      $('#f02').attr('disabled', true);
      $('#f03').attr('disabled', true);
      $('#f04').attr('disabled', true);
      $('#f05').attr('disabled', true);
      $('#f06').attr('disabled', true);
      $('#f07').attr('disabled', true);
      $('#f08').attr('disabled', true);
      $('#f09').attr('disabled', true);
      $('#signature').attr('disabled', true);
      $('#document').attr('hidden', true);
    }else{
      $('#uploadtext').html("");
      $('#f01').attr('disabled', false);
      $('#f02').attr('disabled', false);
      $('#f03').attr('disabled', false);
      $('#f04').attr('disabled', false);
      $('#f05').attr('disabled', false);
      $('#f06').attr('disabled', false);
      $('#f07').attr('disabled', false);
      $('#f08').attr('disabled', false);
      $('#f09').attr('disabled', false);
      $('#signature').attr('disabled', false);
      $('#document').attr('hidden', false);
    }
    alert('Your desired amount is for C.I approval. You will be informed for the approved amount once C.I is done.');
  });
</script>
@endsection

