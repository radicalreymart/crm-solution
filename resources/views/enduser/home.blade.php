<!-- @extends('layouts.app1') -->

@section('loanstyle')
<style>
	.alert{
		color: black !important;
	}
	[type=file] {
			position: absolute;
			filter: alpha(opacity=0);
			opacity: 0;
	}
	input,
	[type=file] + label {
		border: 1px solid black;
		border-radius: 3px;
		text-align: left;
		padding: 0px;
		width: 100%;
		height: 30px;
		margin: 0;
		left: 0;
		position: relative;
	}
	[type=file] + label {
		text-align: center;
		left: 0em;
		top: 0.5em;
		/* Decorative */
		background: blue;
		color: #fff;
		border: black;
		cursor: pointer;
	}
	[type=file] + label:hover {
		background: #3399ff;
	}
	label.shorten_text1 {
	 max-width: 100%;
		/*padding: px;*/
		text-overflow: ellipsis;
		white-space: nowrap;
		overflow: hidden;
	}

	label.shorten_text1:hover {
		overflow: visible;
	}
	label.shorten_text1:before{
		content: attr(title);
	}
	.step {
		position:relative;
		padding:10px;
		border-left:5px solid rgb(122, 197, 228);
		position:relative;
		margin-left:1em;
		padding-left:2em;
	}
	.step h2{
		border-top-right-radius: 1em;
		border-bottom-right-radius: 1em;
		border: 3px solid rgb(122, 197, 228);
		margin:0;
		text-indent:1em;
		color:rgb(122, 197, 228);
		font-weight:normal;
		font-size:1.3em;
		margin-left:-1em;
	}
	/* The Balls*/
	.step h2::before{
		content:attr(data-step-id);
		border-radius: 2em;
		background-color: rgb(122, 197, 228);
		width:2em;
		height:2em;
		display:inline-block;
		position:absolute;
		left:-1.1em;
		top:0;
		text-align:center;
		line-height:2em;
		font-size:1.2em;
		font-weight:normal;
		color: #EEEEEE;
		text-indent:0;
	}
	/* Active step: */
	.step.active h2{
		background-color:rgb(122, 197, 228);
		color:#FFFFFF;
		font-weight:bold;
	}
	.step.active h2::before{
		border-right:3px solid #FFFFFF;
	}
	/* Arrow */
	.step::before{
		content:"";
		border:0.8em solid transparent;
		border-top-color:rgb(122, 197, 228);
		border-top-width:1.5em;
		position:absolute;
		display:inline-block;
		left:-0.9em;
		bottom:-0.9em;
	}
	/* Inactive */
	.step.inactive{ border-color:#bbbbbb; }
	.step.inactive::before{ border-top-color:#bbbbbb; }
	.step.inactive h2{ color:#aaaaaa;border-color:#bbbbbb; }
	.step.inactive h2::before{background-color: #bbbbbb;}

	/* The last step: */
	[class*='step']:last-of-type{border-color:transparent;}
	[class*='step']:last-of-type::before{content:none;}

	/* ----------------------- */
	/* The button*/
	/*button{
		background-color:#BBBBBB;
		pointer-events: none;
		border:none;
		color:#FFFFFF;
		border-radius:5px;
		font-size:1.2em;
		padding:5px 10px;
		outline:none;
	}*/
	.step.active .back,
	.step.active .next{
		background-color:rgb(122, 197, 228);
		cursor:pointer;
		pointer-events: auto;
	}
	.step.inactive button{
		visibility:hidden;
	}
	button::-moz-focus-inner {border: 0;}
	/* ----------------------- */
	/* Basic page layout */
	/*body {
		max-width:600px;
		margin:auto;
		border:1px solid #aaa;
		padding:1em;
		font-size:1em;
		font-family:Arial,Helvetica,sans-serif;
	}*/
	/*@media all and (max-width: 400px){
		.step h2 {font-size:1.1em}
	}
	tr {float: left!important; width: 50%!important;}*/
</style>
@endsection

@section('content')
<!-- Masthead-->
<header class="masthead mb-1">
	
</header>
    @include('flash-message')
	@include('sweetalert::alert')
<div class="container mt-5 col-md-8" id="sign_up">
	<div class="card p-2 pt-3 bg-light border-0">
		<div class="row"> 
			<div class="col-md-12 order-md-1">
				<div class="card mx-3 shadow-lg border-0">
					<div class="card-header bg-light text-primary py-2 pl-3 border-0">
						<h6 class="m-0 default-primary">Member Information 
			              <a class="logout hvr-icon-push float-right" id="logoutid" data-feedback='{{url("/enduser/sitefeedback", "logout")}}' data-url="{{ url('/force-logout') }}">{{ __('Logout') }}<i class="now-ui-icons media-1_button-power hvr-icon"></i></a>
			          	</h6>
					</div>
					<div class="card-body px-3">
						<div class="justify-content">
							<input type="hidden" value="{{url('/')}}" id="url" name="url">
							
						<div class="row">
							<div class="col">
								Loans
							</div>
							<div class="col">
								Payment
							</div>
						</div>
						<div class="row">
							<div class="col">
						 		@if(empty($preloan1))
									<a class="btn btn-outline-light btn-xl text-uppercase" style="color: black; border-color: black; " type="button" href="{{url('/preloan/create')}}">Loan now</a>
								@else
									<a class="btn btn-outline-light" style="color: black; height: 40px; border-color: black; " type="button" href="{{route('enduser.preloan.index')}}" > <p id="">Loan List</p> <br> </a>
								@endif
							</div>
							<div class="col">
								@if(empty($optm))
									<a class="btn btn-outline-light btn-xl text-uppercase " style="color: black; border-color: black; " type="button" href="{{route('enduser.optm.create')}}">Online Payment</a>
								@else
									<a class="btn btn-outline-light" style="color: black; height: 40px; border-color: black; " type="button" href="{{route('enduser.optm.index')}}" > <p id="">Payment List</p> <br> </a>
								@endif
							</div>
						</div>


						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

	
@endsection

@section('preloan')
<script>
	// JS for setting the "active" step class


	$("[type=file]").on("change", function(){
	// Name of file and placeholder
	var file = this.files[0].name;
	var dflt = $(this).attr("placeholder");
	if($(this).val()!=""){
		$(this).next().text(file);
	} else {
		$(this).next().text(dflt);
	}
	});



	$(document).on('keydown', 'input[pattern]', function(e){
		var input = $(this);
		var oldVal = input.val();
		var regex = new RegExp(input.attr('pattern'), 'g');

		setTimeout(function(){
			var newVal = input.val();
			if(!regex.test(newVal)){
				input.val(oldVal); 
			}
		}, 1);
	});

$(document).ready(function(){
	setTimeout(function(){
	// alert('hei');
	   $("div.alert").remove();
	}, 5000 ); // 5 secs
		$.ajaxSetup({
				headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
		});
});


</script>
@endsection

