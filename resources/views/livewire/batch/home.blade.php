@extends('layouts.app', [
    'class' => 'sidebar-mini ',
    'namePage' => 'Batch Index',
    'activePage' => 'batch-management',
    'activeNav' => '',
])

@section('indexdeleteall')
  <script src="{{ asset('assets') }}/js/indexdeleteall.js"></script>
@endsection
@section('content')
<div>
    @livewire('batchv1')
</div>
@endsection
@section('batchscript')

<script src="{{ asset('assets') }}/js/batchdatepicker.js"></script>
<script src="{{ asset('assets') }}/js/editbatchdatepicker.js"></script>
<script type="text/javascript">
     window.addEventListener('DOMContentLoaded', event => {
    // Simple-DataTables
    // https://github.com/fiduswriter/Simple-DataTables/wiki
    
    const datatablesSimple = document.getElementById('datatablesSimple');
    if (datatablesSimple) {
        new simpleDatatables.DataTable(datatablesSimple, {
    searchable: false,
    fixedHeight: true
        });
    }
  });
        window.livewire.on('batchStore', () => {
            $('#exampleModal').modal('hide');
            $('.modal-backdrop').remove();
            setTimeout(function(){
   // alert('hei');
       $("div.alert").remove();
    }, 5000);
        });
</script>
@endsection