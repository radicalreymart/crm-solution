@extends('layouts.app', [
    'class' => 'sidebar-mini ',
    'namePage' => 'Client Index',
    'activePage' => 'client-management',
    'activeNav' => '',
])

@section('indexdeleteall')
  <script src="{{ asset('assets') }}/js/indexdeleteall.js"></script>
@endsection
@section('content')
<div>
    @livewire('clientv1')
</div>
@endsection
@section('batchscript')

<script type="text/javascript">
     window.addEventListener('DOMContentLoaded', event => {
    // Simple-DataTables
    // https://github.com/fiduswriter/Simple-DataTables/wiki
    
    const datatablesSimple = document.getElementById('datatablesSimple');
    if (datatablesSimple) {
        new simpleDatatables.DataTable(datatablesSimple, {
    searchable: false,
    fixedHeight: true
        });
    }
  });
        window.livewire.on('clientStore', () => {
            $('#exampleModal').modal('hide');
            $('.modal-backdrop').remove();
            setTimeout(function(){
   // alert('hei');
       $("div.alert").remove();
    }, 5000);
        });
</script>
@endsection