<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Branch;

class Member extends Model
{
    use HasFactory;
    protected $table = 'master_file';
    public $timestamps = false;
    protected $fillable = [
          'member_no'
        , 'fname'
        , 'mname'
        , 'lname'
        , 'long_name'
        , 'branch_id'
        , 'contact_no'
        , 'email'
        , 'member_type'
        , 'member_status'
        , 'gender'
        , 'member_date'
        , 'birthdate'
        , 'uni_key'
        , 'points'
        , 'migs_fl'
        , 'migs_year'
        , 'migs_reason'
        , 'old_points'
        , 'crea_by'
        , 'crea_date'
        , 'upd_by'
        , 'upd_date'

    ];

    public function branch()
    {
        return $this->belongsTo('App\Models\Branch','branch_id','id');
    }
    
}
