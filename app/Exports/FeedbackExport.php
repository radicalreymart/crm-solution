<?php

namespace App\Exports;

use App\Models\Feedback;
use App\Http\Controllers\FeedbackController;
use Maatwebsite\Excel\Concerns\FromCollection;
use Carbon;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithCustomCsvSettings;
use PhpOffice\PhpSpreadsheet\Cell\Cell;
use PhpOffice\PhpSpreadsheet\Cell\DataType;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class FeedbackExport implements  FromQuery, WithHeadings, WithMapping, ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */
    protected $from_date;
    protected $to_date;

    public function __construct($from_date, $to_date) {
        $this->from_date = $from_date;
        $this->to_date = $to_date;
    }
    public function query()
    {
        $from = $this->from_date;
        $to = $this->to_date;
        $branch = auth()->user()->branch_id;
            $feedback = Feedback::with('member', 'branch', 'branchLoc');
            // dd($miuf->get());
            if($branch == 9){
                $feedback->orderBy('id');
            }else{
                $feedback->where('branch_loc', $branch)
                ->orderBy('id');
                // ->orderBy('serv_status');
            }
            $feedback->whereBetween('created_at', [$from, $to]);
            $feedback = $feedback->orderBy('id');
            // dd($feedback->get());
        return $feedback;
    }


    public function headings(): array
    {
        return [
         'member_no'
         , 'fullname'
         , 'gender'
         , 'branch_id'
         , 'feedback_for'
         , 'date_exp'
         , 'member_type'
        , 'transaction_nature'
        ,'rate1'
        , 'rate2'
        ,'rate3'
        ,'rate4'
        ,'rate5'
        ,'rate6'
         , 'comment_suggestion'
         , 'last_feedback'
         , 'crea_by'
         , 'upd_by'
         , 'created_at'
         , 'updated_at'
        ];
    }

    public function map($feedback): array
    {
        $from = $this->from_date;
        $to = $this->to_date;
        if($feedback->gender == 1){
            $gender = 'Male';
        }else{
            $gender = 'Female';
        }
        if($feedback->member_no == null){
            $type = 'Non Member';
        }elseif($feedback->member->member_type == 1){
            $type = 'Associate Member';
        }elseif($feedback->member->member_type == 2){
            $type = 'Regular Member';
        }

        foreach($feedback->transaction_nat as $key=>$value){
            if(!is_null($value)){
            $transaction_nat[] = $key;
            }
        }
        // dd($feedback);
         $feedback_controller = new FeedbackController;
         $feedback_controller->updateFeedback($feedback, $from, $to);
        
        return [
            // 'hello'
              "'".$feedback->member_no
            , $feedback->fullname
            , $gender
            , $feedback->branch->name
            , $feedback->branchLoc->name
            , $feedback->exp_date
            , $type
            , $transaction_nat
            , $feedback->transaction_exp['rate1']
            , $feedback->transaction_exp['rate2']
            , $feedback->transaction_exp['rate3']
            , $feedback->transaction_exp['rate4']
            , $feedback->transaction_exp['rate5']
            , $feedback->transaction_exp['rate6']
            , $feedback->comment_suggestion
            ,Carbon\Carbon::parse($feedback->last_feedback)->format('m-d-Y')
            , $feedback->crea_by
            , $feedback->upd_by
            , Carbon\Carbon::parse($feedback->created_at)->format('m-d-Y')
            , Carbon\Carbon::parse($feedback->updated_at)->format('m-d-Y')
        ];
    }

}
