<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Yajra\Address\HasAddress;
use App\Models\Occupation;
use App\Models\Batch;
use App\Models\MarketingChannel;
use App\Models\Office;
use App\Models\Branch;
// use App\Models\Region;
// use App\Models\Province;
// use App\Models\City;
// use App\Models\Barangay;
// use App\Models\pmesMaster;
// use App\Models\ServiceTrxnMaster;

class Client extends Model
{
    use HasFactory;
    use HasAddress;

    protected $table = 'client_masters';
     protected $casts = ['of_tag'];
                protected $fillable = [
        'fname'
      ,'mname'
      ,'lname'
      ,'fullname'
      ,'contact_no'
      ,'token'
      ,'fb_link'
      ,'member_status'
      ,'member_no'
      ,'seminar_pref'
      ,'batch_id'
      ,'gmail_address'
      ,'sched_stat'
      ,'sched_date'
      ,'branch_id'
      ,'gender'
      ,'birthdate'
      ,'age'
      ,'street'
      ,'barangay_id'
      ,'city_id'
      ,'province_id'
      ,'region_id'
      ,'barangay_primary'
      ,'city_primary'
      ,'province_primary'
      ,'region_primary'
      ,'occupation_id'
      ,'other_occupation'
      ,'of_tag'
      ,'purpose_tag'
      ,'marketing_channel_id'
      ,'staff_referral_cd'
      ,'data_consent_fl'
      ,'crea_by'
      ,'upd_by'
      ,'soft_deleted_at'
            ];

     
     public function batch()
    {
        return $this->belongsTo('App\Models\Batch','batch_id','id');
    }
     public function occupation()
    {
        return $this->belongsTo(Occupation::class);
    }
     public function marketingChannel()
    {
        return $this->belongsTo(MarketingChannel::class);
    }
    public function branch()
    {
        return $this->belongsTo(Branch::class);
    }
     
    // public function svctrn()
    // {
    //     return $this->hasMany(ServiceTrxnMaster::class);
    // }
    //  public function pmes()
    // {
    //     return $this->hasOne(pmesMaster::class);
    // }
}
