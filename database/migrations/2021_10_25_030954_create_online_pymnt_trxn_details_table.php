<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOnlinePymntTrxnDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('online_pymnt_trxn_details', function (Blueprint $table) {
            $table->engine = "InnoDB";
            $table->id();
            $table->unsignedBigInteger('online_pymnt_id');
            $table->foreign('online_pymnt_id', 'payment_fk_online_pymnt_id')->references('id')->on('online_pymnt_trxn_masters');

            $table->unsignedBigInteger('acct_id')->nullable();
            $table->foreign('acct_id', 'payment_fk_acct_id')->references('id')->on('acct_masters');

            $table->string('ref_number', 15)->index()->nullable();
            // $table->string('ref_number', 15)->nullable();
            // $table->foreign('ref_number', 'fk_ref_number')->references('ref_number')->on('pre_loan_applications');

            $table->decimal('amount', 10, 2)->nullable();
            $table->string('crea_by');
            $table->string('upd_by')->nullable();
            $table->datetime('soft_deleted_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('online_pymnt_trxn_details');
    }
}
