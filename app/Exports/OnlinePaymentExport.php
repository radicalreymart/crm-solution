<?php

namespace App\Exports;

use App\Models\OnlinePymntTrxnMaster;
use App\Models\OnlinePymntTrxnDetail;
use Maatwebsite\Excel\Concerns\FromCollection;
use Carbon;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithCustomCsvSettings;
use PhpOffice\PhpSpreadsheet\Cell\Cell;
use PhpOffice\PhpSpreadsheet\Cell\DataType;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class OnlinePaymentExport implements FromQuery, WithHeadings, WithMapping, ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */
     protected $from_date;
    protected $to_date;

    public function __construct($from_date, $to_date) {
        $this->from_date = $from_date;
        $this->to_date = $to_date;
    }
    public function query()
    {
        $from = $this->from_date;
        $to = $this->to_date;
        // $to = Carbon\Carbon::now();
            $branch = auth()->user()->branch_id;
            $optm  = OnlinePymntTrxnMaster::with('usermember', 'bank', 'branch');
            // dd($miuf->get());
            if($branch == 9){
                // $pmes->query();
                $optm->whereNull('soft_delete_at');
            }else{
                // $pmes->query()
                $optm->where('client_branch', $branch)->whereNull('soft_delete_at');
                // ->orderBy('serv_status');
            }
            $optm->whereBetween('created_at', [$from, $to]);
            $optm = $optm->orderBy('id');
            // dd($optm->get());
        return $optm;

    }
    public function headings(): array
    {
        return [
            'id',
            "member_no"
            ,"contact_no" 
            ,"birthdate" 
            ,"fb_link" 
            ,"street" 
            ,"barangay_id" 
            ,"city_id"
            ,"province_id" 
            ,"region_id"
            ,"fullname" 
        ,'documents'
        ,'bank_id'
        ,'branch_id'
        ,'total_amt'
        ,'payment_details'
        ,'date_trxn_rcvd'
        ,'date_trxn_or_entry'
        ,'or_ref_no'
        ,'or_by'
        ,'status'
        ,'crea_by'
        ,'created_at'
        ,'upd_by'
        ,'updated_at'
        ];
    }
    public function map($optm): array
    {
        $optd  = OnlinePymntTrxnDetail::with('account')->where('online_pymnt_id', $optm->id)
        ->get()->toArray();
        $i = 0;
        foreach ($optd as $data) {
        $payment_details[] = number_format($data['amount'], 2).', '.$data['account']['description'];
        }
        
        if($optm->usermember->region == null){
            $barangay = null;
             $city = null;
             $province = null;
             $region = null;
        }else{
             $barangay = $optm->usermember->barangay->name;
             $city = $optm->usermember->city->name;
             $province = $optm->usermember->province->name;
             $region = $optm->usermember->region->name;
        }
        ($optm->status == 2 ? $status = 'OR Pending' :  $status = 'Complete');
        return [
            $optm->id
            , $optm->usermember->member_no
            ,$optm->usermember->contact_no
            ,$optm->usermember->birthdate
            ,$optm->usermember->fb_link
            ,$optm->usermember->street
            , $barangay
            , $city
            , $province
            , $region
            ,$optm->usermember->crea_by
            ,$optm->file_path
        ,$optm->bank->description
        ,$optm->branch->name
        ,number_format($optm->total_amt, 2)
        ,$payment_details
        ,$optm->date_trxn_rcvd
        ,$optm->date_trxn_or_entry
        ,$optm->or_ref_no
        ,$optm->or_by
        ,$status
        ,$optm->crea_by
        ,$optm->created_at
        ,$optm->upd_by
        ,$optm->updated_at
        ];
    }
}
