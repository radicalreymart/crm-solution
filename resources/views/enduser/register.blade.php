@extends('layouts.app1')

@section('loanstyle')
<style>
.alert{
  color: black !important;
}
[type=file] {
    position: absolute;
    filter: alpha(opacity=0);
    opacity: 0;
}
input,
[type=file] + label {
  border: 1px solid black;
  border-radius: 3px;
  text-align: left;
  padding: 0px;
  width: 100%;
  height: 30px;
  margin: 0;
  left: 0;
  position: relative;
}
[type=file] + label {
  text-align: center;
  left: 0em;
  top: 0.5em;
  /* Decorative */
  background: blue;
  color: #fff;
  border: black;
  cursor: pointer;
}
[type=file] + label:hover {
  background: #3399ff;
}
label.shorten_text1 {
 max-width: 100%;
  /*padding: px;*/
  text-overflow: ellipsis;
  white-space: nowrap;
  overflow: hidden;
}

label.shorten_text1:hover {
  overflow: visible;
}
label.shorten_text1:before{
  content: attr(title);
}
</style>
@endsection

@section('content')
    <!-- Masthead-->
    <header class="masthead mb-1">
      
    </header>

    <div class="container mt-5 col-md-8" id="sign_up">
        <div hidden id="member_verify"  class="card p-2 pt-3 bg-light border-0">
            <div class="row"> 
              <div class="col-md-12 order-md-1">
                <form method="POST" name='formname1'  >
                    @csrf
                  <input type="hidden" value="{{url('/')}}" id="url1" name="url1">
                  <div class="card mx-3 shadow-lg border-0">
                    <div class="card-header bg-light text-primary py-2 pl-3 border-0">
                      <h6 class="m-0 default-primary">Personal Information<a class="text-uppercase float-right" style="color: black; border-color: black; " type="button" href="{{url('/login')}}">Already registered?</a></h6>
                    </div>
                    <div class="card-body px-3">
                      <div class="row px-3">
                        <div class="col-md-6 mb-3">
                          <label for="member_no2">Member Number</label>
                          <div class="row px-3">

                            

                          <input type="text" oninput="this.value = this.value.toUpperCase()"  class="col-md form-control form-control-sm  @error('member_no2') is-invalid @enderror" id="member_no2"  name="member_no2" placeholder="Ex: 0005, SRR-0251 or KV-0001"    value="{{ old('member_no2') }}"  autocomplete="member_no2" required />

                          </div>
                          <div class="invalid-feedback">
                            Member Number is required.
                          </div>
                        </div>
                        <div class="col-md-6 mb-3">
                          <label for="unique_key1">Unique Key</label>

                          <input type="text" class="form-control form-control-sm  @error('unique_key1') is-invalid @enderror" id="unique_key1"  name="unique_key1" placeholder="Ex: oTw31e (case sensitive)"  maxlength="6"  autocomplete="unique_key1"  value="{{ old('member_no2') }}" required />

                          <div class="invalid-feedback">
                            Unique Key is required.
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <button class="btn btn-secondary mt-5 mb-5 btn-block" style="background-color: rgb(7,17,174);" type="submit" id="btn-verify">Submit</button>
                </form>
              </div>
            </div>
        </div>





        <div hidden id="member_reg" class="card p-2 pt-3 bg-light border-0">
            <div class="row"> 
              <div class="col-md-12 order-md-1">
                <form method="POST" name='formname' enctype="multipart/form-data" action="{{ route('preloan.store') }}">
                  <input type="hidden" value="{{url('/')}}" id="url" name="url">
                                            @csrf
                  <div class="card mx-3 shadow-lg border-0">
                    <div class="card-header bg-light text-primary py-2 pl-3 border-0">
                      <h6 class="m-0 default-primary">Personal Information
                        <a class=" hvr-icon-push float-right" onClick="window.location.href = '/enduser/register';">{{ __('Back') }}<i class="far fa-caret-square-left hvr-icon"></i></a>
                      </h6>
                    </div>
                    <div class="card-body px-3">
                      <div class="row px-3">
                        <div class="col-md-4 mb-3">
                          <label for="username">Username <span class="text-danger">*</span></label>
                          <input type="text" class="form-control form-control-sm  @error('username') is-invalid @enderror" id="username" required minlength='3' min="3" name="username" placeholder="Username" autocomplete="username">
                          <p id="resultUser"></p>
                          <div class="invalid-feedback">
                            Username is required.
                          </div>
                        </div>
                        <div class="col-md-4 mb-3">
                          <label for="password">Password <span class="text-danger">*</span></label>
                          <input type="password" class="form-control form-control-sm  @error('password') is-invalid @enderror" id="password" required name="password" minlength='8' min="8"  placeholder="Password" autocomplete="password">
                          <p id="resultPass"></p>
                          <div class="invalid-feedback">
                            Password is required.
                          </div>
                        </div>
                        <div class="col-md-4 mb-3">
                          <label for="password-confirm">Confirm Password <span class="text-danger">*</span></label>
                          <input type="password" class="form-control form-control-sm  @error('password-confirm') is-invalid @enderror"  required placeholder="Password Confirmation" minlength='8' min="8" id="password-confirm" name="password-confirm">
                          <p id="resultConfirm"></p>
                          <div class="invalid-feedback">
                            Password is required.
                          </div>
                        </div>
                      </div>
                      <div class="row px-3">
                        <div class="col-md-6 mb-3">
                          <label for="member_no1">Member Number</label>
                          <input readonly tabindex="-1" type="text" class="form-control form-control-sm  @error('member_no1') is-invalid @enderror" id="member_no1" required name="member_no1" placeholder="Member Number" value="{{ old('member_no1') }}"  autocomplete="member_no1">
                          <div class="invalid-feedback">
                            Member Number is required.
                          </div>
                        </div>
                        <div class="col-md-6 mb-3">
                          <label for="unique_key">Unique Key</label>
                          <input readonly tabindex="-1" type="text" class="form-control form-control-sm  @error('unique_key') is-invalid @enderror" id="unique_key" required name="unique_key" placeholder="Unique Key" value="{{ old('unique_key') }}"  autocomplete="unique_key">
                          <div class="invalid-feedback">
                            Unique Key is required.
                          </div>
                        </div>
                      </div>
                      <div class="row px-3">
                        <div class="col-md-6 mb-3">
                          <label for="fullname">Full Name</label>
                          <input readonly type="text" class="form-control form-control-sm  @error('fullname') is-invalid @enderror" id="fullname" tabindex="-1" required name="fullname"  value="{{ old('fullname') }}"  autocomplete="fullname">
                        </div>
                        <div class="col-md-6 mb-3">
                          <label for="gender">Gender</label>
                          <input readonly type="text" class="form-control form-control-sm  @error('gender') is-invalid @enderror" id="gender" tabindex="-1" required name="gender"  value="{{ old('gender') }}"  autocomplete="gender">
                          
                        </div>
                      </div> <!-- Long Name -->
                      <div class="row px-3">
                        <div class="col-md-4 mb-3">
                          <label for="birthdate">Birthday</label>
                          <input readonly type="date" class="form-control form-control-sm  @error('birthdate') is-invalid @enderror" id="birthdate" tabindex="-1" required name="birthdate"  value="{{ old('birthdate') }}"  autocomplete="birthdate">

                         <!--  <select class="custom-select custom-select-sm  custom-select custom-select-sm -sm d-block w-100 @error('age_group')  is-invalid @enderror" id="age_group"  required  type="text" name="age_group" autocomplete="age_group">
                            <option value="">Select Age Group</option>
                            <option value="0" @if (old('age_group') == '0') selected="selected" @endif>18 - 24</option>
                            <option value="1" @if (old('age_group') == '1') selected="selected" @endif>25 - 34</option>
                            <option value="2" @if (old('age_group') == '2') selected="selected" @endif>35 - 44</option>
                            <option value="3" @if (old('age_group') == '3') selected="selected" @endif>45 - 54</option>
                            <option value="4" @if (old('age_group') == '4') selected="selected" @endif>55 - 64</option>
                            <option value="5" @if (old('age_group') == '5') selected="selected" @endif>65 - Up</option>
                          </select> -->
                          <div class="invalid-feedback">
                            Brithday is required.
                          </div>
                        </div>
                        <div class="col-md-4 mb-3">
                          <label for="branch">Branch <span class="text-danger">*</span></label>
                          <select class="form-control custom-select custom-select-sm @error('branch_id') is-invalid @enderror" id="branch_id"  type="text" name="branch_id" autocomplete="branch_id" >
                            <option value="" selected disabled>Select Branch</option>
                            @foreach($branches as $item)
                            <option id="branch" value="{{$item->id}}">{{$item->name}}</option>
                            @endforeach
                          </select>
                          <div class="invalid-feedback">
                            Branch is required.
                          </div>
                        </div>
                        <div class="col-md-4 mb-3">
                          <label for="contact_no">Contact Number <span class="text-danger">*</span></label>
                          <div class="input-group input-group-sm">
                            <div style="border: dimgrey !important;" class="input-group-prepend">
                              <span class="input-group-text">+63</span>
                            </div>
                            <input type="text" class="form-control form-control-sm @error('contact_no') is-invalid @enderror" id="contact_no" placeholder="10-digit Mobile Number"  minlength="0" maxlength="10" value="{{ old('contact_no') }}"  onkeydown="return ( event.ctrlKey || event.altKey 
                        || (47<event.keyCode && event.keyCode<58 && event.shiftKey==false) 
                        || (95<event.keyCode && event.keyCode<106)
                        || (event.keyCode==8) || (event.keyCode==9) 
                        || (event.keyCode>34 && event.keyCode<40) 
                        || (event.keyCode==46))" name="contact_no" style="border-radius: 0;"  required autocomplete="contact_no"  >
                            <div class="invalid-feedback" style="width: 100%;">
                              Contact Number is required.
                            </div>
                          </div>
                        </div>
                      </div> <!-- Birthdate, Age and Gender -->
                      <div class="row px-3">
                        
              
                        <div class="col-md-6 mb-3 mt-0">
                          <label for="gmail_address">Email Address  <span class="text-danger">*</span></label>
                            <input type="email" class="form-control-sm form-control @error('email') is-invalid @enderror" id="email" value="{{ old('email') }}" required name="email" autocomplete="email">

                            <p id="result"></p>
                            <div class="invalid-feedback" >
                              Your Email Address is required.
                            </div>
                        </div>
                        <div class="col-md-6 mb-3">
                          <label for="gmail_address">Facebook Link</label>
                            <i class="fa-brands fa-facebook-messenger"></i>
                          <input type="text"  class="form-control form-control-sm @error('fb_link') is-invalid @enderror" id="fb_link" value="{{ old('fb_link') }}"   name="fb_link" autocomplete="fb_link">
                          <div class="invalid-feedback">
                            This field is required.
                          </div>
                          </div>  
                      </div> <!-- Contact Information -->
                        <div class="row px-3">
                          
                          
                       </div>
                      <div class="row px-3">
                        <div class="col-md-12 mb-3">
                          
                        </div>
                      </div>
                    </div>
                  </div>
          
                  <div class="card mt-4 mx-3 shadow-lg border-0">
                    <div class="card-header bg-light text-primary pt-2 pl-3 border-0">
                      <h6 class="m-0 default-primary">Present Address  <span class="text-danger">*</span></h6>
                    </div>
                    <div class="card-body">
                      <div class="row px-3">
                        <div class="col-md-4 mb-3">
                          <label for="region_code">Region  <span class="text-danger">*</span></label>
                          <select class="custom-select custom-select-sm @error('region_id') is-invalid @enderror" id="region_id"  type="text" name="region_id"  required >
                            <option value="">Select Region</option>
                             @foreach($regions as $item)
                            <option id="region_code" value="{{$item->region_id}}">{{$item->name}}</option>
                            @endforeach
                          </select>
                          <div class="invalid-feedback">
                            Region is required.
                          </div>
                        </div>
                        <div class="col-md-4 mb-3">
                          <label for="province_id">Province  <span class="text-danger">*</span></label>
                          <select readonly class="form-control custom-select custom-select-sm"  required id="province_id"   name="province_id" autocomplete="province_id" >
                            <option value="">Select Province</option>
                          </select>
                          <div class="invalid-feedback">
                            Province is required.
                          </div>
                        </div>
                        <div class="col-md-4 mb-3">
                          <label for="city_id">City/Municipality  <span class="text-danger">*</span></label>
                          <select readonly class="form-control custom-select custom-select-sm" id="city_id"  required   name="city_id" autocomplete="city_id" >
                            <option value="">Select City/Municipality</option>
                          </select>
                          <div class="invalid-feedback">
                            City/Municipality is required.
                          </div>
                        </div>
                      </div> <!-- Region, Province, Municiaplity -->
                      <div class="row px-3">
                        <div class="col-md-4 mb-3">
                          <label for="barangay_id">Barangay</label>
                          <select  class="form-control custom-select custom-select-sm"  required readonly id="barangay_id"  type="text" name="barangay_id" autocomplete="barangay_id" >
                            <option value="">Select Barangay</option>
                          </select>
                          <div class="invalid-feedback">
                            Barangay is required.
                          </div>
                        </div>
                        <div class="col-md-8 mb-3">
                          <label for="street">Street</label>
                          <input type="text" class="form-control form-control-sm @error('street') is-invalid @enderror" id="street" value="{{ old('street') }}"  required placeholder="Bldg. No./Street/Apartment No." name="street">
                        </div>
                      </div> <!-- Barangay, Street -->
                    </div>
                  </div>
          
                  <div class="card border-none mt-4 mx-3 shadow-lg">
                    <div class="card-body">
                      <h6 class="card-title">Data Privacy Consent</h6>
                        <p class="card-text">In compliance with the Data Privacy Act of 2012 and Credit Information Corporation (CIC), and its Implementing Rules and Regulations, I agree and authorize the MHRMPC to use my personal information to process any transaction related in the availment of MHRMPC products and services and administer the benefits as stated in the policy and other service agreements and inform me of future customer campaign using the personal information I shared with the coop.</p>
                        <div class="custom-control custom-checkbox">
                          <input type="checkbox" name="data_consent_fl" value="on"  class="custom-control-input" id="same-address">
                          <label class="custom-control-label" for="same-address">I agree</label>
                        </div>
                    </div>
                  </div>
                  <button class="btn btn-secondary mt-5 mb-5 btn-block" style="background-color: rgb(7,17,174);" type="submit" id="btn-save">Submit</button>
                </form>
              </div>
            </div>
        </div>
    </div>
  
@endsection

@section('preloan')
<!-- <script type="text/javascript" src="date.js"></script> -->
<script>

function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
} 
  $("[type=file]").on("change", function(){
  // Name of file and placeholder
  var file = this.files[0].name;
  var dflt = $(this).attr("placeholder");
  if($(this).val()!=""){
    $(this).next().text(file);
  } else {
    $(this).next().text(dflt);
  }
});

$(document).on('keydown', 'input[pattern]', function(e){
  var input = $(this);
  var oldVal = input.val();
  var regex = new RegExp(input.attr('pattern'), 'g');

  setTimeout(function(){
    var newVal = input.val();
    if(!regex.test(newVal)){
      input.val(oldVal); 
    }
  }, 1);
});

$(document).ready(function(){

                 $('#member_verify').attr('hidden', false);
 $.ajaxSetup({
        headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
  const swalWithBootstrapButtons = Swal.mixin({
                  customClass: {
                    confirmButton: 'btn btn-success',
                    cancelButton: 'btn btn-danger'
                  },
                  buttonsStyling: false
                }) 

    $('#btn-verify').click(function (event) {
         // alert('hello');
          try{
          // var id = $("#id").val();
          var prefix_cd = $("#prefix_cd").val();
          var member_no2 = $("#member_no2").val();
          var unique_key1 = $("#unique_key1").val();
          $("#btn-verify").html('Please Wait...');
          $("#btn-verify"). attr("disabled", true);
            // event.preventDefault();           
          }
            catch(err) {
            swalWithBootstrapButtons.fire({
                  title: 'Error!',
                  text: err,
                  icon: 'warning',
                  confirmButtonText: 'Confirmed',
                }).then((result) => {
             // window.location.reload();
            $("#btn-verify").html('Submit');
            $("#btn-verify"). attr("disabled", false);
                })
            }
        // ajax

        $.ajax({
            type:"get",
            url: "{{ url('enduser-verify') }}",
            // headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            data: {
              // id:id,
              prefix_cd:prefix_cd,
              member_no2:member_no2,
              unique_key1:unique_key1,
            },
            dataType: 'json',
            success: function(data){
             // alert(JSON.stringify(data.birthdate));
              //  var str = mydate[0].toString("M/dd/yyyy");
              // var birthdate = data[0].birthdate;
             //  alert(str);
                $.each( data, function( key, value ) {
              if(key == 'error'){
               swalWithBootstrapButtons.fire({
                title: 'Error!',
                text: value,
                icon: 'warning',
                cancelButtonText: 'Ok',
             }).then((result) => {
             // window.location.reload();
             // alert('success');
             })   
              }else{
               var mydate =  data[0].birthdate.split(' ');
                if(data[0].gender == 'F'){
                  var gender = 'Female';
                }else{
                  var gender = 'Male';  
                }
                swalWithBootstrapButtons.fire({
                title: 'Success!',
                icon: 'success',
                confirmButtonText: 'Confirmed',
                  }).then((result) => {
                 $('#member_verify').attr('hidden', true);
                 $('#member_reg').attr('hidden', false);
                 $('#member_no1').val(data[0].member_no);
                 $('#unique_key').val(unique_key1);
                 $('#fullname').val(data[0].long_name);
                 $('#branch_id').val(data[0].branch_id);
                 $('#username').val(data[0].member_no);
                 $('#gender').val(gender);
                 $('#birthdate').val(mydate[0]);
             })   
              }
                });
            $("#btn-verify").html('Submit');
            $("#btn-verify").attr("disabled", false);
           }
           ,error: function (data) {
            var errors = [];
            $.each( data.responseJSON['errors'], function( key, value ) {
                errors += value+'<br>';
            });
            // alert(errors);
            swalWithBootstrapButtons.fire({
                  title: 'Error!',
                  html: errors,
                  icon: 'warning',
                  confirmButtonText: 'Confirmed',
                }).then((result) => {
                 // window.location.reload();
                    $("#btn-verify").html('Submit');
                    $("#btn-verify"). attr("disabled", false);
                })
            }
        });

    });
    const validateUser = () => {
      const $resultUser = $('#resultUser');
      const username = $('#username').val();
       $resultUser.text('');
       if(username.length < 4){
          $resultUser.text('Username must be minimum of 4 characters.');
          $resultUser.css('color', 'red');
       }
    }
      $('#username').on('input', validateUser);

    const validatePass = () => {
      const $resultPass = $('#resultPass');
      const password = $('#password').val();
       $resultPass.text('');
       if(password.length < 8){
          $resultPass.text('Password must be minimum of 8 characters.');
          $resultPass.css('color', 'red');
       }
    }
      $('#password').on('input', validatePass);

      // const validateConfirm = () => {
      // const $resultConfirm = $('#resultConfirm');
      // const password1 = $('#password').val();
      // const password_confirmation = $('#password-confirm').val();
      //  $resultConfirm.text('');
      //  if(password_confirmation  != password1){
      //     $resultConfirm.text('Password does not match.');
      //     $resultConfirm.css('color', 'red');
      //  }
      // }
      // $('#password-confirm').on('input', validateConfirm);

    const validateEmail = (email) => {
            return email.match(
              /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
            );
          };

          const validate = () => {
            const $result = $('#result');
            const email = $('#email').val();
            $result.text('');
            var email_input = $('#email').val();
            var base_path = $("#url").val();


            if (validateEmail(email)) {

            $.ajax({
                type:"get",
                url: base_path+"/validateEmail/",
                data: {email:email},
                // dataType: 'json',
                success: function(data){
                  // alert(data);
                  var email1 = data;
                  if(email1 == 'nottaken'){
                    $result.text(email + ' is valid.');
                    $result.css('color', 'green');
                  }
                  else{
                    $result.text(email + ' is already taken.');
                    $result.css('color', 'red');
                  }

                }
              });
            } else {
              $result.text(email + ' is not valid.');
              $result.css('color', 'red');
            }
            return false;
          }

          $('#email').on('input', validate);
    
    $('#btn-save').click(function (event) {
         // alert('hello');

          var fb_link = $("#fb_link").val();
          try{
          // var id = $("#id").val();
          var username = $("#username").val();
          var password = $("#password").val();
          var password_confirm = $("#password-confirm").val();

          // alert(password.length);
          if(password.length > 7 ){
            // if(password_confirmation === password){

            // }else{
            //   Swal.fire('password and confirm password needs to be the same');
            //   event.preventDefault();    
            //   return false;
            // }
          }else{
              Swal.fire('password needs to be minimum of 8 characters');
            event.preventDefault();    
            return false;
          }

          

          var member_no1 = $("#member_no1").val();
          var email = $("#email").val();
          var contact_no = $("#contact_no").val();
          var branch_id = $("#branch_id").val();
          var long_name = $("#fullname").val();
          var birthdate = $("#birthdate").val();
          var street = $("#street").val();
          var barangay_id = $("#barangay_id").val();
          var city_id = $("#city_id").val();
          var province_id = $("#province_id").val();
          var region_id = $("#region_id").val();
          $("#btn-save").html('Please Wait...');
          $("#btn-save"). attr("disabled", true);
            event.preventDefault();           
          }
            catch(err) {
            swalWithBootstrapButtons.fire({
                  title: 'Error!',
                  text: err,
                  icon: 'warning',
                  confirmButtonText: 'Confirmed',
                }).then((result) => {
             // window.location.reload();
            $("#btn-save").html('Submit');
            $("#btn-save"). attr("disabled", false);
                })
            }
        // ajax


        $.ajax({
            type:"POST",
            url: "{{ url('enduser/storeusermember') }}",
            // headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            data: {
              // id:id,
            username:username,
            password:password,
            password_confirm:password_confirm,
            member_no:member_no1,
            email:email,
            contact_no:contact_no, 
            branch_id:branch_id, 
            long_name:long_name, 
            birthdate:birthdate, 
            fb_link:fb_link, 
            street:street, 
            barangay_id:barangay_id, 
            city_id:city_id, 
            province_id:province_id, 
            region_id:region_id, 
            },
            dataType: 'json',
            success: function(data){
             // alert(JSON.stringify(data));
             swalWithBootstrapButtons.fire({
                title: 'Success!',
                text: 'Please check you email for verification',
                icon: 'success',
                confirmButtonText: 'Confirmed',
                allowOutsideClick: false,
             }).then((result) => {
              // alert(JSON.stringify(email+password))
                   document.location.href = "{{ url('/') }}";
             })  
             //    $.each( data, function( key, value ) {
             //    alert(data);
             //  if(key == 'error'){
             //    // var errors = [];
             //    // $.each( data.responseJSON['errors'], function( key, value ) {
             //    //     errors += value+'<br>';
             //    // });
             //    // // alert(errors);
             //    // swalWithBootstrapButtons.fire({
             //    //       title: 'Error!',
             //    //       html: errors,
             //    //       icon: 'warning',
             //    //       confirmButtonText: 'Confirmed',
             //    //     }).then((result) => {
             //    //      // window.location.reload();
             //    //         $("#btn-save").html('Submit');
             //    //         $("#btn-save"). attr("disabled", false);
             //    //     })
             //  }else{
             //    swalWithBootstrapButtons.fire({
             //    title: 'Success!',
             //    text: 'Please check you email for verification',
             //    icon: 'success',
             //    confirmButtonText: 'Confirmed',
             //    allowOutsideClick: false,
             // }).then((result) => {
             //  // alert(JSON.stringify(email+password))
             //       document.location.href = "{{ url('/') }}";
             // })   
             //  }
             //    });
            // $("#btn-save").html('Submit');
            // $("#btn-save").attr("disabled", false);
           }
           ,error: function (data) {
            // alert(JSON.stringify(data));
           var errors = [];
                $.each( data.responseJSON['errors'], function( key, value ) {
                    errors += value+'<br>';
                });
                // alert(errors);
                swalWithBootstrapButtons.fire({
                      title: 'Error!',
                      html: errors,
                      icon: 'warning',
                      confirmButtonText: 'Confirmed',
                    }).then((result) => {
                     // window.location.reload();
                        $("#btn-save").html('Submit');
                        $("#btn-save"). attr("disabled", false);
                    })
            // // $.each( data, function( key, value ) {
            // // alert(data.responseText[0]);
            // console.log(data.responseText);
            //  swalWithBootstrapButtons.fire({
            //     title: 'Error!',
            //     text: data.responseText,
            //     icon: 'error',
            //     confirmButtonText: 'Confirmed',
            //  }).then((result) => {
            // $("#btn-save").html('Submit');
            // $("#btn-save").attr("disabled", false);
            //  })
            // // });
            }
            
        });

    });
});
</script>
  <script src="{{ asset('assets') }}/js/addressDropdown.js"></script>
@endsection

