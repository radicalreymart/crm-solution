<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
// use App\Models\ServiceTrxnMaster;
use App\Models\User;
use App\Models\Client;

class Branch extends Model
{
    protected $table = 'branch';
    use HasFactory;
    protected $fillable = [
        'cd',
        'name',
        'crea_by',
        'created_at',
        'upd_by',
        'updated_at',
        'soft_deleted_at',
    ];
    public function user()
    {
        return $this->hasMany(User::class);
    }
    public function client()
    {
        return $this->hasMany(Client::class);
    }
    // public function svcTMaster()
    // {
    //     return $this->hasMany(ServiceTrxnMaster::class);
    // }



}
