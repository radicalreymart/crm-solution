<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\UserMember;
use App\Models\BankMaster;
use App\Models\Branch;
use App\Models\OnlinePymntTrxnDetail;

class OnlinePymntTrxnMaster extends Model
{
    use HasFactory;
    protected $table = 'online_pymnt_trxn_masters';
    // use HasFactory;
    protected $casts = [
        'file_path' => 'array',
    ];
    protected $fillable = [
        'usermember_id',
        'file_path',
        'bank_id',
        'branch_id',
        'total_amt',
        'date_trxn_rcvd',
        'date_trxn_or_entry',
        'or_ref_no',
        'or_by',
        'status',
        'crea_by',
        'created_at',
        'upd_by',
        'updated_at',
        'soft_deleted_at',
    ];
    public function usermember()
    {
        return $this->belongsTo('App\Models\UserMember','usermember_id', 'id');
    }
    public function bank()
    {
        return $this->belongsTo(BankMaster::class);
    }
    public function branch()
    {
        return $this->belongsTo(Branch::class);
    }
    
}
