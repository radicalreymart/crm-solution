<?php

namespace App\Http\Livewire;

use App\Models\Batch;
use Livewire\Component;
use Livewire\WithPagination;
use Carbon\Carbon;

class SearchBatchHistory extends Component
{
    use WithPagination;
    public $searchTerm;

    public function render()
    {

        $now = Carbon::now()->format('m/d/Y');
        $now1 = Carbon::now();
        $searchTerm = '%'.$this->searchTerm.'%';
        return view('livewire.batch.index-history',
        ['batch' => Batch::whereDate('sched_date','like', $searchTerm)->orWhere('batch_cd','like', $searchTerm)
        ->orderBy('sched_date', 'DESC')
        ->paginate(10),'now' =>$now]);
    }
}
