@extends('layouts.app', [
    'class' => 'sidebar-mini ',
    'namePage' => 'Department Edit',
    'activePage' => 'department-management',
    'activeNav' => '',
])

@section('content')
<div class="panel-header panel-header-sm">
  </div>
  <div class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="card-header">
                <div class="float-left">
                    <h4 class="card-title">{{ __('Edit Department') }} {{'ID Number '}} {{$department->id }}</h4>
                </div>
                <div class="float-right">
                    <a class="btn btn-primary" href="{{ url('/department/') }}"> Back</a>
                </div>
          
        <div class="card">
        <div class="card-body">
                    @if(count($errors) > 0 )
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
                <ul class="p-0 m-0" style="list-style: none;">
                @foreach($errors->all() as $error)
                <li>{{$error}}</li>
                @endforeach
                </ul>
                </div>
                @endif
                    <form action="{{ route('department.update', $department->id) }}" method="POST">
                        @csrf

                          {{ method_field('PUT') }}

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Code') }}</label>

                            <div class="col-md-6">
                                <input id="cd" type="text" class="form-control @error('cd') is-invalid @enderror" name="cd" value="{{$department->cd }}" required autocomplete="cd" autofocus />

                                @error('cd')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Department Name') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{$department->name }}" required autocomplete="name" autofocus />

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                            </div>
                        </div>
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">

                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </div>
                    </form>
        </div>


        </div>
      </div>
    </div>
  </div>

@endsection
