<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Feedback;
use App\Models\Branch;

class Feedback extends Model
{
    use HasFactory;

    protected $table = 'feedback';
    protected $casts = [
    'transaction_nat' => 'array', // Will convarted to (Array)
    'transaction_exp' => 'array', // Will convarted to (Array)
    ];
    protected $fillable = [
     'member_no'
     , 'fullname'
     , 'gender'
     , 'status'
     , 'branch_id'
     , 'branch_loc'
     , 'exp_date'
     , 'member_type'
     , 'transaction_nat'
     , 'transaction_exp'
     , 'comment_suggestion'
     , 'last_feedback'
     , 'signiture'
     , 'crea_by'
     , 'upd_by'
    ];

    public function member()
    {
        return $this->belongsTo('App\Models\Member','member_no','member_no');
    }
    public function branch()
    {
        return $this->belongsTo('App\Models\Branch','branch_id','id');
    }
    public function branchLoc()
    {
        return $this->belongsTo('App\Models\Branch','branch_loc','id');
    }
}
