<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePMESMastersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pmesMasters', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('client_id');
            $table->foreign('client_id', 'pmes_fk_client_id')->references('id')->on('client_masters');
            $table->unsignedBigInteger('client_branch');
            $table->foreign('client_branch', 'pmes_fk_client_branch')->references('id')->on('branch');
            $table->unsignedBigInteger('batch_id')->nullable();
            $table->foreign('batch_id', 'pmes_fk_batch_id')->references('id')->on('batch');
            $table->unsignedBigInteger('batch_sched_date')->nullable();
            $table->integer('serv_status');
            $table->integer('attendance')->nullable();
            $table->string('membership')->nullable();
            $table->string('crea_by');
            $table->string('upd_by')->nullable();
            $table->datetime('soft_deleted_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pmes_masters');
    }
}
