<?php

namespace App\Http\Controllers;

use App\Models\Batch;
use App\Models\PMES;
use App\Models\Client;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendMail;
use Alert;

class BatchController extends Controller
{

    public function deleteAll(Request $request)
    {
        $ids = $request->ids;
        $batches = Batch::whereIn('id',explode(",",$ids))->get();
        $pmesClients = PMES::wherein('batch_id',explode(",",$ids))->whereNull('soft_deleted_at')->get();
         if($pmesClients->count())
        {
            $json = ['error' => 'Please delete PMES corresponding to this entry.'];
        }else
        {
            if($batches->count())
            {
                foreach($batches as $data)
                {
                    $batch = Batch::find($data->id);
                    $batch->soft_deleted_at = Carbon::now();
                    $batch->save();
                }
            }
          $json = ['success'=>"Batch Deleted successfully."];
        }
        return response()->json($json);
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $department = auth()->user()->department_id;
        $isAdmin = auth()->user()->role_id;
        if($department == 1 || $isAdmin == 0){
            $mindate = Carbon::now()->addDay()->format('Y-m-d'.'\T'.'H:i:s.v');
            $now = Carbon::now()->format('m/d/Y');
            $now1 = Carbon::now();
            $participant = null;
            // $sched_date ;
            $batch  = Batch::whereDate('sched_date', '>=' , $now1)->whereNull('soft_deleted_at')->get();
            // dd($batch);
            
            return view('batch.index'
                , compact('batch', 'now', 'mindate', 'participant')
            );
        }else{
            Alert::error("You do not have the permission to access this page.");
            return redirect()->route('home');
            // dd('ccd');
        }
        
    }
    public function historyindex()
    {
        $mindate = Carbon::now()->addDay()->format('Y-m-d'.'\T'.'H:i:s.v');
        $now = Carbon::now()->format('m/d/Y');
        $now1 = Carbon::now();
        $participant = null;
        // $sched_date ;
        $batch  = Batch::all();
        // dd($batch);
        
        return view('batch.historyindex'
            , compact('batch', 'now', 'mindate', 'participant')
        );
    }

    public function participant(Request $request)
    {
       $client_name = PMES::with('client')->where('batch_id',$request->id)->whereNull('soft_deleted_at')->get();
       return response()->json($client_name);
    }


    public function storebatchv2(Request $request)
    {
        // dd($request->all());
        $validatedDate = $request->validate
        ([
            'batch_cd' => 'required|unique:batch,batch_cd,'.$request['id'],
            'sched_date' => 'required',
            'online_vid_tool' => 'required',
            // 'meeting_cd' => 'required',
        ],
        ['batch_cd.required' => 'Batch code is required',
        'online_vid_tool.required' => 'Preferred application is required',
        'sched_date.required' => 'Schedule date is required',
        ]);
        if(isset($request->id)){
            $upd_by = auth()->user()->uname;
            $updated_at = Carbon::now();
        }else{
            $upd_by = null;
            $updated_at = null;
        }
        $sched_date = Carbon::parse($request->sched_date)->format('Y-m-d'.' '.'H:i:s.v');

        $batch_upd = 
        [
            'id' => $request->id,
        ];
        $batch_create = 
        [
            'batch_cd'     => $request->batch_cd,
            'sched_date'     => $sched_date,
            'online_vid_tool'    => $request->online_vid_tool,
            'meeting_cd' =>     $request->meeting_cd,
            'meeting_pw'        => $request->meeting_pw,
            'resched_remarks'        => $request->resched_remarks,
            'batch_stat' => 0,
            'crea_by'       => auth()->user()->uname,
            'upd_by'         => $upd_by,
            'updated_at'     => $updated_at,
        ];
        Batch::updateOrcreate($batch_upd, $batch_create);
        if($request->id != null){
            $pmes = PMES::with('client')->where('batch_id', $request['id'])->get();
        if($pmes->count())
        {
            foreach($pmes as $data)
            {
                // dd($data->client->gmail_address);
                $meeting_cd = Batch::where('id', $request['id'])->first()->meeting_cd;
                $sched_date = Batch::where('id', $request['id'])->first()->sched_date;
                $resched_remarks = Batch::where('id', $request['id'])->first()->resched_remarks;
                $vid_tool = Batch::where('id', $request['id'])->first()->online_vid_tool;
                $email = $data->client->gmail_address;
                if($vid_tool == 0){
                    $link = 'https://meet.google.com/'.$meeting_cd;
                    $description = 'Online Pre-Membership Education Webinar';
                }elseif($vid_tool == 1){
                    $link = $meeting_cd;
                    $description = 'Online Pre-Membership Education Webinar';
                }else{
                    $link = null;
                    $meeting_cd = null;
                    $description = 'Online Pre-Membership Education Seminar';
                }
                $start_date = \Carbon\Carbon::parse($sched_date)->format('Ymd\THis');
                $end_date = \Carbon\Carbon::parse($sched_date)->addHour(2)->format('Ymd\THis');
                $details = 
                [

                'vid_tool' => $vid_tool,
                'link'   => $link,
                'meeting_cd' => $meeting_cd,
                'sched_date' => $sched_date,
                'resched_remarks' => $request['resched_remarks'],
                'text' => 'OLPMEW'.\Carbon\Carbon::parse($sched_date)->format('Ymd'),
                'description' => $description,
                'startdate' => $start_date,
                'enddate' => $end_date,
                'email' => $email,
                ];

                $editclient = Client::find($data->client->id);
                $editclient->sched_date = $sched_date;
                $editclient->save();
                // dd($meeting_cd);
                Mail::to($email)->queue(new SendMail($details));
            }
        }
        $request->session()->flash('success', 'Batch updated successfully!');
        }else{
        $request->session()->flash('success', 'Batch created successfully!');    
        }
                return response()->json(['success' => 'success'], 200);
        // return redirect()->route('pmes.index')->with('success', 'PMES created successfully.');
      
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     public function editmodal(Request $request, Batch $batch)
    {
        //
        $where = array('id' => $request->id);
        $batch  = Batch::where($where)->first();
        $sched_date = Carbon::parse($batch->sched_date)->format('Y-m-d'.'\T'.'H:i:s.v');
        $json = array(
            'id' => $request->id,
            'batch_cd'     => $batch->batch_cd,
            'sched_date'     => $sched_date,
            'online_vid_tool'    => $batch->online_vid_tool,
            'meeting_cd' => $batch->meeting_cd,
            'meeting_pw'        => $batch->meeting_pw,
            'resched_remarks'     => $batch->resched_remarks,
        );
         // $request->session()->flash('success', 'member updated successfully!');
        return response()->json($json);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request)
    {
        $rules = array(
            'batch_cd' => 'required|unique:batch',
            'sched_date' => 'required',
            'online_vid_tool' => 'required',
            'meeting_cd' => 'required',
            'resched_remarks' => 'required',
        );
         $validator = Validator::make($request->all(), $rules);
            $batch  = Batch::find($id);
            $batch->batch_cd        = $request->get('batch_cd');
            $batch->sched_date      = $request['sched_date'];
            $batch->online_vid_tool = $request->get('online_vid_tool');
            $batch->meeting_cd      = $request->get('meeting_cd');
            $batch->resched_remarks = $request->get('resched_remarks');
            $batch->meeting_pw      = $request->get('meeting_pw');
            $batch->updated_at      = Carbon::now();
            $batch->upd_by          = auth()->user()->uname;
            $batch->save();

            $pmes = PMES::with('client')->where('batch_id', $id)->get();
            if($pmes->count())
            {
            foreach($pmes as $data)
            {
                // dd($data->client->gmail_address);
                $meeting_cd = Batch::where('id', $id)->first()->meeting_cd;
                $sched_date = Batch::where('id', $id)->first()->sched_date;
                $resched_remarks = Batch::where('id', $id)->first()->resched_remarks;
                $email = $data->client->gmail_address;
                $details = 
                [
                'meeting_cd' => $meeting_cd,
                'sched_date' => $sched_date,
                'resched_remarks' => $resched_remarks
                ];

                $editclient = Client::find($data->client->id);
                $editclient->sched_date = $sched_date;
                $editclient->save();
                // dd($meeting_cd);
                Mail::to($email)->queue(new SendMail($details));
            }
            }

        return redirect()->route('batch/index')
            ->with('success', 'Batch updated successfully');
    }
    public function getBatches($batch_id)
    {
        $batches = Batch::where("id",$batch_id)->get(['sched_date','online_vid_tool','meeting_cd','meeting_pw']);
        return response()->json($batches);
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Project  $project
     * @return \Illuminate\Http\Response
     */
    
    // public function destroy($id)
    // {
    //     $batch  = Batch::find($id);
    //     $batch ->delete();

    //     return response()->json(['success'=>"Batch deleted successfully.", 'tr'=>'tr_'.$id]);
      
    // }
    
}
