<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\AcctMaster;


class TransactionType extends Model
{
    use HasFactory;
    protected $table = 'transaction_types';
    // use HasFactory;
    protected $fillable = [
        'cd',
        'description',
        'crea_by',
        'created_at',
        'upd_by',
        'updated_at',
        'soft_deleted_at',
    ];
    public function account()
    {
        return $this->hasMany('App\Models\AcctMaster','id');
        // return $this->hasMany(AcctMaster::class);
    }
}
