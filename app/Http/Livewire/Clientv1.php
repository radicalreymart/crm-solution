<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Livewire\WithPagination;
use Carbon\Carbon;
use App\Models\Client;
use App\Models\Region;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use App\Models\Barangay;
use Yajra\Address\HasAddress;
use App\Models\City;
use App\Models\Province;
use App\Models\Country;
use App\Models\Occupation;
use App\Models\MarketingChannel;
use App\Models\Branch;
use App\Models\Batch;
use App\Models\PMES;
// use App\Models\ServiceTrxnMaster;
use Yajra\Address\AddressServiceProvider;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendMail;


class Clientv1 extends Component
{
    use WithPagination;
    public $searchTerm;
    public $client_id, $fname,$mname,$lname,$contact_no,$member_status,$member_no,$seminar_pref,$gmail_address,$gender,$birthdate,$sched_date,$street,$branch_id,$occupation,$other_occupation,$marketing_channel,$sched_stat,$of_tag,$purpose_tag,$staff_referral_cd,$data_consent_fl,$batch_id,$age;
    public $updateMode = false;
    public $regions;
    public $province;
    public $city;
    public $batch;
    public $selectedRegion = NULL;
    public $selectedBatch = NULL;
    public $selectedProvince;
    public $selectedCity;
    public $selectedBarangay;
    public $selectedOccu;

    public function mount()
    {
        $now = Carbon::now();
        $this->batch = Batch::whereDate('sched_date', '>=', $now)->get();
        $this->regions = Region::all();
        $this->province = collect();
        $this->barangay = collect();
        $this->city = collect();

           

    }



    public function updatedSelectedRegion($regions)
    {
        if (!is_null($regions)) {
            // dd($regions);
            $this->province = Province::where('region_id', $regions)->get();
        }
    }
    public function updatedSelectedProvince($province)
    {
        if (!is_null($province)) {
            $this->city = City::where('province_id', $province)->get();
        }
    }
    public function updatedSelectedCity($city)
    {
        if (!is_null($city)) {
            $this->barangay = Barangay::where('city_id', $city)->get();
        }
    }
    public function updatedOccupation($selectedOccu)
    {
        if($this->occupation == null)
        {  
            $this->other_occupation = $selectedOccu;
            // $this->showoccu = true;
        }else{
            $this->other_occupation = null;
            // $this->showoccu = false;
        }
    }
    public function updatedMember_status()
    {
        if($this->member_status == "")
        {
            $this->member_status = null;
            $this->member_no = null;
            $this->staff_referral_cd = null;
        }
        elseif($this->member_status == 0)
        {  
            $this->member_no = null;
        }else{
            $this->staff_referral_cd = null;
        }
    }
    

    

    public function render()
    {
        $searchTerm = '%'.$this->searchTerm.'%';

        $client = Client::with( 'marketingChannel', 'branch', 'batch')
        ->whereNull('soft_deleted_at')
        // ->where('fullname','like', $searchTerm)
        ->Where(function ($query) {
            $searchTerm = '%'.$this->searchTerm.'%';
            $query->where('gmail_address','like', $searchTerm)
                      ->orwhere('fullname','like', $searchTerm)
                      ->orWhere('contact_no','like', $searchTerm);
            })
        // ->Where('gmail_address','like', $searchTerm)
        ->get();
        // ->paginate(5);
        // dd($client);     
        $occupations = Occupation::all();
        $branches = Branch::all();
        $marketingChannel = MarketingChannel::all();
        $now = Carbon::now();
        $batch_sched = Batch::all();
        $batches = Batch::whereDate('sched_date', '>', $now)->get();

        return view('livewire.client.index',
        compact('client', 'occupations', 'branches', 'marketingChannel', 'now', 'batches', 'batch_sched'));
    }
    private function resetInputFields()
    {
        $this->fname = '';$this->mname = '';$this->lname = '';$this->contact_no = '';$this->member_status = '';$this->member_no = '';$this->seminar_pref = '';$this->gmail_address = '';$this->gender = '';$this->birthdate = '';$this->sched_date = '';$this->barangay_primary = '';$this->city_primary = '';$this->province_primary = '';$this->region_primary = '';$this->street = '';$this->barangay_id = '';$this->city_id = '';$this->province_id = '';$this->region_id = '';$this->branch_id = '';$this->occupation = '';$this->other_occupation = '';$this->marketing_channel = '';$this->sched_stat = '';$this->of_tag = '';$this->purpose_tag = '';$this->staff_referral_cd = '';$this->data_consent_fl = '';$this->batch_id = '';$this->age = '';
    }

    

    public function store()
    {
        // $this->updatedOccupation();
        // dd($this);
        $validatedDate = $this->validate
        ([
            'fname' => 'required',
            'lname' => 'required',
            'contact_no' => 'required|digits_between:1,10',
            'gmail_address' => 'required|email|regex:/gmail/',
            'gender' => 'required',
            'birthdate' => 'required|date',
            'age' => 'required',
            'selectedRegion' => 'required',
            'selectedProvince' => 'required',
            'selectedCity' => 'required',
            'street' => 'required',
            'other_occupation' => 'required_if:occupation,==,null|nullable',
            'of_tag' => 'required',
            'marketing_channel' => 'required',
            'purpose_tag' => 'required',
            'member_status' => 'required',
            'seminar_pref' => 'required',
            'branch_id' => 'required',
        ]);
        $intChannel = intval($this->marketing_channel);
        $fullname = $this->fname.' '. $this->mname.' '.$this->lname;
        $contact_no = '+63'.$this->contact_no;
        if($this->batch_id == null){
            $sched_stat = 0;
        }else{
            $sched_stat = 1;
        }
        if($this->occupation == 'other'){
            $occupation_id = null;
        }else{
            $occupation_id = $this->occupation ;
        }
        if($this->data_consent_fl == null){
            $data_consent_fl = 0;
        }else
        {
            $data_consent_fl = $this->data_consent_fl;
        }
        // dd($occupation_id);
        $client = Client::create([
             'fname' => $this->fname
             ,'mname' => $this->mname
             ,'lname' => $this->lname
             ,'fullname' => $fullname
             ,'age' => $this->age
             ,'contact_no' => $contact_no
             ,'member_status' => $this->member_status
             ,'member_no' => $this->member_no
             ,'seminar_pref' => $this->seminar_pref
             ,'gmail_address' => $this->gmail_address
             ,'gender' => $this->gender
             ,'birthdate' => $this->birthdate
             ,'batch_id' => $this->selectedBatch
             ,'street' => $this->street
             ,'barangay_id' => $this->selectedBarangay
             ,'city_id' => $this->selectedCity
             ,'province_id' => $this->selectedProvince
             ,'region_id' => $this->selectedRegion
             ,'branch_id' => $this->branch_id
             ,'occupation_id' => $occupation_id
             ,'other_occupation' => $this->other_occupation
             ,'marketing_channel_id' => $intChannel
             ,'sched_stat' => $sched_stat
             ,'of_tag' => $this->of_tag
             ,'purpose_tag' => $this->purpose_tag
             ,'staff_referral_cd' => $this->staff_referral_cd
             ,'data_consent_fl' => $data_consent_fl
             // ,'batch_id' => $this->batch_id
             ,'crea_by' => auth()->user()->uname,
            'created_at' => Carbon::now(),
        ]);
        if($this->batch_id == null){
            PMES::create([
                 'client_id' => $client->id,
                 'batch_id' => null,
                 'serv_status' => 0,
                 'crea_by' => auth()->user()->uname,
            ]);
        }else{
            $participant = Batch::where('id', $this->batch_id)->first()->participant;
            PMES::create([
                 'client_id' => $client->id,
                 'batch_id' => $this->batch_id,
                 'serv_status' => 1,
                 'crea_by' => auth()->user()->uname,
            ]);
            Batch::whereId($this->batch_id)->update([
                'participant' => $participant+1,
            ]);
            $meeting_cd = Batch::where('id', $this->batch_id)->first()->meeting_cd;
            $sched_date = Batch::where('id', $this->batch_id)->first()->sched_date;
            $email = $this->gmail_address;
            $details = [
            'meeting_cd' => $meeting_cd,
            'sched_date' => $sched_date,
            'resched_remarks' => '.'
            ];

            $editclient = Client::find($client->id);
            $editclient->sched_date = $sched_date;
            $editclient->save();
            // dd($meeting_cd);
            Mail::to($email)->queue(new SendMail($details));
            }
        session()->flash('success', 'Client Created Successfully.');

        $this->resetInputFields();

        $this->emit('clientStore'); // Close model to using to jquery

    }
     public function edit($id)
    {
        // $this->updatedMember_status();
        $this->updateMode = true;
        $client = Client::where('id',$id)->first();
        // dd($client->region_id);

         $this->province = Province::where('region_id', $client->region_id)->get();
         $this->city = City::where('province_id', $client->province_id)->get();
         $this->barangay = Barangay::where('city_id', $client->city_id)->get();
        // $sched_date = Carbon::parse($batch->sched_date)->format('Y-m-d'.'\T'.'H:i:s.v');
        $this->client_id = $id;
        $this->fname = $client->fname;
        $this->mname  = $client->mname;
        $this->lname = $client->lname;
        $this->birthdate = Carbon::parse($client->birthdate)->format('Y-m-d');
        $this->age = $client->age;
        $this->gmail_address = $client->gmail_address;
        $this->gender = $client->gender;
        $this->contact_no = substr($client->contact_no, -10);
        $this->selectedRegion = $client->region_id;
        $this->selectedProvince = $client->province_id;
        $this->selectedCity = $client->city_id;
        $this->selectedBarangay = $client->barangay_id;
        $this->street = $client->street;
        $selectedOccu = $client->other_occupation;
        if($client->occupation_id == null){
            $this->other_occupation = $client->other_occupation;
        }else{
            $this->occupation = $client->occupation_id;
            $this->other_occupation = null;
        }
        $this->of_tag = $client->of_tag;
        $this->marketing_channel = $client->marketing_channel_id;
        $this->purpose_tag = $client->purpose_tag;
        $this->member_status = $client->member_status;
        $this->member_no = $client->member_no;
        $this->staff_referral_cd = $client->staff_referral_cd;
        $this->seminar_pref = $client->seminar_pref;
        $this->selectedBatch = $client->batch_id;
        $this->branch_id = $client->branch_id;
        $this->data_consent_fl = $client->data_consent_fl;
    }
     public function cancel()
    {
        $this->updateMode = false;
        $this->resetInputFields();

    }
    public function update()
    {
        $this->updatedMember_status();
        // dd($this);
         
        $validatedDate = $this->validate
        ([
            'fname' => 'required',
            'lname' => 'required',
            'contact_no' => 'required|digits_between:1,10',
            'gmail_address' => 'required|email|regex:/gmail/',
            'gender' => 'required',
            'birthdate' => 'required|date',
            'age' => 'required',
            'selectedRegion' => 'required',
            'selectedProvince' => 'required',
            'selectedCity' => 'required',
            'street' => 'required',
            'other_occupation' => 'required_if:occupation,==,null|nullable',
            'of_tag' => 'required',
            'marketing_channel' => 'required',
            'purpose_tag' => 'required',
            'member_status' => 'required',
            'seminar_pref' => 'required',
            'branch_id' => 'required',
        ]);

        // dd($this);
        $intChannel = intval($this->marketing_channel);
        $fullname = $this->fname.' '. $this->mname.' '.$this->lname;
        $contact_no = '+63'.$this->contact_no;
        if($this->occupation == 'other'){
            $occupation_id = null;
        }else{
            $occupation_id = $this->occupation ;
        }
         $client = Client::find($this->client_id);
            $client->update([
            'fname' => $this->fname
             ,'mname' => $this->mname
             ,'lname' => $this->lname
             ,'fullname' => $fullname
             ,'age' => $this->age
             ,'contact_no' => $contact_no
             ,'member_status' => $this->member_status
             ,'member_no' => $this->member_no
             ,'seminar_pref' => $this->seminar_pref
             ,'gmail_address' => $this->gmail_address
             ,'gender' => $this->gender
             ,'birthdate' => $this->birthdate
             ,'batch_id' => $this->selectedBatch
             ,'sched_date' => $this->sched_date
             ,'street' => $this->street
             ,'barangay_id' => $this->selectedBarangay
             ,'city_id' => $this->selectedCity
             ,'province_id' => $this->selectedProvince
             ,'region_id' => $this->selectedRegion
             ,'branch_id' => $this->branch_id
             ,'occupation_id' => $occupation_id
             ,'other_occupation' => $this->other_occupation
             ,'marketing_channel_id' => $intChannel
             ,'of_tag' => $this->of_tag
             ,'purpose_tag' => $this->purpose_tag
             ,'staff_referral_cd' => $this->staff_referral_cd
             ,'data_consent_fl' => $this->data_consent_fl
            ,'upd_by' => auth()->user()->uname
            ,'updated_at' => Carbon::now()
        ]);
        session()->flash('success', 'Client Edited Successfully.');

        $this->resetInputFields();

        $this->emit('clientStore'); // Close model to using to jquery
    }

}

