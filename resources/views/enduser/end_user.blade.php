@extends('layouts.app1')
@section('content')
  <style>
    .alert{
      color: black !important;
    }
  </style>
    <!-- <nav class="navbar navbar-expand-lg navbar-light bg-white">
      <div class="container-fluid">
        <a class="navbar-brand" href="#">
          <img src="{{asset('assets/img/logo.jpg')}}" alt="" width="180" class="d-inline-block align-text-top">
        </a>
      </div>
    </nav> -->
    <!-- Masthead-->
    <header class="masthead mb-1">
      <div class="container mt-1 pt-1">
            @include('flash-message')
            @include('sweetalert::alert')
          <div class="masthead-subheading m-3">Magandang Buhay!</div>
          <div class="masthead-heading text-uppercase m-3">Be a member! <br> Be one of us!</div>
          <p class="">Thank you for showing your interest to be part of our Cooperative. Please fill out the form below with the necessary information, so we can contact you as soon as possible..</p>
          <a class="btn btn-outline-light btn-xl text-uppercase" type="button" href="#sign_up">Sign-up Now!</a>
      </div>
    </header>

    <div class="container mt-5" id="sign_up">
      <div class="card p-2 pt-3 bg-light border-0">
        <div class="row"> 
          <div class="col-md-12 order-md-1">
            <form method="POST" name='formname' action="{{ route('enduser.store') }}">
              <input type="hidden" id="enduser" name="enduser" value="enduser" >
                                        @csrf
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
              <div class="card mx-3 shadow-lg border-0">
                <div class="card-header bg-light text-primary py-2 pl-3 border-0">
                  <h6 class="m-0 default-primary">Personal Information</h6>
                </div>
                <div class="card-body px-3">
                  <div class="row px-3">
                    <div class="col-md-4 mb-3">
                      <label for="fname">First Name  <span class="text-danger">*</span></label>
                      <input type="text" class="form-control form-control-sm  @error('fname') is-invalid @enderror" id="fname" name="fname" placeholder="First Name" value="{{ old('fname') }}"  autocomplete="fname">
                      <div class="invalid-feedback">
                        First Name is required.
                      </div>
                    </div>
                    <div class="col-md-4 mb-3">
                      <label for="address2">Middle Name  <span class="text-muted">(Optional)</span></label>
                      <input type="text" value="{{ old('mname') }}" name="mname" class="form-control form-control-sm" id="mname" placeholder="Middle Name">
                    </div>
                    <div class="col-md-4 mb-3">
                      <label for="lname">Last Name  <span class="text-danger">*</span></label>
                      <input type="text" value="{{ old('lname') }}" name="lname" class="form-control form-control-sm @error('lname') is-invalid @enderror" id="lname" placeholder="Last Name"  autocomplete="lname" >
                      <div class="invalid-feedback">
                        Last Name is required.
                      </div>
                    </div>
                  </div> <!-- Long Name -->
                  <div class="row px-3">
                    <div class="col-md-5 mb-3">
                      <label for="birthdate">Birthdate  <span class="text-danger">*</span></label>
                      <input type="date" id="datepicker"  class="form-control form-control-sm @error('birthdate') is-invalid @enderror" name="birthdate" value="{{ old('birthdate') }}">
                      <div class="invalid-feedback">
                        Birthdate is required.
                      </div>
                    </div>
                    <div class="col-md-2 mb-3">
                      <label for="age">Age</label>
                      <input type="text" tabindex="-1" class="form-control form-control-sm" id="agecal" placeholder="" readonly name="age">
                    </div>
                    <div class="col-md-5 mb-3">
                      <label for="gender">Gender  <span class="text-danger">*</span></label>
                      <select class="custom-select custom-select-sm  custom-select custom-select-sm -sm d-block w-100 @error('gender') is-invalid @enderror" id="gender"  type="text" name="gender" autocomplete="gender">
                        <option value="">Select Gender</option>
                        <option value="0">Male</option>
                        <option value="1">Female</option>
                        <option value="2">Rather not to say</option>
                      </select>
                      <div class="invalid-feedback">
                        Please select your gender preference.
                      </div>
                    </div>
                  </div> <!-- Birthdate, Age and Gender -->
                  <div class="row px-3">
                    <div class="col-md-4 mb-3">
                      <label for="contact_no">Contact Number  <span class="text-danger">*</span></label>
                      <div class="input-group input-group-sm">
                        <div class="input-group-prepend">
                          <span class="input-group-text">+63</span>
                        </div>
                        <input type="text" class="form-control form-control-sm @error('contact_no') is-invalid @enderror" id="contact_no" placeholder="10-digit Mobile Number"  minlength="0" maxlength="10" onkeydown="return ( event.ctrlKey || event.altKey 
                    || (47<event.keyCode && event.keyCode<58 && event.shiftKey==false) 
                    || (95<event.keyCode && event.keyCode<106)
                    || (event.keyCode==8) || (event.keyCode==9) 
                    || (event.keyCode>34 && event.keyCode<40) 
                    || (event.keyCode==46))" name="contact_no" autocomplete="contact_no" title="" style="border-radius: 0;" >
                        <div class="invalid-feedback" style="width: 100%;">
                          Contact Number is required.
                        </div>
                      </div>
                    </div>
          
                    <div class="col-md-4 mb-3 mt-0">
                      <label for="gmail_address">Gmail Address  <span class="text-danger">*</span></label>
                        <input type="email" class="form-control form-control-sm @error('gmail_address') is-invalid @enderror" id="gmail_address" placeholder="example@gmail.com"  name="gmail_address" autocomplete="gmail_address">
                        <div class="invalid-feedback" style="width: 100%;">
                          Your Gmail Address is required.
                        </div>
                    </div>

                    <div class="col-md-4 mb-3 mt-0">

                      <label for="gmail_address">Facebook Link
                        <i class="fa-brands fa-facebook-messenger"></i>
                        <!-- <img style="padding-left: 5px; padding-top: 0px; width: 50%; height: 30%;" src="{{asset('assets/img/fblink.jpg')}}"> -->
                      </label>
                        <input   type="text"  class="form-control form-control-sm @error('fb_link') is-invalid @enderror" id="fb_link"   name="fb_link" autocomplete="fb_link">
                        <div class="invalid-feedback" style="width: 100%;">
                          Your Gmail Address is required.
                        </div>
                    </div>  
                  </div> <!-- Contact Information -->
                  <div class="row px-3">
                    <div class="col-md-6 mb-3">
                      <label for="occupation">Occupation  <span class="text-danger">*</span></label>
                      <select class="custom-select custom-select-sm  custom-select custom-select-sm -sm  d-block w-100 @error('occupation') is-invalid @enderror" id="occupation"  type="text" name="occupation" autocomplete="occupation">
                        <option value="">Select Occupation</option>
                         @foreach($occupations as $item)
                          <option id="occupation" value="{{$item->id}}">{{$item->name}}</option>
                          @endforeach
                          <option value="0">Other</option>
                      </select>
                      <div class="invalid-feedback">
                        Occupation is required.
                      </div>
                    </div>
                    <div id="div-occu" class="col-md-6 mb-3">
                      <label for="occupation"></label>
                      <input type="text" id="other_occupation" placeholder="Other Occupation" name="other_occupation" hidden class="form-control form-control">

                    </div>
                  </div> <!-- Occupation -->
                </div>
              </div>
      
              <div class="card mt-4 mx-3 shadow-lg border-0">
                <div class="card-header bg-light text-primary pt-2 pl-3 border-0">
                  <h6 class="m-0 default-primary">Present Address  <span class="text-danger">*</span></h6>
                </div>
                <div class="card-body">
                  <div class="row px-3">
                    <div class="col-md-4 mb-3">
                      <label for="region_code">Region  <span class="text-danger">*</span></label>
                      <select class="form-control custom-select custom-select-sm @error('region_id') is-invalid @enderror" id="region_id"  type="text" name="region_id" >
                        <option value="">Select Region</option>
                         @foreach($regions as $item)
                        <option id="region_code" value="{{$item->region_id}}">{{$item->name}}</option>
                        @endforeach
                      </select>
                      <input type="text" id="region_hidden" name="region_hidden" hidden readonly  class="form-control form-control-sm">
                      <div class="invalid-feedback">
                        Region is required.
                      </div>
                    </div>
                    <div class="col-md-4 mb-3">
                      <label for="province_id">Province  <span class="text-danger">*</span></label>
                      <select readonly class="form-control  custom-select custom-select-sm" id="province_id"  readonly name="province_id" autocomplete="province_id" >
                        <option value="">Select Province</option>
                      </select>
                      <input type="text" id="province_hidden" name="province_hidden" hidden readonly  class="form-control form-control-sm">
                      <div class="invalid-feedback">
                        Province is required.
                      </div>
                    </div>
                    <div class="col-md-4 mb-3">
                      <label for="city_id">City/Municipality  <span class="text-danger">*</span></label>
                      <select  class="form-control  custom-select custom-select-sm" id="city_id"   name="city_id" autocomplete="city_id" readonly>
                        <option value="">Select City/Municipality</option>
                      </select>
                      <input type="text" id="city_hidden" name="city_hidden" hidden readonly  class="form-control form-control-sm">
                      <div class="invalid-feedback">
                        City/Municipality is required.
                      </div>
                    </div>
                  </div> <!-- Region, Province, Municiaplity -->
                  <div class="row px-3">
                    <div class="col-md-4 mb-3">
                      <label for="barangay_id">Barangay</label>
                      <select readonly class="form-control custom-select custom-select-sm" readonly id="barangay_id"  type="text" name="barangay_id" autocomplete="barangay_id" >
                        <option value="">Select Barangay</option>
                      </select>
                      <input type="text" id="barangay_hidden" name="barangay_hidden" hidden readonly  class="form-control form-control-sm">
                      <div class="invalid-feedback">
                        Barangay is required.
                      </div>
                    </div>
                    <div class="col-md-8 mb-3">
                      <label for="street">Address  <span class="text-danger">*</span></label>
                      <input type="text" class="form-control form-control-sm @error('street') is-invalid @enderror" id="street" placeholder="Bldg. No./ Street / Subdivision" name="street">
                    </div>
                  </div> <!-- Barangay, Street -->
                </div>
              </div>
              
              <div class="card mt-4 mx-3 shadow-lg border-0">
                <div class="card-header bg-light text-primary py-2 pl-3 border-0">
                  <h6 class="m-0 default-primary">Membership Preferences </h6>
                </div>
                <div class="card-body">
                  <div class="row px-3">
                    <div class="col-md-6 mb-3">
                      <label for="member_status">Membership Status  <span class="text-danger">*</span></label>
                      <select class=" custom-select custom-select-sm form-control @error('member_status') is-invalid @enderror" id="member_status"  type="text" name="member_status" autocomplete="member_status" >
                        <option value="" selected disabled>Select Status</option>
                        <option value= 2 >Non-Member</option>
                        <option value= 0 >Returning Member</option>
                        <option value= 1 >From Associate Member</option>
                      </select>
                      
                      <div class="invalid-feedback">
                        This field is required.
                      </div>
                    </div>
                    <div id="div-memno" class="col-md-6 mb-3">
                      <label for="member_no">Member No. <span  class="text-muted">(Leave blank if not applicable)</span></label>
                      <input type="text" class="form-control form-control-sm" id="member_no" placeholder="Member No." name="member_no">
                    </div>
                  </div> <!-- Membership Status -->
                  <div class="row px-3">
                    <div class="col-md-4 mb-3">
                      <label for="seminar_pref">Online or Walk-in Seminar?  <span class="text-danger">*</span></label>
                      <select class="custom-select custom-select-sm  @error('seminar_pref') is-invalid @enderror"  id="seminar_pref"  type="text" name="seminar_pref" autocomplete="seminar_pref" >
                        <option value="" selected disabled>Select</option>
                        <option value="0">Online Video Conference</option>
                        <option value="1">Walk-in</option>
                      </select>
                      <div class="invalid-feedback">
                        Seminar Preference is required.
                      </div>
                    </div>
                    <div class="col-md-4 mb-3">
                      <label for="branch_id">Preferred MHRMPC Branch  <span class="text-danger">*</span></label>
                      <select class="form-control custom-select custom-select-sm @error('branch_id') is-invalid @enderror" id="branch_id"  type="text" name="branch_id" autocomplete="branch_id" >
                        <option value="" selected disabled>Select Branch</option>
                        @foreach($branches as $item)
                        <option id="branch" value="{{$item->id}}">{{$item->name}}</option>
                        @endforeach
                        
                      </select>
                      <div class="invalid-feedback">
                        This field is required.
                      </div>
                    </div>
                    <div class="col-md-4 mb-3">
                      <label for="batch_id">Preferred Schedule  <span class="text-danger">*</span></label>
                      <select class="form-control custom-select custom-select-sm @error('batch_id') is-invalid @enderror" id="batch_id"  type="text" name="batch_id" autocomplete="batch_id" >
                        <option value="" selected disabled>Select</option>
                      @foreach($batches as $batch)
                      <option  value="{{$batch->id}}">{{ \Carbon\Carbon::parse($batch->sched_date)->format('m-d-Y')}}, @if($batch->online_vid_tool == 2) Walk-in @else Online Video Conference  @endif</option>
                      @endforeach
                      </select>
                      <div class="invalid-feedback">
                        This field is required.
                      </div>
                    </div>
                  </div> <!-- Preferences -->
                </div>
              </div>
        
              <div class="card mt-4 mx-3 shadow-lg border-0">
                <div class="card-header bg-light text-primary py-2 pl-3 border-0">
                  <h6 class="m-0 default-primary">Please answer the survey to improve our services.</h6>
                </div>
                <div class="card-body">
                  <div class="row px-3">
                    <div class="col-md ">
                      <label for="of_tag">Are you an Overseas Filipino Working/Residing abroad?  <span class="text-danger">*</span></label>
                      <div class=" row">
                      <div class="form-check form-check-inline">
                        <input class="form-check" type="radio" id="selfy" name="self" value="1">
                        <label class="form-check-label" for="selfy">Yes, I am an Overseas Filipino Worker and I am residing abroad. </label>
                      </div>
                      </div>
                      <div class=" row">
                      <div class="form-check form-check-inline">
                        <input class="form-check" type="radio" id="selfn" name="self" value="0">
                        <label class="form-check-label" for="selfn">No, I am not an Overseas Filipino Worker.</label>
                      </div>
                      </div>
                      <div class="invalid-feedback">
                        This field is required.
                      </div>
                    </div>
                    <div class="col-md ">
                      <label for="of_tag">Do you have relative/s working/residing abroad?  <span class="text-danger">*</span></label>
                      <div class=" row">
                      <div class="form-check form-check-inline">
                        <input class="form-check" type="radio" id="relativey" name="relative" value="1">
                        <label class="form-check-label" for="relativey">Yes, I have relative/s working and residing abroad.</label>
                      </div>
                      </div>
                      <div class=" row">
                      <div class="form-check form-check-inline">
                        <input class="form-check" type="radio" id="relativen" name="relative" value="0">
                        <label class="form-check-label" for="relativen">No, I don’t have OFW relative/s.</label>
                      </div>
                      </div>
                      <div class="invalid-feedback">
                        This field is required.
                      </div>
                    </div>
                  </div> <!-- OF Survey -->
                  <div style="padding-top: 20px;" class="row px-3">
                    <div class="col-md-6 mb-3">
                      <label for="marketing_channel">Where did you hear about Most Holy Rosary Multi-Purpose Cooperative?  <span class="text-danger">*</span></label>
                      <select class="form-control custom-select custom-select-sm @error('marketing_channel') is-invalid @enderror"   id="marketing_channel"  type="text" name="marketing_channel" autocomplete="marketing_channel" >
                        <option value="" selected disabled>Select</option>
                         @foreach($marketingChannel as $item)
                        <option  value="{{$item->id}}">{{$item->name}}</option>
                        @endforeach
                      </select>
                      <div class="invalid-feedback">
                        This field is required.
                      </div>
                    </div>
                    <div class="col-md-6 mb-3">
                      <label style="padding-bottom: 20px;" for="purpose_tag">Why do you want to become an MHRMPC member?  <span class="text-danger">*</span></label>
                      <select class="custom-select custom-select-sm  d-block w-100 @error('purpose_tag') is-invalid @enderror"  id="purpose_tag"  type="text" name="purpose_tag" autocomplete="purpose_tag" >
                        <option value="">Select</option>
                        <option value= 0 >For investment only</option>
                        <option value= 1 >For investment and savings</option>
                        <option value= 2 >To avail loan products only</option>
                        <option value= 3 >To avail trading products and discounts</option>
                        <option value= 4 >To avail all products and services offered (investment, savings, loans, tradings, etc.</option>
                      </select>
                      <div class="invalid-feedback">
                        This field is required.
                      </div>
                    </div>
                  </div> 
                </div>
              </div>
      
              <div class="card border-none mt-4 mx-3 shadow-lg">
                <div class="card-body">
                  <h6 class="card-title">Data Privacy Consent</h6>
                    <p class="card-text">In compliance with the Data Privacy Act of 2012 and Credit Information Corporation (CIC), and its Implementing Rules and Regulations, I agree and authorize the MHRMPC to use my personal information to process any transaction related in the availment of MHRMPC products and services and administer the benefits as stated in the policy and other service agreements and inform me of future customer campaign using the personal information I shared with the coop.</p>
                    <div class="custom-control custom-checkbox">
                      <input type="checkbox" name="data_consent_fl" value="on" class="custom-control-input" id="same-address">
                      <label class="custom-control-label" for="same-address">I agree</label>
                    </div>
                </div>
              </div>
              <button class="btn btn-secondary mt-5 mb-5 btn-block" style="background-color: rgb(7,17,174);" type="submit">Submit</button>
            </form>
          </div>
      </div>
  </div>
  </div>
  <!-- boostrap model -->
    <div class="modal fade" id="ajax-pmes-model" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title" id="ajaxPMESModel"></h4>
          </div>
          <div id="modalCatcher" class="modal-body">
            <form action="javascript:void(0)" class="form-horizontal" method="POST">
              <input type="hidden" name="id" id="id">
              <input type="hidden" value="{{url('/')}}" id="url" name="url">
              <input type="hidden" name="fname1" id="fname1">
              <input type="hidden" name="mname1" id="mname1">
              <input type="hidden" name="lname1" id="lname1">
              <div class="form-group">
                <label for="name" class="col-sm-6 control-label">Full Name</label>
                <div class="col-sm-12">
                  <input type="text" class="form-control" id="fullname" name="fullname" readonly value="" maxlength="50" required="">
                </div>
              </div>
              <div class="row px-3">
                <div class="col-md-5 mb-3">
                  <label for="birthdate">Birthdate</label>
                  <input type="date" id="datepicker1"  class="form-control @error('birthdate') is-invalid @enderror" name="birthdate" value="{{ old('birthdate') }}">
                  <div class="invalid-feedback">
                    Birthdate is required.
                  </div>
                </div>
                <div class="col-md-2 mb-3">
                  <label for="age">Age</label>
                  <input type="text" tabindex="-1" class="form-control form-control-sm" id="agecal1" placeholder="" readonly name="age">
                </div>
                <div class="col-md-5 mb-3">
                  <label for="gender">Gender</label>
                  <select class="custom-select custom-select-sm  custom-select custom-select-sm -sm d-block w-100" id="gender1"  type="text" name="gender" autocomplete="gender">
                    <option value="">Select Gender</option>
                    <option value="0">Male</option>
                    <option value="1">Female</option>
                    <option value="2">Rather not to say</option>
                  </select>
                  <div class="invalid-feedback">
                    Please select your gender preference.
                  </div>
                </div>
              </div>
                  <div class="row px-3">
                    <div class="col-md-5 mb-3">
                      <label for="contact_no">Contact Number</label>
                      <div class="input-group input-group-sm">
                        <div class="input-group-prepend">
                          <span class="input-group-text">+63</span>
                        </div>
                        <input type="text"  class="form-control form-control-sm" id="contact_no1" placeholder="10-digit Mobile Number"  minlength="0" maxlength="10" onkeydown="return ( event.ctrlKey || event.altKey 
                    || (47<event.keyCode && event.keyCode<58 && event.shiftKey==false) 
                    || (95<event.keyCode && event.keyCode<106)
                    || (event.keyCode==8) || (event.keyCode==9) 
                    || (event.keyCode>34 && event.keyCode<40) 
                    || (event.keyCode==46))" name="contact_no" autocomplete="contact_no" title="" style="border-radius: 0;">
                        <div class="invalid-feedback" style="width: 100%;">
                          Contact Number is required.
                        </div>
                      </div>
                    </div>
          
                    <div class="col-md-7 mb-3 mt-0">
                      <label for="gmail_address">Gmail Address</label>
                        <input type="email" class="form-control form-control-sm" id="gmail_address1" placeholder="example@gmail.com"  name="gmail_address" autocomplete="gmail_address">
                        <div class="invalid-feedback" style="width: 100%;">
                          Your Gmail Address is required.
                        </div>
                    </div>  
                  </div>
                  <div class="row px-3">
                    <div class="col-md-6 mb-3">
                      <label for="occupation">Occupation</label>
                      <select class="custom-select custom-select-sm  custom-select custom-select-sm -sm  d-block w-100" id="occupation1"  type="text" name="occupation" autocomplete="occupation">
                        <option value="">Select Occupation</option>
                         @foreach($occupations as $item)
                          <option id="occupation" value="{{$item->id}}">{{$item->name}}</option>
                          @endforeach
                          <option value='0'>Other</option>
                      </select>
                      <div class="invalid-feedback">
                        Occupation is required.
                      </div>
                    </div>
                    <div class="col-md-6 mb-3">
                      <label for="other_occupation"></label>
                      <input type="text" id="other_occupation1" name="other_occupation" hidden class="form-control form-control-sm">
                    </div>
                  </div>
                  <div class="row px-3">
                    <div class="col-md-4 mb-3">
                      <label for="region_code">Region</label>
                      <select class="form-control custom-select-sm" id="region_id1"  type="text" name="region_id" >
                        <option value="">Select Region</option>
                         @foreach($regions as $item)
                        <option id="region_code" value="{{$item->region_id}}">{{$item->name}}</option>
                        @endforeach
                      </select>
                      <input type="text" id="region_hidden" name="region_hidden" hidden readonly  class="form-control form-control-sm">
                      <div class="invalid-feedback">
                        Region is required.
                      </div>
                    </div>
                    <div class="col-md-4 mb-3">
                      <label for="province_id">Province</label>
                      <select readonly class="form-control  custom-select-sm" id="province_id1"  readonly name="province_id" autocomplete="province_id" >
                       
                      </select>
                      <input type="text" id="province_hidden" name="province_hidden" hidden readonly  class="form-control form-control-sm">
                      <div class="invalid-feedback">
                        Province is required.
                      </div>
                    </div>
                    <div class="col-md-4 mb-3">
                      <label for="city_id">City/Municipality</label>
                      <select  class="form-control  custom-select-sm" id="city_id1"   name="city_id" autocomplete="city_id" readonly>
                       
                      </select>
                      <input type="text" id="city_hidden" name="city_hidden" hidden readonly  class="form-control form-control-sm">
                      <div class="invalid-feedback">
                        City/Municipality is required.
                      </div>
                    </div>
                  </div> <!-- Region, Province, Municiaplity -->
                  <div class="row px-3">
                    <div class="col-md-4 mb-3">
                      <label for="barangay_id">Barangay</label>
                       <select  class="form-control  custom-select-sm" id="barangay_id1"   name="barangay_id1"  readonly>
                        
                      </select>
                      <input type="text" id="barangay_hidden" name="barangay_hidden" hidden readonly  class="form-control form-control-sm">
                      <div class="invalid-feedback">
                        Barangay is required.
                      </div>
                    </div>
                    <div class="col-md-8 mb-3">
                      <label for="street">Address</label>
                      <input type="text" class="form-control form-control-sm" id="street1" placeholder="Bldg. No./Street/Apartment No." name="street">
                    </div>
                  </div> <!-- Barangay, Street -->
                  <div class="row px-3">
                      <div class="col-md-6 mb-3">
                        <label for="member_status">Membership Status</label>
                        <select class=" custom-select-sm form-control" id="member_status1"  type="text" name="member_status" autocomplete="member_status" >
                          <option value="" selected disabled>Select Status</option>
                          <option value= 0>New Member</option>
                          <option value= 1 >Returning Member</option>
                          <option value= 2 >From Associate Member</option>
                        </select>
                        
                        <div class="invalid-feedback">
                          This field is required.
                        </div>
                      </div>
                      <div id="div-memno1" hidden class="col-md-6 mb-3">
                        <label for="member_no">Member No.</span></label>
                        <input type="text" class="form-control form-control-sm" hidden id="member_no1" placeholder="Member No." name="member_no">
                      </div>
                      <div id="div-staffcd1" hidden class="col-md-6 mb-3">
                        <label for="staff_referral_cd">Staff Referral</span></label>
                        <input hidden type="text" class="form-control form-control-sm" id="staff_referral_cd1" placeholder="Name or Code" name="staff_referral_cd">
                      </div>
                  </div> <!-- Membership Status -->
                  <div class="row px-3">
                    <div class="col-md-4 mb-3">
                      <label for="seminar_pref">Online or Walk-in Seminar?</label>
                      <select class="form-control custom-select-sm  d-block w-100 @error('seminar_pref') is-invalid @enderror" id="seminar_pref1"  type="text" name="seminar_pref" autocomplete="seminar_pref" >
                        <option value="">Select</option>
                        <option value="0">Online Video Conference</option>
                        <option value="1">Walk-in / Physical Appearance</option>
                      </select>
                      <div class="invalid-feedback">
                        Seminar Preference is required.
                      </div>
                    </div>
                    <div class="col-md-4 mb-3">
                      <label for="branch_id">Preferred MHRMPC Branch</label>
                      <select class="form-control custom-select-sm @error('branch_id') is-invalid @enderror" id="branch_id1"  type="text" name="branch_id" autocomplete="branch_id" >
                        <option value="" selected disabled>Select Branch</option>
                        @foreach($branches as $item)
                        <option id="branch" value="{{$item->id}}">{{$item->name}}</option>
                        @endforeach
                        
                      </select>
                      <div class="invalid-feedback">
                        This field is required.
                      </div>
                    </div>
                    <div class="col-md-4 mb-3">
                      <label for="batch_id">Preferred Schedule</label>
                      <select class="form-control custom-select-sm @error('batch_id') is-invalid @enderror" id="batch_id1"  type="text" name="batch_id" autocomplete="batch_id" >
                        <option value="" selected disabled>Select</option>
                      @foreach($batches as $batch)
                      <option  value="{{$batch->id}}">{{ \Carbon\Carbon::parse($batch->sched_date)->format('m-d-Y')}}, @if($batch->online_vid_tool == 2) Walk-in @else Online Video Conference  @endif
                      </option>
                      @endforeach
                      </select>
                      <div class="invalid-feedback">
                        This Preferred Schedule is required.
                      </div>
                    </div>
                    <div class="row px-3">
                    <div class="col-md-8 mb-3">
                      <div class="col-md ">
                      <label for="of_tag">Are you an Overseas Filipino Working/Residing abroad?</label>
                      <div class=" row">
                      <div class="form-check form-check-inline">
                        <input class="form-check" type="radio" id="1selfy" name="self1" value="1">
                        <label class="form-check-label" for="1selfy"> Yes, I am an Overseas Filipino Worker and I am residing abroad.</label>
                      </div>
                      </div>
                      <div class=" row">
                      <div class="form-check form-check-inline">
                        <input class="form-check" type="radio" id="1selfn" name="self1" value="0">
                        <label class="form-check-label" for="1selfn"> No, I am not an Overseas Filipino Worker.</label>
                      </div>
                      </div>
                      <div class="invalid-feedback">
                        This field is required.
                      </div>
                    </div>
                    <div class="col-md ">
                      <label for="of_tag">Do you have relative/s working/residing abroad? </label>
                      <div class=" row">
                      <div class="form-check form-check-inline">
                        <input class="form-check" type="radio" id="1relativey" name="relative1" value="1">
                        <label class="form-check-label" for="1relativey"> Yes, I have relative/s working and residing abroad.</label>
                      </div>
                      </div>
                      <div class=" row">
                      <div class="form-check form-check-inline">
                        <input class="form-check" type="radio" id="1relativen" name="relative1" value="0">
                        <label class="form-check-label" for="1relativen"> No, I don’t have OFW relative/s.</label>
                      </div>
                      </div>
                      <div class="invalid-feedback">
                        This field is required.
                      </div>
                    </div>
                    </div>
                  </div> <!-- OF Survey -->
                  <div class="row px-3">
                    <div class="col-md-6 mb-3">
                      <label for="marketing_channel">Where did you hear about us?</label>
                      <select class="form-control custom-select-sm"  id="marketing_channel1"  type="text" name="marketing_channel" autocomplete="marketing_channel" >
                        <option value="" selected disabled>Select</option>
                         @foreach($marketingChannel as $item)
                        <option  value="{{$item->id}}">{{$item->name}}</option>
                        @endforeach
                      </select>
                      <div class="invalid-feedback">
                        This field is required.
                      </div>
                    </div>
                    <div class="col-md-6 mb-3">
                      <label for="purpose_tag">Why did you choose membership with us?</label>
                      <select class="custom-select custom-select-sm  d-block w-100" id="purpose_tag1"  type="text" name="purpose_tag" autocomplete="purpose_tag" >
                        <option value="">Select</option>
                        <option value= 0 >For investment only</option>
                        <option value= 1 >For investment and savings</option>
                        <option value= 2 >To avail loan products only</option>
                        <option value= 3 >To avail trading products and discounts</option>
                        <option value= 4 >To avail all products and services offered (investment, savings, loans, tradings, etc.</option>
                      </select>
                      <div class="invalid-feedback">
                        This field is required.
                      </div>
                    </div>
                  </div>

              <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-primary btn-save" id="btn-save">Yes
                </button>
              </div>
            </form>
          </div>
          <div class="modal-footer">
            
          </div>
        </div>
      </div>
    </div>
<!-- end bootstrap model -->
 <script>
  // window.addEventListener("load", function() {
$(document).ready(function($){
    $.ajaxSetup({
        headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $('#modalCatcher').on('click', '#btn-save', function () {
            $('#btn-save').click(function (event) {
                 // alert(formData);


                 var id = $("#id").val();
                  var fname = $("#fname1").val();
                  var mname = $("#mname1").val();
                  var lname = $("#lname1").val();
                  var fullname = $("#fullname").val();
                  var birthdate = $("#datepicker1").val();
                  var age = $("#agecal1").val();
                  var gender = $("#gender1").val();
                  var contact_no = $("#contact_no1").val();
                  var occupation = $("#occupation1").val();
                  var other_occupation = $("#other_occupation1").val();
                  var region_id = $("#region_id1").val();
                  var province_id = $("#province_id1").val();
                  var city_id = $("#city_id1").val();
                  var barangay_id = $("#barangay_id1").val();
                  var street = $("#street1").val();
                  var self = $("#self1").val();
                  var relative = $("#relative1").val();
                  var marketing_channel = $("#marketing_channel1").val();
                  var purpose_tag = $("#purpose_tag1").val();
                  var member_status = $("#member_status1").val();
                 // alert(JSON.stringify(member_status));
                  var member_no = $("#member_no1").val();
                  var staff_referral_cd = $("#staff_referral_cd1").val();
                  var seminar_pref = $("#seminar_pref1").val();
                  var batch_id = $("#batch_id1").val();
                  var branch_id = $("#branch_id").val();
                  var gmail_address = $("#gmail_address1").val();

                  
                  $("#btn-save").html('Please Wait...');
                  $("#btn-save"). attr("disabled", true);
                // event.preventDefault();
                // ajax
                    $.ajax({
                    type:"POST",
                    url: "{{ url('update-verfied-enduser') }}",
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    data: {
                      id:id,
                      fname:fname,
                      mname:mname,
                      lname:lname,
                      fullname:fullname,
                      birthdate:birthdate,
                      age:age,
                      gender:gender,
                      contact_no:contact_no,
                      occupation:occupation,
                      other_occupation:other_occupation,
                      region_id:region_id,
                      province_id:province_id,
                      city_id:city_id,
                      barangay_id:barangay_id,
                      street:street,
                      // of_tag:of_tag,
                      marketing_channel:marketing_channel,
                      purpose_tag:purpose_tag,
                      member_status:member_status,
                      member_no:member_no,
                      staff_referral_cd:staff_referral_cd,
                      seminar_pref:seminar_pref,
                      batch_id:batch_id,
                      branch_id:branch_id,
                      gmail_address:gmail_address,
                    },
                    dataType: 'json',
                    success: function(res){
                      // alert('success');
                      // alert(JSON.stringify(res));
                     window.location.reload();
                    $("#btn-save").html('Submit');
                    $("#btn-save"). attr("disabled", false);
                    }
                    ,error: function(xhr, status, error){
                   var errorMessage = xhr.status + ': ' + xhr.statusText
                   alert('Error? - ' + errorMessage);
                    }
                    });
              });
    });
}); 
</script>
@endsection
@section('enduser')
  <script src="{{ asset('assets') }}/js/core/jquery.min.js"></script>
  <script src="{{ asset('assets') }}/js/core/popper.min.js"></script>
  <script src="{{ asset('assets') }}/js/core/bootstrap.min.js"></script>
  <script src="{{ asset('assets') }}/js/plugins/perfect-scrollbar.jquery.min.js"></script>
  <script src="{{ asset('assets') }}/js/batchschedule.js"></script>
  <script src="{{ asset('assets') }}/js/addressDropdown.js"></script>
  <script src="{{ asset('assets') }}/js/extrajs.js"></script>
  <script src="{{ asset('assets') }}/js/clientotheroccu.js"></script>
  <script src="{{ asset('assets') }}/js/clientverifier.js"></script>
  <script src="{{ asset('assets') }}/js/autoageDatepicker.js"></script>
@endsection

