
<!-- Modal -->
 <div class="modal fade" id="ajax-client-model" aria-hidden="true" data-backdrop="static" data-keyboard="false">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title" id="ajaxClientModel"></h4>
            <button type="button" class="btn btn-default close-modal" id="close-modal" >Close</button>
          </div>
          <div class="modal-body">
                <div class="card-body" >
                    <div class="form-group row">
                        <form id="insert_form" name='formname' >
                        <input type="hidden" name="id" id="id">
                        <input type="hidden" value="{{url('/')}}" id="url" name="url">
                           <div class="form-group row">
                           <div class="card-header1">{{ __('Pre-Membership Education Seminar Pre-Registration Form') }}</div>
                           </div>
                           <!-- full name -->
                           <div class="form-group row">
                                   <label for="name" class="col-md-4 col-form-label  text-md-left ">{{ __('Full Name') }}  <span class="text-danger">*</span></label>
                           </div>
                           <div class="form-group row">
                               <div style="padding-top: 10px;" class="col-md-4">
                                   <input id="fname" type="text" wire:model="fname" class="form-control @error('fname') is-invalid @enderror" name="fname" value="{{ old('fname') }}" placeholder="First Name"  autocomplete="fname" autofocus />
                                   @error('fname')
                                       <span class="invalid-feedback" role="alert">
                                           <strong>{{ $message }}</strong>
                                       </span>
                                   @enderror
                               </div>
                               <div style="padding-top: 10px;" class="col-md-4">
                                   <input id="mname" type="text" wire:model="mname" class="form-control @error('mname') is-invalid @enderror" name="mname" value="{{ old('mname') }}" placeholder="Middle Name"  autocomplete="mname" autofocus />
                                   @error('mname')
                                       <span class="invalid-feedback" role="alert">
                                           <strong>{{ $message }}</strong>
                                       </span>
                                   @enderror
                               </div>
                               <div class="col-md-4" style="padding-top: 10px;">
                                   <input id="lname" type="text"  wire:model="lname" class="form-control @error('lname') is-invalid @enderror" name="lname" value="{{ old('lname') }}" placeholder="Surname"  autocomplete="lname" autofocus />
                                   @error('lname')
                                       <span class="invalid-feedback" role="alert">
                                           <strong>{{ $message }}</strong>
                                       </span>
                                   @enderror
                               </div>
                           </div> 
                           <!-- bday-age-gender -->
                            <div class="form-group row">
                                <label for="name" class="col-md-4 col-form-label float-left">{{ __('Birthdate') }}  <span class="text-danger">*</span></label>
                                <div class="col-md-8">
                                <input type="date" wire:model="birthdate"  id="datepicker"  class="form-control @error('birthdate') is-invalid @enderror" name="birthdate" value="{{ old('birthdate') }}">
                                </div>
                                @error('datepicker')
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                            <div class="form-group row">
                                <label for="name" class="col-md-4 col-form-label float-left">{{ __('Age') }}</label>
                                <div class="col-md-8">
                                <!--- date picker --->
                                <input type="text" readonly wire:model='age' autocomplete="age" id="agecal" tabindex="-1" name="age"  class="form-control">
                                @error('age')
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                                </div>
                            </div> 
                            <div class="form-group row">
                                <label for="name" class="col-md-4 col-form-label float-left">{{ __('Gender') }}  <span class="text-danger">*</span></label>
                                <div class="col-md-8">
                                <select id="gender" wire:model="gender" type="text" class="form-control @error('gender') is-invalid @enderror" name="gender" value="{{ old('gender') }}"  autocomplete="gender" autofocus>

                                <option value="" >Select Gender</option>
                                <option value="0" >Male</option>
                                <option value="1" >Female</option>
                                </select>
                                @error('gender')
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                                </div>
                            </div>
                            <!-- contact no-gmail address -->
                            <div class="form-group row">
                                <label for="name" class="col-md-4 col-form-label text-md-left">{{ __('Contact Number') }}  <span class="text-danger">*</span></label>
                                <div class="col-md-8 form-cotrol input-group">
                                    <div class="input-group-prepend form-cotrol">
                                        <div class="input-group-text form-cotrol"><i>{{ __('+63') }}</i></div>
                                    </div>
                                    <input id="contact_no"  wire:model="contact_no" type="text" onkeydown="return ( event.ctrlKey || event.altKey 
                                    || (47<event.keyCode && event.keyCode<58 && event.shiftKey==false) 
                                    || (95<event.keyCode && event.keyCode<106)
                                    || (event.keyCode==8) || (event.keyCode==9) 
                                    || (event.keyCode>34 && event.keyCode<40) 
                                    || (event.keyCode==46))" minlength="0" maxlength="10"  class="input-group form-control @error('contact_no') is-invalid @enderror" name="contact_no" value="{{ old('contact_no') }}" placeholder="9386166587 "  autocomplete="contact_no" title="Ex: 9386166587" />
                                    @error('contact_no')
                                    <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror    
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="name" class="col-md-4 col-form-label text-md-left">{{ __('Gmail Address') }}  <span class="text-danger">*</span></label>
                                <div class="col-md-8">
                                    <input id="gmail_address" wire:model="gmail_address" type="text" class="form-control @error('gmail_address') is-invalid @enderror" name="gmail_address" value="{{ old('gmail_address') }}" placeholder="Gmail Email Address"  autocomplete="gmail_address" autofocus />

                                    @error('gmail_address')
                                    <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <!-- present address -->
                            
                            <div class="form-group row">
                                <label for="name" class="col-md col-form-label float-left">{{ __('Present Address') }}  <span class="text-danger">*</span></label>
                            </div>
                            <div class="form-group row">
                                <div style="padding-top: 10px;" class="col-md-6">
                                <select id="region_id" wire:model="selectedRegion" name="region_id" class="form-control">
                                <option selected value="0">Select Region</option>
                                 @foreach($regions as $item)
                                <option id="region_code" value="{{$item->region_id}}">{{$item->name}}</option>
                                @endforeach
                                </select>
                                <div id='response'></div>
                                @error('region_id')
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                                </div>
                                <div style="padding-top: 10px;" class="col-md-6">
                                    <select id="province_id" wire:model="selectedProvince" name="province_id" class="form-control">
                                        <option selected value="0">Select Province</option>
                                    </select>
                                <div id='response'></div>
                                @error('province_id')
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                                </div>
                                
                            </div>
                            <div  class="form-group row">
                                <div style="padding-top: 10px;" class="col-md-6">
                                    <select id="city_id" wire:model="selectedCity" name="city_id" class="form-control">
                                    <option selected value="0">Select City</option>
                                    </select>
                                <div id='response'></div>
                                @error('city_id')
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                                </div>
                                <div style="padding-top: 10px;" class="col-md-6">
                                <select id="barangay_id" wire:model="selectedBarangay" name="barangay_id" class="form-control">
                                        <option selected value="0">Select Barangay</option>
                                    </select>
                                <div id='response'></div>
                                @error('barangay_id')
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                                </div>
                            </div>
                            <div  class="form-group row">
                                <div style="padding-top: 10px;" class="col-md">
                                    <input id="street" type="text" wire:model="street" class="form-control @error('street') is-invalid @enderror" name="street" value="{{ old('street') }}" placeholder="Bldg. No./ Street / Subdivision"  autocomplete="street" autofocus />
                                    @error('street')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <!-- occu-oftag-marChanel-purpose -->
                            <div class="form-group row">
                                <label for="name" class="col-md col-form-label float-left">{{ __('Occupation') }}  <span class="text-danger">*</span></label>
                            </div>
                            <div class="form-group row">
                                <div  class="col-md" id="div-occu"  >
                                <select id="occupation" wire:model="occupation" name="occupation" class="form-control">
                                <option value="" selected disabled>Select Occupation</option>
                                @foreach($occupations as $item)
                                <option id="occupation" value="{{$item->id}}">{{$item->name}}</option>
                                @endforeach
                                <option value="0">Other</option>
                                </select>
                                </div>
                                <div id="div-other-occu" class="col-md" hidden>
                                  <input type="text" id="other_occupation" name="other_occupation" hidden class="form-control form-control">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="card-header1">{{ __('Overseas Worker Inforamtion') }}</div>
                            </div>
                            <div class="form-group row">
                                <label for="name" class="col-md col-form-label float-left">{{ __('Are you an Overseas Filipino Working/Residing abroad?') }}  <span class="text-danger">*</span></label>
                            </div>
                            <div class="form-group row">
                                <div  class="col-md" >
                                      <div class="form-check form-check-inline">
                                        <input class="form-check" id="selfy" type="radio" name="self" value="1">
                                        <label class="form-check-label" for="selfy">Yes, I am an Overseas Filipino Worker and I am residing abroad.</label>
                                      </div>
                                      <div class="form-check form-check-inline">
                                        <input class="form-check" id="selfn" type="radio" name="self" value="0">
                                        <label class="form-check-label" for="selfn">No, I am not an Overseas Filipino Worker.</label>
                                      </div>
                                <!-- <select id="of_tag" name="of_tag" wire:model="of_tag" class="form-control">
                                <option value="">Select</option>
                                <option value= 0 >I am Overseas Filipinos working/residing abroad</option>
                                <option value= 1 >I have relative/s working/residing abroad</option>
                                <option value= 2 >I dont have relatives nor myself working/residing abroad</option>
                                </select> -->

                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="name" class="col-md col-form-label float-left">{{ __('Do you have relative/s working/residing abroad?') }}  <span class="text-danger">*</span></label>
                            </div>
                            <div class="form-group row">
                                <div  class="col-md" >
                                      <div class="form-check form-check-inline">
                                        <input class="form-check" id="relativey" type="radio" name="relative" value="1">
                                        <label class="form-check-label" for="relativey">Yes, I have relative/s working and residing abroad.</label>
                                      </div>
                                      <div class="form-check form-check-inline">
                                        <input class="form-check" id="relativen" type="radio" name="relative" value="0">
                                        <label class="form-check-label" for="relativen">No, I don’t have OFW relative/s.</label>
                                      </div>
                                <!-- <select id="of_tag" name="of_tag" wire:model="of_tag" class="form-control">
                                <option value="">Select</option>
                                <option value= 0 >I am Overseas Filipinos working/residing abroad</option>
                                <option value= 1 >I have relative/s working/residing abroad</option>
                                <option value= 2 >I dont have relatives nor myself working/residing abroad</option>
                                </select> -->
                                
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="card-header1">{{ __('MHRMPC Survey') }}</div>
                            </div>
                            <div class="form-group row">
                                <label for="name" class="col-md col-form-label float-left">{{ __('Where did you hear about Most Holy Rosary Multi-Purpose Cooperative?') }}  <span class="text-danger">*</span></label>
                            </div>
                            <div class="form-group row">
                                <div  class="col-md" >
                                <select id="marketing_channel" wire:model="marketing_channel" name="marketing_channel" class="form-control">
                                <option value="">Select</option>
                                @foreach($marketingChannel as $item)
                                <option  value="{{$item->id}}">{{$item->name}}</option>
                                @endforeach
                                </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="name" class="col-md col-form-label float-left">{{ __('Why do you want to become an MHRMPC member?') }}  <span class="text-danger">*</span></label>
                            </div>
                            <div class="form-group row">
                                <div  class="col-md" >
                                <select id="purpose_tag" wire:model="purpose_tag" name="purpose_tag" class="form-control">
                                <option value="">Select</option>
                                <option value= 0 >For investment only</option>
                                <option value= 1 >For investment and savings</option>
                                <option value= 2 >To avail loan products only</option>
                                <option value= 3 >To avail trading products and discounts</option>
                                <option value= 4 >To avail all products and services offered (investment, savings, loans, tradings, etc.</option>
                                </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="card-header1">{{ __('Membership Preference') }}</div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-6">
                                <label for="name" class="col-md-10 col-form-label float-left">{{ __('Membership Status') }}  <span class="text-danger">*</span></label>
                                </div>
                                <div class="col-md-6">
                                    <select id="member_status" wire:model="member_status" name="member_status" class="form-control">
                                    <option value="">Select</option>
                                    <option value= 2 >Non Member</option>
                                    <option value= 0 >Returning Member</option>
                                    <option value= 1 >From Associate Member</option>
                                    </select>
                                    <div id='response'></div>
                                    @error('member_status')
                                    <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div id="div-memno" class="form-group row">
                                <div class="col-md-6">
                                <label  class="col-md col-form-label float-left">Member Number<span class="text-muted">(Optional)</span></label>
                                </div>
                                <div   class="col-md-6">
                                    <input   id="member_no" wire:model="member_no" placeholder="Member No" name="member_no"  class="form-control"></input>
                                    @error('member_no')
                                    <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-md-6">     
                                    <label for="name" class="col-md-10 col-form-label float-left">{{ __('How do you want to do the PMES?') }}  <span class="text-danger">*</span></label>
                                </div>
                                <div class="col-md-6">
                                    <select id="seminar_pref" wire:model="seminar_pref" name="seminar_pref" class="form-control">
                                    <option value="">Select</option>
                                    <option value="0">Online Video Conference</option>
                                    <option value="1">Walk-in / Physical Appearance</option>
                                    </select>
                                    <div id='response'></div>
                                    @error('seminar_pref')
                                    <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-6">
                                    <label for="name" class="col-md col-form-label float-left">{{ __('Which branch is the most accessible to you?') }}  <span class="text-danger">*</span></label>
                                </div>
                                <div class="col-md-6">
                                    <select id="branch_id" wire:model="branch_id" name="branch_id" class="form-control">
                                    <option value="">Select</option>
                                    @foreach($branches as $item)
                                    <option value="{{$item->id}}">{{$item->name}}</option>
                                    @endforeach
                                    </select>
                                    <div id='response'></div>
                                    @error('branch_id')
                                    <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-6">
                                    <label for="name" class="col-md-10 col-form-label float-left">{{ __('Preferred Schedule') }}  <span class="text-danger">*</span></label>
                                </div>
                                <div class="col-md-6">
                                    <select id="batch_id" wire:model="selectedBatch" name="batch_id" class="form-control">
                                    <option value="">Select</option>
                                    @foreach($batches as $data)
                                    <option  value="{{$data->id}}">{{ \Carbon\Carbon::parse($data->sched_date)->format('m-d-Y')}} | {{\Carbon\Carbon::parse($data->sched_date)->format('h:i a')}}, @if($data->online_vid_tool == 2) Walk-in @else Online Video Conference  @endif</option>
                                      @endforeach
                                    </select>
                                    <div id='response'></div>
                                    @error('batch_id')
                                    <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                                
                            <div id="batch_schedule">
                            <div class="form-group row">
                            <div class="card-header1">{{ __('Batch Schedule') }}</div>
                            </div>
                            <div class="form-group row">
                                <div  style="padding-top: 10px;" class="col-md-5">     
                                    <label for="name" class="col-md-10 col-form-label float-left">{{ __('Batch Code') }}  <span class="text-muted">(Optional)</span></label>
                                </div>
                                <div style="padding-top: 10px;" class="col-md-7">
                                    <select  id="batch_id1" wire:model="batch_id" name="batch_id1" class="form-control">
                                    <option value="">Select</option>
                                    @foreach($batches as $batch)
                                    <option  value="{{$batch->id}}">{{$batch->batch_cd}}</option>
                                    @endforeach
                                    </select>
                                    <div id='response'></div>
                                    @error('batch_id')
                                    <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                    </div>
                            </div>
                            <div class="form-group row">
                                    <div  style="padding-top: 10px;"class="col-md-5">
                                    <label for="name" class="col-md-10 col-form-label float-left">{{ __('Schedule Date') }}</label>
                                </div>
                                    <!-- Schedule Date -->
                                    <div style="padding-top: 10px;" class="col-md-7">
                                    <input type="text" tabindex="-1" readonly id="sched_date" class="form-control" name="sched_date">
                                    </div>
                                    <!-- Schedule Date -->
                            </div>
                            <div class="form-group row">
                                <div style="padding-top: 10px;" class="col-md-5">
                                    <label for="name" class="col-md-10 col-form-label float-left">{{ __('Schedule Time') }}</label>
                                    </div>
                                    <div style="padding-top: 10px;" class="col-md-7">
                                    <input class="form-control" tabindex="-1" readonly type="text" id="sched_time" name="sched_time">
                                    </div>
                            </div>
                            <div class="form-group row">
                                <div style="padding-top: 10px;" class="col-md-5">     
                                    <label for="name" class="col-md-10 col-form-label float-left">{{ __('Preferred Application') }}</label>
                                </div>
                                <!-- Online Video Tool -->
                                <div style="padding-top: 10px;" class="col-md-7">
                                <input class="form-control" tabindex="-1" readonly type="text" id="tool" name="tool">
                                </div>
                            </div>
                            <div class="form-group row">
                                <!-- Schedule Date -->
                                <div style="padding-top: 10px;" class="col-md-5">
                                    <label for="name" class="col-md-10 col-form-label float-left">{{ __('Meeting Code') }}</label>
                                </div>
                                
                                <div style="padding-top: 10px;" class="col-md-7">
                                <input type="text" readonly tabindex="-1" id="meeting_cd" class="form-control" name="meeting_cd">
                                </div>
                            </div>
                            <div class="form-group row">
                                <!-- Schedule Date -->
                                <div style="padding-top: 10px;" class="col-md-5">
                                    <label for="name" class="col-md-10 col-form-label float-left">{{ __('Meeting Password') }}</label>
                                </div>
                                <div class="col-md-7">
                                <input class="form-control" tabindex="-1" readonly type="text" id="meeting_pw" name="meeting_pw">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-10 ">
                                <label for="name">{{ __('By clicking “Yes”, it means that you are giving us your consent to use your data you are about to submit. Thank you!') }}</label>
                                </div>
                                <div style="justify-content: center;" class="col-md-1 ">
                                <label for="name">{{ __('Yes') }}</label>
                                </div>
                                <div class="col-md-1 ">
                                <input name="data_consent_fl" value="on" checked type="checkbox" >
                                </div>
                            </div>
                            </div>
                        </form>
                    </div>    
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" tabindex="-1" class="btn btn-secondary close-modal" >Close</button>
                <button type="submit" class="btn btn-primary btn-save" id="btn-save">Save
                </button>
            </div>
        </div>
    </div>
</div>

@section('clientjs')
  <script src="{{ asset('assets') }}/js/autoageDatepicker.js"></script>
  <script src="{{ asset('assets') }}/js/batchschedule.js"></script>
  <script src="{{ asset('assets') }}/js/addressDropdown.js"></script>
  <script src="{{ asset('assets') }}/js/extrajs.js"></script>
  <script src="{{ asset('assets') }}/js/clientotheroccu.js"></script>
  <script src="{{ asset('assets') }}/js/clientverifier.js"></script>
@endsection