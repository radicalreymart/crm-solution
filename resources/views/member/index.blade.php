@extends('layouts.app', [
    'class' => 'sidebar-mini ',
    'namePage' => 'Member Index',
    'activePage' => 'member-management',
    'activeNav' => 'settings',
])

@section('indexdeleteall')
  <script src="{{ asset('assets') }}/js/indexdeleteall.js"></script>
@endsection
@section('content')
<style>

li.li a {
  text-decoration : none; 
}
ul a {
  font-size : .8em;
}


nav.nav {
  width : 200px; 
}

span.span {
  padding : 3px;
  font-size : 1.2em;
  font-variant : small-caps;
  cursor : pointer;
  display: block;
}

span.span::after {
  float: right;
  right: 10%;
}

.slide {
  clear:both;
  width:80%;
  padding-left: 0px;
  height:0px;
  overflow: hidden;
  text-align: center;
  transition: height .4s ease;
}

.slide li.li {padding : 3px;}

#touch {position: absolute; opacity: 0; height: 0px;}    

#touch:checked + .slide {height: 170px;} 
#touch:checked + .nav {width: 250px !important;} 
</style>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css">

    @include('auth.passwords.confirmmodal')

<div class="panel-header panel-header-sm">
  </div>
  <div class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header">
            @include('flash-message') 
       @include('sweetalert::alert')
                <div class="float-left">
                    <h4 class="card-title">Member</h4>
                </div>
                <div class="float-right">
                      <a href="javascript:void(0)" title="Add Member" class="btn btn-success" id="addNewMember">
                        <li class="fas fa-plus-circle fa-lg" aria-hidden="true"></li>
                      </a>
                    <!-- <button class="btn btn-danger delete_all" data-url="{{ url('DeleteAllMember') }}">Delete All Selected</button> -->
                </div>
          </div>
          <div class="card-body">
            <div id="modalCatcher" class="table-responsive">
              <input type="hidden" value="{{url('/')}}" id="url" name="url">
                
              <table id='empTable'>
                            <thead>
                                <tr>
                    <th >Action</th>
                    <th >Member <br>Number</th>
                    <th >Name</th>
                    <th >Unique <br>Key</th>
                    <th >
                       Branch 
                    </th>
                    <th>Status
                    </th>
                    <th >Type</th>
                    <th>Gender</th>
                                </tr>
                            </thead>
                            <tbody>
                            @if($empTable->count())
                     @foreach ($empTable as $row)
                          <tr>
                          <td>
                            <a href="javascript:void(0)" title="Show Details" class="show" data-id="{{ $row->id }}">
                              <i class="fas fa-edit  fa-lg"></i>
                            </a>
                          @csrf
                          @method('DELETE')
                          </td>
                          <td>{{ $row->member_no}}</td>
                          <td>{{ $row->long_name }}</td>
                          <td>{{ substr($row->uni_key, -6) }}</td>
                          <td>
                              @if($row->branch_id == 1)
                               Main Branch
                              @elseif($row->branch_id == 2)
                               Kasiglahan Branch
                              @elseif($row->branch_id == 3)
                               San Mateo Branch
                              @elseif($row->branch_id == 4)
                               Batasan Branch
                              @elseif($row->branch_id == 5)
                               Cainta Branch
                              @elseif($row->branch_id == 6)
                               San Rafael Branch
                              @endif
                          </td>
                          <td>
                              @if($row->member_status == 1)
                                Active
                              @elseif($row->member_status == 2)
                                Delinquent
                              @elseif($row->member_status == 3)
                                Inactive
                              @elseif($row->member_status == 4)
                                Closed
                              @elseif($row->member_status == 5)
                                Pending for Closure
                               @endif
                          </td>
                          <td>
                              @if($row->member_type == 1)
                                Associate Member
                              @elseif($row->member_type == 2)
                                Regular Member
                                @endif
                          </td>
                          <td>{{ $row->gender }}</td>
                          </tr>
                          
                        @endforeach
                    @endif
                            </tbody>
                        </table>

                
              
            </div>
          </div>
        </div>
      </div>
      
    </div>
  </div>
  <!-- Modal -->
<div class="modal fade" id="ajax-membershow-model" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
            <h4 class="modal-title" id="ajaxMemberShowModel"></h4>
            <button type="button" class="btn btn-default"  id="close-modal" >Close</button>
            </div>
            <div class="modal-body">
                <table class="table" id="dynamicAddRemove">
                    <tr>
                        <td>Member Number</td>
                        <td id="member_no2"></td>
                    </tr>
                    <tr>
                        <td>Unique Key</td>
                        <td id="uni_key3"></td>
                    </tr>
                    <tr>
                        <td>Name</td>
                        <td id="long_name1"></td>
                    </tr>
                    <tr>
                        <td>Branch</td>
                        <td id="branch1"></td>
                    </tr>
                    <tr>
                        <td>Status</td>
                        <td id="status1"></td>
                    </tr>
                    <tr>
                        <td>Type</td>
                        <td id="type1"></td>
                    </tr>
                    <tr>
                        <td>Gender</td>
                        <td id="gender1"></td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="ajax-member-model" aria-hidden="true" data-backdrop="static" data-keyboard="false">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title" id="ajaxMemberModel"></h4>
            <button type="button" class="btn btn-default close-modal" id="close-modal" >Close</button>
          </div>
          <div class="modal-body">
            <form action="javascript:void(0)" id="form" class="form-horizontal">
              <!-- {{ csrf_field() }} -->
              <!-- <input type="hidden" name="_method" value="POST"> -->
              <input type="hidden" name="id" id="id">
              
               <div class="form-group row">
                  <label for="name" class="col-md-4 col-form-label text-md-right">Branch<span class="text-danger">*</span></label>
                   <div class="col-md-6">
                      <select id="branch_id"  name="branch_id" class="form-control">
                           <option value="">Select Branch</option>
                      @foreach($branch as $item)
                      <option value="{{$item->id}}">{{$item->name}}</option>
                      @endforeach
                      </select>
                   <span class="text-danger">
                      <strong>{{$errors->first('branch_id') ?? null}}
                      </strong>
                  </span>
                  </div>
              </div>  

              <div class="form-group row">
                  <label for="member_no" class="col-md-4 col-form-label text-md-right">{{ __('Member Number') }}<span class="text-danger">*</span></label>
                  <!-- <div  class="col-md-3">
                      <input  oninput="this.value = this.value.toUpperCase()"  id="pre_member_no" type="text" wire:model="pre_member_no" maxlength='4' minlength='2' onkeydown="return /[a-z]/i.test(event.key)"  class="form-control @error('pre_member_no') is-invalid @enderror" name="pre_member_no" value="{{ old('pre_member_no') }}" required autocomplete="pre_member_no" autofocus />
                  </div> -->
                  <div  class="col-md-6">
                      <input  id="member_no" type="text" wire:model="member_no" class="member_noClass form-control @error('member_no') is-invalid @enderror" name="member_no" value="{{ old('member_no') }}" required autocomplete="member_no" autofocus />
                     
                      @error('member_no')
                          <span class="invalid-feedback" role="alert">
                              <strong>{{ $message }}</strong>
                          </span>
                      @enderror
                  </div>
              </div>
              <div class="form-group row">
                  <div  class="col-md-4">
                      <p >{{ __(' ')}}</p>
                  </div>
                  <div  class="col-md">
                      <p id="result"></p>
                  </div>
              </div>
              <div class="form-group row">
                  <label for="uni_key" class="col-md-4 col-form-label text-md-right">{{ __('Unique Key') }}<span class="text-danger">*</span></label>
                  
                  <div  class="col-md-3">
                      <input  oninput="this.value = this.value.toUpperCase()"  id="uni_key" type="text" wire:model="uni_key" class="form-control @error('uni_key') is-invalid @enderror" maxlength="6" minlength="6" name="uni_key" value="{{ old('uni_key') }}" required autocomplete="uni_key" autofocus />
                      @error('uni_key')
                          <span class="invalid-feedback" role="alert">
                              <strong>{{ $message }}</strong>
                          </span>
                      @enderror
                  </div>
                  <div style="padding-left: 0px ;" class="col-md-3">
                      <button id="randombutton" style="margin-left: 0px; margin-top: 0px; border-radius:30px;" type="button" class=" btn btn-primary random" >{{ __('Randomize')}}
                          <i hidden id="loading" class="fas fa-spinner fa-spin"></i>
                        </button>
                  </div>
              </div>
              <div class="form-group row">
                  <div  class="col-md-4">
                      <p >{{ __(' ')}}</p>
                  </div>
                  <div  class="col-md-6">
                      <p id="result1"></p>
                  </div>
              </div>
              <div class="form-group row">
                  <label for="fname" class="col-md-4 col-form-label text-md-right">{{ __('First Name') }}<span class="text-danger">*</span></label>

                  <div class="col-md-6">
                      <input id="fname" type="text" wire:model="fname" class="form-control @error('fname') is-invalid @enderror" name="fname" value="{{ old('fname') }}" required autocomplete="fname" autofocus />

                      @error('fname')
                          <span class="invalid-feedback" role="alert">
                              <strong>{{ $message }}</strong>
                          </span>
                      @enderror
                  </div>
              </div>
              <div class="form-group row">
                  <label for="mname" class="col-md-4 col-form-label text-md-right">{{ __('Middle Name') }}</label>

                  <div class="col-md-6">
                      <input id="mname" type="text" wire:model="mname" class="form-control " name="mname" value="{{ old('mname') }}" required autocomplete="mname" autofocus />

                  </div>
              </div>
              <div class="form-group row">
                  <label for="lname" class="col-md-4 col-form-label text-md-right">{{ __('Last Name') }}<span class="text-danger">*</span></label>

                  <div class="col-md-6">
                      <input id="lname" type="text" wire:model="lname" class="form-control @error('lname') is-invalid @enderror" name="lname" value="{{ old('lname') }}" required autocomplete="lname" autofocus />

                      @error('lname')
                          <span class="invalid-feedback" role="alert">
                              <strong>{{ $message }}</strong>
                          </span>
                      @enderror
                  </div>
              </div>
              <div id="fullname_div" hidden class="form-group row">
                  <label for="lname" class="col-md-4 col-form-label text-md-right">{{ __('Full Name') }}</label>

                  <div class="col-md-6">
                      <input id="fullname" readonly type="text" wire:model="fullname" class="form-control @error('fullname') is-invalid @enderror" name="fullname" value="{{ old('fullname') }}" required autocomplete="fullname" autofocus />

                      @error('fullname')
                          <span class="invalid-feedback" role="alert">
                              <strong>{{ $message }}</strong>
                          </span>
                      @enderror
                  </div>
              </div>
              <div class="form-group row">
                  <label for="gender" class="col-md-4 col-form-label text-md-right">{{ __('Gender') }}</label>

                  <div class="col-md-6">
                      <select id="gender" name="gender" class="form-control">
                          <option value="">Select Gender</option>
                          <option value="F">Female</option>
                          <option value="M">Male</option>
                      </select>
                      @error('gender')
                          <span class="invalid-feedback" role="alert">
                              <strong>{{ $message }}</strong>
                          </span>
                      @enderror
                  </div>
              </div>

              <div class="form-group row">
                  <label for="name" class="col-md-4 col-form-label ">{{ __('Contact Number') }}  <span class="text-danger">*</span></label>
                  <div class="col-md-6 form-cotrol input-group">
                      <div class="input-group-prepend form-cotrol">
                          <div class="input-group-text form-cotrol"><i>{{ __('+63') }}</i></div>
                      </div>
                      <input id="contact_no"  wire:model="contact_no" type="text" onkeydown="return ( event.ctrlKey || event.altKey 
                      || (47<event.keyCode && event.keyCode<58 && event.shiftKey==false) 
                      || (95<event.keyCode && event.keyCode<106)
                      || (event.keyCode==8) || (event.keyCode==9) 
                      || (event.keyCode>34 && event.keyCode<40) 
                      || (event.keyCode==46))" minlength="0" maxlength="10"  class="input-group form-control @error('contact_no') is-invalid @enderror" name="contact_no" value="{{ old('contact_no') }}" placeholder="9386166587 "  autocomplete="contact_no" title="Ex: 9386166587" />
                      @error('contact_no')
                      <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                      </span>
                      @enderror    
                  </div>
              </div>
              
              <div class="form-group row">
                  <label for="gender" class="col-md-4 col-form-label text-md-right">{{ __('Birthdate') }}<span class="text-danger">*</span></label>

                  <div class="col-md-6">
                      <input type="date"  class="form-control @error('birthdate') is-invalid @enderror"  id="birthdate" name="birthdate" value="{{ old('birthdate') }}">
                      @error('birthdate')
                          <span class="invalid-feedback" role="alert">
                              <strong>{{ $message }}</strong>
                          </span>
                      @enderror
                  </div>
              </div>
              
              <div class="form-group row">
                  <label for="gender" class="col-md-4 col-form-label text-md-right">{{ __('Email') }}<span class="text-danger">*</span></label>

                  <div class="col-md-6">
                      <input id="email"  type="text" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" placeholder=" Email Address"  autocomplete="gmail_address" autofocus />
                      @error('email')
                          <span class="invalid-feedback" role="alert">
                              <strong>{{ $message }}</strong>
                          </span>
                      @enderror
                  </div>
              </div>

              <div class="form-group row">
                  <label for="member_status" class="col-md-4 col-form-label text-md-right">{{ __('Member Status') }}<span class="text-danger">*</span></label>

                  <div class="col-md-6">
                      <select id="member_status" name="member_status" class="form-control">
                          <option value="">Select Status</option>
                          <option value="1">Active</option>
                          <option value="2">Delinquent</option>
                          <option value="3">Inactive</option>
                          <option value="4">Closed</option>
                          <option value="5">Pending for Closure</option>
                      </select>
                      @error('member_status')
                          <span class="invalid-feedback" role="alert">
                              <strong>{{ $message }}</strong>
                          </span>
                      @enderror
                  </div>
              </div>

              <div class="form-group row">
                  <label for="member_type" class="col-md-4 col-form-label text-md-right">{{ __('Member Type') }}<span class="text-danger">*</span></label>

                  <div class="col-md-6">
                      <select id="member_type" name="member_type" class="form-control">
                          <option value="">Select Type</option>
                          <option value="1">Associate Member</option>
                          <option value="2">Regular Member</option>
                      </select>
                      @error('member_type')
                          <span class="invalid-feedback" role="alert">
                              <strong>{{ $message }}</strong>
                          </span>
                      @enderror
                  </div>
              </div>
              <!-- <div class="form-group row">
                  <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('Email') }}</label>

                  <div class="col-md-6">
                      <input id="email" wire:model="email" type="text" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus />

                      @error('email')
                          <span class="invalid-feedback" role="alert">
                              <strong>{{ $message }}</strong>
                          </span>
                      @enderror
                  </div>
              </div>
               -->

              <div class="col-sm-offset-2 col-sm-10">
                <button type="button" class=" btn btn-primary btn-save" id="btn-save">{{ __('Save')}}
                  <i hidden id="loading" class="fas fa-spinner fa-spin"></i>
                </button>
              </div>
            </form>
          </div>
          <div class="modal-footer">
            
          </div>
        </div>
      </div>
    </div>

<script type="text/javascript" src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script>
  // window.addEventListener("load", function() {
    window.addEventListener('DOMContentLoaded', event => {
    // Simple-DataTables
    // https://github.com/fiduswriter/Simple-DataTables/wiki
    
        // const datatablesSimple = document.getElementById('datatablesSimple');
        // if (datatablesSimple) {
        //     new simpleDatatables.DataTable(datatablesSimple, {
        // fixedHeight: true,
        // paging:5
        //     });
        // }
    });
$(document).ready(function(){
   $('#empTable').DataTable({
             
          });
     const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
      },
      buttonsStyling: false
    });


    const base_path = $("#url").val();
  // $('#branch_id').onchange(function () {
  //   var selected = $(this).value;
  //   alert(selected);
  //   // var branch_id = e.value;
  //   });
     
 $('#branch_id').on('change',function()
    {
        var selected = $(this).val();
        // alert(selected);
        if( selected == 'KBO'){
            $('#pre_member_no').val('KV')
        }
        if( selected == 'SMBO'){
            $('#pre_member_no').val('SMR')
        }
        if( selected == 'BBO'){
            $('#pre_member_no').val('QCR')
        }
        if( selected == 'CBO'){
            $('#pre_member_no').val('CBR')
        }
        if( selected == 'SRBO'){
            $('#pre_member_no').val('SRR')
        }
        if( selected == ''){
            $('#pre_member_no').val('')
        }
        if( selected == 'MAIN'){
            $('#pre_member_no').val('')
        }
    });

    function makeid(length) {
        var result           = '';
        var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
        var charactersLength = characters.length;
        for ( var i = 0; i < length; i++ ) {
          result += characters.charAt(Math.floor(Math.random() * 
     charactersLength));
       }
       return result;
    }
    const validate = () => {
            const $result = $('#result');
            const $result1 = $('#result1');
            const premember_no = $('#pre_member_no').val();
            const sufmember = $('#member_no').val();

            if(premember_no == ''){
            var member_no = sufmember;
            $('#uni_key').val(makeid(6))
            }else{
            var member_no = premember_no+'-'+sufmember;
            $('#uni_key').val(makeid(6))
            }
            const Loneuni_key = $('#uni_key').val();
            var uni_key = member_no+Loneuni_key;
            // $('#uni_key').val('WC0736')
            // Loneuni_key;
            // WC0736
            $result.text('');
            $result1.text('');
            // var email_input = $('#email').val();

            $.ajax({
                type:"get",
                url: base_path+"/validateMemberNo/",
                data: {member_no:member_no},
                // dataType: 'json',
                success: function(data){
                  // alert(data);
                  var member_no = data;
                  if(member_no == 'nottaken'){
                    $result.text('Member number is valid.');
                    $result.css('color', 'green');
                  }
                  else{
                    $result.text('Member number is already taken.');
                    $result.css('color', 'red');
                  }

                }
              });

            $.ajax({
                type:"get",
                url: base_path+"/validateUniKey/",
                data: {uni_key:uni_key},
                // dataType: 'json',
                success: function(data){
                    // $result1.text(data);
                  var uni_key = data;
                  if(uni_key == 'nottaken'){
                    $result1.text('Unique Key is valid.');
                    $result1.css('color', 'green');
                  }
                  else{
                    $result1.text('Unique Key is already taken.');
                    $result1.css('color', 'red');
                  }

                }
              });
            
            return false;
          }
          $('#member_no').on('input', validate);
  $('#randombutton').on('click', function () 
    {
        const $result1 = $('#result1');
        const premember_no = $('#pre_member_no').val();
        const sufmember = $('#member_no').val();

        if(premember_no == ''){
        var member_no = sufmember;
        // $('#uni_key').val(makeid(6))
        }else{
        var member_no = premember_no+'-'+sufmember;
        // $('#uni_key').val(makeid(6))
        }
        const Loneuni_key = $('#uni_key').val(makeid(6));
        // $('#uni_key').val(makeid(6))
            var uni_key = member_no+Loneuni_key;
            // $('#uni_key').val('WC0736')
        $.ajax({
                type:"get",
                url: base_path+"/validateUniKey/",
                data: {uni_key:uni_key},
                // dataType: 'json',
                success: function(data){
                    // $result1.text(data);
                  var uni_key = data;
                  if(uni_key == 'nottaken'){
                    $result1.text('Unique Key is valid.');
                    $result1.css('color', 'green');
                  }
                  else{
                    $result1.text('Unique Key is already taken.');
                    $result1.css('color', 'red');
                  }

                }
              });
    });



   
$('.close-modal').click(function () {
    var modal_array = 
    [
      '#ajax-membershow-model'
      ,'#ajax-member-model'
    ];
    closeModal(modal_array);
       // $('#ajax-pmes-model').modal('hide');
    });
    $.clearInput = function () {
        $('#form').find('input[type=text], input[type=password], input[type=number], input[type=email], select, textarea').val('');
        $('#result1').html('');
        $('#result').html('');
        $('#id').val(null);
    };
     $('.modal').on('hidden.bs.modal', function(){
             // window.location.reload();
             $('#form').removeData('bs.modal')
    $.clearInput();
    // $(this).find('form')[0].reset();
    });

    $.ajaxSetup({
        headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('#addNewMember').click(function () {
       $('#addMemberForm').trigger("reset");
       $('#ajaxMemberModel').html("Add Member");
       $('#ajax-member-model').modal('show');
        $('#pre_member_no').attr('readonly', false);
        $('#member_no').attr('readonly', false);
        $('#uni_key').attr('readonly', false);
        $('#uni_key').attr('readonly', false);
        $('#randombutton').attr('hidden', false);
        $('#fullname_div').attr('hidden', true);
              // $('#member_no1').attr('readonly', false);
    });
      // $('#modalCatcher').on('click', '#btn-save', function () {
    $('#btn-save').on('click', function () 
    {
        try{
          var id = $("#id").val();
          var fname = $("#fname").val();
          var mname = $("#mname").val();
          var lname = $("#lname").val();
          var branch_id = $("#branch_id").val();
          var member_no = $("#member_no").val();
          var premember_no = $("#pre_member_no").val();
          var gender = $("#gender").val();
          var contact_no = $("#contact_no").val();
          var email = $("#email").val();
          var birthdate = $("#birthdate").val();
          var member_status = $("#member_status").val();
          var member_type = $("#member_type").val();
          var uni_key = $("#uni_key").val();
          // alert(id+cd+description+trxn_type_id);
          $("#btn-save").html('Please Wait...');
          $("#btn-save"). attr("disabled", true);           
              }
            catch(err) {
            swalWithBootstrapButtons.fire({
                  title: 'Error!',
                  text: err,
                  icon: 'warning',
                  confirmButtonText: 'Confirmed',
                }).then((result) => {
             // window.location.reload();
            $("#btn-save").html('Submit');
            $("#btn-save"). attr("disabled", false);
                })
            }
        var base_path = $("#url").val();
        $.ajax({
                type:"post",
                url: base_path+"/add-member",
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                data: {
                  id:id,
                  fname:fname,
                  mname:mname,
                  lname:lname,
                  member_no:member_no,
                  // premember_no:premember_no,
                  gender:gender,
                  birthdate:birthdate,
                  contact_no:contact_no,
                  email:email,
                  member_type:member_type,
                  branch_id:branch_id,
                  member_status:member_status,
                  uni_key:uni_key,

                },
                dataType: 'json',
                success: function(res){
                    // alert(JSON.stringify(res))
                    if(res.status == 'errors'){
                      var errors = [];
                       $.each( res.message, function( key, value ) {
                            errors += value+'<br>';
                        });
                        swalWithBootstrapButtons.fire({
                          title: 'Error!',
                          html: errors,
                          icon: 'warning',
                          confirmButtonText: 'Confirmed',
                        }).then((result) => {
                         // window.location.reload();
                            $("#btn-save").html('Submit');
                            $("#btn-save"). attr("disabled", false);
                        })
                    }
                    if(res.success == 'success'){
                      swalWithBootstrapButtons.fire({
                              title: 'Success!',
                              icon: 'success',
                              confirmButtonText: 'Confirmed',
                            }).then((result) => {
                         window.location.reload();})
                        $("#btn-save").html('Submit');
                        $("#btn-save"). attr("disabled", false);
                    }
                }
                ,error: function (data) {
                  alert(data.responseText);
                     // var errors = [];
                     //   $.each( data.responseJSON['errors'], function( key, value ) {
                     //        errors += value+'<br>';
                     //    });
                     //    swalWithBootstrapButtons.fire({
                     //          title: 'Error!',
                     //          html: errors,
                     //          icon: 'warning',
                     //          confirmButtonText: 'Confirmed',
                     //        }).then((result) => {
                     //         // window.location.reload();
                     //            $("#btn-save").html('Submit');
                     //            $("#btn-save"). attr("disabled", false);
                     //        })
                }
            });
    });
    $('#modalCatcher').on('click', '.show', function () 
    {
        var id = $(this).data('id');
        var base_path = $("#url").val();
        $.ajax({
                type:"post",
                url: base_path+"/edit-member",
                data: { id: id },
                dataType: 'json',
                success: function(res){
                    // alert(JSON.stringify(res.email));
                    $('#ajaxMemberModel').html("Edit Member");
                    $('#ajax-member-model').modal('show');
                    $('#id').val(id);
                    document.getElementById("branch_id").options.selectedIndex = res.branch;
                    // $('#branch_id').value(res.branch);
                        // $('#pre_member_no').attr('readonly', true);
                    if( res.branch == 2){
                        $('#pre_member_no').val('KV');
                    }
                    if( res.branch == 3){
                        $('#pre_member_no').val('SMR');
                    }
                    if( res.branch == 4){
                        $('#pre_member_no').val('QCR');
                    }
                    if( res.branch == 5){
                        $('#pre_member_no').val('CBR');
                    }
                    if( res.branch == 6){
                        $('#pre_member_no').val('SRR');
                    }
                    if( res.branch == 1){
                        $('#pre_member_no').val('');
                    }
                    // $('#pre_member_no').attr('readonly', true);
                    $('#member_no').attr('readonly', true);
                    $('#uni_key').attr('readonly', true);
                    $('#uni_key').attr('readonly', true);
                    $('#randombutton').attr('hidden', true);
                    $("#fname").val(res.fname);
                    $("#mname").val(res.mname);
                    $("#lname").val(res.lname);
                    $("#fullname").val(res.long_name);
                    $('#fullname_div').attr('hidden', false);
                    $("#birthdate").val(res.birthdate);
                    // var member_no = res.member_no.split('-');
                    // if(member_no.length == 2){
                    //   var member_no1 = member_no[1];
                    // }else{
                    //   var member_no1 = res.member_no;
                    // }
                    // alert(member_no[1]);
                    $(".member_noClass").val(res.member_no);
                    $("#email").val(res.email);
                      $("#contact_no").val(res.contact_no);
                    $("#uni_key").val(res.uni_key.substr(-6));
                    // alert(res.gender);
                    document.getElementById("member_status").options.selectedIndex = res.status;
                    document.getElementById("member_type").options.selectedIndex = res.type;
                    document.getElementById('gender').value = res.gender;
                }
                ,error: function (data) {
                     // alert('please delete the preloan connected to this preloan/preloan');
                     alert(data.responseText);  
                }
        });
    });
    // function clear_icon()
    //  {
    //   $('#id_icon').html('');
    //   $('#post_title_icon').html('');
    //  }

    //  function fetch_data(page, sort_type, sort_by, query)
    //  {
    //   $.ajax({
    //     type:"get",
    //     url: base_path+"/pagination/fetch_data?page="+page+"&sortby="+sort_by+"&sorttype="+sort_type+"&query="+query,
    //    success:function(data)
    //    {
    //     // alert(JSON.stringify(data));
    //     $('tbody').html('');
    //     $('tbody').html(data);
    //    }
    //    ,error: function (data) {
    //                  // alert('please delete the preloan connected to this preloan/preloan');
    //                  console.log(data.responseText);  
    //             }
    //   })
    //  }

    //  $(document).on('keyup', '#search', function(){
    //   var query = $('#search').val();
    //   var column_name = $('#hidden_column_name').val();
    //   var sort_type = $('#hidden_sort_type').val();
    //   var page = $('#hidden_page').val();
    //   fetch_data(page, sort_type, column_name, query);
    //  });
     // $(document).on('click', '.branchsearch', function(){
     //  var query1 = $(this).data('query');
     //  // alert(query);
     //    $.ajax({
     //      type:"get",
     //      url: base_path+"/pagination/fetch_data?&query1="+query1,
     //     success:function(data)
     //     {
     //      console.log(base_path+"/pagination/fetch_data?&query1="+query1)
     //      $('tbody').html('');
     //      $('tbody').html(data);
     //     }
     //     ,error: function (data) {
     //                   // alert('please delete the preloan connected to this preloan/preloan');
     //                   console.log(data.responseText);  
     //              }
     //    })
     //  // fetch_data(page, sort_type, column_name, query);
     // });

     // $(document).on('click', '.sorting', function(){
     //  var column_name = $(this).data('column_name');
     //  var order_type = $(this).data('sorting_type');
     //  var reverse_order = '';
     //  if(order_type == 'asc')
     //  {
     //   $(this).data('sorting_type', 'desc');
     //   reverse_order = 'desc';
     //   clear_icon();
     //   $('#'+column_name+'_icon').html('<span class="glyphicon glyphicon-triangle-bottom"></span>');
     //  }
     //  if(order_type == 'desc')
     //  {
     //   $(this).data('sorting_type', 'asc');
     //   reverse_order = 'asc';
     //   clear_icon
     //   $('#'+column_name+'_icon').html('<span class="glyphicon glyphicon-triangle-top"></span>');
     //  }
     //  $('#hidden_column_name').val(column_name);
     //  $('#hidden_sort_type').val(reverse_order);
     //  var page = $('#hidden_page').val();
     //  var query = $('#search').val();
     //  fetch_data(page, reverse_order, column_name, query);
     // });


     // $(document).on('click', '.pagination a', function(event){
     //  event.preventDefault();
     //  var page = $(this).attr('href').split('page=')[1];
     //  $('#hidden_page').val(page);
     //  var column_name = $('#hidden_column_name').val();
     //  var sort_type = $('#hidden_sort_type').val();

     //  var query = $('#search').val();

     //  $('li').removeClass('active');
     //        $(this).parent().addClass('active');
     //  fetch_data(page, sort_type, column_name, query);
     // });
    //   $('#modalCatcher').on('click', '.show', function () 
    // {
    //     $('#ajaxMemberShowModel').html("Member Details");
    //     $('#ajax-membershow-model').modal('show');
    //     var id = $(this).data('id');
    //     var base_path = $("#url").val();
    //     $.ajax({
    //             type:"post",
    //             url: base_path+"/edit-member",
    //             data: { id: id },
    //             dataType: 'json',
    //             success: function(res){
    //               // alert(res);
    //               $('#member_no2').html(res.member_no); 
    //               if(res.branch == 1){
    //                    var branch_id = 'Main Branch';
    //               }
    //                 else if(res.branch == 2){
    //                  var branch_id = 'Kasiglahan Branch';
    //                 }
    //                 else if(res.branch == 3){
    //                  var branch_id = 'San Mateo Branch';
    //                 }
    //                 else if(res.branch == 4){
    //                  var branch_id = 'Batasan Branch';
    //                 }
    //                 else if(res.branch == 5){
    //                  var branch_id = 'Cainta Branch';
    //                 }
    //                 else if(res.branch == 6){
    //                  var branch_id = 'San Rafael Branch';
    //                 }

    //                   if(res.status == 1){
    //                       var status = 'Active';
    //                   }if(res.status == 2){
    //                       var status = 'Delinquent';
    //                   }if(res.status == 3){
    //                       var status = 'Inactive';
    //                   }if(res.status == 4){
    //                       var status = 'Closed';
    //                   }if(res.status == 5){
    //                       var status = 'Pending for Closure';
    //                     }

    //                   if(res.type == 1){
    //                       var type = 'Associate Member';
    //                   }if(res.type == 2){
    //                       var type = 'Regular Member';
    //                   }
    //               $('#uni_key3').html(res.uni_key.substr(4, 10));
    //               $('#long_name1').html(res.long_name);
    //               $('#gender1').html(res.gender);
    //               $('#branch1').html(branch_id);
    //               $('#status1').html(status);
    //               $('#type1').html(type);
    //             }
    //             ,error: function (data) {
    //                  // alert('please delete the preloan connected to this preloan/preloan');
    //                  alert(data.responseText);  
    //             }
    //         });
    // });


});
</script>
@endsection

   

