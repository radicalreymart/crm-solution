<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Client;

class Occupation extends Model
{
    use HasFactory;
    protected $table = 'occupation';
    use HasFactory;
    protected $fillable = [
        'cd',
        'name',
        'crea_by',
        'created_at',
        'upd_by',
        'updated_at',
        'soft_deleted_at',
    ];
    
    public function client()
    {
        return $this->hasMany(Client::class);
    }
}
