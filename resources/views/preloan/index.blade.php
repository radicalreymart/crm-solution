@extends('layouts.app', [
    'class' => 'sidebar-mini ',
    'namePage' => 'Loan Index',
    'activePage' => 'preloan-management',
    'activeNav' => '',
])

@section('indexdeleteall')
  <script src="{{ asset('assets') }}/js/indexdeleteall.js"></script>
<style>
.dataTable-top,.dataTable-bottom {
    position:absolute;
    top:210; 
    padding-bottom: 10px; 
    }
    .dataTable-bottom{
    padding-top: 30px; 
    }
    .dataTable-top.fixed, .dataTable-bottom.fixed{
        position:fixed;
        top:0;
    } 
    .dataTable-container{
        padding-top: 50px;
    }
    .card-body{
      margin-bottom: 50px;
    }
     @media screen and (max-width: 412px) {
th.sticky-col {
  text-align: center;
  background-color: #ccffff;
  z-index: 998; 
}

td.sticky-col {
    word-wrap: break-word;
  background-color: #ccffff;
}
.meeting-col {
  width: unset;
  min-width: unset;
  max-width: unset;
  left: unset;
}
.first-col {
  width: unset;
  min-width: unset;
  max-width: unset;
  left: unset;
}

.second-col {
  width: unset;
  min-width: unset;
  max-width: unset;
  left: unset;
}
.second-colc {
  width: unset;
  min-width: unset;
  max-width: unset;
  left: unset;
}
.third-col {
  text-align: center;
  width: unset;
  min-width: unset;
  max-width: unset;
  left: unset;
}
.fourth-col {
  width: unset;
  min-width: unset;
  max-width: unset;
  left: unset;
}
.fifth-col {
  width: unset;
  min-width: unset;
  max-width: unset;
  left: unset;
}
.sixth-col {
  width: unset;
  min-width: unset;
  max-width: unset;
  left: unset;
}
}
  </style>
@endsection
@section('content')
    @include('auth.passwords.confirmmodal')
    @include('preloan.modal')
<div class="panel-header panel-header-sm">
  </div>
  <div class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header">
            @include('flash-message')
                <div class="float-left">
                    <h4 class="card-title">Loan Application</h4>
                </div>
                <div class="float-right">
                    <button class="btn btn-danger delete_all" data-url="{{ url('DeleteAllPreLoanApplication') }}">Delete All Selected</button>
                    <form  action="{{ route('exportLoan') }}"  >
                        {{ csrf_field() }}
                        Start Date
                    <input style="right: 5px;" class="form-control  start" max="{{ Carbon\Carbon::now()->format('Y-m-d'); }}" value="{{Carbon\Carbon::now()->format('Y-m-d'); }}" type="date" id="start" name="start">
                        End Date
                    <input style="right: 5px;" class="form-control  start" max="{{ Carbon\Carbon::now()->format('Y-m-d'); }}" value="{{Carbon\Carbon::now()->format('Y-m-d'); }}" type="date" id="end" name="end">
                        <button class="btn btn-primary "    >Download Excel </button>
                    </form>
                </div>
          </div>
          <div class="card-body">
            <div id="modalCatcher" class="table-responsive">
              <input type="hidden" value="{{url('/')}}" id="url" name="url">
              <table id="datatablesSimple" class="table">
                <thead  class=" text-primary">
                    <th  style=" z-index: 500; width: 30px;" class="sticky-col first-col"><input type="checkbox" id="master"></th>
                    <th  style="z-index: 500; width: 30px; color: #8ebf42;" class="sticky-col second-col"><i class="fa fa-paperclip" aria-hidden="true"></i></th>
                    <th  style=" z-index: 500; width: 30px;" class="sticky-col third-col" >Member</th>
                    <th style=" z-index: 500; width: 30px;" class="sticky-col fourth-col" >Contact Number</th>
                    <th style=" z-index: 500;" class="sticky-col fifth-col">Loan Status</th>
                    <th >Share Capital</th>
                    <th >Amount Applied/<br>Approved</th>
                    <th >Loan Product</th>
                    <th >Loan Purpose</th>
                    <th >Payment</th>
                    <th >Income Source</th>
                    <th>Date Created</th> 
                </thead>
                <tbody>
                    @if($preloan->count())
                    @foreach ($preloan as  $key => $data)    
                    <tr>

                    <td class="sticky-col first-col">
                        <input type="checkbox" class="sub_chk" data-id="{{$data->id}}"></td>
                    <td class="sticky-col second-col ">
                      <a href="javascript:void(0)" title="Loan Details" class="edit" data-id="{{ $data->id }}">
                        <i class="fas fa-eye  fa-lg"></i>
                      </a>
                      @if($data->file_path1 != null)
                      <a href="javascript:void(0)" title="Show Requirement" class="attachment" data-id="{{ $data->id }}">
                        <i  class="fas fa-file-image hvr-pulse fa-lg" aria-hidden="true"></i>
                      </a>
                      @endif
                    </td>
                    <td style="z-index: 1 ;" id="sticky-col{{$key}}" class="sticky-col third-col">{{$data->usermember->member->member_no}}<br>{{$data->usermember->member->long_name}}
                    </td>
                    <td id="sticky-col"  class="sticky-col fourth-col">+63{{$data->usermember->contact_no}}</td>
                    @if($data->loan_status == 1)
                    <td id="sticky-col"  class="sticky-col fifth-col">
                         <div class="row float-left ">
                            <div class="col-md-2">
                                <a href="javascript:void(0)" title="Decline" id="loan-decline" data-stat="0" data-id="{{ $data->id }}">
                                <input type="hidden" value="1" id="loanapprove" name="loanapprove">
                                    <i  class="far fa-times-circle hvr-pulse fa-lg" aria-hidden="true"></i>
                                </a>
                            </div>
                            <div class="col-md-2">
                                <a href="javascript:void(0)" title="Approve" id="loan-approve" data-stat="0" data-id="{{ $data->id }}">
                                <input type="hidden" value="1" id="loanapprove" name="loanapprove">
                                    <i  class="far fa-check-circle hvr-pulse fa-lg" aria-hidden="true"></i>
                                </a>
                            </div>
                         </div>
                    <br>Pending<br>(loan approval)</td>
                    
                    @elseif($data->loan_status == 3)
                    <td id="sticky-col"  class="sticky-col fifth-col">
                    Declined<br><a href="javascript:void(0)" title="Remarks" onclick="swal.fire('{{$data->approval_remarks}}')" >
                        Remarks
                      </a></td>
                    @elseif($data->loan_status == 2)
                    <td  id="sticky-col"  class="sticky-col fifth-col">
                    Approved <br> <a href="javascript:void(0)" title="Remarks" onclick="swal.fire('{{$data->approval_remarks}}')" >
                        Remarks
                      </a></td>
                    @else
                    <td id="sticky-col"  class="sticky-col fifth-col">
                         For  Assessment (Click if already assessed)
                         <a href="javascript:void(0)" title="Assessed" id="loan-decline" data-stat="0" data-id="{{ $data->id }}">
                                <input type="hidden" value="1" id="loanapprove" name="loanapprove">
                                    <i class="fa-solid fa-file-circle-check fa-lg"></i>
                                </a>
                    </td>
                    
                    @endif
                    <td >{{number_format($data->share_capital, 2)}}</td>
                    <td >{{ number_format($data->amount_needed, 2)}}/ {{ number_format($data->amount_approved, 2)}}</td>
                    <td>
                        {{$data->loan_type->name}}
                    </td>
                    <td>
                    @if(is_null($data->loan_purpose))
                            {{$data->other_purpose}}
                        @else  
                        @switch($data->loan_purpose)
                            @case(0)
                                Business-Related Expense
                                @break
                            @case(1)
                                Debt Consolidation
                                @break
                            @case(2)
                                Education  / Tuition fees
                                @break
                            @case(3)
                                Health / Medical fees
                                @break
                            @case(4)
                                Home Renovation / Improvements
                                @break
                            @case(5)
                                Life Events / Celebration
                                @break
                            @case(6)
                                Personal Consumption
                                @break
                            @case(7)
                                Purchase of Appliance / Gadget / Furniture> 
                                @break
                            @case(8)
                                Start-up Business 
                                @break
                            @case(9)
                                Vacation / Travel Expense
                                @break
                            @case(10)
                                Vehicle Repair / Purchase
                                @break
                            @default 
                        @endswitch
                        @endif
                    </td>
                    <td>
                    @switch($data->payment_mode)
                        @case(1)
                            Daily/  
                            @break
                        @case(2)
                            Weekly/ 
                            @break
                        @case(3)
                            Semi-monthly /
                            @break
                        @case(4)
                            Monthly/ 
                            @break
                        @case(5)
                            Lumpsum/ 
                            @break
                        @case(6)
                            Lumpsum (for 1 month only)/ 
                            @break
                        @default 
                    @endswitch
                     @foreach($payment_terms as $item)
                        @if($data->payment_terms === $item->id) 
                        {{ $item->name }} 
                        @endif 
                     @endforeach
                    </td>
                    <td >
                        @if(is_null($data->occupation_id))
                            {{$data->other_occupation}}
                        @else
                            {{$data->occupation->name}}
                        @endif
                    </td>
                    <td>{{ date_format($data->created_at, 'jS M Y') }}</td>
                    @endforeach
                    @endif
                </tbody>
              </table>

            </div>
          </div>
        </div>
      </div>
      
    </div>
  </div>
<script>
   // window.addEventListener("load", function() {
//     window.addEventListener('DOMContentLoaded', event => {
// //        
//     // Simple-DataTables
//     // https://github.com/fiduswriter/Simple-DataTables/wiki
//     const datatablesSimple = document.getElementById('datatablesSimple');
//     if (datatablesSimple) { 
//         new simpleDatatables.DataTable(datatablesSimple, {
//     fixedHeight: true,
//     // searchable: false,
//     perPage:5,
//         });
//     }
//   });
$(document).ready(function(e){
    $('#datatablesSimple').DataTable({

  });
     
   const swalWithBootstrapButtons = Swal.mixin({
                  customClass: {
                    confirmButton: 'btn btn-success',
                    cancelButton: 'btn btn-danger'
                  },
                  buttonsStyling: false
                }) 

    $.clearInput = function () {
        $('insert_form').find('input[type=text], input[type=radio], input[type=password], input[type=number], input[type=email], select, textarea').val('');
        $('insert_form1').find('input[type=text], input[type=radio], input[type=password], input[type=number], input[type=email], select, textarea').val('');
        $("input:radio").prop("checked", false);
    };
     $('.modal').on('hidden.bs.modal', function(){
             // window.location.reload();
             // $('#insert_form').removeData('bs.modal')
    $.clearInput();
    // $(this).find('form')[0].reset();
    $('#attachment-table').find("#tbody").empty();
    });
    $('.close-modal').click(function () {
        var modal_array = 
    [
      '#ajax-approve-model',
      '#ajax-decline-model'
    ];
    closeModal(modal_array);
    $('.textarea').val('');
       //  $.clearInput();
       // $('#ajax-preloan-model').modal('hide');
    });

    $.ajaxSetup({
        headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('#addNewpreloan').click(function () {
       $('#ajaxPreloanModel').trigger("reset");
       $('#ajaxPreloanModel').html("Add loan");
       $('#ajax-preloan-model').modal('show');
    });

    // $('#modalCatcher').on('click', '#btn-save-approval', function () 
    // {

    // $('#btn-save-decline').click(function (event) {
    //       var base_path = $("#url").val();

    //     try{
    //       var id                = $('#idapprove').val();
    //       var approval_remarks  = $("#approval_remarks").val();
    //     }
    //         catch(err) {
    //         swalWithBootstrapButtons.fire({
    //               title: 'Error!',
    //               text: err,
    //               icon: 'warning',
    //               confirmButtonText: 'Confirmed',
    //             }).then((result) => {
    //          // window.location.reload();
    //         $("#btn-save").html('Submit');
    //         $("#btn-save"). attr("disabled", false);
    //             })
    //         }
    //         $.ajax({
    //             type:"post",
    //             url: base_path+"/loanapproval",
    //             dataType: 'json',
    //             data: {
    //                 id:id,
    //               approval_remarks:approval_remarks,
    //             },
    //             success: function(res){
    //             swalWithBootstrapButtons.fire({
    //                   title: 'Success!',
    //                   icon: 'success',
    //                   confirmButtonText: 'Confirmed',
    //                 }).then((result) => {
    //              window.location.reload();})
    //               // alert(JSON.stringify(res));
    //             $("#btn-save").html('Submit');
    //             $("#btn-save"). attr("disabled", false);
    //             }
    //             ,error: function (data) {
    //                  // alert('please delete the preloan connected to this preloan/preloan');
    //                  alert(data.responseText);  
    //             }

    //         });

    // });

    // $('#btn-save-approval').click(function (event) {
    //       var base_path = $("#url").val();

    //     try{
    //       var id                = $('#idapprove').val();
    //       var amount_approved   = $("#amount_approved1").val();
    //       var approval_remarks  = $("#approval_remarks").val();
    //       var loan_status       = $("#loanapprove").val();
    //     }
    //         catch(err) {
    //         swalWithBootstrapButtons.fire({
    //               title: 'Error!',
    //               text: err,
    //               icon: 'warning',
    //               confirmButtonText: 'Confirmed',
    //             }).then((result) => {
    //          // window.location.reload();
    //         $("#btn-save").html('Submit');
    //         $("#btn-save"). attr("disabled", false);
    //             })
    //         }
    //         $.ajax({
    //             type:"post",
    //             url: base_path+"/loanapproval",
    //             dataType: 'json',
    //             data: {
    //                 id:id,
    //               amount_approved:amount_approved,
    //               approval_remarks:approval_remarks,
    //               loan_status:loan_status,
    //             },
    //             success: function(res){
    //             swalWithBootstrapButtons.fire({
    //                   title: 'Success!',
    //                   icon: 'success',
    //                   confirmButtonText: 'Confirmed',
    //                 }).then((result) => {
    //              window.location.reload();})
    //               // alert(JSON.stringify(res));
    //             $("#btn-save").html('Submit');
    //             $("#btn-save"). attr("disabled", false);
    //             }
    //             ,error: function (data) {
    //                  // alert('please delete the preloan connected to this preloan/preloan');
    //                  alert(data.responseText);  
    //             }

    //         });

    // });
    // $('#loan-approve').click(function (event) {
    $('#modalCatcher').on('click', '#loan-decline', function () 
    {
        var id =  $(this).data('id');
        var loanstatus =  $(this).data('stat');
          var base_path = $("#url").val();
        $("#iddecline").val(id);
        // alert(id+'-'+stat);
       $('#ajaxDeclineModel').trigger("reset");
       $('#ajaxDeclineModel').html("Loan Assesment Done <br> <span class='text-muted'> Is it for approval or needs more requirements?</span>");
       $('#ajax-decline-model').modal('show');
   });
    $('#modalCatcher').on('click', '#loan-approve', function () 
    {
        var id =  $(this).data('id');
        var loanstatus =  $(this).data('stat');
          var base_path = $("#url").val();
        // alert(id+'-'+stat);
       $('#ajaxApproveModel').trigger("reset");
       $('#ajaxApproveModel').html("Approve Loan");
       $('#ajax-approve-model').modal('show');
           $.ajax({
                type:"post",
                url: base_path+"/edit-preloan",
                dataType: 'json',
                data: {
                  id:id,
                },
                success: function(res){
                  // alert(JSON.stringify(res.approval_remarks));
                  $("#idapprove").val(res.id);
                  $("#payment_terms1").val(res.payment_terms);
                  $("#payment_mode1").val(res.payment_mode);
                  $("#amount_needed1").val(res.amount_needed);
                  $("#amount_approved1").val(res.amount_needed);
                  // $("#approval_remarks1").val(res.approval_remarks);
                 // window.location.reload();
                }
                ,error: function (data) {
                     // alert('please delete the preloan connected to this preloan/preloan');
                     alert(data.responseText);  
                }

             });
         // $.ajax({
         //    type:"post",
         //    url: base_path+"/loanstatus/"+id,
         //    dataType: 'json',
         //    data: {
         //      loanstatus:loanstatus,
         //    },
         //    success: function(res){
         //    swalWithBootstrapButtons.fire({
         //          title: 'Success!',
         //          icon: 'success',
         //          confirmButtonText: 'Confirmed',
         //        }).then((result) => {
         //     window.location.reload();})
         //      // alert(JSON.stringify(res));
         //    $("#btn-save").html('Submit');
         //    $("#btn-save"). attr("disabled", false);
         //    }
         //    ,error: function (data) {
         //         // alert('please delete the preloan connected to this preloan/preloan');
         //         alert(data.responseText);  
         //    }

         // });
    });
    $('#btn-save').click(function (event) {
        try{
          var base_path = $("#url").val();
         // alert(JSON.stringify(jQuery('#insert_form').serializeArray()));
          var id = $("#id").val();
          var fullname = $("#fullname").val();
          var age_group = $("#age_group").val();
          var gender = $("#gender").val();
          var contact_no = $("#contact_no").val();
          var region_id = $("#region_id").val();
          var province_id = $("#province_id").val();
          var city_id = $("#city_id").val();
          var barangay_id = $("#barangay_id").val();
          var street = $("#street").val();
          var fb_link = $("#fb_link").val();
          var amount_needed = $("#amount_needed").val();
          var loan_product = $("#loan_product").val();
          var loan_purpose = $("#loan_purpose").val();
          var payment_terms = $("#payment_terms").val();
          var payment_mode = $("#payment_mode").val();
          var occupation_id = $("#occupation_id").val();
          var montly_income = $("#montly_income").val();
          var montly_expense = $("#montly_expense").val();
          var marketing_channel_id = $("#marketing_channel_id").val();
          var member_status = $("#member_status").val();
          var email = $("#email").val();
          $("#btn-save").html('Please Wait...');
          $("#btn-save"). attr("disabled", true);           
              }
            catch(err) {
            swalWithBootstrapButtons.fire({
                  title: 'Error!',
                  text: err,
                  icon: 'warning',
                  confirmButtonText: 'Confirmed',
                }).then((result) => {
             // window.location.reload();
            $("#btn-save").html('Submit');
            $("#btn-save"). attr("disabled", false);
                })
            }
          $.ajax({
            type:"POST",
            url: base_path+"/preloan/store",
            // headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            data: 
            // JSON.stringify(jQuery('#insert_form').serializeArray()),
            {
              id:id,
              fullname:fullname,
              age_group:age_group,
              gender:gender,
              contact_no:contact_no,
              region_id:region_id,
              province_id:province_id,
              city_id:city_id,
              barangay_id:barangay_id,
              street:street,
              fb_link:fb_link,
              amount_needed:amount_needed,
              loan_product:loan_product,
              loan_purpose:loan_purpose,
              payment_terms:payment_terms,
              payment_mode:payment_mode,
              occupation_id:occupation_id,
              montly_income:montly_income,
              montly_expense:montly_expense,
              marketing_channel_id:marketing_channel_id,
              member_status:member_status,
              email:email,
            },
            dataType: 'json',
            success: function(res){
              // alert('success');
              swalWithBootstrapButtons.fire({
                  title: 'Success!',
                  icon: 'success',
                  confirmButtonText: 'Confirmed',
                }).then((result) => {
             window.location.reload();})
              // alert(JSON.stringify(res));
            $("#btn-save").html('Submit');
            $("#btn-save"). attr("disabled", false);
            }
            ,error: function (data) {
                 // alert('please delete the preloan connected to this preloan/preloan');
                 alert(data.responseText);  
            }
        });

    });

        $('#modalCatcher').on('click', '#dropdown-btn', function () 
    {
        var id = $(this).data('id');
        var base_path = $("#url").val();
        $.ajax({
                type:"get",
                url: base_path+"/preloancount",
                data: { id: id },
                dataType: 'json',
                success: function(res){
                $.each( res, function( key, value ) {
                var elems = document.querySelectorAll("TH");
                    // elems.style.position = "static";
                var index = 0, length = elems.length;
                for ( ; index < length; index++) {
                    elems[index].style.position = "static";
                }
                document.getElementById("sticky-col"+key).classList.add('nosticky');
                // setTimeout(function(){
                //    window.location.reload(1);
                // }, 5000);
                });
            }
        });

        // var src = document.createAttribute("src");
    });
        $('#modalCatcher').on('click', '.attachment', function () 
    {
                      $('#ajaxAttachmentModel').html("Requirements");
                      $('#ajax-attachment-model').modal('show');
        var id = $(this).data('id');
        var base_path = $("#url").val();
        // alert(id+base_path);
        $('#attachmentID').val(id);
        $.ajax({
                type:"get",
                url: base_path+"/show-attachment",
                data: { id: id },
                dataType: 'json',
                success: function(res){
                    // alert(res);
                    var frmcontroler = JSON.stringify(res).split(',');
                    var expAttacment = res[0].split('|');
                    var basesigniture = res[1].signiture;
                    var splitsigniture = res[1].signiture.split('-');
                    var imgsrc1 = base_path+'/assets/img/uploads/loans/'+splitsigniture[0]+'/'+basesigniture;
                    // alert(JSON.stringify(splitsigniture[1]));
                    document.getElementById("name"+8).innerHTML = splitsigniture[1];
                    var img1 = document.getElementById("img"+8);
                    var aid1 = document.getElementById("href"+8);
                    var src1 = document.createAttribute("src");
                    var href1 = document.createAttribute("href");
                    src1.value = imgsrc1;
                    img1.setAttributeNode(src1);  
                    href1.value = imgsrc1;
                    aid1.setAttributeNode(href1);
                    //   // $('#id').val(res.id);
                    $.each( expAttacment, function( key, value ) {
                        var prefix = value.split('-');
                        // var array
                        var imgsrc = base_path+'/assets/img/uploads/loans/'+prefix[0]+'/'+value;
                        // alert(prefix[1]);
                        document.getElementById("name"+key).innerHTML = prefix[1];
                        var img = document.getElementById("img"+key);
                        var aid = document.getElementById("href"+key);
                        var src = document.createAttribute("src");
                        var href = document.createAttribute("href");
                        src.value = imgsrc;
                        img.setAttributeNode(src);  
                        href.value = imgsrc;
                        aid.setAttributeNode(href);
                      // var row = document.getElementById("tablerow");
                      // var cell = row.insertCell(0);
                      // cell.innerHTML = '<img src='+imgsrc+' height=100 width=300>';
                    });

                }
                ,error: function (data) {
                     // alert('please delete the preloan connected to this preloan/preloan');
                     alert(data.responseText);  
                }
            });
    });

    $('.btn-save').click(function (event) {
        $('.btn-save').html('Please Wait');
        $('.btn-save').attr('disabled', true);
        $('.close-btn').html('Please Wait');
        $('.close-btn').attr('disabled', true);
        $(this).closest('form').submit(); 
    });
    $('#modalCatcher').on('click', '.edit', function () 
    {
        var id = $(this).data('id');
        var base_path = $("#url").val();
                $.ajax({
                        type:"POST",
                        url: base_path+"/edit-preloan",
                        data: { id: id },
                        dataType: 'json',
                        success: function(res){
                            // alert(JSON.stringify(res.age_group));
                        if(res.gender == "F"){
                            console.log('female');
                            var gender = 1;
                        }else{
                            console.log('male');
                            var gender = 0;
                        }
                          $('#ajaxPreloanModel').html("Loan Details");
                          $('#created_at').html("Date Created: "+res.created_at);
                          $('#ajax-preloan-model').modal('show');
                          if(res.co_maker != null){
                              $("#co_maker").val(res.co_maker);
                              $("#comakerdiv").attr('hidden', false);
                          }else{
                              $("#comakerdiv").attr('hidden', true);
                          }
                          $('#id').val(res.id);
                          $("#fullname").val(res.fullname);
                          $("#birthdate").val(res.birthdate);
                          $("#amount_needed").val(res.amount_needed);
                          $("#amount_approved").val(res.amount_approved);
                          $("#share_capital").val(res.share_capital);
                          $("#loan_product").val(res.loan_product);
                          $("#loan_product").attr('title', res.loan_type);
                          switch(res.loan_product){
                            case 4:
                              $('#savings_salary_div').attr('hidden', false);
                              $('#savings_salary_label').html('Time Deposit');
                              $('#savings_salary_amount').val(res.savings_salary_amount);
                              break;
                            case 5:
                              $('#savings_salary_div').attr('hidden', false);
                              $('#savings_salary_label').html('Wealth Building Program');
                              $('#savings_salary_amount').val(res.savings_salary_amount);
                              break;
                            case 6:
                              $('#savings_salary_div').attr('hidden', false);
                              $('#savings_salary_label').html('Lockin Savings');
                              $('#savings_salary_amount').val(res.savings_salary_amount);
                              break;
                            case 18:
                              $('#savings_salary_div').attr('hidden', false);
                              $('#savings_salary_label').html('Salary');
                              $('#savings_salary_amount').val(res.savings_salary_amount);
                              break;
                            case 19:
                              $('#savings_salary_div').attr('hidden', false);
                              $('#savings_salary_label').html('Salary');
                              $('#savings_salary_amount').val(res.savings_salary_amount);
                              break;
                            case 20:
                              $('#savings_salary_div').attr('hidden', false);
                              $('#savings_salary_label').html('Mid-Year Bonus');
                              $('#savings_salary_amount').val(res.savings_salary_amount);
                              break;
                            case 21:
                              $('#savings_salary_div').attr('hidden', false);
                              $('#savings_salary_label').html('Year-End Bonus');
                              $('#savings_salary_amount').val(res.savings_salary_amount);
                              break;
                            case 22:
                              $('#savings_salary_div').attr('hidden', false);
                              $('#savings_salary_label').html('Performance Evaluation Incentives');
                              $('#savings_salary_amount').val(res.savings_salary_amount);
                              break;
                            case 23:
                              $('#savings_salary_div').attr('hidden', false);
                              $('#savings_salary_label').html('Performance Based Bonus');
                              $('#savings_salary_amount').val(res.savings_salary_amount);
                              break;
                            case 24:
                              $('#savings_salary_div').attr('hidden', false);
                              $('#savings_salary_label').html('Salary');
                              $('#savings_salary_amount').val(res.savings_salary_amount);
                              break;
                            case 25:
                              $('#savings_salary_div').attr('hidden', false);
                              $('#savings_salary_label').html('Mid-Year Bonus');
                              $('#savings_salary_amount').val(res.savings_salary_amount);
                              break;
                            case 26:
                              $('#savings_salary_div').attr('hidden', false);
                              $('#savings_salary_label').html('Year-End Bonus');
                              $('#savings_salary_amount').val(res.savings_salary_amount);
                              break;
                            default:
                              var result = null;
                              break;  
                          }

                          if(res.loan_purpose != null){
                              $("#loan_purpose").val(res.loan_purpose);
                              $("#loan_purpose").attr('hidden', false);
                              $("#other_purpose").attr('hidden', true);
                          }else{
                            // alert('hidden purpose');
                              $("#loan_purpose").attr('hidden', true);
                              $("#other_purpose").val(res.other_purpose);
                              $("#other_purpose").attr('hidden', false);
                          }
                          $("#payment_terms").val(res.payment_terms);
                          $("#payment_mode").val(res.payment_mode);
                          $("#occupation_id").val(res.occupation_id);
                          $("#montly_income").val(res.montly_income);
                          $("#montly_expense").val(res.montly_expense);
                          $("#email").val(res.email);
                          $("#gender").val(gender);
                          $("#fb_link").val(res.fb_link);
                          $("#montly_income").attr('disabled', true);
                          $("#montly_expense").attr('disabled', true);
                          $("#email").attr('disabled', true);
                          $("#gender").attr('disabled', true);
                          $("#fb_link").attr('disabled', true);
                          $("#contact_no").val(res.contact_no);
                          $("#region_id").val(res.region_id);
                          if(res.occupation_id != null){
                              $("#occupation_id").val(res.occupation_id);
                              $("#occupation_id").attr('hidden', false);
                              $("#other_occupation").attr('hidden', true);
                          }else{
                              $("#occupation_id").attr('hidden', true);
                              $("#other_occupation").val(res.other_occupation);
                              $("#other_occupation").attr('hidden', false);
                          }
                          $("#region_id").attr('disabled', true);
                          var province_id1 = res.province_id;
                          $.ajax({
                                        type:"GET",
                                        // base_path+'getProvince/'+region_code,
                                        url:base_path+'/getProvince/'+res.region_id,
                                        success:function(res){               
                                        if(res){
                                        $("#province_id").empty();
                                        $("#province_id").attr('readonly', false);
                                        $("#province_id").append('<option value="">Select Province</option>');                            
                                        $("#city_id").append('<option value="">Select City</option>');
                                        $("#barangay_id").empty();
                                        $("#barangay_id").append('<option value="">Select Barangay</option>');
                                        $.each(res,function(key,value){
                                            $("#province_id").append('<option class="form-control" value="'+ value['province_id'] +'">'+ value['name'] +'</option>');
                                        });
                                          $("#province_id").val(province_id1);
                                          $("#province_id").attr('disabled', true);
                                        }else{
                                           $("#province_id").empty();
                                        }
                                        }
                                        });
                          var city_id1 = res.city_id;
                          $.ajax({
                                       type:"GET",
                                       url:base_path+'/getCities/'+province_id1,
                                       success:function(res){  
                                        if(res){
                                            $("#city_id").attr('readonly', false);
                                            $("#city_id").empty();
                                            $("#city_id").append('<option value="">Select City</option>');
                                            $("#barangay_id").empty();
                                            $("#barangay_id").append('<option value="">Select Barangay</option>');
                                            $.each(res,function(key,value){
                                                $("#city_id").append('<option class="form-control" value="'+ value['city_id'] +'">'+ value['name'] +'</option>');
                                            });
                                            $("#city_id").val(city_id1);
                                            $("#city_id").attr('disabled', true);
                                        }else{
                                           $("#city_id").empty();
                                        }
                                       }
                                        });
                          var barangay_id1 = res.barangay_id;
                          $.ajax({
                                       type:"GET",
                                       url:base_path+'/getBarangays/'+city_id1,
                                       success:function(res){  
                                        if(res){
                                           $("#barangay_id").attr('readonly', false);
                                           $("#barangay_id").empty();
                                            $("#barangay_id").append('<option value="">Select Barangay</option>');
                                            $.each((res),function(key,value){
                                                $("#barangay_id").append('<option class="form-control" value="'+ value['code'] +'">'+ value['name'] +'</option>');
                                            });
                                          $("#barangay_id").val(barangay_id1);
                                          $("#barangay_id").attr('disabled', true);
                                        }else{
                                           $("#barangay_id").empty();
                                        }
                                       }
                                    });
                          $("#street").val(res.street);
                          $("#street").attr('disabled', true);
                          // $("#marketing_channel_id").val(res.marketing_channel_id);
                          $("#member_status").val(res.member_status);
                       }
                        ,error: function (data) {
                             var err = JSON.parse(data.responseText);
                             if(err['message'] == 'Password confirmation required.'){   
                             alert(err['message']);
                             window.location.href = "password/confirm";
                             }else{
                             // alert('please delete the preloan connected to this preloan/preloan');
                             alert(data.responseText);
                             }
                         }
                    });
    });

     
 });
</script>
@endsection


