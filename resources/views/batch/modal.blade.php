
<!-- Modal -->
<div class="modal fade" id="ajax-batch-model" aria-hidden="true" data-backdrop="static" data-keyboard="false">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title" id="ajaxBatchModel"></h4>
            <button type="button" class="btn btn-default close-modal"  >Close</button>
          </div>
          <div class="modal-body">
            <form action="javascript:void(0)" class="form-horizontal" method="POST">
              <input type="hidden" name="id" id="id">
              <div class="form-group row">
                    <label for="name" class="col-md-6">{{ __('Batch Code') }}</label>

                    <div class="col-md-6">
                        <input id="batch_cd" type="text" id="batch_cd" class="form-control @error('batch_cd') is-invalid @enderror" name="batch_cd" value="{{ old('batch_cd') }}" required autocomplete="batch_cd" autofocus />

                        @error('batch_cd')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>

             <div class="form-group row">
                <label for="name" class="col-md-6">Schedule Date and Time</label>
                 <div class="col-md-6">
                    <input type="datetime-local" id="sched_date" class="form-control"  name="sched_date" min="{{$mindate}}" mindate='{{$mindate}}' >
                    <span class="text-danger">
                        <strong>{{$errors->first('sched_date') ?? null}}
                        </strong>
                    </span>
                </div>
            </div> 

            <div class="form-group row">
                    <label for="employee_role" class="col-md-6 ">{{ __('Preferred Application') }}</label>

                    <div class="col-md-6">
                        <select name="online_vid_tool" id="online_vid_tool" class="form-control">
                                        <option value="">Select...</option>
                                        <option value = 0>Google Meet</option>
                                        <option value = 1 >Zoom </option>
                                        <option value = 2>Walk-in</option>
                        </select>
                        @error('online_vid_tool')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>

                        <div id="meeting_cd_div" hidden class="form-group row">
                            <label for="meeting_cd" id="meeting_cd_lb" class="col-md-6 "></label>

                            <div class="col-md-6">
                                <input id="meeting_cd_text"  type="text" class="form-control @error('meeting_cd') is-invalid @enderror" name="meeting_cd" value="{{ old('meeting_cd') }}" placeholder="ovy-gbku-ghz" required autocomplete="meeting_cd" autofocus disabled hidden />
                                <textarea  id="meeting_cd_textarea" style="height: 200% !important; width: 100% !important; background-color: rgb(0 19 98);   color: white;" class=" @error('meeting_cd') is-invalid @enderror" name="meeting_cd" value="{{ old('meeting_cd') }}" placeholder="https://us02web.zoom.us/j/7864183054?pwd=QmNwRzk3UWNocFM4RmZSekJEb3VyUT09" required autocomplete="meeting_cd" disabled hidden></textarea>

                                @error('meeting_cd')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        

                        <div id="meeting_pw_div" hidden class="form-group row">
                            <label for="meeting_pw" class="col-md-6 ">{{ __('Meeting Password') }}</label>

                            <div class="col-md-6">
                                <input id="meeting_pw"  type="meeting_pw" class="form-control @error('meeting_pw') is-invalid @enderror" name="meeting_pw" required autocomplete="new-meeting_pw">

                                @error('meeting_pw')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row" id="remark_id" >
                            <label for="name" class="col-md-6 ">{{ __('Reschedule Remarks') }}</label>

                            <div class="col-md-6">
                                
                                <textarea id="resched_remarks" style="border-color: black;" class="form-control" name="resched_remarks"  required autocomplete="resched_remarks" autofocus></textarea>

                                @error('resched_remarks')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

              <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-primary btn-save" id="btn-save">Save
                </button>
              </div>
            </form>
          </div>
          <div class="modal-footer">
            
          </div>
        </div>
      </div>
    </div>

    <!-- Modal -->
<div class="modal fade" id="ajax-participant-model" aria-hidden="true" data-backdrop="static" data-keyboard="false">
      <div class="modal-dialog">
        <div class="modal-content" style="width: 602px;">
          <div class="modal-header">
            <h4 class="modal-title" id="ajaxParticipantModel"></h4>
            <button type="button"  class="btn btn-default" onclick="$('#tbody').empty()" data-dismiss='modal' id="close-modal" >Close</button>
          </div>
          <div class="modal-body">
            <table class="table" id="participant-table" >
                        <thead class=" text-primary">
                            <th>
                                Fullname
                            </th>
                            <th>
                                Contact Number
                            </th>
                            <th>
                                Email
                            </th>
                            <th>
                                Attendance
                            </th>
                        </thead>
                        <tbody id="tbody">
                            
                        </tbody>
                    </table>
          </div>
          <div class="modal-footer">
            
          </div>
        </div>
      </div>
    </div>