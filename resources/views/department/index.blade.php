@extends('layouts.app', [
    'class' => 'sidebar-mini ',
    'namePage' => 'Department Index',
    'activePage' => 'department-management',
    'activeNav' => 'settings',
])

@section('indexdeleteall')
  <script src="{{ asset('assets') }}/js/indexdeleteall.js"></script>
@endsection
@section('content')

    @include('auth.passwords.confirmmodal')
<div class="panel-header panel-header-sm">
  </div>
  <div class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header">
            @include('flash-message')
                <div class="float-left">
                    <h4 class="card-title">Department</h4>
                </div>
                <div class="float-right">
                    <a class="btn btn-success" href="{{ route('department.create') }}">
                        <li class="fas fa-plus-circle fa-lg" aria-hidden="true"></li>
                    </a>
                    <button class="btn btn-danger delete_all" data-url="{{ url('DeleteAllDepartment') }}">Delete All Selected</button>
                </div>
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table">
                <thead class=" text-primary">
                    <th><input type="checkbox" id="master"></th>
                    <th >Action</th>
                    <th>Code</th>
                    <th>Name</th>
                    <th>Created By</th>
                    <th>Date Created</th>
                    <th>Updated By</th>
                    <th>Date Updated</th>
                </thead>
                <tbody>
                    @if($department->count())
                    @foreach ($department as $data)
                    <tr>
                    <td ><input type="checkbox" class="sub_chk" data-id="{{$data->id}}"></td>
                    <td>
                    <a href="{{ route('department.edit', $data->id) }}">
                    <i class="fas fa-edit  fa-lg"></i>
                    </a>


                    @csrf
                    @method('DELETE')
                    </td>
                    <td>{{ $data->cd }}</td>
                    <td>{{ $data->name }}</td>
                    <td>{{ $data->crea_by }}</td>
                    <td>{{ date_format($data->created_at, 'jS M Y') }}</td>

                    @if(isset($data->upd_by))
                    <td>{{ $data->upd_by }}</td>
                    @else
                    <td></td>
                    @endif
                    @if(isset($data->updated_at))
                    <td>{{ date_format($data->updated_at, 'jS M Y') }}</td>
                    @else
                    <td></td>
                    @endif
                    

                    </tr>
                    @endforeach
                    @endif
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      
    </div>
  </div>
    
@endsection

   

