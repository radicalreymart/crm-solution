<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Yajra\Address\HasAddress;
use App\Models\MarketingChannel;
use App\Models\Occupation;
use App\Models\UserMember;
use App\Models\LoanType;
use App\Models\PaymentTerms;
use App\Models\Branch;

class PreLoanApplication extends Model
{
    use HasFactory;
    use HasAddress;

    protected $table = 'pre_loan_applications';
    protected $fillable = [
            'usermember_id'
            ,'share_capital'
            ,'branch_id'
            ,'other_occupation'
            ,'co_maker'
            ,'file_path1'
            ,'signiture'
            ,'loan_status'
            ,'ref_number'
            ,'amount_needed'
            ,'savings_salary_amount'
            ,'loanable_amount'
            ,'amount_approved'
            ,'approval_remarks'
            ,'loan_product'
            ,'loan_purpose'
            ,'other_purpose'
            ,'payment_terms'
            ,'payment_mode'
            ,'occupation_id'
            ,'montly_income'
            ,'montly_expense'
            ,'data_consent_fl'
            ,'marketing_channel_id'
            ,'crea_by'
            ,'upd_by'

        ];
    public function marketing()
    {
        return $this->belongsTo('App\Models\MarketingChannel','marketing_channel_id','id');
    }
    public function occupation()
    {
        return $this->belongsTo('App\Models\Occupation','occupation_id','id');
    }
    public function usermember()
    {
        return $this->belongsTo('App\Models\UserMember','usermember_id','id');
    }

    public function loan_type()
    {
        return $this->belongsTo('App\Models\LoanType','loan_product','id');
    }
    public function terms()
    {
        return $this->belongsTo('App\Models\PaymentTerms','payment_terms','id');
    }
    public function branch()
    {
        return $this->belongsTo('App\Models\Branch','branch_id','id');
    }
    public function region()
    {
        return $this->belongsTo('App\Models\Region','region_id','client.region_id');
    }
    public function province()
    {
        return $this->belongsTo('App\Models\Province','province_id','client.province_id');
    }
    public function city()
    {
        return $this->belongsTo('App\Models\City','city_id','client.city_id');
    }
    public function barangay()
    {
        return $this->belongsTo('App\Models\Barangay','barangay_id','client.code');
    }

}
