  <div class="content">
<div class="panel-header panel-header-sm">
</div>
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header">
            @include('flash-message')
                <div class="float-left">
                    <h4 class="card-title">Batch History</h4>
                </div>
                <div class="float-right">
                    <a class="btn btn-primary" href="{{ url('/batch') }}">
                        Batch Index
                    </a>
                </div>
          </div>
            <div class="card-body">
            <input type="text"  class="form-control" placeholder="Search Batch Code or Schedule Date" wire:model="searchTerm" />
            <div class="table-responsive">
              <table class="table">
                <thead class=" text-primary">
                    <th >Batch Code</th>
                    <th>Schedule Date</th>
                    <th>Schedule Time</th>
                    <th>Pref. Application</th>
                    <th class="meeting-col">Meeting Link</th>
                    <th class="shorten_text">Reschedule Remarks</th>
                    <th>Meeting Password</th>
                    <th>Participant Number</th>
                    <th>Batch Status</th>
                    <th>Date Created</th>
                    <th>Created By</th>
                </thead>
                <tbody>
                    @if($batch->count())
                    @foreach ($batch as $data)
                    <tr>
                    <td>{{ $data->batch_cd }}</td>
                    
                    <td>{{ \Carbon\Carbon::parse($data->sched_date)->format('m-d-Y')}}</td>
                    
                    <td>{{ \Carbon\Carbon::parse($data->sched_date)->format('h:i a')}}</td>
                    
                    @if($data->online_vid_tool == 0)
                    <td>Google Meet</td>
                    @endif
                    
                    <td class="meeting-col"><a href="https://meet.google.com/{{$data->meeting_cd}}">https://meet.google.com/{{$data->meeting_cd}}</a></td>

                    <td>{{$data->resched_remarks}}</td>
                    
                    @if(is_null($data->meeting_pw))
                    <td></td>
                    @else
                    <td>{{$data->meeting_pw}}</td>
                    @endif
                    <td>{{$data->participant}}</td>

                    @if(\Carbon\Carbon::parse($data->sched_date)->format('m/d/Y') == $now)
                    <td>Today</td>
                    @elseif(\Carbon\Carbon::parse($data->sched_date)->format('m/d/Y') <= $now)
                    <td>Expired</td>
                    @elseif($data->resched_remarks !== '.')
                    <td>Rescheduled</td>
                    @elseif(!is_null($data->soft_deleted_at))
                    <td>Soft Deleted</td>
                    @elseif($data->participant < 5)
                    <td>Insufficient Participant</td>
                    @else
                    <td>Pending</td>
                    @endif
                    
                    <td>{{ date_format($data->created_at, 'm-d-Y') }}</td>
                    
                    <td>{{ $data->crea_by }}</td>
                    </tr>
                    @endforeach
                    @endif
                </tbody>
              </table>
            </div>
              {{ $batch->links() }}
          </div>
        </div>
      </div>
      
    </div>
  </div>