<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PaymentTerms extends Model
{ 
    protected $table = 'payment_terms';
    use HasFactory;
    protected $fillable = [
        'cd',
        'name',
        'months',
        'crea_by',
        'created_at',
        'upd_by',
        'updated_at',
        'soft_deleted_at',
    ];}
