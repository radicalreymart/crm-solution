<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Http\Requests\UserRequest;
use Storage;
use File;
use Excel;
use App\Exports\FeedbackExport;
use App\Models\Branch;
use App\Models\Feedback;
use App\Models\UserMember;
use App\Models\Member;
use Alert;

class FeedbackController extends Controller
{
    //
    public function index()
    {
        $department = auth()->user()->department_id;
        $branch = auth()->user()->branch_id;
        $isAdmin = auth()->user()->role_id;
        if($department == 1 ||$department == 4||$department == 5|| $isAdmin == 0){

            $feedback = Feedback::with('branch', 'branchLoc');

            if($branch == 9){
                $feedback->orderBy('last_feedback', 'desc');
            }else{
                $feedback->where('branch_loc', $branch)
                ->orderBy('last_feedback', 'desc');
            }

            $feedback = $feedback->get();
            // dd($feedback);
            return view('feedback.index', compact('feedback'));
        }else{
            Alert::error("You do not have the permission to access this page.");
            return redirect()->route('home');
        }
    }
     public function create()
    {   

        // $user_id1 = auth()->user()->id;
        // $user_info1 = UserMember::with('member')->where('user_id', $user_id1)->first();
        // $latest_feedback = Feedback::where('member_no', $user_info1->member_no)->orderBy('last_feedback', 'desc')->first('id');

        // dd($latest_feedback);
        $branches  = Branch::whereNull('soft_deleted_at')
        ->Where('cd', '!=', 'TRD')
        ->Where('cd', '!=', 'FR')
        ->Where('cd', '!=', 'ADMIN')
        ->get();
        $branch_loc  = Branch::whereNull('soft_deleted_at')
        ->Where('cd', '!=', 'ADMIN')
        ->get();
        if(!Auth::guest()){
        $user_id = auth()->user()->id;
        $user_info = UserMember::with('member')->where('user_id', $user_id)
        // ->pluck('member_no', 'crea_by');
        ->first();
        $gender = $user_info->member->gender;
        $type = $user_info->member->member_type;
        $fname = $user_info->member->fname;
        $fullname = $user_info->member->long_name;
        $mname = $user_info->member->mname;
        $lname = $user_info->member->lname;
        $branch = $user_info->member->branch_id;
        // dd($user_info->member->branch_id);
        }else{
        $user_info = null;
        $fname = null;
        $fullname = null;
        $mname = null;
        $lname = null;
        $branch = null;
        $type = null;
        $gender = null;
        }

        return view('enduser.feedback', compact('branches', 'user_info', 'fullname', 'fname', 'mname', 'lname', 'gender', 'branch', 'type', 'branch_loc'));
    }
    public function store(Request $request)
    {
        // dd($request->all());
        $validator = Validator::make($request->all(), [
          "gender" => 'required'
          // ,"branch_id" => 'required'
          // ,"member_type" => 'required'
          ,"branch_loc" => 'required'
          ,"exp_date" => 'required'
          ,"rate1" => 'required'
          ,"rate2" => 'required'
          ,"rate3" => 'required'
          ,"rate4" => 'required'
          ,"rate5" => 'required'
          ,"rate6" => 'required'
          ,"comment" => 'required'
        ], $message = ['required' => 'Please fill all of fields with *']
         );
        if ($validator->fails())
        {
            Alert::error('Validation Error: '.$validator->errors())->autoClose(null);
            return redirect()->back()->with('error', 'Validation Error: '.$validator->errors()->first());   
        }
        // dd($request->all());

        if(!Auth::guest()){
        $user_id = auth()->user()->id;
        $member = UserMember::where('user_id', $user_id)->first('member_no');
        $member_no = $member->member_no;
        $crea_by = 'Registered :'.$member_no.':'.$fullname;
        }else{
            if($request->nonregmem == 1){
                $member_no = $request->nonregmem_num;
                $fullname = $request->fullname;
            }else{
                $member_no = null;
                $fullname = $request->fname.' '.$request->mname.' '.$request->lname;
            }
        $crea_by = 'Non-Registered : '.$fullname;
        }
        if($request->branch_loc == 7){
            // $transaction_nat = [];
            $trading = explode(',',$request->trading);
            $i = 0;
            foreach($trading as $data){
                $i++;
                $flipped[] = $data;
            }
            $transaction_nat = array_flip($flipped);
        }elseif($request->branch_loc == 8){
             $fiestia = explode(',',$request->fiestia);
            $i = 0;
            foreach($fiestia as $data){
                $i++;
                $flipped[] = $data;
            }
            $transaction_nat = array_flip($flipped);
        }else{
            $transaction_nat = array(
              "rsdeposit" => $request->rsdeposit
              ,"psdeposit" => $request->psdeposit
              ,"time_deposit" => $request->time_deposit
              ,"share_capital" => $request->share_capital
              ,"rswithdraw" => $request->rswithdraw
              ,"pswithdraw" => $request->pswithdraw
              ,"member_app" => $request->member_app
              ,"member_termination" => $request->member_termination
              ,"member_benefits" => $request->membership_inquiry
              ,"membership_inquiry" => $request->member_benefits
              ,"PMES" => $request->PMES
              ,"loan_payment" => $request->loan_payment
              ,"loan_application" => $request->loan_application
              ,"loan_release" => $request->loan_release
              ,"bayad_center" => $request->bayad_center
            );
        }

        $transaction_exp = array(
           "rate1" => $request->rate1
          ,"rate2" => $request->rate2
          ,"rate3" => $request->rate3
          ,"rate4" => $request->rate4
          ,"rate5" => $request->rate5
          ,"rate6" => $request->rate6
        );

        if($request->nonregmem == 1){
            if($request->gender1 == 'M'){
            $gender = 1;
            }else{
            $gender = 2;
            }
            if($request->branch_id === null){
                $branch = Branch::where('cd', $request->branch_id1)->first('id'); 
                $branch_id = $branch->id; 
            }else{
                $branch_id = $request->branch_id; 
            }
            // dd($branch_id);
            // $member_type = $request->gender1;
            // dd($branch->id);
        }else{
            $gender = $request->gender;
            $branch_id = $request->branch_id;
            $member_type = $request->gender;
        }
            // dd('one');
        $randInt = rand(100,999);
        $folderPath = str_replace('\\', '/', public_path('assets/img/uploads/feedback/'.$randInt.Carbon::now()->format('Ymd').'/'));

        // $folderPath = str_replace('\\', '/', '/home3/mhrmpcoo/crm.mhrmpcoop.com.ph/assets/img/uploads/feedback/'.$randInt.Carbon::now()->format('Ymd').'/');
        
        // dd($folderPath);
        // File::makeDirectory($folderPath);
        mkdir($folderPath, 0777, true);
        $image_parts = explode(";base64,", $request->hidden_sig);
        $image_type_aux = explode("image/", $image_parts[0]);
        $image_type = $image_type_aux[1];
        $image_base64 = base64_decode($image_parts[1]);
        $file = $folderPath . $randInt.Carbon::now()->format('Ymd') .'-Signiture'. '.'.$image_type;
        $str_signiture = $randInt.Carbon::now()->format('Ymd').'-Signiture'. '.'.$image_type;
        file_put_contents($file, $image_base64);

       Feedback::create([
          "fullname" => $fullname
          ,"member_no" => $member_no
          ,"gender" => $gender
          ,"exp_date" => $request->exp_date
          ,"branch_id" => $branch_id
          ,"branch_loc" => $request->branch_loc
          ,"member_type" => $request->member_type
          ,"transaction_nat" => $transaction_nat
          ,"transaction_exp" => $transaction_exp
          ,'comment_suggestion' => $request->comment
          ,'last_feedback' => Carbon::now()->addMonth(3)
          ,'signiture' => $str_signiture
          ,'crea_by' => $crea_by
        ]);
       
       
        // dd(Alert::success('Success', 'Thank you for your feedback'));
        Alert::success('Success', 'Thank you for your feedback');
        if(!Auth::guest()){
        return redirect()->route('enduser.home');
        }else{
        return redirect()->route('login');
        }
    }
    public function lastFeedback(Request $request)
    {
        // dd($input);
        if(!Auth::guest()){
        $user_id = auth()->user()->id;
        $user_info = UserMember::with('member')->where('user_id', $user_id)->first();
        $member_no = $user_info->member_no;
        }else{
        $member_no = $request->nonregmem_num;
        }
        // dd($me);
        $latest_feedback = Feedback::where('member_no', $member_no)->orderBy('last_feedback', 'desc')->first('last_feedback');

        if(empty($latest_feedback)){
            $feedbackAble = array('status'=>1);
        }else{
            $now = Carbon::now()->format('d/m/Y');

            $date_format = Carbon::parse($latest_feedback->last_feedback)->format('d/m/Y');
            if($now == $date_format){
                $feedbackAble = array('status'=>1);
            }else{
                $feedbackAble = array('status'=>0,'date'=>Carbon::parse($latest_feedback->last_feedback)->subMonth(3)->format('d/m/Y'));
            }
        }
        return response()->json($feedbackAble);
    }

    public function lastFeedbackNonRegNonMem(Request $request)
    {
        // $latest_feedback = Feedback::where('member_no', $member_no)->orderBy('last_feedback', 'desc')->first('last_feedback');
        $fname = '%'.$request->fname.'%';
        $lname = '%'.$request->lname.'%';
        $latest_feedback = Feedback::Where('fullname','like', $fname)
        ->Where('fullname','like', $lname)
        ->orderBy('last_feedback', 'desc')->first('last_feedback');
        $latest_feedback1 = Feedback::Where('fullname','like', $fname)
        ->Where('fullname','like', $lname)
        ->orderBy('last_feedback', 'desc')->first();
        if(empty($latest_feedback)){
            $feedbackAble = array('status'=>1);
        }else{
            $now = Carbon::now()->format('d/m/Y');
            $date_format = Carbon::parse($latest_feedback->last_feedback)->format('d/m/Y');
               // $feedAble = array('status'=>1);
            if($now == $date_format){
                $feedbackAble = array('status'=>1);
            }else{
                $feedbackAble = array('status'=>0,'date'=>Carbon::parse($latest_feedback->last_feedback)->subMonth(3)->format('d/m/Y'), 'latest_feedback' => $latest_feedback1);
            }
        }
        
        return response()->json($feedbackAble);
    }

    public function feedbackAPI($input)
    {
        $member_info = Member::with('branch')->where('member_no', $input)->get();
        // dd($member_info[0]->branch->name);
        if($member_info->count()){
        return response()->json($member_info);
        }else{
        return withError('Member Number provided does not match our entry');
        }
    }

    public function empfeedbackAPI($input)
    {
        $feedback = Feedback::where('id', $input)->with('branch', 'branchLoc')->get();
        return response()->json($feedback);
        
    }
    public function updateFeedback($array, $from, $to) 
    {
        $feedback = $array->whereNull('status')
        ->whereBetween('created_at', [$from, $to])->get(); 
        // dd($miuf);
        foreach($feedback as $data){
            $feedbackupdate = Feedback::find($data->id);
            $feedbackupdate->status = 1;
            $feedbackupdate->save();
        }
        // return response()->json('update');
    }
    public function exportExcel(Request $request) 
    {
        $start = $request->start.' 00:00:00';
        $end = $request->end.' 23:59:59';
        $start_end = $start.', '.$end;
        $date = date('M-d-Y', strtotime($start)).'-'.date('M-d-Y', strtotime($end));
        

        return \Excel::download(new FeedbackExport($start, $end), 'Walk-in Feedback('.$date.').'.'xlsx');
    }
    
}
