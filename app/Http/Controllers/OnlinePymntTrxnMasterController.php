<?php

namespace App\Http\Controllers;

use App\Models\OnlinePymntTrxnMaster;
use App\Models\OnlinePymntTrxnDetail;
use App\Models\BankMaster;
use App\Models\Branch;
use App\Models\MemberMaster;
use App\Models\AcctMaster;
use App\Models\STM;
use App\Models\UserMember;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Alert;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\UserRequest;
use App\Mail\OnlinePaymentMail;
use Illuminate\Support\Facades\Mail;
use App\Exports\OnlinePaymentExport;
use Maatwebsite\Excel\Facades\Excel;

class OnlinePymntTrxnMasterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $department = auth()->user()->department_id;
        $isAdmin = auth()->user()->role_id;
        $branch = auth()->user()->branch_id;
        // dd(auth()->user()->uname);
        if($department == 3 || $isAdmin == 0){
            $optm1  = OnlinePymntTrxnMaster::with('usermember','bank');
            
            if($branch == 9){
               $optm1->whereNull('soft_deleted_at');
           }else{
               $optm1->where('branch_id', $branch)
               ->whereNull('soft_deleted_at');
           }
             $optm =   $optm1->get();

            $bank  = BankMaster::whereNull('soft_deleted_at')->get();
            // $member  = MemberMaster::whereNull('soft_deleted_at')->get();
            $account  = AcctMaster::whereNull('soft_deleted_at')->get();
            // dd($optm);
            return view('optm.index', compact('optm', 'bank', 'account'));
        }else{
            Alert::error("You do not have the permission to access this page.");
            return redirect()->route('home');
        }
    }

    public function autocompleteSearch(Request $request)
    {
       $member = [];
        if($request->has('term')){
            $search = $request->term;
            $member = MemberMaster::select("member_no", "long_name")
                      ->where('long_name', 'LIKE', "%$search%")
                      ->get();
                      // $json1 = array();
            if(count($member) > 0){
                foreach ($member as $data)
                {
                    $json1[] = array('label' => $data->long_name, 'value' => $data->member_no);
                }
            }else
            {
                    $json1[] = array('label' => 'no results', 'value' => 'no results');
            }            
            return response()->json($json1);
                
        }

         
    } 

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user_id = auth()->user()->id;
        $usermember_id = UserMember::where('user_id', $user_id)->pluck('id');
        $branch_iddef = UserMember::where('user_id', $user_id)->pluck('branch_id')->first();
        $branches  = Branch::whereNull('soft_deleted_at')
        ->Where('cd', '!=', 'TRD')
        ->Where('cd', '!=', 'FR')
        ->Where('cd', '!=', 'ADMIN')
        ->get();
        $optm  = OnlinePymntTrxnMaster::with('usermember','bank')
        ->where('usermember_id', $usermember_id)
        ->where('status', 1) 
        ->get();
        // dd($optm);
        if(count($optm)){
            return redirect()->route('enduser.optd.create');
        }else{
        $account  = AcctMaster::whereNull('soft_deleted_at')->get();
        $bank  = BankMaster::whereNull('soft_deleted_at')->get();
        return view('enduser.optm.create', compact('bank', 'account', 'branches', 'branch_iddef'));
        }
        // dd($optm);
     
    }
    public function enduseroptmindex()
    {
        $user_id = auth()->user()->id;
        $usermember_id = UserMember::where('user_id', $user_id)->pluck('id');
        $optm  = OnlinePymntTrxnMaster::with('usermember','bank')
        ->where('usermember_id', $usermember_id)
        // ->whereNull('soft_deleted_at')
        ->get();
        // dd($optm);
        $optm_status = OnlinePymntTrxnMaster::with('usermember','bank')
        ->where('status', 1)->get(); 
        // $bank  = BankMaster::whereNull('soft_deleted_at')->get();
        // // $member  = MemberMaster::whereNull('soft_deleted_at')->get();
        // $account  = AcctMaster::whereNull('soft_deleted_at')->get();
        return view('enduser.optm.index', compact('optm', 'optm_status'));
     
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate([
            'cd' => 'required',
            'description' => 'required',
        ]);
       OnlinePymntTrxnMaster::create([
             'cd' => $request['cd'],
             'description' => $request['description'],
            'crea_by' => auth()->user()->uname,
        ]);
        return redirect()->route('optm.index')
            ->with('success', 'Online Payment Transaction created successfully.');
    }
    public function enduserstore(Request $request)
    {
        $request->validate([
            'total_amt' => 'required',
            'bank_id' => 'required',
        ]);

        $user_id = auth()->user()->id;
        $usermember_id = UserMember::where('user_id', $user_id)->first('id');
        $files = $request->file('attachment');
        $randInt = rand(100,999);
        $extension=$files->getClientOriginalExtension();
        $filename = array('optmdoc' => $randInt.Carbon::now()->format('Ymd').('-').'optmdoc'.'.'.$extension, 'optmor' => null);
            // dd($files);
        $files->move('assets/img/uploads/payments/'.$randInt.Carbon::now()->format('Ymd').'/', $filename);
        $optm_create = [
            'usermember_id' => $usermember_id->id,
            'total_amt' => $request['total_amt'],
            'bank_id' => $request['bank_id'],
            'date_trxn_rcvd' => Carbon::now(),
            'file_path' => $filename,
            'status' => 1,
            'crea_by' => auth()->user()->uname,
        ];
        // dd($optm_create);
        OnlinePymntTrxnMaster::create($optm_create);
        return redirect()->route('enduser.optd.create')
            ->with('success', 'Online Payment Transaction created successfully.');
    }
    public function giveOR(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'or_ref_no' =>'required', 
            'or_doc' =>'required|mimes:pdf', 
        ]
        ,[
            'or_ref_no.required' => 'Reference Number is required.',
            'or_doc.required' => 'Document is required.'
        ]);
        if ($validator->fails())
        {
            $a = implode('<br>',$validator->errors()->all());
            Alert::error('Validation Error: <br>'.$a)->autoClose(null);
            return redirect()->back();        
        }
        $files = $request->file('or_doc');
        $optm = OnlinePymntTrxnMaster::with('usermember.user')->find($request->id);
        $email = $optm->usermember->user->email;
        $number = explode('-',$optm->file_path['optmdoc']);
        $extension=$files->getClientOriginalExtension();
        $filename = $number[0].('-').'optmor'.'.'.$extension;
        $files->move('assets/img/uploads/payments/'.$number[0].'/', $filename);
        $file_path = array('optmdoc' => $optm->file_path['optmdoc'],'optmor' =>  $filename);
        $optm->status = 3;
        $optm->or_ref_no = $request->or_ref_no;
        $optm->file_path = $file_path;
        $optm->date_trxn_or_entry = Carbon::now();
        $optm->or_by = auth()->user()->uname;
        $optm->upd_by = auth()->user()->uname;
        $optm->save();

        $details = [
            'receipt' => $request->or_ref_no,
            'ORPDF'   => $filename,

        ];
        // dd($details);
        Mail::to($email)->queue(new OnlinePaymentMail($details));
        // return new App\Mail\OnlinePaymentMail($details);

        alert::success('success', 'Online Payment Official Receipt created successfully!');
        return redirect()->route('optm.index');
    }

    public function pdfview($input)
    {
        $input_number = explode('-',$input);
        $file_path = 'assets/img/uploads/payments/'.$input_number[0].'/'.$input;
        return response()->file($file_path);
    }

    public function storeoptm(Request $request)
    {
        $validatedDate = $request->validate
        ([
            'member_no' =>'required',
            'bank_id' =>'required',
            'total_amt' =>'required',
            'date_trxn_rcvd' =>'required', 
            'date_trxn_or_entry' =>'required', 
            'or_by' =>'required',   
            'or_ref_no' =>'required', 
            'status' =>'required', 
        ]);
        if(isset($request->id)){
            $upd_by = auth()->user()->uname;
            $updated_at = Carbon::now();
        }else{
            $upd_by = null;
            $updated_at = null;
        }
        $bank_cd = $request->bank_id;
        $optm_upd = 
        [
            'id' => $request->id,
        ];
        $optm_create = 
        [
            'member_no' => $request->member_no,
            'bank_id' => $bank_cd,
            'total_amt' => $request->total_amt,
            'date_trxn_rcvd' => $request->date_trxn_rcvd,
            'date_trxn_or_entry' => $request->date_trxn_or_entry,
            'or_by' => $request->or_by,
            'or_ref_no' => $request->or_ref_no,
            'status' => $request->status,
            'crea_by'     => auth()->user()->uname,
            'upd_by'         => $upd_by,
            'updated_at'     => $updated_at,
        ];
        $optm = OnlinePymntTrxnMaster::updateOrcreate($optm_upd, $optm_create);
        OnlinePymntTrxnDetail::create([
                 'online_pymnt_id' => $optm->id,
                 'acct_id' => null,
                 'amount' => null,
                 'crea_by' => auth()->user()->uname,
            ]);
        // STM::create([
        //             'optm_id' => $optm->id,
        //             'service_details'=> '.',
        //             'service_status' => 1,
        //             'service_type_id' => 1,
        //             'crea_by' => auth()->user()->uname,
        //             ]);
        if($request->id != null){
        $request->session()->flash('success', 'Online Payment Transaction updated successfully!');
        }else{
        $request->session()->flash('success', 'Online Payment Transaction created successfully!');    
        }
        return response()->json(['success' => 'success'], 200);
        // return response($optm_create);
        // return redirect()->route('pmes.index')->with('success', 'PMES created successfully.');
      
    }

    public function storeoptd(Request $request)
    {
        // dd($request->all());
        $sitefeedback = $request->sitefeedback;
        $request->validate([
            'total_amt' => 'required',
            'bank_id' => 'required',
        ]);

        $user_id = auth()->user()->id;
        $usermember_id = UserMember::where('user_id', $user_id)->first('id');
        $files = $request->file('attachment');
        $randInt = rand(100,999);
        $extension=$files->getClientOriginalExtension();
        $filename = $randInt.Carbon::now()->format('Ymd').('-').'optmdoc'.'.'.$extension;
        //     // dd($files);
        $files->move('assets/img/uploads/payments/'.$randInt.Carbon::now()->format('Ymd').'/', $filename);
        $file_path = array('optmdoc' => $filename, 'optmor' => null);
        $optm_create = [
            'usermember_id' => $usermember_id->id,
            'total_amt' => $request['total_amt'],
            'bank_id' => $request['bank_id'],
            'branch_id' => $request['branch_id'],
            'date_trxn_rcvd' => Carbon::now(),
            'file_path' => $file_path,
            'status' => 2,
            'crea_by' => auth()->user()->uname,
        ];
        // dd($optm_create);
        $optm = OnlinePymntTrxnMaster::create($optm_create);

        // $online_pymnt_id = $request->online_pymnt_id;
        $acct_id = $request->acct_id;
        $amount = $request->amount;

        $data1 = [];
        $data2 = [];
        $i = -1;
        foreach ($amount as $data)
        {
            $i++;
        // dd($i);
            OnlinePymntTrxnDetail::create(
                [
                'online_pymnt_id' => $optm->id,
                'acct_id' => $acct_id[$i],
                'amount' => $amount[$i],
                'crea_by' => auth()->user()->uname,
                ]
            );
        }

        if($sitefeedback == 'yes'){
        return redirect()->route('enduser.sitefeedback', 'non-logout');
        }else{
        // $request->session()->flash('success', 'Online Payment Transaction successfully!');
        alert::success('success', 'Online Payment Transaction successfully!');
        return redirect()->route('enduser.optm.index');
        }
        // return response()->json($request->all());
        // return response()->json(['success' => 'success'], 200);
        // return $request->all();
    }
    
    public function edit($id)
    {
        $optm  = OnlinePymntTrxnMaster::find($id);
       return view('optm.edit', [
        'optm' => $optm 
        ]);
    }
    public function OPTMmodal(Request $request, OnlinePymntTrxnDetail $optd)
    {
        //
        $where = array('id' => $request->id);
        $optd  = OnlinePymntTrxnDetail::where('online_pymnt_id',$where)->whereNull('soft_deleted_at')->get();
        $totalamt =  OnlinePymntTrxnMaster::where('id', $where)->first('total_amt');
        $count = 0;
        foreach($optd as $data){
            $array = array('id' => $data->id
                , 'online_pymnt_id' => $data->online_pymnt_id
                , 'acct_id' => $data->acct_id
                , 'amount' => $data->amount
                , 'count' => $count+1
                , 'total_amt' => $totalamt->total_amt
            );  
            $count++;
            $json[] = $array; 
        }
         // $request->session()->flash('success', 'optm updated successfully!');
        return response()->json($json);
    }
    public function attachment(Request $request, OnlinePymntTrxnMaster $optm)
    { 
        //
        $where = array('id' => $request->id);
        $optm  = OnlinePymntTrxnMaster::where($where)->first();
        $json = array('id' => $request->id
            ,'file_path' => $optm->file_path
            , 'status' => $optm->status
            , 'or_ref_no' => $optm->or_ref_no
        );
        return response()->json($json);
    }
    public function OPTDmodal(Request $request, OnlinePymntTrxnDetail $optd)
    { 
        //
        $where = array('id' => $request->id);
        $optd  = OnlinePymntTrxnDetail::whereNull('soft_deleted_at')->with('account')->where('online_pymnt_id', $where)->get();
        foreach($optd as $data){
            $array = array(
                 'online_pymnt_id' => $data->online_pymnt_id
                , 'description' => $data->account->description
                , 'amount' => number_format($data->amount,2)
            );  
            $json[] = $array; 
        }
        return response()->json($json);
    }

    public function editmodal(Request $request, OnlinePymntTrxnMaster $optm)
    { 
        //
        $where = array('id' => $request->id);
        $optm  = OnlinePymntTrxnMaster::where($where)->first();
        $json = array('id' => $request->id
            , 'member_no' => $optm->member_no
            , 'long_name' => $optm->usermember->crea_by
            , 'member_no' => $optm->usermember->member_no
            , 'bank_id' => $optm->bank_id
            , 'total_amt' => $optm->total_amt
            , 'date_trxn_or_entry' => $optm->date_trxn_or_entry ? Carbon::parse($optm->date_trxn_or_entry)->format('Y-m-d') : null
            , 'date_trxn_rcvd' => Carbon::parse($optm->date_trxn_rcvd)->format('Y-m-d')
            , 'date_trxn_or_entry1' => Carbon::parse($optm->date_trxn_or_entry)->format('Y-m-d')
            , 'or_ref_no' => $optm->or_ref_no
            , 'or_by' => $optm->or_by
            , 'status' => $optm->status
            , 'file_path' => $optm->file_path
            , 'crea_by' => $optm->crea_by
            , 'upd_by' => $optm->upd_by
        );
         // $request->session()->flash('success', 'optm updated successfully!');
        return response()->json($json);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request)
    {
        $rules = array(
            'cd' => 'required',
            'description' => 'required',
        );
         $validator = Validator::make($request->all(), $rules);
         $optm  = OnlinePymntTrxnMaster::find($id);
            $optm ->cd      = $request->get('cd');
            $optm ->description       = $request->get('description');
            $optm ->updated_at      = Carbon::now();
            $optm ->upd_by = auth()->user()->uname;
            $optm ->save();
       
        return redirect()->route('optm.index')
            ->with('success', 'Online Payment Transaction updated successfully');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Project  $project
     * @return \Illuminate\Http\Response
     */
    
    public function destroy($id)
    {
        $optm  = OnlinePymntTrxnMaster::find($id);
        $optm ->delete();

        return response()->json(['success'=>"Online Payment Transaction deleted successfully.", 'tr'=>'tr_'.$id]);

    }
    public function deleteAll(Request $request)
    {
        $ids = $request->ids;
        $optm = OnlinePymntTrxnMaster::whereIn('id',explode(",",$ids))->get();
        $optd = OnlinePymntTrxnDetail::whereIn('online_pymnt_id',explode(",",$ids))->whereNull('soft_deleted_at')->get();
        if($optd->count())
        {
            $json = ['error' => 'Please delete Online Payment Transaction Details corresponding to this entry.'];
        }else
        {
            if($optm->count())
            {
                foreach($optm as $data)
                {
                    $optm = OnlinePymntTrxnMaster::find($data->id);
                    $optm->soft_deleted_at = Carbon::now();
                    $optm->save();
                }
            }
            $json = ['success'=>"Online Payment Transaction Deleted successfully."];
        }
        return response()->json($json);
    }
    public function exportExcel(Request $request) 
    {
        $start = $request->start.' 00:00:00';
        $end = $request->end.' 23:59:59';
        $start_end = $start.', '.$end;
        $date = date('M-d-Y', strtotime($start)).'-'.date('M-d-Y', strtotime($end));
        return \Excel::download(new OnlinePaymentExport($start, $end), 'Online Payment('.$date.').'.'xlsx');
    }
}
