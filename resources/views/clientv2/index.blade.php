@extends('layouts.app', [
    'class' => 'sidebar-mini ',
    'namePage' => 'Client Index',
    'activePage' => 'client-management',
    'activeNav' => '',
])

@section('indexdeleteall')
  <script src="{{ asset('assets') }}/js/indexdeleteall.js"></script>
  <style>
    .dataTable-top,.dataTable-bottom {
    position:absolute;
    top:210; 
    padding-bottom: 10px; 
    }
    .dataTable-bottom{
    padding-top: 30px; 
    }
    .dataTable-top.fixed, .dataTable-bottom.fixed{
        position:fixed;
        top:0;
    } 
    .dataTable-container{
        padding-top: 50px;
    }
    .card-body{
      margin-bottom: 50px;
    }
      
  </style>
@endsection
@section('content')
    @include('auth.passwords.confirmmodal')
    @include('clientv2.modal')
<div class="panel-header panel-header-sm">
  </div>
  <div class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header">
            @include('flash-message')
                <div class="float-left">
                    <h4 class="card-title">Client</h4>
                </div>
                <div class="float-right">
                    <a href="javascript:void(0)" title="Add Client" class="btn btn-success" id="addNewclient">
                        <li class="fas fa-plus-circle fa-lg" aria-hidden="true"></li>
                      </a>
                    <button class="btn btn-danger delete_all" data-url="{{ url('DeleteAllClient') }}">Delete All Selected</button>
                </div>
          </div>
          <div class="card-body" >
            <div id="modalCatcher" class="table-responsive">
              <input type="hidden" value="{{url('/')}}" id="url" name="url">
              <table style="position: relative;" id="datatablesSimple" class="table">
                <thead class=" text-primary">
                    <th style=" z-index: 999;"  class="noSort sticky-col first-col"><input type="checkbox" id="master"></th>
                    <th style=" z-index: 999; " class="sticky-col second-colc">Action</th>
                    <!-- <th class="sticky-col third-col"  >Client ID</th> -->
                    <th style=" z-index: 999; " class="sticky-col third-col" >Full Name</th>
                    <th style=" z-index: 999;  " class="sticky-col fourth-col" >Contact Number</th>
                    <th style=" z-index: 999; left: 360px;" class="sticky-col ">Member Status</th>
                    <th >Service Status</th>
                    <th >Preferred Schedule</th>
                    <th >Seminar Preference</th>
                    <th >Email Address</th>
                    <th >Preferred  Branch</th>
                    <th >Gender</th>
                    <th >Age</th>
                    <th >Barangay</th>
                    <th >City</th>
                    <th >Province</th>
                    <th>Updated by</th>
                    <th>Date Updated</th>
                </thead>
                <tbody>
                    @if($client->count())
                    @foreach ($client as $data)
                    <tr>
                    <td class="sticky-col first-col"><input type="checkbox" class="sub_chk" data-id="{{$data->id}}"></td>
                    <td class="sticky-col second-col">
                      <a href="javascript:void(0)" title="Edit Client Type" class="edit" data-id="{{ $data->id }}">
                        <i class="fas fa-edit  fa-lg"></i>
                      </a>
                    @csrf
                    @method('DELETE')
                    </td>
                    <!-- <td  class=" sticky-col third-col" >{{$data->id}}</td> -->
                    <td class="sticky-col third-col" >{{$data->fullname}}</td>
                    <td class="sticky-col fourth-col" >{{$data->contact_no}}</td>
                    @if($data->member_status == 0)
                        <td class="sticky-col fifth-col" >Returning Member</td>
                        @elseif($data->member_status == 1)
                        <td class="sticky-col fifth-col" >From Associate Member</td>
                        @else
                        <td class="sticky-col fifth-col" >Non Member</td>
                    @endif
                    @if($data->sched_stat == 0)
                        <td>Unverified Email</td>
                        @elseif($data->sched_stat == 1)
                        <td>Batch Pending</td>
                        @else
                        <td>Email Sent</td>
                    @endif
                    @if(is_null($data->batch_id))
                        <td></td>
                        @else
                        <td>{{\Carbon\Carbon::parse($data->batch->sched_date)->format('m-d-Y')}}</td>
                    @endif
                    @if($data->seminar_pref == 0)
                        <td>Online Video Conference</td>
                        @else
                        <td>Walk-in / Physical Appearance</td>
                    @endif
                    <td>{{$data->gmail_address}}</td>
                    <td>{{$data->branch->name}}</td>
                    @if($data->gender == 0)
                        <td>Male</td>
                        @else
                        <td>Female</td>
                    @endif
                    <td>{{$data->age}}</td>
                    <td>{{$data->barangay->name}}</td>
                    <td>{{$data->city->name}}</td>
                    <td>{{$data->province->name}}</td>

                    @if(isset($data->upd_by))
                        <td>{{ $data->upd_by }}</td>
                        @else
                        <td></td>
                    @endif
                    @if(isset($data->updated_at))
                        <td>{{ date_format($data->updated_at, 'jS M Y') }}</td>
                        @else
                        <td></td>
                    @endif
                    
                   
                    @endforeach
                    @endif
                </tbody>
              </table>

            </div>
          </div>
        </div>
      </div>
      
    </div>
  </div>
    
    <!-- boostrap model -->
    
<!-- end bootstrap model -->

<script>
   // window.addEventListener("load", function() {
  //   window.addEventListener('DOMContentLoaded', event => {

  //   // Simple-DataTables
  //   // https://github.com/fiduswriter/Simple-DataTables/wiki
  //   const datatablesSimple = document.getElementById('datatablesSimple');
    
  //   if (datatablesSimple) { 
  //       new simpleDatatables.DataTable(datatablesSimple, 
  //       {
  //           columnDefs: [{ targets: "noSort", orderable: false }],
  //           fixedHeight: true,
  //           perPage:5,
  //       });
  //   }

  // });
$(document).ready(function(){
    $('#datatablesSimple').DataTable({

  });
  const swalWithBootstrapButtons = Swal.mixin({
                  customClass: {
                    confirmButton: 'btn btn-success',
                    cancelButton: 'btn btn-danger'
                  },
                  buttonsStyling: false
                }) 
    $.clearInput = function () {
        $('insert_form').find('input[type=text], input[type=password], input[type=number], input[type=email], select, textarea').val('');
    };
     $('.modal').on('hidden.bs.modal', function(){
             // window.location.reload();
             // $('#insert_form').removeData('bs.modal')
    $.clearInput();
    $(this).find('form')[0].reset();
    });

        // $.clearInput();
    $('.close-modal').click(function () {
        var modal_array = 
    [
      '#ajax-client-model'
    ];
        closeModal(modal_array);
       // $('#ajax-client-model').modal('hide');
    });

    $.ajaxSetup({
        headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('#addNewclient').click(function () {
       $('#addEditClientForm').trigger("reset");
       $('#ajaxClientModel').html("Add client");
       $('#ajax-client-model').modal('show');
              $('#batch_schedule').attr('hidden', false);
    });
      // $('#modalCatcher').on('click', '#btn-save', function () {
    $('#btn-save').click(function (event) {
        var base_path = $("#url").val();
         // alert(JSON.stringify(jQuery('#insert_form').serializeArray()));
          try {
            var id = $("#id").val();
          var fname = $("#fname").val();
          var mname = $("#mname").val();
          var lname = $("#lname").val();
          var birthdate = $("#datepicker").val();
          var age = $("#agecal").val();
          var gender = $("#gender").val();
          var contact_no = $("#contact_no").val();
          var occupation = $("#occupation").val();
          var other_occupation = $("#other_occupation").val();
          var region_id = $("#region_id").val();
          var province_id = $("#province_id").val();
          var city_id = $("#city_id").val();
          var barangay_id = $("#barangay_id").val();
          var street = $("#street").val();
          var self = document.querySelector('input[name="self"]:checked').value;
          var relative = document.querySelector('input[name="relative"]:checked').value; 
          var marketing_channel = $("#marketing_channel").val();
          var purpose_tag = $("#purpose_tag").val();
          var member_status = $("#member_status").val();
         // alert(JSON.stringify(member_status));
          var member_no = $("#member_no").val();
          var staff_referral_cd = $("#staff_referral_cd").val();
          var seminar_pref = $("#seminar_pref").val();
          var batch_id = $("#batch_id").val();
          var batch_id1 = $("#batch_id1").val();
          var branch_id = $("#branch_id").val();
          var gmail_address = $("#gmail_address").val();
          $("#btn-save").html('Please Wait...');
          $("#btn-save"). attr("disabled", true);            
      }
            catch(err) {
            swalWithBootstrapButtons.fire({
                  title: 'Error!',
                  text: err,
                  icon: 'warning',
                  confirmButtonText: 'Confirmed',
                }).then((result) => {
             // window.location.reload();
            $("#btn-save").html('Submit');
            $("#btn-save"). attr("disabled", false);
                })
            }

        // event.preventDefault();
        // ajax
            $.ajax({
            type:"POST",
            url: base_path+"/add-update-clientv2",
            // headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            data: {
              id:id,
              fname:fname,
              mname:mname,
              lname:lname,
              birthdate:birthdate,
              age:age,
              gender:gender,
              contact_no:contact_no,
              occupation:occupation,
              other_occupation:other_occupation,
              region_id:region_id,
              province_id:province_id,
              city_id:city_id,
              barangay_id:barangay_id,
              street:street,
              self:self,
              relative:relative,
              marketing_channel:marketing_channel,
              purpose_tag:purpose_tag,
              member_status:member_status,
              member_no:member_no,
              staff_referral_cd:staff_referral_cd,
              seminar_pref:seminar_pref,
              batch_id1:batch_id1,
              batch_id:batch_id,
              branch_id:branch_id,
              gmail_address:gmail_address,
            },
            dataType: 'json',
             success: function(res){
              // alert('success');
              swalWithBootstrapButtons.fire({
                  title: 'Success!',
                  icon: 'success',
                  confirmButtonText: 'Confirmed',
                }).then((result) => {
             window.location.reload();})
            $("#btn-save").html('Submit');
            $("#btn-save"). attr("disabled", false);
            }
            ,error: function (data) {
                 // alert('please delete the client connected to this client/client');
                 var errors = [];
           $.each( data.responseJSON['errors'], function( key, value ) {
                errors += value+'<br>';
            });
            // alert(errors);
            swalWithBootstrapButtons.fire({
                  title: 'Error!',
                  html: errors,
                  icon: 'warning',
                  confirmButtonText: 'Confirmed',
                }).then((result) => {
                 // window.location.reload();
                    $("#btn-save").html('Submit');
                    $("#btn-save"). attr("disabled", false);
                })
             }
           //  ,error: function(xhr, status, error){
           // var errorMessage = xhr.status + ': ' + xhr.statusText
           // alert('Error? - ' + errorMessage);
           //  }
            });

    });
     $('#modalCatcher').on('click', '.edit', function () {

        var id = $(this).data('id');
                var base_path = $("#url").val();
        // ajax
        //   alert( base_path );
        $.ajax({
            type:"POST",
            url: base_path+"/edit-clientv2",
            data: { id: id },
            dataType: 'json',
            success: function(res){
                // alert(JSON.stringify(res.sched_stat));
                // var date = res.sched_date->format('m-d-Y');
              // $('#remark_id').attr('hidden', false);
              $('#ajaxClientModel').html("Edit client");
              $('#ajax-client-model').modal('show');
              $('#id').val(res.id);
              $("#fname").val(res.fname);
              $("#mname").val(res.mname);
              $("#lname").val(res.lname);
              $("#datepicker").val(res.birthdate);
              $("#agecal").val(res.age);
              $("#gender").val(res.gender);
              $("#contact_no").val(res.contact_no);
              $("#occupation").val(res.occupation_id);
              var other_occupation = res.occupation_id;
              if(other_occupation == null){
                // alert('other_occupation');
                $('#div-occu').attr('class', 'col-md');
                $('#other_occupation').attr('hidden', false);
                $('#div-other-occu').attr('hidden', false);
                $("#other_occupation").val(res.other_occupation);
                $("#occupation").val(0);
              }else{
                // alert('occupation');
                $('#div-occu').attr('class', 'col-md');
                $('#other_occupation').attr('hidden', true);
                $('#div-other-occu').attr('hidden', true);
                $("#other_occupation").empty();
                  $("#occupation").val(res.occupation_id);
              }
              $("#region_id").val(res.region_id);
              var province_id1 = res.province_id;
              $.ajax({
                            type:"GET",
                            // base_path+'getProvince/'+region_code,
                            url:base_path+'/getProvince/'+res.region_id,
                            success:function(res){               
                            if(res){
                            $("#province_id").empty();
                            $("#province_id").attr('readonly', false);
                            $("#province_id").append('<option value="">Select Province</option>');                            
                            $("#city_id").append('<option value="">Select City</option>');
                            $("#barangay_id").empty();
                            $("#barangay_id").append('<option value="">Select Barangay</option>');
                            $.each(res,function(key,value){
                                // var province_id2 = value['province_id'];
                                // alert(JSON.stringify(province_id2));
                                $("#province_id").append('<option class="form-control" value="'+ value['province_id'] +'">'+ value['name'] +'</option>');
                            });
                              $("#province_id").val(province_id1);
                            }else{
                               $("#province_id").empty();
                            }
                            }
                            });
              var city_id1 = res.city_id;
              $.ajax({
                           type:"GET",
                           url:base_path+'/getCities/'+province_id1,
                           success:function(res){  
                            if(res){
                                $("#city_id").attr('readonly', false);
                                $("#city_id").empty();
                                $("#city_id").append('<option value="">Select City</option>');
                                $("#barangay_id").empty();
                                $("#barangay_id").append('<option value="">Select Barangay</option>');
                                $.each(res,function(key,value){
                                    $("#city_id").append('<option class="form-control" value="'+ value['city_id'] +'">'+ value['name'] +'</option>');
                                });
                                $("#city_id").val(city_id1);
                            }else{
                               $("#city_id").empty();
                            }
                           }
                            });
              var barangay_id1 = res.barangay_id;
              $.ajax({
                           type:"GET",
                           url:base_path+'/getBarangays/'+city_id1,
                           success:function(res){  
                            if(res){
                               $("#barangay_id").attr('readonly', false);
                               $("#barangay_id").empty();
                                $("#barangay_id").append('<option value="">Select Barangay</option>');
                                $.each((res),function(key,value){
                                    $("#barangay_id").append('<option class="form-control" value="'+ value['code'] +'">'+ value['name'] +'</option>');
                                });
                              $("#barangay_id").val(barangay_id1);
                            }else{
                               $("#barangay_id").empty();
                            }
                           }
                        });
              $("#street").val(res.street);
              // $("#of_tag").val(res.of_tag);
              var exploftag = res.of_tag.split(",");
              // alert(exploftag[0]);
                if(exploftag[0] == 1){
                $("#selfy").prop("checked", true);
                }else{
                $("#selfn").prop("checked", true);
                }
                if(exploftag[1] == 1){
                $("#relativey").prop("checked", true);
                }else{
                $("#relativen").prop("checked", true);
                }
              $("#marketing_channel").val(res.marketing_channel_id);
              $("#purpose_tag").val(res.purpose_tag);
              $("#member_status").val(res.member_status);
              var member_status1 = $("#member_status").val();
              // alert(member_status1);
              if(member_status1 == 2){
                $("#div-memno").attr('hidden', true);
                $('#member_no').attr('hidden', true);
              }else{
                $("#div-memno").attr('hidden', false);
                $('#member_no').attr('hidden', false);
              }
             // alert(JSON.stringify(member_status));
              $("#member_no").val(res.member_no);
              $("#staff_referral_cd").val(res.staff_referral_cd);
              $("#seminar_pref").val(res.seminar_pref);
              $("#batch_id").val(res.batch_id);
              $("#batch_id1").val(res.batch_id1);
              $("#branch_id").val(res.branch_id);
              $("#gmail_address").val(res.gmail_address);
              var sched_stat = JSON.stringify(res.sched_stat);
              if(sched_stat != 0){
                  $('#batch_schedule').attr('hidden', true);
              }
              // $('#resched_remarks').val(res.resched_remarks);
           }
            ,error: function (data) {
                 var err = JSON.parse(data.responseText);
                 if(err['message'] == 'Password confirmation required.'){   
                 alert(err['message']);
                 window.location.href = "password/confirm";
                 }else{
                 // alert('please delete the client connected to this client/client');
                 alert(data.responseText);
                 }
             }
        });

    });

     
 });
</script>
@endsection


