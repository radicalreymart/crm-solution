<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\UserController;
use App\Http\Controllers\BranchController;
use App\Http\Controllers\LoanTypeController;
use App\Http\Controllers\LoanTypePayModeController;
use App\Http\Controllers\DepartmentController;
use App\Http\Controllers\OccupationController;
use App\Http\Controllers\MarketingChannelController;
use App\Http\Controllers\TransactionTypeController;
use App\Http\Controllers\BatchController;
use App\Http\Controllers\ClientController;
use App\Http\Controllers\PMESController;
use App\Http\Controllers\BankMasterController;
use App\Http\Controllers\MemberMasterController;
use App\Http\Controllers\OnlinePymntTrxnMasterController;
use App\Http\Controllers\OnlinePymntTrxnDetailController;
use App\Http\Controllers\AcctMasterController;
use App\Http\Controllers\PreLoanApplicationController;
use App\Http\Controllers\FeedbackController;
use App\Http\Controllers\MIUFController;
use App\Http\Controllers\VerificationController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\HomeController;
use App\Http\Livewire\Batchv1;
use Illuminate\Http\Request;
use App\Models\PreLoanApplication;
use App\Models\OnlinePymntTrxnMaster;
use App\Models\UserMember;
use App\Models\MIUF;
use App\Models\Feedback;
use App\Models\SiteFeedback;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\Mail\SendMail;
use App\Mail\LoanMail;
use App\Mail\OnlinePaymentMail;
use App\Exports\PMESExport;
use App\Exports\LoanExport;
use App\Exports\OnlinePaymentExport;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', [App\Http\Controllers\HomeController::class, 'index']);

Route::get('/filedoc/{input}', [OnlinePymntTrxnMasterController::class, 'pdfview'])->name('filedoc');

Route::get('/auth-check', function(){
    if(!Auth::check()){
    return "logged out";
    }else{
    return "logged in";
    }
});

Route::get('/allnull/{input}', function(Request $request){
	$input = $request->input;
	if($input == 'miuf'){
		$exportNull = MIUF::all();
	foreach($exportNull as $data){
            $miufupdate = MIUF::find($data->id);
            $miufupdate->status = null;
            $miufupdate->encode_by = null;
            $miufupdate->encode_date = null;  
            $miufupdate->save();
        }
	}elseif($input == 'feedback'){
		$exportNull = Feedback::all();
	foreach($exportNull as $data){
            $miufupdate = Feedback::find($data->id);
            $miufupdate->status = null;
            $miufupdate->save();
        }
	}else{
		$exportNull = SiteFeedback::all();
	foreach($exportNull as $data){
            $miufupdate = SiteFeedback::find($data->id);
            $miufupdate->status = null;
            $miufupdate->save();
        }
	}

    return "success";

});


Route::get('/force-logout', function(){
    Auth::logout();
    return redirect()->to('/')->with('warning', 'Force logout activated.');
});

Route::get('/clear-cache', function(){
Artisan::call('optimize:clear');
return "Cache is Cleared";
});
Route::get('/clear-route', function(){
Artisan::call('route:clear');
return "Route is Cleared";
});
Route::get('/route-list', function(){
Artisan::call('route:list');
dd(Artisan::output());
});


Auth::routes(

);

Route::get('/demomail/{input}/{input1}', function ($input, $input1) {
	// dd($input);
	if($input == 'pmes'){
		if($input1 == 0){
            $link = 'https://meet.google.com/testing';
            $description = 'Online Pre-Membership Education Webinar';
            $meeting_cd = 'testing code';
        }elseif($input1 == 1){
            $link = 'https://us05web.zoom.us/j/86189281691?pwd=aWJSMVRDM2xRQURIcTNVcXRrOTMvQT09';
            $description = 'Online Pre-Membership Education Webinar';
            $meeting_cd = null;
        }else{
            $link = null;
                $meeting_cd = null;
            $description = 'Online Pre-Membership Education Seminar';
        }

		$startdate =  \Carbon\Carbon::parse('6 March 2019 15:00')->format('Ymd\THis');
		$enddate =  \Carbon\Carbon::parse('6 March 2019 17:00')->format('Ymd\THis');            
		$details = 
	                [
	                	'vid_tool' => $input1,
	                'link'   => $link,
            		'meeting_cd' => $meeting_cd,
	                'sched_date' => '2021-10-15 05:23:04',
	                'resched_remarks' => 'testing',
	                'text' => 'Creating calender feeds',
	                'description' => $description,
	                'startdate' => $startdate,
	                'enddate' => $enddate,
	                ];
	    return new App\Mail\SendMail($details);
	}elseif($input == 'verification'){
	    $details = 
	                [
	                'id' => '204',
	                'token' => 'o2PiKJxCaqoSJkVma2U4',
	                ];
	    return new App\Mail\VerificationMail($details);	
	}elseif($input == 'loan'){
		$decline_type = $input1;
		if($decline_type == 'temp'){
			$remarks = 'your attachment is not up to snuff';
		}elseif($decline_type == 'final'){
			$remarks = 'sorry but your application is declined';
		}else{
			$remarks = 'approved'; 
		}
		$details = [
            'decline_type' => $decline_type,
            // 'decline_type' = $decline,
            'remarks' => $remarks,
        ];
        // dd($details);
	    return new App\Mail\LoanMail($details);
	}elseif($input == 'payment'){
		
		$details = [
            'receipt' 	=> 'ortesting',
            'ORPDF'		=> '51820220228-optmor.pdf'
        ];
        // dd($details);
	    return new App\Mail\OnlinePaymentMail($details);
	}

});


Route::get('/demoexport/{input}/{input1}/{input2}', function ($input, $input1,$input2) {
	// dd($input1.' '.$input2);
        $start = $input1.' 00:00:00';
        $end = $input2.' 23:59:59';
        $date = date('M-d-Y', strtotime($start)).'-'.date('M-d-Y', strtotime($end));
	 return \Excel::download(new OnlinePaymentExport($start, $end), 'Online Payment('.$date.').'.'xlsx');
});


 
Route::get('/verifyEmail/{id}/{token}', [App\Http\Controllers\ClientController::class, 'verifyEmail'])->name('verifyEmail');

Route::get('/enduser-verify', [App\Http\Controllers\MemberMasterController::class, 'member_verify'])->name('enduser-verify');
Route::get('/enduser/register', [App\Http\Controllers\MemberMasterController::class, 'member_register'])->name('enduser.register');
Route::post('/enduser/storeusermember', [App\Http\Controllers\MemberMasterController::class, 'storeusermember'])->name('enduser.storeusermember');

Route::get('/validateEmail/', [MemberMasterController::class, 'validateEmail'])->name('validateEmail');

Route::get('/payment_modeAPI/{id}', [PreLoanApplicationController::class, 'payment_modeAPI'])->name('payment_modeAPI');
Route::get('/payment_termAPI/{id}', [PreLoanApplicationController::class, 'payment_termAPI'])->name('payment_termAPI');
Route::get('/payment_termMonths/{id}', [PreLoanApplicationController::class, 'payment_termMonths'])->name('payment_termMonths');
Route::get('/loanstatusAPI/{id}', [PreLoanApplicationController::class, 'loanstatusAPI'])->name('loanstatusAPI');

Route::get('/feedbackAPI/{input}', [FeedbackController::class, 'feedbackAPI'])->name('feedbackAPI');
Route::get('/lastFeedback', [FeedbackController::class, 'lastFeedback'])->name('lastFeedback');
Route::get('/lastFeedbackNonRegNonMem', [FeedbackController::class, 'lastFeedbackNonRegNonMem'])->name('lastFeedbackNonRegNonMem');


Route::get('/enduser/feedback', [App\Http\Controllers\FeedbackController::class, 'create'])->name('enduser.feedback');
Route::post('/feedback.store', [App\Http\Controllers\FeedbackController::class, 'store'])->name('feedback-store');
// Route::get('/enduser/login', [App\Http\Controllers\MemberMasterController::class, 'login'])->name('enduser.login');





	

Route::group(['middleware' => 'is_enduser'], function () {
	Route::get('/email/verify', [App\Http\Controllers\VerificationController::class, 'show'])->name('verification.notice');
    Route::get('/email/verify/{id}/{hash}', [App\Http\Controllers\VerificationController::class, 'verify'])->name('verification.verify')->middleware(['signed']);
    Route::post('/email/resend', [App\Http\Controllers\VerificationController::class, 'resend'])->name('verification.resend');
    
		Route::get('/enduser/edit', [App\Http\Controllers\MIUFController::class, 'edit'])->name('enduser.edit');
		Route::post('/enduser/update', [App\Http\Controllers\MIUFController::class, 'update'])->name('enduser.update');

	Route::group(['middleware' => 'is_verified'], function () {

	Route::get('/validateCoMaker/', [MemberMasterController::class, 'validateMemberNo'])->name('validateCoMaker');

		Route::get('/enduser/sitefeedback/{input}', [App\Http\Controllers\SiteFeedbackController::class, 'create'])->name('enduser.sitefeedback');
		Route::post('/sitefeedback.store', [App\Http\Controllers\SiteFeedbackController::class, 'store'])->name('sitefeedback-store');

		Route::get('/sitefeedbackAPI/{input}', [App\Http\Controllers\SiteFeedbackController::class, 'sitefeedbackAPI'])->name('sitefeedbackAPI');
		Route::get('/lastsiteFeedback', [App\Http\Controllers\SiteFeedbackController::class, 'lastsiteFeedback'])->name('lastsiteFeedback');
		Route::get('/lastsiteFeedbackNonRegNonMem', [App\Http\Controllers\SiteFeedbackController::class, 'lastsiteFeedbackNonRegNonMem'])->name('lastsiteFeedbackNonRegNonMem');


		Route::get('enduser/home', [HomeController::class, 'memberHome'])->name('enduser.home');

		Route::get('/preloan/create', [App\Http\Controllers\PreLoanApplicationController::class, 'create'])->name('preloan.create');
		Route::post('/preloan/store', [App\Http\Controllers\PreLoanApplicationController::class, 'store'])->name('preloan.store');
		Route::get('/edit-preloan-get', [PreLoanApplicationController::class, 'edit'])->name('edit-preloan-get');
		// Route::put('/loanstatus/{id}', [PreLoanApplicationController::class, 'loanstatus'])->name('loanstatus');
		// Route::put('/loanstatus2/{id}', [PreLoanApplicationController::class, 'loanstatusmodal'])->name('loanstatus2');
		Route::post('delete-loan', [PreLoanApplicationController::class, 'deleteAll'])->name('delete-loan');
		Route::get('/enduser-attachment', [PreLoanApplicationController::class, 'enduserAttachment'])->name('enduser-attachment');
		Route::post('/enduser-edit-preloan', [PreLoanApplicationController::class, 'edit'])->name('enduser-edit-preloan');
		Route::get('enduser/preloan/index', [App\Http\Controllers\PreLoanApplicationController::class, 'enduserpreloanindex'])->name('enduser.preloan.index');

		Route::post('/enduser-edit-mode-terms', [PreLoanApplicationController::class, 'editmodeterms'])->name('enduser-edit-mode-terms');


		Route::get('enduser/optm/index', [App\Http\Controllers\OnlinePymntTrxnMasterController::class, 'enduseroptmindex'])->name('enduser.optm.index');
		Route::get('enduser/optm/create', [App\Http\Controllers\OnlinePymntTrxnMasterController::class, 'create'])->name('enduser.optm.create');
		Route::post('enduser/optm/store', [App\Http\Controllers\OnlinePymntTrxnMasterController::class, 'enduserstore'])->name('enduser.optm.store');
		Route::post('enduser-add-optd', [OnlinePymntTrxnMasterController::class, 'storeoptd'])->name('enduser.add.optd');
		Route::post('/optmtracking', [OnlinePymntTrxnMasterController::class, 'attachment'])->name('optmtracking');
		Route::post('/enduserattachment', [OnlinePymntTrxnMasterController::class, 'attachment'])->name('enduserattachment');
		Route::post('/OPTDmodal', [OnlinePymntTrxnMasterController::class, 'OPTDmodal'])->name('OPTDmodal');
		// Route::get('enduser/optd/create', [App\Http\Controllers\OnlinePymntTrxnDetailController::class, 'create'])->name('enduser.optd.create');

});

});

	Route::get('/enduser/create', [App\Http\Controllers\ClientController::class, 'endusercreate'])->name('enduser.create');
	Route::post('/enduser/store', [App\Http\Controllers\ClientController::class, 'storeclient'])->name('enduser.store');
	Route::post('/update-verfied-enduser', [ClientController::class, 'verifieduser'])->name('update-verfied-enduser');





Route::get('/getBatches/{batch_id}', [BatchController::class, 'getBatches'])->name('getBatches');
// Route::get('/getBatches/{batch_id}', [BatchC::class, 'getBatches'])->name('getBatches');

Route::get('/getClient1/{oldclient}', [ClientController::class, 'getClient1'])->name('getClient1');


Route::get('/getProvince/{region_code}', [ClientController::class, 'getProvince'])->name('getProvince');
Route::get('/getCities/{province_code}', [ClientController::class, 'getCities'])->name('getCities');
Route::get('/getBarangays/{city_code}', [ClientController::class, 'getBarangays'])->name('getBarangays');
Route::get('/getHiddenRegion/{region_code}', [ClientController::class, 'getHiddenRegion'])->name('getRegions');
Route::get('/getHiddenProvince/{province_code}', [ClientController::class, 'getHiddenProvince'])->name('getHiddenProvince');
Route::get('/getHiddenCities/{city_code}', [ClientController::class, 'getHiddenCities'])->name('getHiddenCities');
Route::get('/getHiddenBarangays/{barangay_code}', [ClientController::class, 'getHiddenBarangays'])->name('getHiddenBarangays');

// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Auth::routes();
Auth::routes(['register' => false]);


Route::group(['middleware' => ['auth','addHeaders','is_employee']], function ($request) {
Route::get('/home', 'App\Http\Controllers\HomeController@index')->name('home');
	// Route::resource('users', UserController::class);
	// Route::resource('batch', 'App\Http\Controllers\BatchController', ['except' => ['show']])->name('batch','*');

	Route::post('DeleteAllBatch', [BatchController::class, 'deleteAll'])->middleware('password.confirm');
	Route::view('batch','livewire.batch.home')->name('batch');
	Route::view('batch/history','livewire.batch.home-history')->name('batch/history');



	Route::group(['middleware' => 'is_admin'], function () {
		Route::post('DeleteAllUser', [UserController::class, 'deleteAll'])->middleware('password.confirm');
		Route::view('users','livewire.user.home')->name('users');
	});
	Route::resource('branch', 'App\Http\Controllers\BranchController', ['except' => ['show']])->name('branch','*');
	Route::post('DeleteAllBranch', [BranchController::class, 'deleteAll'])->middleware('password.confirm');

	Route::resource('loanType', 'App\Http\Controllers\LoanTypeController', ['except' => ['show']])->name('loanType','*');
	Route::post('DeleteAllLoanType', [LoanTypeController::class, 'deleteAll'])->middleware('password.confirm');

	Route::resource('loanTypePayment', 'App\Http\Controllers\LoanTypePayModeController', ['except' => ['show']])->name('loanTypePayment','*');
	Route::post('DeleteAllLoanTypePayment', [LoanTypePayModeController::class, 'deleteAll'])->middleware('password.confirm');

	Route::resource('loanTypeTerm', 'App\Http\Controllers\LoanTypePayTermController', ['except' => ['show']])->name('loanTypeTerm','*');
	Route::post('DeleteAllLoanTypeTerm', [LoanTypePayTermController::class, 'deleteAll'])->middleware('password.confirm');

	Route::resource('department', 'App\Http\Controllers\DepartmentController', ['except' => ['show']])->name('department','*');
	Route::post('DeleteAllDepartment', [DepartmentController::class, 'deleteAll'])->middleware('password.confirm');

	Route::resource('occupation', 'App\Http\Controllers\OccupationController', ['except' => ['show']])->name('occupation','*');
	Route::post('DeleteAllOccupation', [OccupationController::class, 'deleteAll'])->middleware('password.confirm');

	Route::resource('marketingChannel', 'App\Http\Controllers\MarketingChannelController', ['except' => ['show']])->name('marketingChannel','*');
	Route::post('DeleteAllMarketingChannel', [MarketingChannelController::class, 'deleteAll'])->middleware('password.confirm');

	Route::resource('transactionType', 'App\Http\Controllers\TransactionTypeController', ['except' => ['show']])->name('transactionType','*');
	Route::post('/edit-transaction', [TransactionTypeController::class, 'editmodal'])->name('edit-transaction');
	Route::post('/add-update-transaction', [TransactionTypeController::class, 'storetransaction'])->name('add-update-transaction');
	Route::post('DeleteAllTransactionType', [TransactionTypeController::class, 'deleteAll'])->middleware('password.confirm');

	Route::resource('bank', 'App\Http\Controllers\BankMasterController', ['except' => ['show']])->name('bank','*');
	Route::post('/edit-bank', [BankMasterController::class, 'editmodal'])->name('edit-bank');
	Route::post('/add-update-bank', [BankMasterController::class, 'storebank'])->name('add-update-bank');
	Route::post('DeleteAllBank', [BankMasterController::class, 'deleteAll'])->middleware('password.confirm');

	Route::resource('batchv2', 'App\Http\Controllers\BatchController', ['except' => ['show']])->name('batchv2','*');
	Route::get('/batchv2History', [BatchController::class, 'historyindex'])->name('batchv2History');
	Route::post('/edit-batchv2', [BatchController::class, 'editmodal'])->name('edit-batchv2');
	Route::post('/add-update-batchv2', [BatchController::class, 'storebatchv2'])->name('add-update-batchv2');
	Route::post('/participant', [BatchController::class, 'participant'])->name('participant');
	Route::post('DeleteAllBatchv2', [BatchController::class, 'deleteAll'])->middleware('password.confirm');


	Route::get('/clientv2', [ClientController::class, 'index'])->name('clientv2');
	Route::post('/edit-clientv2', [ClientController::class, 'editmodal'])->name('edit-clientv2');
	Route::post('/add-update-clientv2', [ClientController::class, 'storeclient'])->name('add-update-clientv2');
	Route::post('DeleteAllClientv2', [ClientController::class, 'deleteAll'])->middleware('password.confirm');

	Route::resource('member', 'App\Http\Controllers\MemberMasterController', ['except' => ['show']])->name('member','*');

	Route::get('/pagination/fetch_data', [MemberMasterController::class, 'fetch_data'])->name('fetch_data');

	Route::get('/validateMemberNo/', [MemberMasterController::class, 'validateMemberNo'])->name('validateMemberNo');

	Route::get('/validateUniKey/', [MemberMasterController::class, 'validateUniKey'])->name('validateUniKey');
	Route::post('/edit-member', [MemberMasterController::class, 'editmodal'])->name('edit-member');
	Route::post('/add-member', [MemberMasterController::class, 'addmember'])->name('add-member');
	Route::post('/add-update-member', [MemberMasterController::class, 'storemember'])->name('add-update-member');
	Route::post('DeleteAllMember', [MemberMasterController::class, 'deleteAll'])->middleware('password.confirm');

	Route::get('/member/getmember/','MemberMasterController@getMember')->name('member.getmember');

	Route::get('exportOPTM', [OnlinePymntTrxnMasterController::class, 'exportExcel'])->name('exportOPTM');
	Route::resource('optm', 'App\Http\Controllers\OnlinePymntTrxnMasterController', ['except' => ['show']])->name('optm','*');
	Route::post('/create-OPTD', [OnlinePymntTrxnMasterController::class, 'OPTMmodal'])->name('create-OPTD');
	Route::post('/edit-optm', [OnlinePymntTrxnMasterController::class, 'editmodal'])->name('edit-optm');
	Route::post('/attachment', [OnlinePymntTrxnMasterController::class, 'attachment'])->name('attachment');
	Route::post('/giveOR', [OnlinePymntTrxnMasterController::class, 'giveOR'])->name('giveOR');
	Route::post('/add-update-optm', [OnlinePymntTrxnMasterController::class, 'storeoptm'])->name('add-update-optm');
	Route::post('DeleteAllOPTM', [OnlinePymntTrxnMasterController::class, 'deleteAll'])->middleware('password.confirm');
	// Route::get('/autocomplete-search-optm', [OnlinePymntTrxnMasterController::class, 'autocompleteSearch'])->name('autocomplete-search-optm');
	Route::get('/autocomplete-search-optm/{term?}', [OnlinePymntTrxnMasterController::class, 'autocompleteSearch']);
	Route::post('add-optd', [OnlinePymntTrxnMasterController::class, 'storeoptd']);
	Route::post('/OPTDmodal1', [OnlinePymntTrxnMasterController::class, 'OPTDmodal'])->name('OPTDmodal1');


	Route::resource('optd', 'App\Http\Controllers\OnlinePymntTrxnDetailController', ['except' => ['show']])->name('optd','*');
	Route::post('/edit-optd', [OnlinePymntTrxnDetailController::class, 'editmodal'])->name('edit-optd');
	Route::post('/add-update-optd', [OnlinePymntTrxnDetailController::class, 'storeoptd'])->name('add-update-optd');
	Route::post('DeleteAllOPTD', [OnlinePymntTrxnDetailController::class, 'deleteAll'])->middleware('password.confirm');

	Route::resource('acctMaster', 'App\Http\Controllers\AcctMasterController', ['except' => ['show']])->name('acctMaster','*');
	Route::post('/edit-acctMaster', [AcctMasterController::class, 'editmodal'])->name('edit-acctMaster');
	Route::post('/add-update-acctMaster', [AcctMasterController::class, 'storeacctMaster'])->name('add-update-acctMaster');
	Route::post('DeleteAllAcctMaster', [AcctMasterController::class, 'deleteAll'])->middleware('password.confirm');

	Route::resource('userv2', 'App\Http\Controllers\UserController', ['except' => ['show']])->name('userv2','*');
	Route::post('/edit-userv2', [UserController::class, 'editmodal'])->name('edit-userv2');
	Route::post('/add-update-userv2', [UserController::class, 'storeuser'])->name('add-update-userv2');
	Route::post('DeleteAllUserv2', [UserController::class, 'deleteAll'])->middleware('password.confirm');

	
	// Route::resource('client', 'App\Http\Controllers\ClientController', ['except' => ['show']])->name('client','*');
	Route::post('DeleteAllClient', [ClientController::class, 'deleteAll'])->middleware('password.confirm');
	Route::get('/client/index', function () {
    return view('client.search-client');
	})->name('client/index');
	Route::view('client','livewire.client.home')->name('client');

	Route::get('exportPMES', [PMESController::class, 'exportExcel'])->name('exportPMES');
	Route::resource('pmes', 'App\Http\Controllers\PMESController', ['except' => ['show']])->name('pmes','*');
	Route::post('/add-update-pmes', [PMESController::class, 'storepmes'])->name('add-update-pmes');
	Route::post('DeleteAllPMES', [PMESController::class, 'deleteAll'])->middleware('password.confirm');
	Route::put('/attendance/{id}', [PMESController::class, 'attendance'])->name('attendance');
	Route::post('/edit-pmes', [PMESController::class, 'editmodal'])->name('edit-pmes');

	Route::get('exportLoan', [PreLoanApplicationController::class, 'exportExcel'])->name('exportLoan');
	Route::get('/preloan', [PreLoanApplicationController::class, 'index'])->name('preloan');
	Route::get('/show-attachment', [PreLoanApplicationController::class, 'getAttachment'])->name('show-attachment');
	Route::get('/preloancount', [PreLoanApplicationController::class, 'preloancount'])->name('preloancount');
	Route::post('/edit-preloan', [PreLoanApplicationController::class, 'edit'])->name('edit-preloan');
	// Route::get('/edit-preloan-get', [PreLoanApplicationController::class, 'edit'])->name('edit-preloan-get');
	Route::post('DeleteAllPreLoanApplication', [PreLoanApplicationController::class, 'deleteAll'])->middleware('password.confirm');
	Route::post('/loanstatus1/', [PreLoanApplicationController::class, 'loanstatus'])->name('loanstatus1');
	Route::post('/loanstatus/{id}', [PreLoanApplicationController::class, 'loanstatusmodal'])->name('loanstatusmodal');
	Route::post('/loanapproval', [PreLoanApplicationController::class, 'loanapproval'])->name('loanapproval');


	Route::get('/miuf', [App\Http\Controllers\MIUFController::class, 'index'])->name('miuf.index');
	Route::get('/miuf/show', [App\Http\Controllers\MIUFController::class, 'show'])->name('miuf.show');
	// Route for export/download tabledata to .csv, .xls or .xlsx
	Route::get('exportMiuf', [MIUFController::class, 'exportExcel'])->name('exportMiuf');

	Route::get('/feedback', [App\Http\Controllers\FeedbackController::class, 'index'])->name('feedback.index');
	Route::get('/sitefeedback', [App\Http\Controllers\SiteFeedbackController::class, 'index'])->name('sitefeedback.index');
	Route::get('/empfeedbackAPI/{input}', [FeedbackController::class, 'empfeedbackAPI'])->name('empfeedbackAPI');
	Route::get('/empsitefeedbackAPI/{input}', [App\Http\Controllers\SiteFeedbackController::class, 'empfeedbackAPI'])->name('empsitefeedbackAPI');
	// Route for export/download tabledata to .csv, .xls or .xlsx
	Route::get('exportFeedback', [FeedbackController::class, 'exportExcel'])->name('exportFeedback');

	Route::get('exportSiteFeedback', [App\Http\Controllers\SiteFeedbackController::class, 'exportExcel'])->name('exportSiteFeedback');
	

	Route::get('/userMember', [App\Http\Controllers\MemberMasterController::class, 'userMemberIndex'])->name('userMember.index');
	Route::post('/edit-usermember', [MemberMasterController::class, 'editusermembermodal'])->name('edit-usermember');
	Route::post('DeleteAllUserMember', [MemberMasterController::class, 'deleteAll'])->middleware('password.confirm');

	// Route::get('/feedback/show', [App\Http\Controllers\FeedbackController::class, 'show'])->name('feedback.show');


	Route::get('profile', ['as' => 'profile.edit', 'uses' => 'App\Http\Controllers\ProfileController@edit']);
	Route::put('profile', ['as' => 'profile.update', 'uses' => 'App\Http\Controllers\ProfileController@update']);
	Route::put('profile/password', ['as' => 'profile.password', 'uses' => 'App\Http\Controllers\ProfileController@password']);
	Route::get('{page}', ['as' => 'page.index', 'uses' => 'App\Http\Controllers\PageController@index']);
});

