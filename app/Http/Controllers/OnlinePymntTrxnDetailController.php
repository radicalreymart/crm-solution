<?php

namespace App\Http\Controllers;

use App\Models\OnlinePymntTrxnDetail;
use Illuminate\Http\Request;
use App\Models\OnlinePymntTrxnMaster;
use App\Models\UserMember;
use App\Models\AcctMaster;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\UserRequest;
use Alert;

class OnlinePymntTrxnDetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $department = auth()->user()->department_id;
        $isAdmin = auth()->user()->role_id;
        $branch = auth()->user()->branch_id;
        // dd(auth()->user()->uname);
        if($department == 3 || $isAdmin == 0){
            
            $optd = OnlinePymntTrxnDetail::whereNull('soft_deleted_at')->with('account','optm')->get();
            $optm = OnlinePymntTrxnMaster::whereNull('soft_deleted_at')->get();
            $account = AcctMaster::whereNull('soft_deleted_at')->get();
            // dd($optd);
            return view('optd.index', compact('optd', 'optm', 'account'));
        }else{
            Alert::error("You do not have the permission to access this page.");
            return redirect()->route('home');
        }
    }
     public function create()
    {
        // $bank  = BankMaster::whereNull('soft_deleted_at')->get();
        $account  = AcctMaster::whereNull('soft_deleted_at')->get();
        $user_id = auth()->user()->id;
        $usermember_id = UserMember::where('user_id', $user_id)->first('id');
        $optm = OnlinePymntTrxnMaster::
        where('usermember_id', $usermember_id->id)
        ->whereNull('soft_deleted_at')
        ->where('status', 1)
        ->get();
        // dd($optm);
        return view('optd.create', compact('optm', 'account'));
     
    }

   public function storeoptd(Request $request)
    {
        $validatedDate = $request->validate
        ([
            'online_pymnt_id' => 'required',
            'acct_id' => 'required',
            'amount' => 'required',
        ]);
        if(isset($request->id)){
            $upd_by = auth()->user()->uname;
            $updated_at = Carbon::now();
        }else{
            $upd_by = null;
            $updated_at = null;
        }
        $optd_upd = 
        [
            'id' => $request->id,
        ];
        $optd_create = 
        [
                'online_pymnt_id' =>  $request->online_pymnt_id,
                'acct_id' => $request->acct_id,
                'amount' =>  $request->amount,
                'crea_by' => auth()->user()->uname,
                'upd_by'         => $upd_by,
                'updated_at'     => $updated_at,
        ];
        // OnlinePymntTrxnDetail::updateOrcreate($optd_upd, $optd_create);
        // return response()->json($optd_create);
        if($request->id != null){
        $request->session()->flash('success', 'Online Payment Transaction Details updated successfully!');
        }else{
        $request->session()->flash('success', 'Online Payment Transaction Details created successfully!');
        }
        return response()->json(['success' => 'success'], 200);
    }

    public function editmodal(Request $request, OnlinePymntTrxnMaster $optm)
    { 
        //
        $where = array('id' => $request->id);
        $optd  = OnlinePymntTrxnDetail::where($where)->first();
        $json = array('id' => $request->id,
            'online_pymnt_id' =>  $optd->online_pymnt_id,
                'acct_id' => $optd->acct_id,
                'amount' =>  $optd->amount
        );
         // $request->session()->flash('success', 'optm updated successfully!');
        return response()->json($json);
    }

    public function deleteAll(Request $request)
    {

        $ids = $request->ids;
        $optd = OnlinePymntTrxnDetail::whereIn('id',explode(",",$ids))->whereNull('soft_deleted_at')->get();
        if($optd->count())
        {
            foreach($optd as $data)
            {
                $optd = OnlinePymntTrxnDetail::find($data->id);
                $optd->soft_deleted_at = Carbon::now();
                $optd->save();
            }
        }
        return response()->json(['success'=>"Online Payment Transaction Detail deleted successfully."]);
    }
}
