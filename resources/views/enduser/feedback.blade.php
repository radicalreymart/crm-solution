@extends('layouts.app1')
@section('content')
  <style>
    .alert{
      color: black !important;
    }
    .rate {
    text-align: center ;
    }
    .radio-lm{  
    transform: scale(1.5);
    }
    .comment{
      border-color: black !important; 
    }
    .comment:hover{
     border-color: black !important;
    }
  </style>
    <!-- <nav class="navbar navbar-expand-lg navbar-light bg-white">
      <div class="container-fluid">
        <a class="navbar-brand" href="#">
          <img src="{{asset('assets/img/logo.jpg')}}" alt="" width="180" class="d-inline-block align-text-top">
        </a>
      </div>
    </nav> -->
    <!-- Masthead-->
    <header class="masthead mb-1">
      <div class="container mt-1 pt-1">
            @include('flash-message')
            @include('sweetalert::alert')
      </div>
    </header>

    <div class="container mt-5" id="sign_up">
      <div class="card p-2 pt-3 bg-light border-0">
        <div class="row justify-content-center"> 
          <!-- <div class="col-md-12 order-md-1">

          </div> -->
          <div class="col-md-8 order-md-1">
            <form method="POST" id="formid" name='formname' action="{{ route('feedback-store') }}" onsubmit="return validateForm()">
              @if(Auth::check())
              <input type="hidden" id="regmem" name="regmem"  >
              <input type="hidden" id="regmem_num" name="regmem_num" value="{{$user_info->member_no}}" >
              <input type="hidden" id="feedback_type" name="feedback_type" value="member" >
              @else
              <input type="hidden" id="nonregmem" name="nonregmem"  >
              <input type="hidden" id="nonregmem_num" name="nonregmem_num"  >
              <input type="hidden" id="feedback_type" name="feedback_type"  >
              @endif
              <input type="hidden" value="{{url('/')}}" id="url" name="url">
                                        @csrf
              <div class="card mx-3 shadow-lg border-0">
                <div class="card-header bg-light text-primary py-2 pl-3 border-0">
                  <h6 class="m-0 default-primary">Personal Information 
                    @if(!Auth::guest())
                    <a class="justify-content"> | Member No: {{$user_info->member_no}}</a>
                    <a class=" hvr-icon-push float-right" onClick="window.location.href = '/enduser/home';" data-url="{{ url('/enduser/home') }}">{{ __('Back') }}<i class="far fa-caret-square-left hvr-icon"></i></a>
                    @else
                    <a hidden id="mem_num" class="justify-content"> </a>
                    <a class=" hvr-icon-push float-right" onClick="window.location.href = '/';" data-url="{{ url('/') }}">{{ __('Back') }}<i class="far fa-caret-square-left hvr-icon"></i></a>
                    @endif
                  </h6>
                </div>
                <div class="card-body px-3">
                  @if(!Auth::guest())
                    <div class="row px-3">
                      <div class="col-md mb-3">
                        <label >Full Name <span class="text-danger">*</span></label>
                        <input type="text" class="fullname form-control form-control-sm  @error('fname') is-invalid @enderror " readonly id="fullname" name="fullname" placeholder="Full Name" @if(!Auth::guest())value="{{$fullname}}" readonly @endif  autocomplete="fullname">
                        <div class="invalid-feedback">
                          Full Name is required.
                        </div>
                      </div>
                    </div> <!-- Long Name -->
                    @else
                    <div hidden id="fullname_div" class="row px-3">
                      <div class="col-md mb-3">
                        <label >Full Name <span class="text-danger">*</span></label>
                        <input type="text" class="fullname form-control form-control-sm  @error('fullname') is-invalid @enderror " readonly id="fullname" name="fullname" placeholder="Full Name" @if(!Auth::guest())value="{{$fullname}}" readonly @endif  autocomplete="fullname">
                        <div class="invalid-feedback">
                          Full Name is required.
                        </div>
                      </div>
                    </div>
                    <div id="FMLName" class="row px-3">
                      <div class="col-md-4 mb-3">
                        <label >First Name <span class="text-danger">*</span></label>
                        <input type="text" class="form-control form-control-sm  @error('fname') is-invalid @enderror " id="fname" name="fname" placeholder="First Name" @if(!Auth::guest())value="{{$fname}}" readonly @endif  autocomplete="fname">
                        <div class="invalid-feedback">
                          First Name is required.
                        </div>
                      </div>
                      <div class="col-md-4 mb-3">
                        <label >Middle Name  <span class="text-muted">(Optional)</span></label>
                        <input type="text" class="form-control form-control-sm  " id="mname" name="mname" placeholder="Middle Name"  @if(!Auth::guest())value="{{$mname}}" readonly @endif  autocomplete="mname">
                      </div>
                      <div class="col-md-4 mb-3">
                        <label >Last Name <span class="text-danger">*</span></label>
                        <input type="text" class="form-control form-control-sm  @error('lname') is-invalid @enderror " id="lname" name="lname" placeholder="Last Name"  @if(!Auth::guest())value="{{$lname}}" tabindex="-1" disabled @endif  autocomplete="lname" >
                        <div class="invalid-feedback">
                          Last Name is required.
                        </div>
                      </div>
                    </div>
                    @endif
                    <div class="row px-3">
                      <div class="col-md-4 mb-3">
                          <label for="fname">Gender <span class="text-danger">*</span></label>
                          <select id="gender" class="gender form-control custom-select custom-select-sm @error('gender') is-invalid @enderror" name="gender" @if(!Auth::guest()) readonly @endif>
                            <option value="" selected disabled>Select Gender</option>
                            <option value="1" @if($gender == 'M') selected @endif>Male</option>
                            <option value="2" @if($gender == "F") selected @endif>Female</option>
                          </select>
                          <input hidden readonly type="text" class="form-control form-control-sm  " id="gender1" name="gender1" >
                          <div class="invalid-feedback">
                            Gender is required.
                          </div>
                        </div>
                        <div class="col-md-4 mb-3">
                          <label for="branch_id">Branch  </label>
                          <select disabled class="form-control custom-select custom-select-sm @error('branch_id') is-invalid @enderror" id="branch_id"  type="text" name="branch_id" autocomplete="branch_id" @if(!Auth::guest()) disabled @endif>
                            <option value="" selected disabled>Select Branch</option>
                            @foreach($branches as $item)
                            <option id="branch" value="{{$item->id}}" {{ $branch == $item->id ? 'selected="selected"' : '' }}>{{$item->name}}</option>
                            @endforeach
                          </select>
                          <input hidden readonly type="text" class="form-control form-control-sm  " id="branch_id1" name="branch_id1" >
                          <div class="invalid-feedback">
                            Branch is required.
                          </div>
                        </div>
                        <div class="col-md-4 mb-3">
                          <label for="lname">Membership Type</label>
                          <select  id="member_type" class="form-control custom-select custom-select-sm @error('gender') is-invalid @enderror" name="member_type" @if(!Auth::guest()) disabled @endif>
                            <option value="" selected disabled>Select Type</option>
                            <option value="3"  >Non Member</option>
                            <option value="1" @if($type == 1) selected @endif>Associate</option>
                            <option value="2" @if($type == 2) selected @endif>Regular</option>
                          </select>
                          <input hidden readonly type="text" class="form-control form-control-sm  " id="member_type1" name="member_type1" >
                          <div class="invalid-feedback">
                            Membership Type is required.
                          </div>
                        </div>
                      </div>
                    <div class="row px-3">
                        <div class="col-md-6 mb-3">
                          <label for="branch_id">This feedback is for  <span class="text-danger">*</span> </label>
                          <select class="branch_loc form-control custom-select custom-select-sm @error('branch_loc') is-invalid @enderror" id="branch_loc"  type="text" name="branch_loc" autocomplete="branch_loc" >
                            <option value="" selected disabled>Select</option>
                            @foreach($branch_loc as $item)
                            <option value="{{$item->id}}" >{{$item->name}}</option>
                            @endforeach
                          </select>
                          <div class="invalid-feedback">
                            Branch Location is required.
                          </div>
                        </div>

                        <div class="col-md-6 mb-3">
                          <label for="branch_id">Date of Experience <span class="text-danger">*</span> </label>
                          <input type="date" class="form-control form-control-sm"  id="exp_date" max="{{\Carbon\Carbon::now()->format('Y-m-d')}}" name="exp_date">
                          <div class="invalid-feedback">
                            Date of Experience is required.
                          </div>
                        </div>
                      </div>
                </div>
              </div>

              <div class="card mt-4 mx-3 shadow-lg border-0">
                <div class="card-header bg-light text-primary pt-2 pl-3 border-0">
                  <h6 class="m-0 default-primary">Please specify the nature of your transaction  </h6>
                  <p class="small font-italic">Mangyaring tukuyin ang uri ng iyong transaksyon</p>
                </div>
                <div id="trading"  hidden class="card-body">
                  <div class="row px-3">
                    <label>MHRMPC Trading Product</label>
                    <input type="text" placeholder="Gumamit ng kuwit upang paghiwalayin ang bawat produkto(,)" disabled class="form-control trading"  name="trading">
                  </div>
                </div>
                <div id="fiestia"  hidden class="card-body">
                  <div class="row px-3">
                    <label>Fiestia Restaurant Product</label>
                    <input type="text" disabled placeholder="Gumamit ng kuwit upang paghiwalayin ang bawat produkto(,)" class="form-control fiestia" name="fiestia">
                  </div>
                </div>
                <div hidden id="branch_exp"   class="card-body">
                  <div class="row px-3">
                      <div id="divrsdeposit" class="col-md-3 mb-3">
                          <input  type="checkbox" class="branch_exp" name="rsdeposit" id="rsdeposit">
                          <label  for="rsdeposit">
                            Regular Savings Deposit
                          </label>
                      </div>
                      <div id="divpsdeposit"  class="col-md-3 mb-3">
                          <input  type="checkbox" class="branch_exp" id="psdeposit" name="psdeposit">
                          <label  for="psdeposit">
                            Purpose Savings Deposit
                          </label>
                      </div>
                      <div id="divtime_deposit"  class="col-md-3 mb-3">
                          <input  type="checkbox" class="branch_exp" id="time_deposit" name="time_deposit">
                          <label  for="time_deposit">
                            Time Deposit
                          </label>
                      </div>
                      <div  id="divshare_capital" class="col-md-3 mb-3">
                          <input  type="checkbox" class="branch_exp" id="share_capital" name="share_capital">
                          <label  for="share_capital">
                            Share Capital Deposit
                          </label>
                      </div>
                      <div id="divrswithdraw"  class="col-md-3 mb-3">
                          <input  type="checkbox" class="branch_exp" id="rswithdraw" name="rswithdraw">
                          <label  for="rswithdraw">
                            Regular Savings Withdrawal
                          </label>
                      </div>
                      <div  id="divpswithdraw" class="col-md-3 mb-3">
                          <input  type="checkbox" class="branch_exp" id="pswithdraw" name="pswithdraw">
                          <label  for="pswithdraw">
                            Purpose Savings Withdrawal
                          </label>
                      </div>
                      <div  id="divmember_termination" class="col-md-3 mb-3">
                          <input  type="checkbox" class="branch_exp" id="member_termination" name="member_termination">
                          <label  for="member_termination">
                            Membership Termination
                          </label>
                      </div>
                      <div   id="divmember_app" class="col-md-3 mb-3">
                          <input  type="checkbox" class="branch_exp" id="member_app" name="member_app">
                          <label  for="member_app">
                            Membership Application
                          </label>
                      </div>
                      <div  id="divloan_application" class="col-md-3 mb-3">
                          <input  type="checkbox" class="branch_exp" id="loan_application" name="loan_application">
                          <label  for="loan_application">
                            Loan Application
                          </label>
                      </div>
                      <div  id="divloan_release" class="col-md-3 mb-3">
                          <input  type="checkbox" class="branch_exp" id="loan_release" name="loan_release">
                          <label  for="loan_release">
                            Loan Release
                          </label>
                      </div>
                      <div  id="divbayad_center" class="col-md-3 mb-3">
                          <input  type="checkbox" class="branch_exp"  id="bayad_center" name="bayad_center" >
                          <label  for="bayad_center">
                            Bayad Center
                          </label>
                      </div>
                      <div  id="divmembership_inquiry" class="col-md-3 mb-3">
                          <input  type="checkbox" class="branch_exp" id="membership_inquiry" name="membership_inquiry" >
                          <label  for="membership_inquiry">
                            Membership Inquiry
                          </label>
                      </div>
                      <div  id="divPMES" class="col-md-3 mb-3">
                          <input type="checkbox" class="branch_exp" id="PMES" name="PMES">
                          <label  for="PMES">
                            Pre-Membership Education Seminar
                          </label>
                      </div>
                      <div  id="divmember_benefits" class="col-md-3 mb-3">
                          <input  type="checkbox" class="branch_exp" id="member_benefits" name="member_benefits">
                          <label  for="member_benefits">
                            Membership Benefits
                          </label>
                      </div>
                      <div id="divloan_payment" class="col-md-3 mb-3">
                          <input  type="checkbox" class="branch_exp" id="loan_payment" name="loan_payment">
                          <label  for="loan_payment">
                            Loan Payment
                          </label>
                      </div>
                    </div>
                </div>

              </div>
              <div class="card mt-4 mx-3 shadow-lg border-0">
                <div class="card-header bg-light text-primary pt-2 pl-3 border-0">
                  <h6 class="m-0 default-primary">Please rate below your transaction experience  </h6>
                  <p class="small font-italic">Paki-rate sa ibaba ng iyong karanasan sa transaksyon</p>
                </div>
                <div class="card-body">
                  <table class="table" id="">
                    <thead>
                      <tr>
                        <th scope="col">Rating Scale:<span class="text-danger"> *</span><br>1 - Needs Improvement <br> 2 - Fair <br> 3 - Satisfactory <br> 4 - Very Satisfactory <br> 5 - Outstanding</th>
                        <th class="rate" scope="col">★<br><br>1</th>
                        <th class="rate" scope="col">★★<br><br>2</th>
                        <th class="rate" scope="col">★★★<br><br>3</th>
                        <th class="rate" scope="col">★★<br>★★<br>4</th>
                        <th class="rate" scope="col">★★★<br>★★<br>5</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <th scope="row">Staff was courteous and helpful<br><p class="small font-italic">Magalang at matulungin ang staff</p></th>
                        <td class="rate radio-lm"><input   type="radio" value=1 name="rate1" id="1rate1"></td>
                        <td class="rate radio-lm"><input  type="radio" value=2 name="rate1" id="1rate2"></td>
                        <td class="rate radio-lm"><input  type="radio" value=3 name="rate1" id="1rate3"></td>
                        <td class="rate radio-lm"><input  type="radio" value=4 name="rate1" id="1rate4"></td>
                        <td class="rate radio-lm"><input  type="radio" value=5 name="rate1" id="1rate5"></td>
                      </tr>
                      <tr>
                        <th scope="row">Guard was courteous and helpful<br><p class="small font-italic">Magalang at matulungin ang guard</p></th>
                        <td class="rate radio-lm"><input   type="radio" value=1 name="rate2" id="2rate1"></td>
                        <td class="rate radio-lm"><input  type="radio" value=2 name="rate2" id="2rate2"></td>
                        <td class="rate radio-lm"><input  type="radio" value=3 name="rate2" id="2rate3"></td>
                        <td class="rate radio-lm"><input  type="radio" value=4 name="rate2" id="2rate4"></td>
                        <td class="rate radio-lm"><input  type="radio" value=5 name="rate2" id="2rate5"></td>
                      </tr>
                      <tr>
                        <th scope="row">The information provided to you by the staff are complete and accurate<br><p class="small font-italic">Ang impormasyong ibinigay sa iyo ng staff ay kumpleto at tumpak</p></th>
                        <td class="rate radio-lm"><input  type="radio" value=1 name="rate3" id="3rate1"></td>
                        <td class="rate radio-lm"><input  type="radio" value=2 name="rate3" id="3rate2"></td>
                        <td class="rate radio-lm"><input  type="radio" value=3 name="rate3" id="3rate3"></td>
                        <td class="rate radio-lm"><input  type="radio" value=4 name="rate3" id="3rate4"></td>
                        <td class="rate radio-lm"><input  type="radio" value=5 name="rate3" id="3rate5"></td>
                      </tr>
                      <tr>
                        <th scope="row">The response was appropriate and punctual<br><p class="small font-italic">Angkop at maagap ang tugon</p></th>
                        <td class="rate radio-lm"><input  type="radio" value=1 name="rate4" id="4rate1"></td>
                        <td class="rate radio-lm"><input  type="radio" value=2 name="rate4" id="4rate2"></td>
                        <td class="rate radio-lm"><input  type="radio" value=3 name="rate4" id="4rate3"></td>
                        <td class="rate radio-lm"><input  type="radio" value=4 name="rate4" id="4rate4"></td>
                        <td class="rate radio-lm"><input  type="radio" value=5 name="rate4" id="4rate5"></td>
                      </tr>
                      <tr>
                        <th scope="row">Office facility appearance is comfortable<br><p class="small font-italic">Ang pasilidad ng opisina ay kumportable</p></th>
                        <td class="rate radio-lm"><input  type="radio" value=1 name="rate5" id="5rate1"></td>
                        <td class="rate radio-lm"><input  type="radio" value=2 name="rate5" id="5rate2"></td>
                        <td class="rate radio-lm"><input  type="radio" value=3 name="rate5" id="5rate3"></td>
                        <td class="rate radio-lm"><input  type="radio" value=4 name="rate5" id="5rate4"></td>
                        <td class="rate radio-lm"><input  type="radio" value=5 name="rate5" id="5rate5"></td>
                      </tr>
                      <tr>
                        <th scope="row">My overall experience is positive<br><p class="small font-italic">Ang aking pangkalahatang karanasan ay positibo</p></th>
                        <td class="rate radio-lm"><input  type="radio" value=1 name="rate6" id="6rate1"></td>
                        <td class="rate radio-lm"><input  type="radio" value=2 name="rate6" id="6rate2"></td>
                        <td class="rate radio-lm"><input  type="radio" value=3 name="rate6" id="6rate3"></td>
                        <td class="rate radio-lm"><input  type="radio" value=4 name="rate6" id="6rate4"></td>
                        <td class="rate radio-lm"><input  type="radio" value=5 name="rate6" id="6rate5"></td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>

              <div class="card mt-4 mx-3 shadow-lg border-0">
                <div class="card-header bg-light text-primary pt-2 pl-3 border-0">
                  <h6 class="m-0 default-primary">Feedback: Comments/Suggestions <span class="text-danger"> *</span></h6>
                </div>
                <div class="card-body">
                  <textarea class="comment form-control" name="comment" id="comment"></textarea>
                </div>
              </div>


                <div class=" card-header bg-light text-primary pt-2 pl-3 border-0">
                  <h6 class="m-0 default-primary">Signature  
                    <a id="clear" class="btn btn-danger btn-sm" type="button"> Clear</a>
                  </h6>
                </div>
                <div  id="manual-sig" class="float-right col-md">
                    <canvas style="background-color: lightgrey;" id="signature"></canvas>
                    <input type="hidden" id="hidden_sig" name="hidden_sig">
                </div>
                <div>&nbsp;</div>
              <button class="btn btn-secondary mt-5 mb-5 btn-block" style="background-color: rgb(7,17,174);" id="btn-save-feedback" type="submit">Submit</button>
            </form>
          </div>
        </div>
      </div>
    </div>

<script src="https://cdn.jsdelivr.net/npm/signature_pad@2.3.2/dist/signature_pad.min.js"></script>
<script>
  var canvas = document.getElementById ("signature");
  var signaturePad = new SignaturePad(canvas);
  $('#clear').click(function (e) {
            e.preventDefault();
            signaturePad.clear();
            $("#signature").val('');
        });
  function validateForm() {
      let feedback_type = document.getElementById("feedback_type").value;
    if(feedback_type == 'non_member'){
      var fname = $('#fname').val();
      var mname = $('#mname').val();
      var lname = $('#lname').val();
      $('#fullname').val(fname+' '+mname+' '+lname);
    }
      let fullname = document.getElementById("fullname").value;
      let branch_loc = document.getElementById("branch_loc").value;
      let exp_date = document.getElementById("exp_date").value;
      let gender = document.getElementById("gender").value;
      let comment = document.getElementById("comment").value;
      let rate1 = $("input[type='radio'][name='rate1']:checked").val();
      let rate2 = $("input[type='radio'][name='rate2']:checked").val();
      let rate3 = $("input[type='radio'][name='rate3']:checked").val();
      let rate4 = $("input[type='radio'][name='rate4']:checked").val();
      let rate5 = $("input[type='radio'][name='rate5']:checked").val();
      let rate6 = $("input[type='radio'][name='rate6']:checked").val();
      let rateArray = [];
      rateArray += rate1+',';
      rateArray += rate2+',';
      rateArray += rate3+',';
      rateArray += rate4+',';
      rateArray += rate5+',';
      rateArray += rate6+',';
      if(branch_loc == 7){
         trading = $('.trading').val();
        // alert(trading);
      }else if( branch_loc == 8){
         fiestia = $('.fiestia').val();
      }else{
        // var branch_exp =$('.branch_exp').prop('checked');
        const yourArray = [];
        $("input:checkbox[class=branch_exp]:checked").each(function(){
            yourArray.push($(this).val());
        });
         checkbox = yourArray.toString().split(',');
          var branch_exp = '';
         if(checkbox[0] == ''){
          branch_exp = '';
         }else{
          branch_exp = 1;
         }
      }
       var ratesplit =  rateArray.includes(undefined); 
      var signatureType = signaturePad.isEmpty(); 
      
      var valmsg =  (fullname === '  ' ? "<br>Name must be filled out<br>" : "") 
      + (branch_loc === '' ? "<br>Feedback for must be filled out<br>" : "") 
      + (exp_date === '' ? "<br>Date of Experience must be filled out<br>" : "") 
      + (gender === '' ? "<br>Gender must be filled out<br>" : "") 
      + (trading === '' ? "<br>Trading Product must be filled out<br>" : "") 
      + (fiestia === '' ? "<br>Fiestia Restaurant Product must be filled out<br>" : "")
      + (branch_exp == '' ? "<br>Please specify the nature of your transaction<br>" : "")
      + (ratesplit == true ? "<br>Please  complete all of the survey rating<br>" : "") 
      + (comment === '' ? "<br>Comment/Suggestions must be filled out<br>" : "")
      + (signatureType ? "<br>Signature must be filled out<br>" : "") ;

      if (fullname === '  ' ||
       branch_loc == '' || 
       exp_date == '' ||  
       gender == ''  ||
       trading == '' ||  
       fiestia == '' ||
       branch_exp == '' ||
       ratesplit == true ||
       comment === '' ||
       signaturePad.isEmpty()
       ) {
      // alert(fullname);
        swal.fire({
          icon: 'warning',
          html:valmsg
        });
        return false;
      }
    }
  $('#btn-save-feedback').click(function (event) {
            // event.preventDefault();
  // var sig_val = signaturePad.value();
  var data = signaturePad.toDataURL('image/png');

  $('#hidden_sig').val(data);
  $("#gender"). attr("disabled", false);
  $("#branch_id"). attr("disabled", false);
  $("#member_type"). attr("disabled", false);
  $("#lname"). attr("disabled", false);
  // validateForm()
  // alert(data);
  });
</script>
 <script>
  // window.addEventListener("load", function() {

  var base_path = $("#url").val();
  var nonregmem_num = $("#nonregmem_num").val();
  var regmem_num = $("#regmem_num").val();
    $.lastFeedback = function (nonregmem_num) {
     $.ajax({
      type:"GET",
      url:base_path+'/lastFeedback',
      data: {nonregmem_num:nonregmem_num},
      success:function(res){
        // alert(JSON.stringify(res));
        if(res.status == 1){

        }else{
          Swal.fire({
            title: 'This member already did a feedback last: '+res.date,
            text: 'Create another feedback?',
            showDenyButton: true,
            confirmButtonText: 'Yes',
            denyButtonText: `No`,
          }).then((result) => {
              if (result.isConfirmed) {
                
              } else if (
                /* Read more about handling dismissals below */
                result.dismiss === Swal.DismissReason.deny
              ) {
                 window.location.href = base_path+'/';
              }
            })
        }
      },
      error: function (data) {
          alert(data.responseText);
      }              
     });
};

$('#lname').focusout(function(event){
  var fname = $('#fname').val();
  var lname = $('#lname').val();
  $.ajax({
      type:"GET",
      url:base_path+'/lastFeedbackNonRegNonMem',
      data: {fname:fname,
        lname:lname
      },
      success:function(res){
        // alert(JSON.stringify(res));
        if(res.status == 1){

        }else{
          Swal.fire({
            title: 'This member already did a feedback last: '+res.date,
            text: 'Create another feedback?',
            showDenyButton: true,
            confirmButtonText: 'Yes',
            denyButtonText: `No`,
          }).then((result) => {
              if (result.isConfirmed) {
                 
              } else if (
                /* Read more about handling dismissals below */
                result.dismiss === Swal.DismissReason.deny
              ) {
                 window.location.href = base_path+'/';
              }
            })
        }
      },
      error: function (data) {
          alert(data.responseText);
      } 

     });
});
function unselectCheckBox(){
  // rsdeposit
  document.getElementById("rsdeposit").checked = false;
  // psdeposit
  document.getElementById("psdeposit").checked = false;
  // time_deposit
  document.getElementById("time_deposit").checked = false;
  // share_capital
  document.getElementById("share_capital").checked = false;
  // rswithdraw
  document.getElementById("rswithdraw").checked = false;
  // pswithdraw
  document.getElementById("pswithdraw").checked = false;
  // member_termination
  document.getElementById("member_termination").checked = false;
  // member_app
  document.getElementById("member_app").checked = false;
  // loan_application
  document.getElementById("loan_application").checked = false;
  // loan_release
  document.getElementById("loan_release").checked = false;
  // bayad_center
  document.getElementById("bayad_center").checked = false;
  // membership_inquiry
  document.getElementById("membership_inquiry").checked = false;
  // PMES
  document.getElementById("PMES").checked = false;
  // member_benefits
  document.getElementById("member_benefits").checked = false;
  // loan_payment
  document.getElementById("loan_payment").checked = false;
}

$("#member_type").on("change", function(){
  var member_type = $(this).val();
  // alert(member_type);
  if(member_type != 3){
    // alert(member_type);
    $('#branch_id').attr('disabled', false);
  }else{
    $('#branch_id').attr('disabled', true);
  }
});
$("#branch_loc").on("change", function(){
  var feedback_for = $(this).val();
  // alert(feedback_for);
  if(feedback_for == 7){
    $("#branch_exp").attr('hidden', true);
    $("#trading").attr('hidden', false);
    $(".trading").attr('disabled', false);
    $("#fiestia").attr('hidden', true);
    $(".fiestia").attr('disabled', true);
    unselectCheckBox();
  }else if(feedback_for == 8){
    $("#branch_exp").attr('hidden', true);
    $("#fiestia").attr('hidden', false);
    $(".fiestia").attr('disabled', false);
    $("#trading").attr('hidden', true);
    $(".trading").attr('disabled', true);
    unselectCheckBox();
  }else{
    $("#branch_exp").attr('hidden', false);
    // $("#branch_exp").attr('disabled', false);
    $("#trading").attr('hidden', true);  
    $(".trading").val('');  
    $("#fiestia").attr('hidden', true);
    $(".fiestia").val('');

  }
});

$(document).ready(function($){
 // $.lastFeedback();
// console.log(data);
  if(regmem_num == null){
      Swal.fire({
        title: 'Are you an MHRMPC Member?',
        text: "Please input your member number. Otherwise, if you're not yet a member, click No and fill out the form",
        input: `text`,
        inputAttributes: {
          autocapitalize: 'on',
          placeholder:"Ex:SRR-0001, SMR-0002, 0001",
        },
        showCancelButton: true,
        confirmButtonText: 'Look up',
        cancelButtonText: 'No',
        showLoaderOnConfirm: true,
        allowOutsideClick: false,
        allowEscapeKey: false,
        preConfirm: (input) => {
          return fetch(base_path+`/feedbackAPI/`+input)
        .then(response => {
          // alert(JSON.stringify(input));
          if (!response.ok) {
            throw new Error(response.statusText)
          }
          return response.json()
        })
        .catch(error => {
          Swal.showValidationMessage(
            `Member Number provided does not match our entry`
          )
        })
        },
        // allowOutsideClick: () => !Swal.isLoading()
      }).then((result) => {
        if (result.isConfirmed) {
          // alert(JSON.stringify(result.value));
        // $("#fname"). attr("readonly", true);
        // $("#fname"). val(result.value[0].fname);
        // $("#mname"). attr("readonly", true);
        // $("#mname"). val(result.value[0].mname);
        // // $("#lname"). attr("readonly", true);
        // $("#lname"). attr("disabled", true);
        // $("#lname"). attr("tabindex", -1);

        $("#fullname_div"). attr("hidden", false);
        $("#FMLName"). attr("hidden", true);
        $("#fullname"). val(result.value[0].long_name);
        $("#gender"). attr("hidden", true);
        $("#gender1"). attr("hidden", false);
        $("#gender1"). val(result.value[0].gender);
        $("#gender"). val(1);
        $("#branch_id"). attr("hidden", true);
        $("#branch_id1"). attr("hidden", false);
        $("#branch_id1"). val(result.value[0].branch.name);
        $("#branch_id"). val(1);
        $("#member_type"). attr("hidden", true);
        $("#member_type1"). attr("hidden", false);
        if(result.value[0].member_type == 1){
          var type = 'Associate Member';
        }else{
          var type = 'Regular Member';
        }
        $("#member_type1"). val(type);
        $("#member_type"). val(1);
        $("#mem_num").html('| Member No: '+result.value[0].member_no);
         $("#mem_num").attr("hidden", false);
         $("#nonregmem").val(1);
         $("#nonregmem_num").val(result.value[0].member_no);
         $.lastFeedback(result.value[0].member_no);
        $("#feedback_type").val('member');

        }else if(
                /* Read more about handling dismissals below */
                result.dismiss === Swal.DismissReason.cancel){
          
        $("#feedback_type").val('non_member');
        $("#divrsdeposit"). attr("hidden", true);
        $("#divpsdeposit"). attr("hidden", true);
        $("#divtime_deposit"). attr("hidden", true);
        $("#divshare_capital"). attr("hidden", true);
        $("#divrswithdraw"). attr("hidden", true);
        $("#divpswithdraw"). attr("hidden", true);
        $("#divmember_app"). attr("hidden", false);
        $("#divPMES"). attr("hidden", false);
        $("#divmembership_inquiry"). attr("hidden", false);
        $("#divmember_benefits"). attr("hidden", true);
        $("#divmember_termination"). attr("hidden", true);
        $("#divloan_payment"). attr("hidden", true);
        $("#divloan_application"). attr("hidden", true);
        $("#divloan_release"). attr("hidden", true);
        }
      })
    }else{
   $.lastFeedback();
        
  }
    $.ajaxSetup({
        headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    

}); 
</script>
@endsection

