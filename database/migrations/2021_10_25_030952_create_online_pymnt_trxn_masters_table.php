<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOnlinePymntTrxnMastersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('online_pymnt_trxn_masters', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('usermember_id')->nullable();
            $table->foreign('usermember_id', 'opt_fk_usermember_id')->references('id')->on('user_member')->onUpdate('set null')->onDelete('set null');

            $table->unsignedBigInteger('bank_id');
            $table->foreign('bank_id', 'payment_fk_bank_id')->references('id')->on('bank_masters');
            $table->decimal('total_amt', 10, 2);
            $table->string('file_path')->nullable();
            $table->datetime('date_trxn_rcvd')->nullable();
            $table->datetime('date_trxn_or_entry')->nullable();
            $table->string('or_ref_no')->nullable();
            $table->string('or_by')->nullable();
            $table->integer('status')->nullable();
            $table->string('crea_by');
            $table->string('upd_by')->nullable();
            $table->datetime('soft_deleted_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('online_pymnt_trxn_masters');
    }
}
