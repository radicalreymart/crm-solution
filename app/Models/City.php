<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\ClientMaster;
use App\Models\Province;
use App\Models\Barangay;


class City extends Model
{
    use HasFactory;
    protected $table = 'cities';
    
   
    public function province()
    {
        return $this->belongsTo(Province::class);
    }
    public function barangay()
    {
        return $this->hasMany(Barangay::class);
    }
    public function clientMaster()
    {
        return $this->hasMany(ClientMaster::class);
    }
}
