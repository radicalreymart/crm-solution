<!-- Modal -->
<div wire:ignore.self class="modal fade" id="participantModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
       <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Participant List</h5>
                <button type="button" class="close" wire:click="cancel" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body table-responsive">
                    <table class="table">
                        <thead class=" text-primary">
                            <th>
                                Fullname
                            </th>
                            <th>
                                Contact Number
                            </th>
                            <th>
                                Email
                            </th>
                            <th>
                                Attendance
                            </th>
                        </thead>
                        <tbody>
                    @if(isset($this->participant))
                    @foreach($this->client_name as $items)
                    <tr>
                        <td>
                    {{$items->client->fullname}}
                        </td>
                        <td>
                    {{$items->client->contact_no}}
                        </td>
                        <td>
                    {{$items->client->gmail_address}}
                        </td>
                    @if($items->client->attendance == 1)
                        <td>
                        Present
                        </td>
                    @elseif($items->client->attendance == 2)
                        <td>
                        Absent
                        </td>
                    @else
                        <td>
                        Pending
                        </td>
                    @endif
                    </tr>
                    @endforeach
                    @endif
                        </tbody>
                    </table>
            </div>
            <div class="modal-footer">
                <button type="button" wire:click.prevent="cancel()" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
       </div>
    </div>
</div>