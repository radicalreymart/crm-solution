$(document).ready(function () {


        $('#master').on('click', function(e) {
             if($(this).is(':checked',true))  
             {
                $(".sub_chk").prop('checked', true);  
             } else {  
                $(".sub_chk").prop('checked', false);  
             }  
        });



        $('.delete_all').on('click', function(e) {


            var allVals = [];  
            $(".sub_chk:checked").each(function() {  
                allVals.push($(this).attr('data-id'));
                // alert(allVals);
            });  


            if(allVals.length <=0)  
            {  
                alert("Please select row.");  
            }  else {  

                // alert(check);
                // var check = document.getElementById("hiddenval").value;

                // var check = confirm("Are you sure you want to delete this row?");  
                // if(check == true){  

                //     var join_selected_values = allVals.join(","); 
                //     $.ajax({
                //         url: $(this).data('url'),
                //         type: 'POST',
                //         headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                //         data: 'ids='+join_selected_values,
                //         success: function (data) {
                //             if (data['success']) {
                //                 $(".sub_chk:checked").each(function() {  
                //                     $(this).parents("tr").remove();
                //                 });
                //                 // var err = JSON.stringify(data);
                //                 // alert(err);
                //                 alert(data['success']);
                //             // } else if (data['message']) {
                //             // alert(data['message']);
                //             // window.location.href = "password/confirm";
                //             } else if (data['error']) {
                //                 alert(data['error']);
                //             } else {
                //                 // alert(JSON.stringify(data));
                //                 alert('Whoops Something went wrong!!');
                //             }
                //         },
                //         error: function (data) {
                //             var err = JSON.parse(data.responseText);
                //             if(err['message'] == 'Password confirmation required.'){   
                //             alert(err['message']);

                //            $('#verifyModalLabel').html("Delete Data");
                //            $('#verifyModal').modal('show');
                //             // window.location.href = "password/confirm";
                //             }else{
                //             alert(data.responseText);
                //             // alert('please delete the pmes connected to this client/batch');
                //             }
                //         }
                //     });
                //   $.each(allVals, function( index, value ) {
                //       $('table tr').filter("[data-row-id='" + value + "']").remove();
                //   });
                // } 

                const swalWithBootstrapButtons = Swal.mixin({
                  customClass: {
                    confirmButton: 'btn btn-success',
                    cancelButton: 'btn btn-danger'
                  },
                  buttonsStyling: false
                })

                swalWithBootstrapButtons.fire({
                  title: 'Are you sure?',
                  text: "You won't be able to revert this!",
                  icon: 'warning',
                  showCancelButton: true,
                  confirmButtonText: 'Yes, delete it!',
                  cancelButtonText: 'No, cancel!',
                  reverseButtons: true
                }).then((result) => {
                  if (result.isConfirmed) {
                    var join_selected_values = allVals.join(","); 
                    $.ajax({
                        url: $(this).data('url'),
                        type: 'POST',
                        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                        data: 'ids='+join_selected_values,
                        success: function (data) {
                            // alert(JSON.stringify(data));
                            if (data['success']) {
                                $(".sub_chk:checked").each(function() {  
                                    $(this).parents("tr").remove();
                                });
                                swalWithBootstrapButtons.fire(
                                  'Deleted!',
                                  'Entry has been deleted.',
                                  'success'
                                )
                                // var err = JSON.stringify(data);
                                // alert(err);
                                // alert(data['success']);
                            // } else if (data['message']) {
                            // alert(data['message']);
                            // window.location.href = "password/confirm";
                            } else if (data['error']) {
                                alert(data['error']);
                            } else {
                                // alert(JSON.stringify(data));
                                alert('Whoops Something went wrong!!');
                            }
                        },
                        error: function (data) {
                            var err = JSON.parse(data.responseText);
                            if(err['message'] == 'Password confirmation required.'){   
                            alert(err['message']);

                           $('#verifyModalLabel').html("Delete Data");
                           $('#verifyModal').modal('show');
                            // window.location.href = "password/confirm";
                            }else{
                            alert(data.responseText);
                            // alert('please delete the pmes connected to this client/batch');
                            }
                        }
                    });
                  $.each(allVals, function( index, value ) {
                      $('table tr').filter("[data-row-id='" + value + "']").remove();
                  });

                    
                  } else if (
                    /* Read more about handling dismissals below */
                    result.dismiss === Swal.DismissReason.cancel
                  ) {
                    swalWithBootstrapButtons.fire(
                      'Cancelled',
                      ' ',
                      'error'
                    )
                  }
                }) 
            }  
        });


        // $('[data-toggle=confirmation]').confirmation({
        //     rootSelector: '[data-toggle=confirmation]',
        //     onConfirm: function (event, element) {
        //         element.trigger('confirm');
        //     }
        // });


        // $(document).on('confirm', function (e) {
        //     var ele = e.target;
        //     e.preventDefault();


        //     $.ajax({
        //         url: ele.href,
        //         type: 'POST',
        //         headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        //         success: function (data) {
        //             if (data['success']) {
        //                 $("#" + data['tr']).slideUp("slow");
        //                 alert(data['success']);
        //             } else if (data['error']) {
        //                 alert(data['error']);
        //             } else {
        //                 alert('Whoops Something went wrong!!');
        //             }
        //         },
        //         error: function (data) {
        //             alert(data.responseText);
        //         }
        //     });


        //     return false;
        // });
    });