<?php

namespace App\Exports;

use Carbon;
use App\Models\MIUF;
use App\Http\Controllers\MIUFController;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithCustomCsvSettings;
use PhpOffice\PhpSpreadsheet\Cell\Cell;
use PhpOffice\PhpSpreadsheet\Cell\DataType;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class MIUFExport   implements  FromQuery, WithHeadings, WithMapping, WithCustomCsvSettings,ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */
    protected $from_date;
    protected $to_date;

    public function __construct($from_date, $to_date) {
        $this->from_date = $from_date;
        $this->to_date = $to_date;
    }
    public function query()
    {
        $from = $this->from_date;
        $to = $this->to_date;
        // $to = Carbon\Carbon::now();
            $branch = auth()->user()->branch_id;
            $miuf  = MIUF::with('member','region', 'province', 'city', 'barangay');
            // dd($miuf->get());
            if($branch == 9){
                $miuf->orderByRaw('-status ASC');
            }else{
                $miuf->whereRelation('member', 'branch_id', $branch)
                ->orderByRaw('-status ASC');
                // ->orderBy('serv_status');
            }
            $miuf->whereBetween('created_at', [$from, $to]);
            $miuf = $miuf->orderBy('id');
        return $miuf;
    }
    
    
    // public function collection()
    // {
    //     return MIUF::all();
    // }
    
    public function getCsvSettings(): array
    {
        return [
            // 'line_ending' => '\n'
        ];
    }

    public function headings(): array
    {
        return [
             'member_no'
            , 'fname'
            , 'mname'
            , 'lname'
            , 'fullname'
            , 'email'
            , 'street'
            , 'barangay'
            , 'city'
            , 'province'
            , 'region'
            , 'zipcode'
            , 'fb_link'
            , 'mobile_no1'
            , 'mobile_no2'
            , 'mobile_no3'
            , 'landline_no1'
            , 'landline_no2'
            , 'landline_no3'
            , 'of_offm'
            , 'occupation_id'
            , 'montly_income'
            , 'emp_bus_name'
            , 'emp_bus_address'
            , 'emp_bus_startdate'
            , 'benificiaries1'
            , 'benificiaries2'
            , 'benificiaries3'
            // , 'signiture'
            , 'editable_at'
            , 'upload_by'
            , 'upload_date'
            , 'crea_by'
            , 'upd_by'
            , 'created_at'
            , 'updated_at'
        ];
    }

    public function map($miuf): array
    {
        // dd($miuf);
        $from = $this->from_date;
        $to = $this->to_date;
        if($miuf->occupation_id == null){
        $occupation = $miuf->other_occupation;
        }else{
        $occupation = $miuf->occupation->name;
        };
        if($miuf->region == null){
            $barangay = null;
             $city = null;
             $province = null;
             $region = null;
        }else{
             $barangay = $miuf->barangay->name;
             $city = $miuf->city->name;
             $province = $miuf->province->name;
             $region = $miuf->region->name;
        }
            // dd($miuf->get());
         $miuf_controller = new MIUFController;
         $miuf_controller->updateMIUF($miuf, $from, $to);

        return [
            // 'hello'
              "'".$miuf->member_no
            , $miuf->fname
            , $miuf->mname
            , $miuf->lname
            , $miuf->fullname
            , $miuf->email
            , $miuf->street
            , $barangay
            , $city
            , $province
            , $region
            , $miuf->zipcode
            , $miuf->fb_link
            , $miuf->mobile_no['mobile_no1']
            , $miuf->mobile_no['mobile_no2']
            , $miuf->mobile_no['mobile_no3']
            , $miuf->landline_no['landline_no1']
            , $miuf->landline_no['landline_no2']
            , $miuf->landline_no['landline_no3']
            , $miuf->of_offm
            , $occupation
            , $miuf->montly_income
            , $miuf->emp_bus_name
            , $miuf->emp_bus_address
            , $miuf->emp_bus_startdate
            , $miuf->benificiaries['relative1'].','.$miuf->benificiaries['relationship1']
            , $miuf->benificiaries['relative2'].','.$miuf->benificiaries['relationship2']
            , $miuf->benificiaries['relative3'].','.$miuf->benificiaries['relationship3']
            // , $miuf->signiture
            , Carbon\Carbon::parse($miuf->editable_at)->format('m-d-Y')
            , $miuf->upload_by
            , Carbon\Carbon::parse($miuf->upload_date)->format('m-d-Y')
            , $miuf->crea_by
            , $miuf->upd_by
            , Carbon\Carbon::parse($miuf->created_at)->format('m-d-Y')
            , Carbon\Carbon::parse($miuf->updated_at)->format('m-d-Y')
        ];
    } 
}
