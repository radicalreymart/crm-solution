<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LoanTypePayTerm extends Model
{
    use HasFactory;
    protected $table = 'loan_type_pay_terms';
    protected $fillable = [
    'loan_type_id',
    'loan_type_name',
    'payterm_id',
    'payterm_name',
    'crea_by',
    'upd_by',
    'soft_deleted_at',
    ];
}
