@extends('layouts.app', [
    'namePage' => 'Login page',
    'class' => 'login-page sidebar-mini ',
    'activePage' => 'login',
    'backgroundImage' => asset('assets') . "/img/bg14.jpg",
])

@section('content')
<style>
 ::-webkit-input-placeholder { /* Edge */
  color: blue !important;
}
:-ms-input-placeholder { /* Internet Explorer 10-11 */
  color: blue !important;
}
::placeholder {
  color: blue !important;
}
</style>
    <div  class="content">
           <div class="col-md" style="text-align: center !important;"> 
            <!-- @include('flash-message') -->
            @include('sweetalert::alert')
           </div>
        <div  class="container">
            <div class="col-md-12 ml-auto mr-auto">
                <div class="header bg-gradient-primary py-10 py-lg-2 pt-lg-12">
                    <div class="container">
                        <div class="header-body text-center mb-7">
                            <div class="row justify-content-center">
                                <div class="col-lg-12 col-md-9">
                                    <p class="text-lead text-light mt-3 mb-0">
                                        @include('alerts.migrations_check')
                                    </p>
                                </div>
                                <div class="col-lg-5 col-md-6">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
                <div  class="col-md-4 ml-auto mr-auto">
                    <input type="hidden" value="{{url('/')}}" id="url" name="url">
                    <form role="form" method="POST" action="{{ route('login') }}">
                        @csrf
                    <div style='background-image: url("assets/img/loginBG.jpg");
                    background-size: cover; border-radius: 200px; height: 600px;' class="card card-login card-plain">
                        <div class="card-header ">
                        <div align="center">
                            <img  src="{{ asset('assets/img/mhrlarge-removebg.png') }}" alt="">
                        </div>
                        </div>
                        <div class="card-body ">
                        <div class="no-border form-control-lg {{ $errors->has('email') ? ' has-danger' : '' }}">
                            <input class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" placeholder="{{ __('Email') }}" type="email" name="email" value="{{ old('email', 'membertest@gmail.com') }}" style="color: inherit;" required autofocus>
                        </div>
                        @if ($errors->has('email'))
                            <span class="invalid-feedback" style="display: block;" role="alert">
                            <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                        <div class=" form-control-lg {{ $errors->has('password') ? ' has-danger' : '' }}">
                            <input placeholder="Password" class="form-control {{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" style="color: black;" placeholder="{{ __('Password') }}" type="password" value="test1234" required>
                        </div>
                        @if ($errors->has('password'))
                            <span class="invalid-feedback" style="display: block;" role="alert">
                            <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                        </div>
                        <div class="card-footer ">
                        <button  type = "submit" class="btn btn-primary btn-round btn-lg btn-block mb-3">{{ __('Get Started') }}</button>
                        
                        <div >
                            <h6>
                            <a style="color: black;" href="{{ route('password.request') }}" class="link footer-link">{{ __('Forgot Password?') }}</a>
                            <a style="color: black;" href="{{ route('enduser.register') }}" class="link footer-link float-right">{{ __('Not Registered yet?') }}</a>
                            </h6>   
                        </div>
                        </div>
                    </div>
                    </form>
                </div>
        </div>
    </div>
@endsection

@push('js')
    <script>
        $(document).ready(function() {
        demo.checkFullPageBackgroundImage();
        });
    </script>
@endpush
