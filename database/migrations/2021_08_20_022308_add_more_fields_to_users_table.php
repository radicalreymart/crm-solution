<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddMoreFieldsToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        Schema::dropIfExists('users');

        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('uname');
            $table->string('password');
            $table->string('email');
            $table->unsignedBigInteger('branch_id')->nullable();
            $table->foreign('branch_id', 'branch_id')->references('id')->on('branch')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->unsignedBigInteger('department_id')->nullable();
            $table->foreign('department_id', 'department_id')->references('id')->on('department')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->integer('employee_role')->nullable();  
            $table->integer('role_id')->nullable();            
            $table->string('crea_by')->nullable();
            $table->string('upd_by')->nullable();
            $table->datetime('soft_deleted_at')->nullable();
            $table->rememberToken();
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
