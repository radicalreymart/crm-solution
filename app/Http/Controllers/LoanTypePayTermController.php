<?php

namespace App\Http\Controllers;

use App\Models\LoanTypePayTerm;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use App\Models\LoanType;
use App\Models\PaymentTerms;

class LoanTypePayTermController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $loanTypeTerm = LoanTypePayTerm::whereNull('soft_deleted_at')->get();
        return view('loanTypeTerm.index', compact('loanTypeTerm'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $loan_type = LoanType::all();
        $payment_terms = PaymentTerms::all();
        return view('loanTypeTerm.create', compact('loan_type', 'payment_terms'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'loan_type_id' => 'required',
            'payterm_id' => 'required',
        ]);
        $loan_type_name = LoanType::where('id', $request['loan_type_id'])->pluck('name')->first();
        $payterm_name = PaymentTerms::where('id', $request['payterm_id'])->pluck('name')->first();
        
        $loanTypeTerm = [
            'loan_type_id' => $request['loan_type_id'],
            'loan_type_name' => $loan_type_name,
            'payterm_id' => $request['payterm_id'],
            'payterm_name' => $payterm_name,
            'crea_by' => auth()->user()->uname,
        ];
        $loanTypeTerm_revive = ['id'=> $request->id,'soft_deleted_at' => null,];
        // dd($loanTypePayment_revive);
        LoanTypePayTerm::updateOrcreate ($loanTypeTerm_revive,$loanTypeTerm);
       if($request->id){
        return redirect()->route('loanTypeTerm.index')
            ->with('success', 'Loan Type-Payment Term updated successfully.');
       }else{
        return redirect()->route('loanTypeTerm.index')
            ->with('success', 'Loan Type-Payment Term created successfully.');
       }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\LoanTypePayTerm  $loanTypePayTerm
     * @return \Illuminate\Http\Response
     */
    public function show(LoanTypePayTerm $loanTypePayTerm)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\LoanTypePayTerm  $loanTypePayTerm
     * @return \Illuminate\Http\Response
     */
    public function edit(LoanTypePayTerm $loanTypePayTerm)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\LoanTypePayTerm  $loanTypePayTerm
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, LoanTypePayTerm $loanTypePayTerm)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\LoanTypePayTerm  $loanTypePayTerm
     * @return \Illuminate\Http\Response
     */
    public function destroy(LoanTypePayTerm $loanTypePayTerm)
    {
        //
    }
}
