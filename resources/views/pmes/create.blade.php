@extends('layouts.app', [
    'class' => 'sidebar-mini ',
    'namePage' => 'PMES Create',
    'activePage' => 'pmes-management',
    'activeNav' => '',
])

@section('pmesjs')
  <script src="{{ asset('assets') }}/js/batchschedule.js"></script>
@endsection
@section('content')
<div class="panel-header panel-header-sm">
  </div>
  <div class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="card-header">
                <div class="float-left">
                    <h4 class="card-title">New PMES</h4>
                </div>
                <div class="float-right">
                    <a class="btn btn-primary" href="{{ url('/pmes/') }}"> Back</a>
                </div>
          
        <div class="card">
                        <div class="card-body">
                                @if(count($errors) > 0 )
                            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                            <ul class="p-0 m-0" style="list-style: none;">
                            @foreach($errors->all() as $error)
                            <li>{{$error}}</li>
                            @endforeach
                            </ul>
                            </div>
                            @endif
                                <form method="POST" action="{{ route('pmes.store') }}">
                                    @csrf

                                <div class="form-group row">
                                    <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Client Name') }}</label>

                                    <div class="col-md-6">
                                        <select id="client_id" name="client_id" class="form-control">
                                        <option value="">Select Client</option>
                                        @foreach($clients as $client)
                                        <option id="client_id" value="{{$client->id}}">{{$client->fullname}}</option>
                                        @endforeach
                                        </select>
                                        @error('client_id')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Batch Code') }}</label>

                                    <div class="col-md-6">
                                       <select id="batch_id" name="batch_id" class="form-control">
                                        <option value="">Select Batch</option>
                                        @foreach($batches as $batch)
                                        <option value="{{$batch->id}}">{{$batch->batch_cd}}</option>
                                        @endforeach
                                        </select>
                                        @error('batch_id')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Schedule Date') }}</label>

                                    <div class="col-md-6">
                                        <input type="text" readonly id="sched_date" class="form-control" name="sched_date">
                                        @error('sched_date')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Schedule Time') }}</label>

                                    <div class="col-md-6">
                                        <input type="text" readonly id="sched_time" class="form-control" name="sched_time">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Preferred Application') }}</label>
                                    <div class="col-md-6">
                                    <input type="text" readonly class="form-control" id="online_vid_tool" name="online_vid_tool">
                                    </div>
                                </div>
                              <div class="form-group row mb-0">
                              <div class="col-md-6 offset-md-4">

                              <button type="submit" class="btn btn-primary"  >Submit</button>
                              </div>
                              </div>
                          </form>
        </div>
        </div>
      </div>
    </div>
  </div>

@endsection
