<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\TransactionType;

class AcctMaster extends Model
{
    use HasFactory;
    protected $table = 'acct_masters';
    // use HasFactory;
    protected $fillable = [
        'acct_cd',
        'description',
        'trxn_type_id',
        'crea_by',
        'created_at',
        'upd_by',
        'updated_at',
        'soft_deleted_at',
    ];
    public function transaction()
    {
        return $this->belongsTo('App\Models\TransactionType','trxn_type_id','id');
        // return $this->belongsTo(TransactionType::class);
    }
}
