@extends('layouts.app', [
    'class' => 'sidebar-mini ',
    'namePage' => 'PMES Index',
    'activePage' => 'pmes-management',
    'activeNav' => '',
])

@section('indexdeleteall')
  <script src="{{ asset('assets') }}/js/indexdeleteall.js"></script>
  <style>
    .dataTable-top > nav:last-child, .dataTable-top > div:last-child, .dataTable-bottom > nav:last-child, .dataTable-bottom > div:last-child{
      float: none;
    }
  </style>
@endsection
@section('content')
    @include('auth.passwords.confirmmodal')

<div class="panel-header panel-header-sm">
  </div>
  <div class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header">
            @include('flash-message')
                <div class="float-left">
                  <div style="padding-left: 20px;" class="row"><p class="card-title h4">PMES</p><i style="padding-top:25px; padding-left: 10px;" class="text-muted">Pre-Member Education Seminar</i></div>
                </div>
                <div class="float-right">
                    <button class="btn btn-danger delete_all" data-url="{{ url('DeleteAllPMES') }}">Delete All Selected</button>
                    <form action="{{ route('exportPMES') }}"  >
                        {{ csrf_field() }}
                        Start Date
                    <input style="right: 5px;" class="form-control  start" max="{{ Carbon\Carbon::now()->format('Y-m-d'); }}" value="{{Carbon\Carbon::now()->format('Y-m-d'); }}" type="date" id="start" name="start">
                        End Date
                    <input style="right: 5px;" class="form-control  start" max="{{ Carbon\Carbon::now()->format('Y-m-d'); }}" value="{{Carbon\Carbon::now()->format('Y-m-d'); }}" type="date" id="end" name="end">
                        <button class="btn btn-primary" id="start"   >Download Excel </button>
                    </form>
                    
                </div>
          </div>
          <div class="card-body">
            <div id="modalCatcher" class="table-responsive">
              <input type="hidden" value="{{url('/')}}" id="url" name="url">
              <table id="datatablesSimple" class="table">
                <thead class=" text-primary">
                    <th ><input type="checkbox" id="master"></th>
                    <th >Action</th>
                    <th >Client Name</th>
                    <th >Client Status</th>
                    <th>Batch Code</th>
                    <th>Schedule Date</th>
                    <th>Schedule Time</th>
                    <th>Preffered App./<br> Batch App.</th>
                    <th>Service Status</th>
                    <th>Attendance</th>
                    <th>Membership Number</th>
                    <th>Date Created</th>
                    <th>Updated By</th>
                    <th>Date Updated</th>
                </thead>
                <tbody>
                   
                    @if($pmes->count())
                    @foreach ($pmes as $data)
                        <tr>
                        <td><input type="checkbox" class="sub_chk" data-id="{{$data->id}}"></td>
                        <td>
                          <div style="justify-content: center;" class="  form-group row" >
                          <a href="javascript:void(0)" title="Edit PMES" class="edit" data-id="{{ $data->id }}">
                            <i class="fas fa-edit  fa-lg"></i>
                          </a>
                          </div>
                        @if(isset($data->batch_id))
                            <div class="btn-group open">
                                <button  class=" dropdown-toggle" title="PMES Attendance" data-toggle="dropdown" >
                                <a class="now-ui-icons design_bullet-list-67"></a>
                                </button>
                                <div  class="dropdown-menu">
                                  <form action="{{ route('attendance', $data->id) }}" method="post">
                                     @method('put')
                                     @csrf
                                    <input type="text" hidden name="attendance" value=1>
                                  <button type="submit"  class="dropdown-item present "><a class="dropdown-item present " style="font-size: 20px;   " data-id="{{ $data->id }}">Present</a></button>
                                  </form>
                                  <form action="{{ route('attendance', $data->id) }}" method="post">
                                     @method('put')
                                     @csrf
                                    <input type="text" hidden name="attendance" value=2>
                                  <button type="submit" class="dropdown-item absent "><a class="dropdown-item present " style="font-size: 20px; " data-id="{{ $data->id }}">Absent</a></button>
                                  </form>
                                </div>
                            </div>
                        @endif
                        </td>
                        @if(is_null($data->client_id))
                          <td>Null</td>
                        @else
                          <td>{{ $data->client->fullname }}</td>
                          @if($data->client->sched_stat == 0)
                              <td>Unverified Email</td>
                              @elseif($data->client->sched_stat == 1)
                              <td>Batch Pending</td>
                              @else
                              <td>Email Sent</td>
                          @endif
                        @endif
                        @if(is_null($data->batch_id))
                          <td>Null</td>
                          <td>Null</td>
                          <td>Null</td>
                          <td>Null</td>
                        @else
                          <td>{{ $data->batch->batch_cd }}</td>
                          <td>{{ \Carbon\Carbon::parse($data->batch->sched_date)->format('m-d-Y')}}</td>
                          <td>{{ \Carbon\Carbon::parse($data->batch->sched_date)->format('H:i a')}}</td>
                           
                           <td>
                            @if(isset($data->batch->online_vid_tool))
                            @php($ovt = $data->batch->online_vid_tool)
                            @else
                            @php($ovt = null)
                            @endif
                            @php($povt = $data->online_vid_tool)
                           {{ $povt === 0 ? 'Google Meet' : ($povt === 1 ? 'Zoom' : 'Walk-in')}}/
                           <br>
                           {{ $ovt === 0 ? 'Google Meet' : ($ovt === 1 ? 'Zoom' : ($ovt === 2 ? 'Walk-in' : ''))}}
                           </td>

                        @endif

                          <td>
                            @php ($ss = $data->serv_status) 
                          {{$ss === 0 ? "Batch Pending" : ($ss == 1 ? "Email Sent" : "Done")}}
                          
                          </td>
                        @if($data->attendance == 1)
                          <td>Present</td>
                          @elseif($data->attendance == 2)
                          <td>Absent</td>
                          @else
                          <td>Pending</td>
                        @endif
                        @if(is_null($data->membership))
                          <td></td>
                          @else
                          <td>{{$data->membership}}</td>
                        @endif 
                        
                        <td>{{ date_format($data->created_at, 'm-d-Y') }}</td>

                        @if(isset($data->upd_by))
                          <td>{{ $data->upd_by }}</td>
                          @else
                          <td></td>
                        @endif
                        @if(isset($data->updated_at))
                          <td>{{ date_format($data->updated_at, 'jS M Y') }}</td>
                          @else
                          <td></td>
                        @endif
                        
                        </tr>
                    @endforeach
                    @endif
                </tbody>
              </table>

            </div>
          </div>
        </div>
      </div>
      
    </div>
  </div>
    
    <!-- boostrap model -->
    <div class="modal fade" id="ajax-pmes-model" aria-hidden="true" data-backdrop="static" data-keyboard="false">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title" id="ajaxPMESModel"></h4>
                <button type="button" class="btn btn-default close-modal" id="close-modal" >Close</button>
          </div>
          <div class="modal-body">
            <form action="javascript:void(0)" class="form-horizontal" method="POST">
              <input type="hidden" name="id" id="id">
              <div class="form-group">
                <label for="name" class="col-sm-2 control-label">Client Name</label>
                <div class="col-sm-12">
                  <input type="text" class="form-control" id="name" name="name" readonly value="" maxlength="50" required="">
                </div>
              </div> 

              <div class="form-group">
                <label for="name" class="col-sm-2 control-label">Batch Code</label>
                <div class="col-sm-12">
                  <select class="form-control m-bot15" id="batch_id1" name="batch_id">
                  @if ($batches->count())
                  <option value="" selected disabled >Select batch</option> 
                  @foreach($batches as $batch)
                  <option value="{{ $batch->id }}" >{{ $batch->batch_cd }}</option> 
                  @endforeach   
                  @endif
                  </select>
                </div>
              </div> 

              <div class="form-group" id="remarkdiv" >
                <label for="name" class="col-sm-2 control-label">Remarks</label>
                <div class="col-sm-12">
                  <textarea id="resched_remarks" style="border-color: black;" class="form-control" name="resched_remarks"  required autocomplete="resched_remarks" autofocus></textarea>
                </div>
              </div>

              <div class="form-group" id="membershipdiv" >
                <label for="name" class="col-sm-2 control-label">Membership Number</label>
                <div class="col-sm-12">
                  <input type="text" class="form-control"  id="membership" name="membership"  value="" maxlength="50" required="">
                </div>
              </div>

              <div class="form-group">
                <label for="name" class="col-sm-2 control-label">Schedule Date</label>
                <div class="col-sm-12">
                  <input type="text" class="form-control" id="sched_date" name="sched_date" readonly value="" maxlength="50" required="">
                </div>
              </div>

              <div class="form-group">
                <label class="col-sm-2 control-label">Schedule Time</label>
                <div class="col-sm-12">
                  <input type="text" class="form-control" id="sched_time" name="sched_time" readonly  value="" required="">
                </div>
              </div>

              <div class="form-group">
                <label class="col-sm-2 control-label">Preferred Tool</label>
                <div class="col-sm-12">
                  <input type="text" class="form-control" id="tool" name="tool" readonly value="" required="">
                </div>
              </div>

              <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-primary btn-save" id="btn-save">Save
                </button>
              </div>
            </form>
          </div>
          <div class="modal-footer">
            
          </div>
        </div>
      </div>
    </div>
<!-- end bootstrap model -->
<script>
  // // window.addEventListener("load", function() {
  //   window.addEventListener('DOMContentLoaded', event => {
  //   // Simple-DataTables
  //   // https://github.com/fiduswriter/Simple-DataTables/wiki
    
  //   const datatablesSimple = document.getElementById('datatablesSimple');
  //   if (datatablesSimple) {
  //       new simpleDatatables.DataTable(datatablesSimple, {
  //   searchable: false,
  //   fixedHeight: true
  //       });
  //   }
  // });
$(document).ready(function(){
  $('#datatablesSimple').DataTable({

  });
  const swalWithBootstrapButtons = Swal.mixin({
                  customClass: {
                    confirmButton: 'btn btn-success',
                    cancelButton: 'btn btn-danger'
                  },
                  buttonsStyling: false
                }) 
  $.clearInput = function () {
        $('insert_form').find('input[type=text], input[type=password], input[type=number], input[type=email], select, textarea').val('');
    };
     $('.modal').on('hidden.bs.modal', function(){
             // window.location.reload();
             // $('#insert_form').removeData('bs.modal')
    $.clearInput();
    $(this).find('form')[0].reset();
    });

    $.ajaxSetup({
        headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('#addNewPMES').click(function () {
       $('#addEditPMESForm').trigger("reset");
       $('#ajaxPMESModel').html("Add PMES");
       $('#ajax-pmes-model').modal('show');
    });
    $('.close-modal').click(function () {
      var modal_array = 
    [
      '#ajax-pmes-model'
    ];
      closeModal(modal_array);
       // $('#ajax-pmes-model').modal('hide');
    });
      // $('#modalCatcher').on('click', '#btn-save', function () {
    $('#btn-save').click(function (event) {
         // alert('hello');
          try{
          var id = $("#id").val();
          var batch_id = $("#batch_id1").val();
          var membership = $("#membership").val();
          var resched_remarks = $("#resched_remarks").val();

          $("#btn-save").html('Please Wait...');
          $("#btn-save"). attr("disabled", true);
          $("#close-modal").html('Please Wait...');
          $("#close-modal").attr("disabled", true);           
              }
            catch(err) {
            swalWithBootstrapButtons.fire({
                  title: 'Error!',
                  text: err,
                  icon: 'warning',
                  confirmButtonText: 'Confirmed',
                }).then((result) => {
             // window.location.reload();
            $("#btn-save").html('Submit');
            $("#btn-save"). attr("disabled", false);
                })
            }
        // event.preventDefault();
        // ajax
        $.ajax({
            type:"POST",
            url: "{{ url('add-update-pmes') }}",
            // headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            data: {
              id:id,
              batch_id:batch_id,
              membership:membership,
              resched_remarks:resched_remarks,
            },
            dataType: 'json',
            success: function(res){
              // alert('success');
            swalWithBootstrapButtons.fire({
                  title: 'Success!',
                  icon: 'success',
                  confirmButtonText: 'Confirmed',
                }).then((result) => {
             window.location.reload();})              // alert(JSON.stringify(res));
            $("#btn-save").html('Submit');
            $("#btn-save"). attr("disabled", false);
           }
          //   ,error: function(xhr, status, error){
          //  var errorMessage = xhr.status + ': ' + xhr.statusText
          //  alert('Error? - ' + errorMessage);
           
          // }
          
           ,error: function (data) {
            // console.log(data.responseText);
                alert(data.responseText);
            }
        });

    });
     $('#modalCatcher').on('click', '.edit', function () {

        var id = $(this).data('id');
                var base_path = $("#url").val();
        // ajax
        //   alert( base_path );
        $.ajax({
            type:"POST",
            url: base_path+"/edit-pmes",
            data: { id: id },
            dataType: 'json',
            success: function(res){
                // alert(JSON.stringify(res));
                // var date = res.sched_date->format('m-d-Y');
                if(res.tool == 0){
                  var tool = 'Google Meet';
                }else if(res.tool == 1){
                  var tool = 'Zoom';
                }else if(res.tool == 2){
                  var tool = 'Walk-in';
                }
              $('#membershipdiv').hide();
              $('#ajaxPMESModel').html("Edit PMES");
              $('#ajax-pmes-model').modal('show');
              $('#id').val(res.id);
              $('#name').val(res.fullname);
              $('#sched_date').val(res.sched_date);
              $('#sched_time').val(res.sched_time);
              $('#tool').val(tool);
              $('#membership').val(res.membership);
              $('#batch_id1').val(res.batch_id);
              if(res.batch_id != null){
              $('#remarkdiv').show();
                }
                else{
              $('#remarkdiv').hide();
                }
              if(res.attendance == 1){
              $('#membershipdiv').show();
              $('#remarkdiv').hide();
                }
                else{
              $('#membershipdiv').hide();
                }
           },
            error: function (data) {
                 var err = JSON.parse(data.responseText);
                 if(err['message'] == 'Password confirmation required.'){   
                 alert(err['message']);
                 window.location.href = "password/confirm";
                 }else{
                 // alert('please delete the pmes connected to this client/batch');
                 alert(data.responseText);
                 }
             }
        });

    });


});
</script>
  <script src="{{ asset('assets') }}/js/batchschedule.js"></script>



@endsection


