<?php

namespace App\Http\Controllers;

use App\Models\TransactionType;
use App\Models\AcctMaster;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\UserRequest;
use Alert;

class TransactionTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $department = auth()->user()->department_id;
        $isAdmin = auth()->user()->role_id;
        $branch = auth()->user()->branch_id;
        // dd(auth()->user()->uname);
        if($department == 3 || $isAdmin == 0){
            
            $transaction  = TransactionType::whereNull('soft_deleted_at')->get();
            
            return view('transactionType.index')
            ->with('transaction', $transaction );
        }else{
            Alert::error("You do not have the permission to access this page.");
            return redirect()->route('home');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('transactionType.create');
     
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate([
            'cd' => 'required',
            'description' => 'required',
        ]);
       TransactionType::create([
             'cd' => $request['cd'],
             'description' => $request['description'],
            'crea_by' => auth()->user()->uname,
        ]);
       
       
        return redirect()->route('transactionType.index')
            ->with('success', 'Transaction created successfully.');
    }

    public function storetransaction(Request $request)
    {
        $request->validate([
            'cd' =>'required',
            'description' =>'required',  
        ],
        ['cd.required' => 'Code is required',
        'description.required' => 'Description is required',
        ]);
        if(isset($request->id)){
            $upd_by = auth()->user()->uname;
            $updated_at = Carbon::now();
        }else{
            $upd_by = null;
            $updated_at = null;
        }
        $transaction_upd = 
        [
            'id' => $request->id,
        ];
        $transaction_create = 
        [
            'cd' => $request->cd,
            'description' => $request->description,
            'crea_by'     => auth()->user()->uname,
            'upd_by'         => $upd_by,
            'updated_at'     => $updated_at,
        ];
        TransactionType::updateOrcreate($transaction_upd, $transaction_create);
        if($request->id != null){
        $request->session()->flash('success', 'Transaction updated successfully!');
        }else{
        $request->session()->flash('success', 'Transaction created successfully!');    
        }
        return response()->json(['success' => 'success'], 200);
        // return redirect()->route('pmes.index')->with('success', 'PMES created successfully.');
      
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Project  $project
     * @return \Illuminate\Http\Response
     */
    // public function show($id, User $user)
    // {
    //     $user = User::find($id);
        
        
       
    //       return view('userTable.show', [
    //     'user' => $user
    //     ]);
    // }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $transaction  = TransactionType::find($id);
       
       return view('transactionType.edit', [
        'transaction' => $transaction 
        ]);

    }
    public function editmodal(Request $request, TransactionType $transaction)
    {
        //
        $where = array('id' => $request->id);
        $transaction  = TransactionType::where($where)->first();
        $json = array('id' => $request->id
            , 'cd' => $transaction->cd
            , 'description' => $transaction->description);
         // $request->session()->flash('success', 'transaction updated successfully!');
        return response()->json($json);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request)
    {
        $rules = array(
            'cd' => 'required',
            'description' => 'required',
        );
         $validator = Validator::make($request->all(), $rules);
         $transaction  = TransactionType::find($id);
            $transaction ->cd      = $request->get('cd');
            $transaction ->description       = $request->get('description');
            $transaction ->updated_at      = Carbon::now();
            $transaction ->upd_by = auth()->user()->uname;
            $transaction ->save();
       
        return redirect()->route('transactionType.index')
            ->with('success', 'Transaction updated successfully');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Project  $project
     * @return \Illuminate\Http\Response
     */
    
    public function destroy($id)
    {
        $transaction  = TransactionType::find($id);
        $transaction ->delete();

        return response()->json(['success'=>"Transaction deleted successfully.", 'tr'=>'tr_'.$id]);

    }
    public function deleteAll(Request $request)
    {

        $ids = $request->ids;
        $transaction = TransactionType::whereIn('id',explode(",",$ids))->get();
        $account = AcctMaster::whereIn('trxn_type_id',explode(",",$ids))->whereNull('soft_deleted_at')->get();
        if($account->count())
        {
            $json = ['error' => 'Please delete Account corresponding to this entry.'];
        }else
        {
            if($transaction->count())
            {
                foreach($transaction as $data)
                {
                    $transaction = TransactionType::find($data->id);
                    $transaction->soft_deleted_at = Carbon::now();
                    $transaction->save();
                }
            }
            $json = ['success'=>"Transaction Deleted successfully."];
        }
        return response()->json($json);
    }
}
