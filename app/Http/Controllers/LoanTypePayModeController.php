<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use App\Models\LoanTypePayMode;
use App\Models\LoanType;

class LoanTypePayModeController extends Controller
{
    //
    public function index()
    {
        $loanTypePayment = LoanTypePayMode::whereNull('soft_deleted_at')->get();
        return view('loanTypePayment.index', compact('loanTypePayment'));
        
    }

    public function create()
    {
        $loan_type = LoanType::all();
        return view('loanTypePayment.create', compact('loan_type'));
     
    }

    public function store(Request $request)
    {
        // dd($request->all());

        $request->validate([
            'loan_type_id' => 'required',
            'paymode_id' => 'required',
        ]);
        $loan_type_name = LoanType::where('id', $request['loan_type_id'])->pluck('name')->first();
        // dd($loan_type_name);
        switch ($request['paymode_id']) {
            case '1':
                $paymode_name = 'Daily';
                break;
            case '2':
                $paymode_name = 'Weekly';
                break;
            case '3':
                $paymode_name = 'Semi-Monthly';
                break;
            case '4':
                $paymode_name = 'Monthly';
                break;
            case '5':
                $paymode_name = 'Lumpsum';
                break;
            case '6':
                $paymode_name = 'Lumpsum (for 1 month only)';
                break;
            default:
                $paymode_name = null;
                break;
        }
        $loanTypePayment = [
            'loan_type_id' => $request['loan_type_id'],
            'loan_type_name' => $loan_type_name,
            'paymode_id' => $request['paymode_id'],
            'paymode_name' => $paymode_name,
            'crea_by' => auth()->user()->uname,
        ];
        $loanTypePayment_revive = ['id'=> $request->id,'soft_deleted_at' => null,];
        // dd($loanTypePayment_revive);
       LoanTypePayMode::updateOrcreate ($loanTypePayment_revive,$loanTypePayment);
       if($request->id){
        return redirect()->route('loanTypePayment.index')
            ->with('success', 'Loan Type-Payment Mode updated successfully.');
       }else{
        return redirect()->route('loanTypePayment.index')
            ->with('success', 'Loan Type-Payment Mode created successfully.');
       }

    }

    public function edit($id)
    {
        $loanTypePayment = LoanTypePayMode::find($id);
        $loan_type = LoanType::all();
       
       return view('loanTypePayment.edit', compact('loanTypePayment', 'loan_type'));

    }

    public function update($id, Request $request, LoanTypePayMode $loanTypePayment)
    {
        
    }


    public function deleteAll(Request $request, LoanTypePayMode $loanTypePayment)
    {
        $ids = $request->ids;
        $loanTypePayment = LoanTypePayMode::whereIn('id',explode(",",$ids))->whereNull('soft_deleted_at')->get();
        if($loanTypePayment->count())
        {
            foreach($loanTypePayment as $data)
            {
                $loanTypePay = LoanTypePayMode::find($data->id);
                $loanTypePay->soft_deleted_at = Carbon::now();
                $loanTypePay->save();
            }
        }
                return response()->json(['success'=>"LoanTypePayMode deleted successfully."]);

    }

}
