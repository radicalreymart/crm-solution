<?php

namespace App\Http\Controllers;

use App\Models\Department;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\UserRequest;

class DepartmentController extends Controller
{
    //
    public function index(Department $department)
    {
        $department = Department::whereNull('soft_deleted_at')->get();
        return view('department.index')
            ->with('department', $department);
        
    }
    public function create()
    {

        return view('department.create');
     
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate([
            'cd' => 'required',
            'name' => 'required',
        ]);
        $department = [
             'cd' => $request['cd'],
             'name' => $request['name'],
            'crea_by' => auth()->user()->uname,
        ];
        $department_revive = ['soft_deleted_at' => null,];
       
       Department::updateOrcreate ($department,$department_revive);
       
        return redirect()->route('department.index')
            ->with('success', 'department created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Project  $project
     * @return \Illuminate\Http\Response
     */
    // public function show($id, User $user)
    // {
    //     $user = User::find($id);
        
        
       
    //       return view('userTable.show', [
    //     'user' => $user
    //     ]);
    // }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $department = Department::find($id);
       
       return view('department.edit', [
        'department' => $department
        ]);

    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request, Department $department)
    {
        $rules = array(
            'cd' => 'required',
            'name' => 'required',
        );
         $validator = Validator::make($request->all(), $rules);
         $department = Department::find($id);
            $department->cd      = $request->get('cd');
            $department->name       = $request->get('name');
            $department->updated_at      = Carbon::now();
            $department->upd_by = auth()->user()->uname;
            $department->save();
       
        return redirect()->route('department.index')
            ->with('success', 'department updated successfully');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Project  $project
     * @return \Illuminate\Http\Response
     */
    
    public function destroy($id)
    {
        $department = Department::find($id);
        $department->delete();

        return response()->json(['success'=>"department deleted successfully.", 'tr'=>'tr_'.$id]);
    }
    public function deleteAll(Request $request, Department $department)
    {
        $ids = $request->ids;
        $departments = Department::whereIn('id',explode(",",$ids))->whereNull('soft_deleted_at')->get();
        if($departments->count())
        {
            foreach($departments as $data)
            {
                $department = Department::find($data->id);
                $department->soft_deleted_at = Carbon::now();
                $department->save();
            }
        }
                return response()->json(['success'=>"department deleted successfully."]);

    }
}
