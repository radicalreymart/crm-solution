<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\ClientMaster;
use App\Models\Region;
use App\Models\City;
class Province extends Model
{
    use HasFactory;
    protected $table = 'provinces';
   
    public function region()
    {
        return $this->belongsTo(Region::class);
    }
    public function city()
    {
        return $this->hasMany(City::class);
    }
    public function clientMaster()
    {
        return $this->hasMany(ClientMaster::class);
    }
}
