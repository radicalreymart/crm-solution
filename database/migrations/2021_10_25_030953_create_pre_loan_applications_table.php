<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePreLoanApplicationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pre_loan_applications', function (Blueprint $table) {
            $table->id();
            // $table->string('email');
            // $table->string('fullname');

            $table->unsignedBigInteger('usermember_id')->nullable();
            $table->foreign('usermember_id', 'pla_fk_usermember_id')->references('id')->on('user_member')->onUpdate('set null')->onDelete('set null');

            $table->unsignedBigInteger('branch_id')->nullable();
            $table->foreign('branch_id', 'pla_fk_branch_id')->references('id')->on('branch_id')->onUpdate('set null')->onDelete('set null');

            $table->string('file_path1', 500)->nullable();
            $table->string('signiture')->nullable();
            // $table->string('contact_no');
            // $table->integer('age_group');
            // $table->integer('gender');
            // $table->string('fb_link');
            // $table->address();
            // $table->integer('member_status')->nullable();
            $table->decimal('amount_needed', 10, 2);
            $table->integer('savings-salary-amount')->nullable();
            $table->integer('loanable_amount')->nullable();
            $table->decimal('share_capital', 10, 2)->nullable();
            $table->string('co_maker')->nullable();
            $table->decimal('amount_approved', 10, 2)->nullable();
            $table->string('approval_remarks')->nullable();
            $table->decimal('montly_expense', 10, 2);
            $table->decimal('montly_income', 10, 2);
            $table->string('ref_number', 15)->index()->nullable();
            // $table->string('ref_number', 15);
            // $table->integer('loan_product');
            $table->unsignedBigInteger('loan_product')->nullable();
            $table->foreign('loan_product', 'pla_fk_loan_product')->references('id')->on('loan_types')->onUpdate('set null')->onDelete('set null');

            $table->integer('loan_purpose')->nullable();
            $table->string('other_purpose')->nullable();
            $table->integer('payment_terms');
            $table->integer('payment_mode');
            $table->integer('loan_status')->nullable();

            $table->unsignedBigInteger('marketing_channel_id')->nullable();
            $table->foreign('marketing_channel_id', 'pla_fk_marketing_channel_id')->references('id')->on('marketing_channel')->onUpdate('set null')->onDelete('set null');

            $table->unsignedBigInteger('occupation_id')->nullable();
            $table->foreign('occupation_id', 'pla_fk_occupation_id')->references('id')->on('occupation')->onUpdate('set null')->onDelete('set null');
            $table->string('other_occupation');

            $table->integer('data_consent_fl');
            $table->string('crea_by');
            $table->string('upd_by')->nullable();
            $table->timestamps();
            $table->datetime('soft_deleted_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pre_loan_applications');
    }
}
