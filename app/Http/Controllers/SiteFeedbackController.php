<?php

namespace App\Http\Controllers;

use App\Models\SiteFeedback;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Http\Requests\UserRequest;
use Storage;
use File;
use Excel;
use App\Exports\SiteFeedbackExport;
use App\Models\Branch;
use App\Models\UserMember;
use App\Models\Member;
use Alert;

class SiteFeedbackController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $department = auth()->user()->department_id;
        $branch = auth()->user()->branch_id;
        $isAdmin = auth()->user()->role_id;
        if($department == 1 || $isAdmin == 0){

            $sitefeedback = SiteFeedback::with('branch');

            if($branch == 9){
                $sitefeedback->orderBy('last_feedback', 'desc');
            }else{
                $sitefeedback->where('branch_id', $branch)
                ->orderBy('last_feedback', 'desc');
            }

            $sitefeedback = $sitefeedback->get();
            // dd($feedback);
            return view('feedback.SFindex', compact('sitefeedback'));
        }else{
            Alert::error("You do not have the permission to access this page.");
            return redirect()->route('home');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request, $input)
    {// $user_id1 = auth()->user()->id;
        // $user_info1 = UserMember::with('member')->where('user_id', $user_id1)->first();
        // $latest_feedback = Feedback::where('member_no', $user_info1->member_no)->orderBy('last_feedback', 'desc')->first('id');

        // dd($latest_feedback);
        $logout = $input;
        $branches  = Branch::whereNull('soft_deleted_at')
        ->Where('cd', '!=', 'TRD')
        ->Where('cd', '!=', 'FR')
        ->Where('cd', '!=', 'ADMIN')
        ->get();
        if(!Auth::guest()){
        $user_id = auth()->user()->id;
        $user_info = UserMember::with('member')->where('user_id', $user_id)
        // ->pluck('member_no', 'crea_by');
        ->first();
        $gender = $user_info->member->gender;
        $type = $user_info->member->member_type;
        $fullname = $user_info->member->long_name;
        $fname = $user_info->member->fname;
        $mname = $user_info->member->mname;
        $lname = $user_info->member->lname;
        $branch = $user_info->member->branch_id;
        // dd($user_info[0]);
        }else{
        $user_info = null;
        $fullname = null;
        $fname = null;
        $mname = null;
        $lname = null;
        $branch = null;
        $type = null;
        $gender = null;
        }

        return view('enduser.sitefeedback', compact('branches', 'user_info', 'fullname', 'fname', 'mname', 'lname', 'gender', 'branch', 'type', 'logout'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
          // "gender" => 'required'
          // ,"branch_id" => 'required'
          // ,"member_type" => 'required'
          "rate1" => 'required'
          ,"rate2" => 'required'
          ,"rate3" => 'required'
          ,"rate4" => 'required'
          ,"rate5" => 'required'
          ,"hidden_sig" => 'required'
          ,"comment" => 'required'
        ]);
        if ($validator->fails())
        {   
            $a = implode(' ',$validator->errors()->all());
            return redirect()->back()->with('error', 'Validation Error: '.$a);   
        }
        // dd($request->all());

        // $fullname = $request->fname.' '.$request->mname.' '.$request->lname;
        if(!Auth::guest()){
        $user_id = auth()->user()->id;
        $member = UserMember::where('user_id', $user_id)->first('member_no');
        $member_no = $member->member_no;
        $crea_by = 'Member:'.$member_no.':'.$request->fullname;
        }else{
            if($request->nonregmem == 1){
                $member_no = $request->nonregmem_num;
            }else{
                $member_no = null;
            }
        $crea_by = 'Non-Registered: '.$request->fullname;
        }

        
        $transaction_exp = array(
           "rate1" => $request->rate1
          ,"rate2" => $request->rate2
          ,"rate3" => $request->rate3
          ,"rate4" => $request->rate4
          ,"rate5" => $request->rate5
        );

        if($request->nonregmem == 1){
            if($request->gender1 == 'M'){
            $gender = 1;
            }else{
            $gender = 2;
            }
            if($request->member_type == 'AM'){
            $member_type = 1;
            }elseif($request->member_type == 'RM'){
            $member_type = 2;
            }else{
                $member_type = null;
            }
            $branch = Branch::where('cd', $request->branch_id1)->first('id'); 
            $branch_id = $branch->id; 
            // $member_type = $request->gender1;
            // dd($branch->id);
        }else{
            $gender = $request->gender;
            $branch_id = $request->branch_id;
            $member_type = $request->gender;
        }
            // dd('one');
        $randInt = rand(100,999);
        $folderPath = str_replace('\\', '/', public_path('assets/img/uploads/sitefeedback/'.$randInt.Carbon::now()->format('Ymd').'/'));

        // $folderPath = str_replace('\\', '/', '/home3/mhrmpcoo/crm.mhrmpcoop.com.ph/assets/img/uploads/sitefeedback/'.$randInt.Carbon::now()->format('Ymd').'/');
        
        // dd($folderPath);
        // File::makeDirectory($folderPath);
        mkdir($folderPath, 0777, true);
        $image_parts = explode(";base64,", $request->hidden_sig);
        $image_type_aux = explode("image/", $image_parts[0]);
        $image_type = $image_type_aux[1];
        $image_base64 = base64_decode($image_parts[1]);
        $file = $folderPath . $randInt.Carbon::now()->format('Ymd') .'-Signiture'. '.'.$image_type;
        $str_signiture = $randInt.Carbon::now()->format('Ymd').'-Signiture'. '.'.$image_type;
        file_put_contents($file, $image_base64);

       SiteFeedback::create([

          "fullname" => $request->fullname
          ,"member_no" => $member_no
          ,"gender" => $gender
          ,"branch_id" => $branch_id
          ,"member_type" => $member_type
          ,"transaction_exp" => $transaction_exp
          ,'comment_suggestion' => $request->comment
          ,'last_feedback' => Carbon::now()->addMonth(3)
          ,'signiture' => $str_signiture
          ,'crea_by' => $crea_by
        ]);
        $logout = $request->logout;
       
        // dd(Alert::success('Success', 'Thank you for your feedback'));
       if($logout == 'logout'){
        auth()->logout();
        Alert::success('Success', 'Thank you for your feedback');
        return redirect()->route('login');
       }else{
        // dd('notlogout');
        Alert::success('Success', 'Thank you for your feedback');
        if(!Auth::guest()){
        return redirect()->route('enduser.home');
        }
       }
    }

    public function lastsiteFeedback(Request $request)
    {
        // dd($input);
        if(!Auth::guest()){
        $user_id = auth()->user()->id;
        $user_info = UserMember::with('member')->where('user_id', $user_id)->first();
        $member_no = $user_info->member_no;
        }else{
        $member_no = $request->nonregmem_num;
        }
        // dd($me);
        $latest_feedback = SiteFeedback::where('member_no', $member_no)->orderBy('last_feedback', 'desc')->first('last_feedback');

        if(empty($latest_feedback)){
            $feedbackAble = array('status'=>1);
        }else{
            $now = Carbon::now()->format('d/m/Y');

            $date_format = Carbon::parse($latest_feedback->last_feedback)->format('d/m/Y');
            if($now == $date_format){
                $feedbackAble = array('status'=>1);
            }else{
                $feedbackAble = array('status'=>0,'date'=>Carbon::parse($latest_feedback->last_feedback)->subMonth(3)->format('d/m/Y'));
            }
        }
        return response()->json($feedbackAble);
    }

    public function lastsiteFeedbackNonRegNonMem(Request $request)
    {
        // $latest_feedback = Feedback::where('member_no', $member_no)->orderBy('last_feedback', 'desc')->first('last_feedback');
        $fname = '%'.$request->fname.'%';
        $lname = '%'.$request->lname.'%';
        $latest_feedback = SiteFeedback::Where('fullname','like', $fname)
        ->Where('fullname','like', $lname)
        ->orderBy('last_feedback', 'desc')->first('last_feedback');
        $latest_feedback1 = SiteFeedback::Where('fullname','like', $fname)
        ->Where('fullname','like', $lname)
        ->orderBy('last_feedback', 'desc')->first();
        if(empty($latest_feedback)){
            $feedbackAble = array('status'=>1);
        }else{
            $now = Carbon::now()->format('d/m/Y');
            $date_format = Carbon::parse($latest_feedback->last_feedback)->format('d/m/Y');
               // $feedAble = array('status'=>1);
            if($now == $date_format){
                $feedbackAble = array('status'=>1);
            }else{
                $feedbackAble = array('status'=>0,'date'=>Carbon::parse($latest_feedback->last_feedback)->subMonth(3)->format('d/m/Y'), 'latest_feedback' => $latest_feedback1);
            }
        }
        
        return response()->json($feedbackAble);
    }

    public function sitefeedbackAPI($input)
    {
        $member_info = Member::where('member_no', $input)->get();
        if($member_info->count()){
        return response()->json($member_info);
        }else{
        return withError('Member Number provided does not match our entry');
        }
    }
    public function empfeedbackAPI($input)
    {
        $sitefeedback = SiteFeedback::where('id', $input)->with('branch')->get();
        return response()->json($sitefeedback);
        
    }
    public function updateFeedback($array, $from, $to) 
    {
        $sitefeedback = $array->whereNull('status')
        ->whereBetween('created_at', [$from, $to])->get(); 
        // dd($sitefeedback);
        foreach($sitefeedback as $data){
            $sitefeedbackupdate = SiteFeedback::find($data->id);
            $sitefeedbackupdate->status = 1;
            $sitefeedbackupdate->save();
        }
        // return response()->json('update');
    }
    public function exportExcel(Request $request) 
    {
        $start = $request->start.' 00:00:00';
        $end = $request->end.' 23:59:59';
        $start_end = $start.', '.$end;
        $date = date('M-d-Y', strtotime($start)).'-'.date('M-d-Y', strtotime($end));
       

        return \Excel::download(new SitefeedbackExport($start, $end), 'Website Feedback('.$date.').'.'xlsx');
    }
    
}
