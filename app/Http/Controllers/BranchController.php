<?php

namespace App\Http\Controllers;

use App\Models\Branch;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\UserRequest;

class BranchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Branch $model)
    {
        $branch = Branch::with('user')->whereNull('soft_deleted_at')->get();
        return view('branch.index')
            ->with('branch', $branch);
        
    }
    public function create()
    {

        return view('branch.create');
     
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate([
            'cd' => 'required',
            'name' => 'required',
        ]);
        $branch = [
             'cd' => $request['cd'],
             'name' => $request['name'],
            'crea_by' => auth()->user()->uname,
        ];
        $branch_revive = ['soft_deleted_at' => null,];
       
       Branch::updateOrcreate ($branch,$branch_revive);
       
        return redirect()->route('branch.index')
            ->with('success', 'Branch created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Project  $project
     * @return \Illuminate\Http\Response
     */
    // public function show($id, User $user)
    // {
    //     $user = User::find($id);
        
        
       
    //       return view('userTable.show', [
    //     'user' => $user
    //     ]);
    // }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $branch = Branch::find($id);
       
       return view('branch.edit', [
        'branch' => $branch
        ]);

    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request, Branch $user)
    {
        $rules = array(
            'cd' => 'required',
            'name' => 'required',
        );
         $validator = Validator::make($request->all(), $rules);
         $branch = Branch::find($id);
            $branch->cd      = $request->get('cd');
            $branch->name       = $request->get('name');
            $branch->updated_at      = Carbon::now();
            $branch->upd_by = auth()->user()->uname;
            $branch->save();
       
        return redirect()->route('branch.index')
            ->with('success', 'Branch updated successfully');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Project  $project
     * @return \Illuminate\Http\Response
     */
    
    public function destroy($id)
    {
        $branch = Branch::find($id);
        $branch->delete();

        return response()->json(['success'=>"Branch deleted successfully.", 'tr'=>'tr_'.$id]);
    }
    public function deleteAll(Request $request, Branch $branch)
    {
        $ids = $request->ids;
        $branches = Branch::whereIn('id',explode(",",$ids))->whereNull('soft_deleted_at')->get();
        if($branches->count())
        {
            foreach($branches as $data)
            {
                $branch = Branch::find($data->id);
                $branch->soft_deleted_at = Carbon::now();
                $branch->save();
            }
        }
                return response()->json(['success'=>"Branch deleted successfully."]);

    }
}
