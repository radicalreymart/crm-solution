<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Yajra\Address\AddressServiceProvider;


class CreateClientMastersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('client_masters', function (Blueprint $table) {
            $table->id();
            $table->string('fname');
            $table->string('mname')->nullable();
            $table->string('lname');
            $table->string('fullname');
            $table->string('contact_no', 13);
            $table->string('fb_link');
            $table->string('token');
            $table->integer('member_status');
            $table->string('member_no')->nullable();
            $table->integer('seminar_pref');
            $table->string('gmail_address');
            $table->datetime('email_verified_at');
            $table->integer('gender');
            $table->datetime('birthdate');
            $table->datetime('sched_date')->nullable();
            $table->integer('age');
            $table->string('barangay_primary')->nullable();
            $table->string('city_primary')->nullable();
            $table->string('province_primary')->nullable();
            $table->string('region_primary')->nullable();
            $table->address();

            $table->unsignedBigInteger('batch_id')->nullable();
            $table->foreign('batch_id', 'cm_fk_batch_id')->references('id')->on('batch')->onUpdate('set null')->onDelete('set null');

            $table->unsignedBigInteger('branch_id')->nullable();
            $table->foreign('branch_id', 'cm_fk_branch_id')->references('id')->on('branch')->onUpdate('set null')->onDelete('set null');

            $table->unsignedBigInteger('occupation_id')->nullable();
            $table->foreign('occupation_id', 'cm_fk_occupation_id')->references('id')->on('occupation')->onUpdate('set null')->onDelete('set null');
            $table->string('other_occupation')->nullable();

            $table->unsignedBigInteger('marketing_channel_id')->nullable();
            $table->foreign('marketing_channel_id', 'cm_fk_marketing_channel_id')->references('id')->on('marketing_channel')->onUpdate('set null')->onDelete('set null');

            $table->integer('sched_stat');
            $table->string('of_tag');
            $table->integer('purpose_tag');
            $table->string('staff_referral_cd')->nullable();
            $table->integer('data_consent_fl');
            $table->string('crea_by');
            $table->string('upd_by')->nullable();
            $table->datetime('soft_deleted_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('client_masters');
    }
}
    