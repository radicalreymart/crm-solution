@extends('layouts.app', [
    'class' => 'sidebar-mini ',
    'namePage' => 'Loan Type Payment Edit',
    'activePage' => 'loan-type-payment-management',
    'activeNav' => '',
])

@section('content')
<div class="panel-header panel-header-sm">
  </div>
  <div class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="card-header">
                <div class="float-left">
                    <h4 class="card-title">{{ __('Edit Loan Type-Payment Mode') }} {{'ID Number '}} {{$loanTypePayment->id }}</h4>
                </div>
                <div class="float-right">
                    <a class="btn btn-primary" href="{{ url('/loanTypePayment/') }}"> Back</a>
                </div>
          
        <div class="card">
        <div class="card-body">
                    @if(count($errors) > 0 )
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
                <ul class="p-0 m-0" style="list-style: none;">
                @foreach($errors->all() as $error)
                <li>{{$error}}</li>
                @endforeach
                </ul>
                </div>
                @endif
                    <form action="{{ route('loanTypePayment.store') }}" method="POST">
                        @csrf

                          {{ method_field('post') }}

                        <div class="form-group row">    
                                        <input type="hidden" value="{{$loanTypePayment->id}}" name="id">
                                        <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Loan Type') }}</label>

                                        <div class="col-md-5">
                                            
                                            <select id="loan_type_id" name="loan_type_id" class="form-control">
                                            <option value="" selected disabled>Select Loan Type</option>
                                            @foreach($loan_type as $item)
                                            <option value="{{$item->id}}" {{ $loanTypePayment->loan_type_id == $item->id ? 'selected':'' }} >{{$item->name}}</option>
                                            @endforeach
                                            </select>

                                            @error('loan_type_id')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                        <div class="col-md-2" style="padding-top: 5px; "><a  href="{{ route('loanType.create') }}">
                                            <li style="color: green;" class="fas fa-plus-circle fa-lg" aria-hidden="true"></li>
                                        </a></div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="name" class="col-md-4 col-form-label text-md-right">{{ __(' Payment Mode') }}</label>

                                        <div class="col-md-6">
                                            <select id="paymode_id" name="paymode_id" class="form-control">
                                                <option value="" selected disabled>Select Payment Mode</option>
                                                <option value="1" {{ $loanTypePayment->paymode_id == 1 ? 'selected':'' }}>Daily</option>
                                                <option value="2" {{ $loanTypePayment->paymode_id == 2 ? 'selected':'' }}>Weekly</option>
                                                <option value="3" {{ $loanTypePayment->paymode_id == 3 ? 'selected':'' }}>Semi-Monthly</option>
                                                <option value="4" {{ $loanTypePayment->paymode_id == 4 ? 'selected':'' }}>Monthly</option>
                                                <option value="5" {{ $loanTypePayment->paymode_id == 5 ? 'selected':'' }}>Lumpsum</option>
                                                <option value="6" {{ $loanTypePayment->paymode_id == 6 ? 'selected':'' }}>Lumpsum (for 1 month only)</option>
                                            </select>


                                            @error('paymode_id')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>

                            </div>
                        </div>
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">

                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </div>
                    </form>
        </div>


        </div>
      </div>
    </div>
  </div>

@endsection
