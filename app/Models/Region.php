<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\ClientMaster;
use App\Models\Country;
use App\Models\Province;

class Region extends Model
{
    use HasFactory;
    protected $table = 'regions';
   
   
    public function province()
    {
        return $this->hasMany(Province::class);
    }
    public function clientMaster()
    {
        return $this->hasMany(ClientMaster::class);
    }
}
