<?php

namespace App\Http\Controllers;

use App\Models\AcctMaster;
use App\Models\TransactionType;
use App\Models\OnlinePymntTrxnDetail;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\UserRequest;
use Alert;

class AcctMasterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $department = auth()->user()->department_id;
        $isAdmin = auth()->user()->role_id;
        $branch = auth()->user()->branch_id;
        // dd(auth()->user()->uname);
        if($department == 3 || $isAdmin == 0){
            $acctMaster  = AcctMaster::with('transaction')->whereNull('soft_deleted_at')->get();
            $transaction = TransactionType::whereNull('soft_deleted_at')->get();
            // dd($acctMaster);
            return view('acctMaster.index', compact('acctMaster', 'transaction'));
        }else{
            Alert::error("You do not have the permission to access this page.");
            return redirect()->route('home');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('acctMaster.create');
     
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate([
            'acct_cd' => 'required',
            'trxn_type_id' => 'required',
            'description' => 'required',
        ],
        ['acct_cd.required' => 'Code is required',
        'trxn_type_id.required' => 'Transaction is required',
        'description.required' => 'Description is required',
        ]);
       AcctMaster::create([
             'acct_cd' => $request['cd'],
             'acct_cd' => $request['trxn_type_id'],
             'description' => $request['description'],
            'crea_by' => auth()->user()->uname,
        ]);
       
       
        return redirect()->route('acctMaster.index')
            ->with('success', 'Account created successfully.');
    }

    public function storeacctMaster(Request $request)
    {
        $request->validate([
            'acct_cd' => 'required',
            'trxn_type_id' => 'required',
            'description' => 'required',
        ],
        ['acct_cd.required' => 'Code is required',
        'trxn_type_id.required' => 'Transaction is required',
        'description.required' => 'Description is required',
        ]);
        if(isset($request->id)){
            $upd_by = auth()->user()->uname;
            $updated_at = Carbon::now();
        }else{
            $upd_by = null;
            $updated_at = null;
        }
        $acctMaster_upd = 
        [
            'id' => $request->id,
        ];
        $acctMaster_create = 
        [
            'acct_cd' => $request->acct_cd,
            'trxn_type_id' => $request->trxn_type_id,
            'description' => $request->description,
            'crea_by'     => auth()->user()->uname,
            'upd_by'         => $upd_by,
            'updated_at'     => $updated_at,
        ];
        AcctMaster::updateOrcreate($acctMaster_upd, $acctMaster_create);
        if($request->id != null){
        $request->session()->flash('success', 'Account updated successfully!');
        }else{
        $request->session()->flash('success', 'Account created successfully!');    
        }
        return response()->json(['success' => 'success'], 200);
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Project  $project
     * @return \Illuminate\Http\Response
     */
    // public function show($id, User $user)
    // {
    //     $user = User::find($id);
        
        
       
    //       return view('userTable.show', [
    //     'user' => $user
    //     ]);
    // }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $acctMaster  = AcctMaster::find($id);
       
       return view('acctMaster.edit', [
        'acctMaster' => $acctMaster 
        ]);

    }
    public function editmodal(Request $request, AcctMaster $acctMaster)
    {
        //
        $where = array('id' => $request->id);
        $acctMaster  = AcctMaster::where($where)->first();
        $json = array('id' => $request->id
            , 'acct_cd' => $acctMaster->acct_cd
            , 'trxn_type_id' => $acctMaster->trxn_type_id
            , 'description' => $acctMaster->description);
         // $request->session()->flash('success', 'acctMaster updated successfully!');
        return response()->json($json);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request)
    {
        $rules = array(
            'cd' => 'required',
            'description' => 'required',
        );
         $validator = Validator::make($request->all(), $rules);
         $acctMaster  = AcctMaster::find($id);
            $acctMaster ->cd      = $request->get('cd');
            $acctMaster ->description       = $request->get('description');
            $acctMaster ->updated_at      = Carbon::now();
            $acctMaster ->upd_by = auth()->user()->uname;
            $acctMaster ->save();
       
        return redirect()->route('acctMaster.index')
            ->with('success', 'Account updated successfully');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Project  $project
     * @return \Illuminate\Http\Response
     */
    
    public function destroy($id)
    {
        $acctMaster  = AcctMaster::find($id);
        $acctMaster ->delete();

        return response()->json(['success'=>"Account deleted successfully.", 'tr'=>'tr_'.$id]);

    }
    public function deleteAll(Request $request)
    {
        $ids = $request->ids;
        $acctMaster = AcctMaster::whereIn('id',explode(",",$ids))->get();
        $optd = OnlinePymntTrxnDetail::whereIn('acct_id',explode(",",$ids))->whereNull('soft_deleted_at')->get();
        if($optd->count())
        {
            $json = ['error' => 'Please delete Online Payment Transaction Details corresponding to this entry.'];
        }else
        {
            if($acctMaster->count())
            {
                foreach($acctMaster as $data)
                {
                    $acctMaster = AcctMaster::find($data->id);
                    $acctMaster->soft_deleted_at = Carbon::now();
                    $acctMaster->save();
                }
            }
            $json = ['success'=>"Account Deleted successfully."];
        }
        return response()->json($json);
    }
}
