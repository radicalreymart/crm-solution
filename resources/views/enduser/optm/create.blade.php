@extends('layouts.app1')
@section('loanstyle')
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.3/css/bootstrap.css">
<style>
.alert{
  color: black !important;
}
[type=file] {
    position: absolute;
    filter: alpha(opacity=0);
    opacity: 0;
}
input,
[type=file] + label {
  border: 1px solid black;
  border-radius: 3px;
  text-align: left;
  padding: 0px;
  width: 100%;
  height: 30px;
  margin: 0;
  left: 0;
  position: relative;
}
[type=file] + label {
  text-align: center;
  left: 0em;
  top: 0.5em;
  /* Decorative */
  background: blue;
  color: #fff;
  border: black;
  cursor: pointer;
}
[type=file] + label:hover {
  background: #3399ff;
}
label.shorten_text1 {
 max-width: 100%;
  /*padding: px;*/
  text-overflow: ellipsis;
  white-space: nowrap;
  overflow: hidden;
}

label.shorten_text1:hover {
  overflow: visible;
}
label.shorten_text1:before{
  content: attr(title);
}

</style>
@endsection

@section('content')
    <!-- Masthead-->
    <header class="masthead mb-1">
      <div class="container mt-1 pt-1">
       @include('sweetalert::alert')
      </div>
    </header>
    
    <div  class="container mt-5 col-md-8" id="sign_up">
      <div  class="card p-2 pt-3 bg-light border-0">
        <div class="row"> 
          <div class="col-md-12 order-md-1">
            <form id="insert_form" name='formname'  enctype="multipart/form-data"  method="post" action="{{ route('enduser.add.optd') }}"> 
            
              <input type="hidden" value="{{url('/')}}" id="url" name="url">
              <input type="hidden" id="sitefeedback" name="sitefeedback">
                                        @csrf
                <div class="card-header bg-light text-primary py-2 pl-3 border-0">
                  <h6 class="m-0 default-primary">Online Payment<a  id="backhome" class=" hvr-icon-push float-right" onClick="window.location.href = '/enduser/home';" data-url="{{ url('/enduser/home') }}">{{ __('Back') }}<i class="far fa-caret-square-left hvr-icon"></i></a>
                    <a hidden id="backOPTM" class=" hvr-icon-push float-right" >{{ __('Back') }}<i class="far fa-caret-square-left hvr-icon"></i></a>
                  </h6>
                </div>


              <div hidden id="OPTDetail" class="card-body px-3">
                <div class="row px-3">
                @csrf
                  @include('flash-message')
                  
                  <div class="row px-3">
                    <div class="col-md-6">  
                      <a type="button"  id="add" title='please fill all input before adding another amount and account' class="disabled-btn btn btn-primary"><i class="fa fa-plus-circle"></i> </a>
                      <a type="button" id="remove" class="btn btn-danger"><i class="fa fa-minus-circle"></i></a>
                    </div>
                  </div>
                      <div  id="featured-items">
                        <div class="featured-item form-group">
                            <label id="lbamount">Amount #1<span class="text-danger"> *</span></label> 
                              <input type="number" class="amount form-control @error('amount1') is-invalid @enderror " onchange="(function(el){el.value=parseFloat(el.value).toFixed(2);})(this)" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" maxlength="9" min="0.00" max="9999999.00" step="0.05" id="amount1" onkeyup="findTotal();"  name="amount[]">
                            <label id="lbaccount">Account #1<span class="text-danger"> *</span></label>
                              <select id="featured1" class="featured_select custom-select" name="acct_id[]" class="form-control featured">
                                  <option disabled selected>Select Account</option>
                                  @if ($account->count())
                                  @foreach($account as $data)
                                  <option value="{{ $data->id }}" >{{ $data->description }}</option>@endforeach
                                  @endif
                              </select>
                        </div>
                      </div>
                  <table class="table">
                    <tbody>
                      <tr>
                        <td>Current Amount</td>
                        <td id="ptotal-amt"></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                      </tr>
                      <tr>
                        <td>Total Amount</td>
                        <td id="ttotal-amt"></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
              
              <div id="OPTMain" class="card-body px-3">
                  <div class="row px-3">
                    <div class="col-md-4 mb-3">  
                      <label for="total_amount">Total Amount<span class="text-danger"> *</span>
                      </label>
                     <input type="number" class="form-control  @error('total_amount') is-invalid @enderror" onchange="(function(el){el.value=parseFloat(el.value).toFixed(2);})(this)"
                     oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"  maxlength="7" min="0.00" max="9999999.00" step="0.05" id="total_amt" name="total_amt">
                    </div>
                    <div  class="col-md-4 mb-3">
                      <label>Bank<span class="text-danger"> *</span></label>
                      <select class="form-control custom-select  " required  id="bank_id" name="bank_id">
                      @if ($bank->count())
                      <option value="" selected>Select Bank</option> 
                      @foreach($bank as $data)
                      <option value="{{ $data->id }}" >{{ $data->cd }}</option> 
                      @endforeach   
                      @endif
                      </select>
                    </div>
                    <div  class="col-md-4 mb-3">
                      <label>Branch<span class="text-danger"> *</span></label>
                      <select class="form-control custom-select  " required  id="branch_id1" name="branch_id">
                      @if ($branches->count())
                      <option value="" selected>Select Branch</option> 
                      @foreach($branches as $data)
                      <option value="{{ $data->id }}" 
                            @if($branch_iddef === $data->id) selected='true' @endif >{{ $data->name }}</option> 
                      @endforeach   
                      @endif
                      </select>
                    </div>
                  </div>
                  <div class="row px-3">
                    <div  class="col-md mb-3">
                     <label>Supporting Documents<span class="text-danger"> *</span></label>
                      <input  id="f01" type="file" placeholder="" name="attachment"  />
                      <label title="Any of the following: GCash, BDO, PNB, etc. " style="color: #fff" for="f01">Any of the following: GCash, BDO, PNB, etc. </label>
                    </div>
                  </div>
              </div>
              
                  <button class="btn btn-secondary mt-5 mb-5 btn-block" style="background-color: rgb(7,17,174);" type="submit" id="btn-save1">Submit</button>
                  <button hidden class="btn btn-secondary mt-5 mb-5 btn-block" style="background-color: rgb(7,17,174);" type="button" id="btn-save-optd"  >Submit</button>
              <!-- <button class="btn btn-secondary mt-5 mb-5 btn-block" style="background-color: rgb(7,17,174);" type="submit" id="btn-save-loan"  >Submit</button> -->

            </form>
          </div>


      </div>
  </div>
</div>


<script>
  var new_id;
var selected_val_arrray = [];
//adding
$(document).on('click', "#add", function() {
  var el = $(".featured-item").last()
  var clone_holder = el.clone(true);

  var current_items_count = $(".featured-item").length
  new_id = current_items_count + 1;

  var select_els = $(".featured_select")
  select_els.each(function() {
    var curr_select_el = $(this);
    var curr_select_el_val = curr_select_el.val();
    selected_val_arrray.push(curr_select_el_val)
  })

  $("#featured-items").append(clone_holder);

  var new_last_featured = $(".featured-item").last()
  new_last_featured.children('label[id="lbamount"]').text('Amount #' + new_id);
  new_last_featured.children('label[id="lbaccount"]').text('Account #' + new_id);
  new_last_featured.children('select').attr('id', 'featured' + new_id)
  new_last_featured.children('input[type="number"]').attr('id', 'amount' + new_id)
  new_last_featured.children('input[type="text"]').attr('id', 'amount_hidden' + new_id)
  new_last_featured.children('input[type="number"]').attr('value', 0)
  new_last_featured.children('input[type="text"]').attr('value', 0 )

  var selected_val_arrray_length = selected_val_arrray.length
  for (var i = 0; i < selected_val_arrray_length; i++) {
    var select_els = $(".featured_select");
    select_els.each(function() {
      var curr_select_el = $(this);
      var curr_select_el_val = curr_select_el.val()
      
      if(selected_val_arrray[i] != curr_select_el_val)
        {
        curr_select_el.find('option[value="' + selected_val_arrray[i] + '"]').prop('disabled', true);
          }
    })
  }

  // $('#add').attr('disabled', true);
})

//for disabling
$(document).on('change', ".featured_select", function() {
  var el = $(this);
  var el_id = el.attr('id');

  var el_value = el.val();

  var select_els = $(".featured_select");
  select_els.each(function() {
    var curr_select_el = $(this);
    var curr_select_el_id = curr_select_el.attr('id');
    if (curr_select_el_id != el_id) {
      console.log("dd");
      curr_select_el.find('option[value="' + el_value + '"]').prop('disabled', true);
    }
  })
  findTotal();
  // enable add button
  // $('#add').attr('disabled', false);
})

//removing
$(document).on('click', "#remove", function() {
  var el = $(".featured-item").last()
  var el_select = el.children('select');
  var el_select_val = el_select.val();

  console.log(el_select_val);
  var select_els = $(".featured_select")
  select_els.each(function() {
    var curr_select_el = $(this);
    curr_select_el.find('option[value="' + el_select_val + '"]').prop('disabled',false);
  })

  if ($(".featured-item").length != 1) {
    el.remove()
         findTotal();
  }

})
</script>

<script>

    // sweetalert
  const swalWithBootstrapButtons = Swal.mixin({
    customClass: {
      confirmButton: 'btn btn-success',
      cancelButton: 'btn btn-danger'
    },
    buttonsStyling: false
  }) 

  

  const total_amtEl = document.querySelector('#total_amt');
  const bank_idEl = document.querySelector('#bank_id');
  const branch_idEl = document.querySelector('#branch_id1');
  const isRequired = value => value === '' ? false : true;

  const checkTotalAmount = () => {
      let valid = false;
      const total_am = total_amtEl.value;
      if (!isRequired(total_am)) {
        Swal.fire('Please fill the total amount field.');  
      } else {
        // alert(total_am);
          valid = true;
      }
      return valid;
  };
  const checkBank = () => {
      let valid = false;
      const bank = bank_idEl.value;
      if (!isRequired(bank)) {
        Swal.fire('Please fill the bank field.');  
      } else {
        // alert(bank);
          valid = true;
      }
      return valid;
  };

  const checkBranch = () => {
      let valid = false;
      const branch = branch_idEl.value;
      if (!isRequired(branch)) {
        Swal.fire('Please fill the branch field.');  
      } else {
        // alert(bank);
          valid = true;
      }
      return valid;
  };
  const checkAttachment = () => {
   
      let valid = false;
      if (!$('#f01').val()) {
        // alert(atttachmentEl);
        Swal.fire('Please upload the supporting document to continue the process.');  
      } else {
          valid = true;
      }
      return valid;
  };

  const checkAccount = () => {

      let valid = false;
      var select_els2 = $(".featured_select");
        select_els2.each(function() {
          var curr_select_el2 = $(this);
          var curr_select_el_val2 = curr_select_el2.val()
          if(curr_select_el_val2 == null){
          // alert(curr_select_el_val2);
            Swal.fire('Please fill all account fields');
            valid = false;
          } else {
        // alert(bank);
          valid = true;
          }
        });
      return valid;
  };

  $('#btn-save1').click(function (event) {
        event.preventDefault();
        
        // alert(total_amt+bank_id+JSON.parse(atttachment));
    let isFormValid = checkTotalAmount() && checkBank() && checkBranch() && checkAttachment();

    if(isFormValid){
      $('#OPTMain').attr('hidden', true);
      $('#OPTDetail').attr('hidden', false);
      $('#backhome').attr('hidden', true);
      $('#backOPTM').attr('hidden', false);
      $('#btn-save-optd').attr('hidden', false);
      $('#btn-save1').attr('hidden', true);
      }
  });

  $('#backOPTM').click(function (event) {
        event.preventDefault();
    $('#OPTMain').attr('hidden', false);
    $('#OPTDetail').attr('hidden', true);
    $('#backOPTM').attr('hidden', true);
    $('#backhome').attr('hidden', false);
    $('#btn-save-optd').attr('hidden', true);
    $('#btn-save1').attr('hidden', false);
  });

  //--
  // file upload
    $("[type=file]").on("change", function(){
  // Name of file and placeholder
    var file = this.files[0].name;
    var dflt = $(this).attr("placeholder");
    if($(this).val()!=""){
      $(this).next().text(file);
    } else {
      $(this).next().text(dflt);
    }
  });
  $(document).on('keydown', 'input[pattern]', function(e){
    var input = $(this);
    var oldVal = input.val();
    var regex = new RegExp(input.attr('pattern'), 'g');

    setTimeout(function(){
      var newVal = input.val();
      if(!regex.test(newVal)){
        input.val(oldVal); 
      }
    }, 1);
  });
  //--

  
    function findTotal(){
        var arr = document.getElementsByName('amount[]');
        var tot=0;
        for(var i=0;i<arr.length;i++){
            if(parseInt(arr[i].value))
                tot += parseFloat(arr[i].value);
        }
        // document.getElementById('total').value = tot;
        $('#ptotal-amt').html(tot.toFixed(2).toString().replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ","));
        var total_amt1 = document.getElementById('total_amt').value;
        // alert(total_amt1);
        $('#ttotal-amt').html(total_amt1.toString().replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ","));
    };

   document.addEventListener("DOMContentLoaded", function(event) {
       findTotal();
    });
    const form = document.getElementById('form');
  //--
  // save OPTD
   $('form #btn-save-optd').click(function (event) {
        var values = [];
        var amount = [];


        $("input[name='amount[]']").each(function() {
            values.push($(this).val());
        });
        
        var select_els1 = $(".featured_select");
        select_els1.each(function() {
          var curr_select_el1 = $(this);
          var curr_select_el_val1 = curr_select_el1.val()
          if(curr_select_el_val1 == null){
            Swal.fire('Not all account/s are not filled');
          }
        });
        var sum = 0;
        for (var i = 0; i < values.length; i++) {
        sum += parseFloat(values[i]);
        }
        for (var a = 1; a <= values.length; a++) {
        amount[a] = $("input[id='amount"+a+"'").val();
        $("input[name='amount_hidden"+a+"'").val(amount[a]);
        // console.log(amount[a]);
        }
        var total_amt = $('#total_amt').val();
        // console.log(jQuery('#insert_form').serializeArray())
         var isFormValid1 = checkAccount();
        if(total_amt == sum )
        {
          // alert(isFormValid1);
              if(isFormValid1)
              {
                  let $form = $(this).closest('form');
                  var sitefeedback = $('#sitefeedback');
                   swalWithBootstrapButtons.fire({
                      title: 'Magandang Buhay!',
                      icon: 'question',
                      text:" Thank you for using our website, Kamay-ari! We would like to know how we could improve our website through this survey. This will take only a few minutes.",
                      showCancelButton: true,
                      confirmButtonText: 'Yes',
                      cancelButtonText: 'No',
                    }).then((result) => {
                      if (result.isConfirmed) {
                        // alert(base_path);
                          sitefeedback.val('yes');
                        $form.submit();
                        // event.preventDefault();
                        // window.location.href = $(this).data('feedback');
                      }else if (
                          /* Read more about handling dismissals below */
                          result.dismiss === Swal.DismissReason.cancel
                        ) {
                           
                          sitefeedback.val('no');
                          $form.submit();
                          }
                    })
              }else{
                event.preventDefault();
                  // alert(total_amt+sum);
                Swal.fire('Please fill all account fields');
              }
        }else{
         findTotal();
        event.preventDefault();
          // alert(total_amt+sum);
        Swal.fire('Total amount: ('+total_amt+') and current amount ('+sum+') of the listed amount must be equal');
          // alert('success');

        }
     });
   
  

</script>
  <script src="{{ asset('assets') }}/js/extrajs.js"></script>
@endsection

