<?php

namespace App\Http\Livewire;

use App\Models\Batch;
use App\Models\PMES;
use App\Models\Client;
use Livewire\Component;
use Livewire\WithPagination;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\UserRequest;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendMail;

class Batchv1 extends Component
{
    use WithPagination;
    public $searchTerm;
    public $batch, $batch_cd,$sched_date,  $meeting_cd, $meeting_pw, $online_vid_tool, 
     $batch_id, $resched_remarks;
    public $updateMode = false;
    public $participant = null;
    public $client_id = null;
    public $client_name = null;

    public function render()
    {
        $mindate = Carbon::now()->addDay()->format('Y-m-d H:i');
                // $sched_date = Carbon::parse($this->sched_date)->format('Y-m-d H:i');

        $now = Carbon::now()->format('m/d/Y');
        $now1 = Carbon::now();
        $participant = null;
        // dd($mindate);
        $searchTerm = '%'.$this->searchTerm.'%';
        return view('livewire.batch.index',
        ['batch1' => Batch::where('batch_cd','like', $searchTerm)
        ->whereNull('soft_deleted_at')
        ->whereDate('sched_date', '>=' , $now1)
        ->get(),'now' =>$now, 'mindate' => $mindate, 'participant', $participant]);
    }
    private function resetInputFields(){
        $this->batch_cd = null;
        $this->meeting_cd = null;
        $this->meeting_pw = null;
        $this->online_vid_tool = null;
        $this->resched_remarks = null;
    }

    public function store()
    {
        $sched_date = Carbon::parse($this->sched_date)->format('Y-m-d H:i');
        // dd($sched_date);
        $validatedDate = $this->validate
        ([
            'batch_cd' => 'required',
            'sched_date' => 'required',
            'online_vid_tool' => 'required',
            'meeting_cd' => 'required',
        ]);
        $batch = [
             'batch_cd' => $this->batch_cd,
                ];
        $batch_revive = ['soft_deleted_at' => null,
             'sched_date' => $sched_date,
             'online_vid_tool' => $this->online_vid_tool,
            'meeting_cd' => $this->meeting_cd,
             'batch_stat' => 0,
            'meeting_pw' =>$this->meeting_pw,
            'crea_by' => auth()->user()->uname,];
       // Batch::updateOrcreate($batch, $batch_revive);

        // $this->updateMode = false;
        session()->flash('success', 'Batch Created Successfully.');
        $this->resetInputFields();

        $this->emit('batchStore'); // Close model to using to jquery

    }
    public function edit($id)
    {

        $batch = Batch::where('id',$id)->first();
        // $sched_date = Carbon::parse($batch->sched_date)->format('Y-m-d'.'\T'.'H:i:s.v');
        $sched_date = Carbon::parse($this->sched_date)->format('Y-m-d H:i');

        $this->batch_id = $id;
        $this->batch_cd = $batch->batch_cd;
        $this->sched_date = $sched_date;
        $this->online_vid_tool = $batch->online_vid_tool;
        $this->meeting_cd = $batch->meeting_cd;
        $this->meeting_pw = $batch->meeting_pw;
        $this->resched_remarks = $batch->resched_remarks;
        $this->updateMode = true;
        // dd($this);
    }
    public function participant($id)
    {
        $this->updateMode = true;
        $this->participant = 1;
       $this->client_name = PMES::with('client')->where('batch_id',$id)->get();
       
    }
     public function cancel()
    {
        $this->updateMode = false;
        $this->resetInputFields();

    }
    public function getBatches($batch_id)
    {
        $batches = Batch::where("id",$batch_id)->get(['sched_date','online_vid_tool','meeting_cd','meeting_pw']);
        return response()->json($batches);
    }
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'batch_cd' => ['required','unique:batch,batch_cd,'],
            'sched_date' => 'required',
            'online_vid_tool' => 'required',
            'meeting_cd' => 'required',
        ]);
    }
    public function update(Batch $batch)
    {   
        $array = array('batch_cd' => $this->batch_cd,'sched_date' => $this->sched_date,'online_vid_tool' => $this->online_vid_tool,'meeting_cd' => $this->meeting_cd, );
        // dd($this);
        $this->validator($array);
        
        // $sched_date = Carbon::parse($this->sched_date)->format('Y-m-d'.'\T'.'H:i:s.v');
        $sched_date = Carbon::parse($this->sched_date)->format('Y-m-d H:i');

        if ($this->batch_id) 
        {
            $batch = Batch::find($this->batch_id);
            $batch->update
        ([
            'batch_cd' => $this->batch_cd,
            'sched_date' => $sched_date,
            'online_vid_tool' => $this->online_vid_tool,
            'meeting_cd' => $this->meeting_cd,
            'meeting_pw' => $this->meeting_pw,
            'resched_remarks' => $this->resched_remarks,
        ]);
            // $this->updateMode = false;
            session()->flash('success', 'Batch!!! Updated Successfully.');
            $this->resetInputFields();
        }
        $pmes = PMES::with('client')->where('batch_id', $this->batch_id)->get();
        if($pmes->count())
        {
            foreach($pmes as $data)
            {
                // dd($data->client->gmail_address);
                $meeting_cd = Batch::where('id', $this->batch_id)->first()->meeting_cd;
                $sched_date = Batch::where('id', $this->batch_id)->first()->sched_date;
                $resched_remarks = Batch::where('id', $this->batch_id)->first()->resched_remarks;
                $email = $data->client->gmail_address;
                $details = 
                [
                'meeting_cd' => $meeting_cd,
                'sched_date' => $sched_date,
                'resched_remarks' => $resched_remarks
                ];

                $editclient = Client::find($data->client->id);
                $editclient->sched_date = $sched_date;
                $editclient->save();
                // dd($meeting_cd);
                Mail::to($email)->queue(new SendMail($details));
            }
        }
    }

    
}