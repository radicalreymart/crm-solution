<!-- Modal -->
<div wire:ignore.self class="modal fade" id="updateModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
       <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Edit User</h5>
                <button type="button" class="close" wire:click="cancel" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group row">
                            <input type="hidden" wire:model="user_id">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('User Name') }}</label>
                            <div class="col-md-6">
                                <input id="edituname" type="text" wire:model="uname" class="form-control @error('uname') is-invalid @enderror" name="uname" value="{{ old('uname') }}"  required autocomplete="uname" autofocus />

                                @error('uname')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">Branch</label>
                             <div class="col-md-6">
                        
                        <select wire:model="branch_id" name="editbranch_id" class="form-control">
                             <option value="">Select Branch</option>
                        @foreach($branch as $item)
                        <option value="{{$item->id}}">{{$item->name}}</option>
                        @endforeach
                        </select>
                         <span class="text-danger">
                            <strong>{{$errors->first('branch_id') ?? null}}
                            </strong>
                        </span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="employee_role" class="col-md-4 col-form-label text-md-right">{{ __('Employee Designation') }}</label>

                            <div class="col-md-6">
                                <select wire:model="employee_role" name="employee_role" class="form-control">
                                    <option value="">Select Designation</option>
                                    <option value="0">Branch Employee</option>
                                    <option value="1">Branch Manager</option>
                                </select>
                                @error('employee_role')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('Email') }}</label>

                            <div class="col-md-6">
                                <input id="editemail" wire:model="email" type="text" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus />

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="editpassword" wire:model="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                            <div class="col-md-6">
                                <input id="editpassword-confirm" type="password" class="form-control" wire:model="password_confirmation" name="password_confirmation" required autocomplete="new-password">
                            </div>
                        </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" wire:click.prevent="cancel()" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" wire:click.prevent="update()" class="btn btn-primary" data-dismiss="modal">Save</button>
            </div>
       </div>
    </div>
</div>