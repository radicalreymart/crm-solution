
<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSiteFeedbackTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sitefeedback', function (Blueprint $table) {
            $table->id();
            $table->integer('status')->nullable();

            $table->string('member_no', 10)->index()->nullable();
            // $table->string('member_no', 10)->index();
            $table->foreign('member_no', 'sfb_fk_member_no')->references('member_no')->on('master_file');

            $table->unsignedBigInteger('branch_id')->nullable();;
            $table->foreign('branch_id', 'sfb_fk_branch')->references('id')->on('branch');

            $table->string('fullname');
            $table->integer('gender');
            $table->integer('member_type')->nullable();
            $table->string('transaction_exp');
            $table->text('comment_suggestion')->nullable();
            $table->string('last_feedback');
            $table->string('signiture');
            $table->string('crea_by');
            $table->string('upd_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sitefeedback');
    }
}
