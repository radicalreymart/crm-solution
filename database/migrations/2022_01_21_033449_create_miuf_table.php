<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Yajra\Address\AddressServiceProvider;

class CreateMIUFTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('miuf', function (Blueprint $table) {
            $table->engine = "InnoDB";
            $table->id();
            // $table->string('member_no', 10)->index()->collation('latin7_general_cs');
            $table->string('member_no', 10)->index();
            $table->foreign('member_no', 'miuf_fk_member_no')->references('member_no')->on('master_file');

            $table->string('fname')->nullable();
            $table->string('mname')->nullable();
            $table->string('lname')->nullable();
            $table->string('fullname')->nullable();
            $table->string('email')->nullable();
            $table->date('birthdate')->nullable();
            $table->address();
            $table->string('zipcode')->nullable();
            $table->string('fb_link')->nullable();
            $table->string('mobile_no')->nullable();
            $table->string('landline_no')->nullable();
            $table->string('of_offm')->nullable();

            $table->unsignedBigInteger('occupation_id')->nullable();
            $table->foreign('occupation_id', 'miuf_fk_occupation_id')->references('id')->on('occupation')->onUpdate('set null')->onDelete('set null');
            $table->string('other_occupation')->nullable();

            $table->decimal('montly_income', 10, 2)->nullable();
            $table->string('emp_bus_name')->nullable();
            $table->string('emp_bus_address')->nullable();
            $table->string('emp_bus_startdate')->nullable();
            $table->string('benificiaries')->nullable();
            $table->string('signiture')->nullable();
            $table->integer('status')->nullable();
            $table->string('encode_by')->nullable();
            $table->datetime('encode_date')->nullable();
            $table->datetime('editable_at')->nullable();
            $table->string('crea_by');
            $table->string('upd_by')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('miuf');
    }
}
