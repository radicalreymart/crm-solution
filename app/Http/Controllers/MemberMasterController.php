<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use App\Http\Requests\UserRequest;
use App\Models\MemberMaster;
use App\Models\User;
use App\Models\Member;
use App\Models\UserMember;
use App\Models\OnlinePymntTrxnMaster;
use App\Models\PreLoanApplication;
use App\Models\Branch;
use App\Models\Barangay;
use App\Models\City;
use App\Models\Province;
use App\Models\Region;
use App\Models\Country;
use App\Models\Occupation;
use App\Models\MarketingChannel;
use App\Models\Batch;
use Alert;
use Illuminate\Auth\Events\Registered;
use Datatables;

class MemberMasterController extends Controller
{
    public function index(Request $request)
    {
        $branch  = Branch::whereNull('soft_deleted_at')
            ->Where('cd', '!=', 'TRD')
            ->Where('cd', '!=', 'FR')
            ->Where('cd', '!=', 'ADMIN')
            ->get();
        $department = auth()->user()->department_id;
        $userBranch = auth()->user()->branch_id;
        $isAdmin = auth()->user()->role_id;
        if($department == 1 || $isAdmin == 0){
            if($isAdmin == 0){
            // dd($isAdmin);
                // $empTable  = Member::query();
                $empTable = Member::all();
                // $data = $this->dataIndex();
            }else{
                $empTable  = Member::where('branch_id', [$userBranch])->get();
                // $data = $this->dataIndex([$userBranch]);
            }
            return view('member.index', compact( 'branch','empTable'));
        }else{

            Alert::error("You do not have the permission to access this page.");
            return redirect()->route('home');
        }
    }

    public function fetch_data(Request $request)
    {
     if($request->ajax())
         {
          $sort_by = $request->get('sortby');
          $sort_type = $request->get('sorttype');
                $query = $request->get('query');
                $query = str_replace(" ", "%", $query);
                // $query1 = $request->get('query1');
                // $query1 = str_replace(" ", "%", $query1);
            // if($query1 == ''){
                $branch = auth()->user()->branch_id;
                $isAdmin = auth()->user()->role_id;
                    // if($isAdmin == 0){
                    // }else{
                    //     $data->where('branch_id', [$branch]);
                    // }
                $data = Member::where('id', 'like', '%'.$query.'%')
                        ->orWhere('member_no', 'like', '%'.$query.'%')
                        ->orWhere('long_name', 'like', '%'.$query.'%')
                        ->orderBy($sort_by, $sort_type)
                        ->where('branch_id', [$branch])
                        ->simplePaginate(10);

          //   }else{
          // $data = Member::Where('branch_id', 'like', '%'.$query1.'%')
          //               ->paginate(10);
            // }
                        // return response()->json($data);
          return view('member.pagination_data', compact('data'))->render();
         }
    }

    public function validateMemberNo(Request $request)
    {
        // $user_id = auth()->user()->id;
        $member_noInput = $request->member_no;
        $member_noIsset = Member::where('member_no', 'like', $member_noInput)->get('member_no');
        // $isnull = $user_emails->email;
        if(count($member_noIsset))
        {
             $json = 'taken';
        }else{
             $json = 'nottaken';
        }
       return response()->json($json);
    }

    public function validateEmail(Request $request)
    {
        // $user_id = auth()->user()->id;
        $email_input = $request->email;
        $user_emails = User::where('email', 'like', $email_input)->get('email');
        // $isnull = $user_emails->email;
        if(count($user_emails))
        {
            $json = $user_emails[0]['email'];
        }else{
             $json = 'nottaken';

        }
        
       return response()->json($json);
    }
    
    public function member_register()
    {
          $regions = Region::all();
          $occupation = Occupation::all();
          $branches  = Branch::whereNull('soft_deleted_at')
            ->Where('cd', '!=', 'TRD')
            ->Where('cd', '!=', 'FR')
            ->Where('cd', '!=', 'ADMIN')
            ->get();
          $marketingChannel = MarketingChannel::all();
          $now = Carbon::now();
          $batches = Batch::whereDate('sched_date', '>', $now)->get();
          return view('enduser.register', compact(
            'occupation',
            'regions',
            'branches',
            'marketingChannel',
            'batches'
        ));
        
    }

    public function member_verify(Request $request)
    {
        $validatedDate = $request->validate
        ([
            'member_no2' => 'required',
            'unique_key1' => 'required',
        ], ['member_no2.required' => 'Member number is required.',
            'unique_key1.required' => 'Unique key is required.']);
        // if($request->prefix_cd == null)
        // {
            $member_no2 = $request->member_no2;
            $unique_key1 = $request->member_no2.$request->unique_key1;
        // }else
        // {
        //     $member_no2 = $request->prefix_cd.'-'.$request->member_no2;
        //     $unique_key1 = $request->prefix_cd.'-'.$request->member_no2.$request->unique_key1;
        // }
        $verifyreg = UserMember::where('member_no', $member_no2)->get();
                // return response()->json($verifyreg[0]->member_no);
        if(isset($verifyreg[0]->member_no)){
            return response()->json(['error' => 'Member Number is already registered ']);
        }else{
                
                $member_no = DB::table('master_file')
                    ->where('member_no', '=', $member_no2)
                    ->get();
                $uni_key = DB::table('master_file')
                ->where('uni_key','=',$unique_key1)
                ->get();
                $uni_match = Member::where('member_no', $member_no2)->get();
                // return response()->json($member_no2);
                if(isset($member_no[0]->member_no) && isset($uni_key[0]->uni_key)){
                    if($uni_key[0]->uni_key == $uni_match[0]->uni_key){
                    return response()->json($uni_match);
                        // return response()->json(['success' => 'success']);
                    }else{
                        return response()->json(['error' => 'Member Number and Unique Key provided does not match']);
                    }
                }elseif(isset($member_no[0]->member_no)){
                    return response()->json(['error' => 'Unique Key provided does not match our entry ']);
                }elseif(isset($uni_key[0]->uni_key)){
                    return response()->json(['error' => 'Member Number provided does not match our entry ']);
                }else{
                    return response()->json(['error' => 'Member number and unique key you have entered are invalid. Please try again ']);
                }
        }
    }
     public function storeusermember(Request $request)
    {
         $message = 
         [
            'unique' => 'Email is already in use',
            'username.required' => 'Username is required',
            'password.required' => 'Password is required',
            'member_no.required' => 'Member number is required',
            'contact_no.required' => 'Contact number is required',
            'branch_id.required' => 'Branch is required',
            'long_name.required' => 'Full name is required',
            'birthdate.required' => 'Birthdate is required',
            'region_id.required' => 'Region is required',
            'province_id.required' => 'Province is required',
            'city_id.required' => 'City is required',
            'street.required' => 'Street is required',
         ];
        $validatedDate = $request->validate
        ([
            'username' => 'required',
            'password' => 'required|string|min:8|same:password_confirm',
            'member_no' => 'required',
            'contact_no' => 'required',
            'branch_id' => 'required',
            'long_name' => 'required',
            // 'fb_link' => 'required',
            'email' => 'required|email|unique:users',
            'birthdate' => 'required',
            'region_id' => 'required',
            'province_id' => 'required',
            'city_id' => 'required',
            'street' => 'required',
        ], $message);
        
        $upd_by = null;
        $updated_at = null;
        $crea_by = $request->long_name;
    

        $user_create = 
        [
            'uname'      => $request->username,
            'password'      => Hash::make($request->password),
            'branch_id'     => $request->branch_id,
            'email' => $request->email,
            'role_id'        => 3,
            'crea_by'       => $request->long_name,
        ];

        $users = User::create($user_create);
        event(new Registered($users));
        
        $usermember_create = 
        [   
            'user_id'       => $users->id,
            'member_no'     => $request->member_no,
            'contact_no'    => $request->contact_no,
            'branch_id'     => $request->branch_id,
            'birthdate'     => $request->birthdate,
            'fb_link'       => $request->fb_link,
            'region_id'     => $request->region_id,
            'province_id'   => $request->province_id,
            'city_id'       => $request->city_id,
            'barangay_id'   => $request->barangay_id,
            'street'        => $request->street,
            'crea_by'       => $request->long_name,
            'upd_by'        => $upd_by,
            'updated_at'    => $updated_at,
        ];
        UserMember::create($usermember_create);
        Auth::loginUsingId($users->id);
        // return back();
         return response()->json($usermember_create);
        
        // // // dd($request->enduser);
        // if($request->enduser != null){
        //     $request->session()->flash('success', 'Registration successfully!'); 
        //     return view('enduser.login');
        // }else{
        //     return response()->json(['success' => 'success'], 200);
        // }
        // return redirect()->route('pmes.index')->with('success', 'PMES created successfully.');
      
    }


    public function create()
    {
        return view('member.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function validateUniKey(Request $request)
    {
        // $user_id = auth()->user()->id;
        $uni_key = $request->uni_key;
        $validUniKey = Member::where('uni_key', 'like', $uni_key)->get();
        // $isnull = $user_emails->email;
        if(count($validUniKey))
        {
            $json = 'taken';
        }else{
             $json = 'nottaken';

        }
        
       return response()->json($json);
    }

    public function addmember(Request $request)
    {
        $message = [
                    'member_no.required' => 'Member Number is required',
                    'uni_key.required' => 'Unique Key is required',
                    'fname.required' => 'First name is required',
                    'lname.required' => 'Last name is required',
                    'branch_id.required' => 'Branch is required',
                    'contact_no.required' => 'Contact Number is required',
                    'gender.required' => 'Gender is required',
                    'birthdate.required' => 'Birthdate is required',
                    'member_type.required' => 'Member type is required',
                    'member_status.required' => 'Member status is required',
                    'email.required' => 'Email is required',
                    ];
        $validator = Validator::make($request->all(), [
            'member_no' => 'required',
            'uni_key' => 'required',
            'fname' => 'required',
            'lname' => 'required',
            'branch_id' => 'required',
            'gender' => 'required',
            'member_type' => 'required',
            'birthdate' => 'required',
            'contact_no' => 'required',
            'email' => 'required',
            'member_status' => 'required',
        ], $message);
        $errors = [];
        if ($validator->fails())
        {
            $errors = $validator->errors()->all();
            return response()->json(['message' => $errors,'status'=>'errors']);
        }
        // if($request->branch_id == 'MAIN'){
        //     $branch_id = 1;
        // }elseif($request->branch_id == 'KBO'){
        //     $branch_id = 2;
        // }elseif($request->branch_id == 'SMBO'){
        //     $branch_id = 3;
        // }elseif($request->branch_id == 'BBO'){
        //     $branch_id = 4;
        // }elseif($request->branch_id == 'CBO'){
        //     $branch_id = 5;
        // }elseif($request->branch_id == 'SRBO'){
        //     $branch_id = 6;
        // }

        // if(!empty($request->premember_no)){
        //     $member_no = $request->premember_no.'-'.$request->member_no;
        // }else{
            $member_no = $request->member_no;
        // }

            $uni_key = $member_no.$request->uni_key;

            $id = $request->id;
       
        $member =  Member::firstOrNew(['id' => $id]);
        if(is_null($id)){
            $member->member_no = $member_no;
            $member->uni_key = $uni_key;
            $member->crea_by = auth()->user()->uname;
            $member->crea_date = Carbon::now();
        }else{
            $member->upd_by = auth()->user()->uname;
            $member->upd_date = Carbon::now();
        }
        
            $member->fname = $request->fname;
            $member->mname = $request->mname;
            $member->lname = $request->lname;
            $member->long_name = $request->fname.' '.$request->mname.' '.$request->lname;
            $member->branch_id = $request->branch_id;
            $member->gender = $request->gender;
            $member->member_type = $request->member_type;
            $member->birthdate = $request->birthdate;
            $member->contact_no = $request->contact_no;
            $member->email = $request->email;
            $member->member_status = $request->member_status;
            $member->save();
             
        return response()->json(['success' => 'success'], 200);
       // Alert::success('Success','Member added.');
       //  return response()->back();
       
            // ->with('success', 'Account created successfully.');
    }

    public function storemember(Request $request)
    {
        $validatedDate = $request->validate
        ([
            'member_no' => 'required',
            'long_name' => 'required',
            'branch_id' => 'required',
            'contact_no' => 'required',
            'email_address' => 'required',
            'gender' => 'required',
            'birthdate' => 'required',
        ]);
        if(isset($request->id)){
            $upd_by = auth()->user()->uname;
            $updated_at = Carbon::now();
        }else{
            $upd_by = null;
            $updated_at = null;
        }
        $member_upd = 
        [
            'member_no' => $request->member_no,
        ];
        $member_create = 
        [
            'long_name'     => $request->long_name,
            'branch_id'     => $request->branch_id,
            'contact_no'    => $request->contact_no,
            'email_address' => $request->email_address,
            'gender'        => $request->gender,
            'birthdate'     => $request->birthdate,
            'crea_by'       => auth()->user()->uname,
            'upd_by'         => $upd_by,
            'updated_at'     => $updated_at,
        ];
        MemberMaster::updateOrcreate($member_upd, $member_create);
        if($request->member_no != null){
        $request->session()->flash('success', 'Member updated successfully!');
        }else{
        $request->session()->flash('success', 'Member created successfully!');    
        }
                return response()->json(['success' => 'success'], 200);
        // return redirect()->route('pmes.index')->with('success', 'PMES created successfully.');
      
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Project  $project
     * @return \Illuminate\Http\Response
     */
    // public function show($id, User $user)
    // {
    //     $user = User::find($id);
        
        
       
    //       return view('userTable.show', [
    //     'user' => $user
    //     ]);
    // }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $member  = MemberMaster::find($id);
       
       return view('member.edit', [
        'member' => $member 
        ]);

    }
    public function editmodal(Request $request, MemberMaster $member)
    {
        //
        $where = array('id' => $request->id);
        $member  = Member::where($where)->first();
        $json = array(
            'member_no' => $member->member_no,
            'long_name'     => $member->long_name,
            'lname'     => $member->lname,
            'fname'     => $member->fname,
            'mname'     => $member->mname,
            'branch'     => $member->branch_id,
            'contact_no'     => $member->contact_no,
            'email'     => $member->email,
            'birthdate'     => Carbon::parse($member->birthdate)->format('Y-m-d'),
            'status'    => $member->member_status,
            'type' => $member->member_type,
            'gender'        => $member->gender,
            'uni_key'     => $member->uni_key);
         // $request->session()->flash('success', 'member updated successfully!');
        return response()->json($json);
    }
    public function editusermembermodal(Request $request, UserMember $usermember)
    {
        //
        $where = array('id' => $request->id);
        $usermember  = UserMember::with('member', 'user', 'region', 'province', 'city', 'barangay')->where($where)->first();
        $json = array(
             'member_no'    => $usermember->member_no
            ,'contact_no'   => $usermember->contact_no
            ,'uname'        => $usermember->user->uname
            ,'email'        => $usermember->user->email
            ,'fb_link'      => $usermember->fb_link
            ,'street'       => $usermember->street
            ,'barangay_id'  => $usermember->barangay->name
            ,'city_id'      => $usermember->city->name
            ,'province_id'  => $usermember->province->name
            ,'region_id'    => $usermember->region->name
            ,'long_name'    => $usermember->member->long_name
            ,'birthdate'    => $usermember->member->birthdate
            ,'branch'       => $usermember->member->branch
            ,'status'       => $usermember->member->status
            ,'type'         => $usermember->member->type
            ,'gender'       => $usermember->member->gender
            ,'created_at'   => Carbon::parse($usermember->created_at)->format('m/d/Y')
        );
         // $request->session()->flash('success', 'member updated successfully!');
        return response()->json($json);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request)
    {
        $rules = array(
            'cd' => 'required',
            'description' => 'required',
        );
         $validator = Validator::make($request->all(), $rules);
         $member  = MemberMaster::find($id);
            $member ->cd      = $request->get('cd');
            $member ->description       = $request->get('description');
            $member ->updated_at      = Carbon::now();
            $member ->upd_by = auth()->user()->uname;
            $member ->save();
       
        return redirect()->route('member.index')
            ->with('success', 'Account updated successfully');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Project  $project
     * @return \Illuminate\Http\Response
     */
    
    public function userMemberIndex()
    {

        $usermember  = UserMember::with('member', 'user', 'region', 'province', 'city', 'barangay')
        // ['user' => function ($query) {
        // $query->with('branch');
        // }])
        ->get();
        // $branch = Branch::whereNull('soft_deleted_at')->get();
        // dd($usermember);
        return view('userMember.index', compact('usermember'));
    }
    public function deleteAll(Request $request)
    {
        $ids = $request->ids;
        $userMember = UserMember::whereIn('id',explode(",",$ids))->get();
       
        if($userMember->count())
        {
            foreach($userMember as $data)
            {
                $userMember = MemberMaster::find($data->id);
                $userMember->UserMember = Carbon::now();
                $userMember->save();
            }
        }
        $json = ['success'=>"UserMember Deleted successfully."];

        return response()->json($json);
    }
}
