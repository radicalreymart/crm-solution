<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMemberMastersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('member_masters', function (Blueprint $table) {
            $table->string('member_no')->index();
            $table->string('long_name');
            $table->unsignedBigInteger('branch_id');
            $table->foreign('branch_id', 'payment_fk_branch_id')->references('id')->on('branch');
            $table->integer('gender');
            $table->date('birthdate');
            $table->string('email_address');
            $table->string('contact_no', 13);
            $table->string('crea_by');
            $table->string('upd_by')->nullable();
            $table->datetime('soft_deleted_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('member_masters');
    }
}
