<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class isAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    // public function handle(Request $request, Closure $next)
    // {
    //     return $next($request);
    // }
    public function handle(Request $request, Closure $next)
    {

        if(auth()->user()->role_id == 0){
            return $next($request);
        }
         return redirect()->route('home')->with(['errors' => 'You do not have the permission to go there.']);
        //  $request->session()->flash('error', 'You do not have the permission to go there.');
        // return view('auth.login');
        
    }
}
