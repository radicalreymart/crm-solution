@extends('layouts.app', [
    'class' => 'sidebar-mini ',
    'namePage' => 'PMES Edit',
    'activePage' => 'pmes-management',
    'activeNav' => '',
])
@section('pmesjs')
  <script src="{{ asset('assets') }}/js/batchschedule.js"></script>
@endsection
@section('content')
<div class="panel-header panel-header-sm">
  </div>
  <div class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="card-header">
                <div class="float-left">
                    <h4 class="card-title">{{ __('Edit PMES') }} {{'ID Number '}} {{$pmes->id }}</h4>
                </div>
                <div class="float-right">
                    <a class="btn btn-primary" href="{{ url('/pmes/') }}"> Back</a>
                </div>
          
        <div class="card">
        <div class="card-body">
                    @if(count($errors) > 0 )
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
                <ul class="p-0 m-0" style="list-style: none;">
                @foreach($errors->all() as $error)
                <li>{{$error}}</li>
                @endforeach
                </ul>
                </div>
                @endif
                    <form action="{{ route('pmes.update', $pmes->id) }}" method="POST">
                        @csrf

                          {{ method_field('PUT') }}
                        <div class="form-group row">
                                         <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Client Name') }}</label>

                                            <div class="col-md-6">
                                                 @if(is_null($pmes->client_id))
                                                 <input type="text" readonly id="client_id1" class="form-control" value="Null">
                                                 <input type="text" hidden readonly id="client_id" class="form-control" value="{{$pmes->client_id}}" name="client_id">
                                                 @else
                                                <input type="text" readonly id="client_id1" class="form-control" value="{{$pmes->client->fullname}}" name="client_id">
                                                 <input type="text" hidden readonly id="client_id" class="form-control" value="{{$pmes->client_id}}" name="client_id">
                                                @endif

                                                
                                                @error('client_id')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Batch Code') }}</label>

                                            <div class="col-md-6">
                                                @if(is_null($pmes->batch_id))
                                                <select id="batch_id" name="batch_id" class="form-control">
                                                <option value="0">Currently Null</option>
                                                @foreach($batches as $batch)
                                                <option value="{{$batch->id}}">{{$batch->batch_cd}}</option>
                                                @endforeach
                                                </select>
                                                @else
                                                <select class="form-control m-bot15" id="batch_id" name="batch_id">
                                                @if ($batches->count())
                                                @foreach($batches as $batch)
                                                <option value="{{ $batch->id }}" {{ $pmes->batch_id == $batch->id ? 'selected="selected"' : '' }}>{{ $batch->batch_cd }}</option> 
                                                @endforeach   
                                                @endif
                                                </select>
                                                @endif

                                                @error('batch_id')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Schedule Date') }}</label>

                                            <div class="col-md-6">
                                                 @if(is_null($pmes->batch_id))
                                                 <input type="text" readonly id="sched_date" class="form-control">
                                                 @else
                                                <input type="text" readonly id="sched_date1" class="form-control" value="{{ \Carbon\Carbon::parse($pmes->batch->sched_date)->format('M d, Y') }}" name="sched_date">
                                                <input type="text" readonly id="sched_date" class="form-control" hidden value="{{$pmes->batch->sched_date}}" 
                                                name="sched_date">
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                                <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Schedule Time') }}</label>
                                                <div class="col-md-6">
                                                @if(is_null($pmes->batch_id))
                                                 <input type="text" readonly id="sched_time" class="form-control">
                                                 @else
                                                <input class="form-control" readonly type="text" id="sched_time" value="{{ \Carbon\Carbon::parse($pmes->batch->sched_date)->format('H:i a') }}" name="sched_time">
                                                @endif
                                                </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Preferred Application') }}</label>
                                            <div class="col-md-6">
                                                @if(is_null($pmes->batch_id))
                                                 <input type="text" readonly id="online_vid_tool" class="form-control">
                                                 @else
                                                 @if($pmes->batch->online_vid_tool == 0)
                                                <input type="text" readonly class="form-control" id="online_vid_tool" value="Google Meet" name="online_vid_tool1">
                                                <input type="text" readonly class="form-control" id="online_vid_tool" hidden value="{{$pmes->batch->online_vid_tool}}" name="online_vid_tool">
                                                @else
                                                <td>!</td>
                                                @endif   
                                                @endif 
                                            </div>
                                        </div>
                            </div>
                        </div>
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">

                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </div>
                    </form>
        </div>


        </div>
      </div>
    </div>
  </div>

@endsection
