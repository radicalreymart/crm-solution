<?php

namespace App\Http\Controllers;

use App\Models\STM;
use Illuminate\Http\Request;

class STMController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\STM  $sTM
     * @return \Illuminate\Http\Response
     */
    public function show(STM $sTM)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\STM  $sTM
     * @return \Illuminate\Http\Response
     */
    public function edit(STM $sTM)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\STM  $sTM
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, STM $sTM)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\STM  $sTM
     * @return \Illuminate\Http\Response
     */
    public function destroy(STM $sTM)
    {
        //
    }
}
