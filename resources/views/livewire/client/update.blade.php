<!-- Modal -->
<div wire:ignore.self class="modal fade" id="updateModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
       <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Edit Client</h5>
                <button type="button" class="close" wire:click="cancel" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form name='formname'>
                    <input type="hidden" value="{{url('/')}}" id="url" name="url">
                    <div class="form-group row">
                                   <label for="name" class="col-md-4 col-form-label text-md-left">{{ __('Full Name') }}</label>
                           </div>
                           <div class="form-group row">
                               <div class="col-md-4">
                                   <input id="fname" type="text" wire:model="fname" class="form-control @error('fname') is-invalid @enderror" name="fname" value="{{ old('fname') }}" placeholder="First Name"  autocomplete="fname" autofocus />
                                   @error('fname')
                                       <span class="invalid-feedback" role="alert">
                                           <strong>{{ $message }}</strong>
                                       </span>
                                   @enderror
                               </div>
                               <div class="col-md-4">
                                   <input id="mname" type="text" wire:model="mname" class="form-control @error('mname') is-invalid @enderror" name="mname" value="{{ old('mname') }}" placeholder="Middle Name"  autocomplete="mname" autofocus />
                                   @error('mname')
                                       <span class="invalid-feedback" role="alert">
                                           <strong>{{ $message }}</strong>
                                       </span>
                                   @enderror
                               </div>
                               <div class="col-md-4">
                                   <input id="lname" type="text"  wire:model="lname" class="form-control @error('lname') is-invalid @enderror" name="lname" value="{{ old('lname') }}" placeholder="Surname"  autocomplete="lname" autofocus />
                                   @error('lname')
                                       <span class="invalid-feedback" role="alert">
                                           <strong>{{ $message }}</strong>
                                       </span>
                                   @enderror
                               </div>
                           </div>
                           <!-- bday-age-gender -->
                            <div class="form-group row">
                                <label for="name" class="col-md-5 col-form-label float-left">{{ __('Birthdate') }}</label>
                                <label for="name" class="col-md-3 col-form-label float-left">{{ __('Age') }}</label>
                                <label for="name" class="col-md-4 col-form-label float-left">{{ __('Gender') }}</label>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-5">
                                <!--- date picker --->
                                <div class="form-group">
                                <input type="date" wire:model="birthdate"  id="datepicker"  class="form-control @error('birthdate') is-invalid @enderror" name="birthdate" value="{{ old('birthdate') }}">
                                </div>
                                @error('datepicker')
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                                </div>
                                <div class="col-md-2">
                                <input type="text" readonly wire:model='age' autocomplete="age" id="agecal" tabindex="-1" name="age"  class="form-control">
                                @error('age')
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                                </div>
                                <div class="col-md-5">
                                <select id="gender" wire:model="gender" type="text" class="form-control @error('gender') is-invalid @enderror" name="gender" value="{{ old('gender') }}"  autocomplete="gender" autofocus>

                                <option value="" >Select Gender</option>
                                <option value="0" >Male</option>
                                <option value="1" >Female</option>
                                </select>
                                @error('gender')
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                                </div>
                            </div> 
                            <!-- contact no-gmail address -->
                            <div class="form-group row">
                                <label for="name" class="col-md-4 col-form-label text-md-left">{{ __('Contact Number') }}</label>
                                <label for="name" class="col-md-4 col-form-label text-md-left">{{ __('Gmail Address') }}</label>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-6 form-cotrol input-group">
                                    <div class="input-group-prepend form-cotrol">
                                        <div class="input-group-text form-cotrol"><i>{{ __('+63') }}</i></div>
                                    </div>
                                    <input id="contact_no"  wire:model="contact_no" type="text" onkeydown="return ( event.ctrlKey || event.altKey 
                                    || (47<event.keyCode && event.keyCode<58 && event.shiftKey==false) 
                                    || (95<event.keyCode && event.keyCode<106)
                                    || (event.keyCode==8) || (event.keyCode==9) 
                                    || (event.keyCode>34 && event.keyCode<40) 
                                    || (event.keyCode==46))" minlength="0" maxlength="10"  class="input-group form-control @error('contact_no') is-invalid @enderror" name="contact_no" value="{{ old('contact_no') }}" placeholder="9386166587 "  autocomplete="contact_no" title="Ex: 9386166587" />
                                    @error('contact_no')
                                    <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror    
                                </div>
                                <div class="col-md-6">
                                    <input id="gmail_address" wire:model="gmail_address" type="text" class="form-control @error('gmail_address') is-invalid @enderror" name="gmail_address" value="{{ old('gmail_address') }}" placeholder="Gmail Email Address"  autocomplete="gmail_address" autofocus />

                                    @error('gmail_address')
                                    <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <!-- present address -->
                            <div class="form-group row">
                                <label for="name" class="col-md-4 col-form-label float-left">{{ __('Present Address') }}</label>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-4">
                                <select id="region_id" wire:model="selectedRegion" name="region_id" class="form-control">
                                <option selected value="0">Select Region</option>
                                 @foreach($regions as $item)
                                <option value="{{$item->region_id}}" >{{$item->name}}</option>
                                @endforeach
                                </select>
                                <input type="text"  id="region_hidden" name="region_hidden"  readonly hidden class="form-control">
                                <div id='response'></div>
                                @error('region_id')
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                                </div>
                                <div class="col-md-4">
                                    <select id="province_id2" wire:model="selectedProvince" name="province_id" class="form-control">
                                    <option  value="0">Select Province</option>
                                    @if(!is_null($selectedRegion))
                                    @foreach($province as $item)
                                    <option  value="{{$item->province_id}}">{{$item->name}}</option>
                                    @endforeach
                                    </select>
                                @endif
                                <input type="text" id="province_hidden"  name="province_hidden" hidden readonly  class="form-control">
                                <div id='response'></div>
                                @error('province_id')
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                                </div>
                                <br>
                                <br>
                                <div class="col-md-4">
                                    <select id="city_id2" wire:model="selectedCity" name="city_id" class="form-control">
                                    <option  value="0">Select City</option>
                                    @if(!is_null($selectedProvince))
                                    @foreach($city as $item)
                                    <option  value="{{$item->city_id}}">{{$item->name}}</option>
                                    @endforeach
                                    </select>
                                
                                @endif
                                <input type="text" id="city_hidden" wire:model="city_hidden" name="city_hidden" hidden readonly  class="form-control">
                                <div id='response'></div>
                                @error('city_id')
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-6">
                                <select id="barangay_id2" wire:model="selectedBarangay" name="barangay_id" class="form-control">
                                        <option  value="0">Select Barangay</option>
                                @if(!is_null($selectedCity))
                                    @foreach($barangay as $item)
                                    <option  value="{{$item->code}}">{{$item->name}}</option>
                                    @endforeach
                                    </select>
                                @endif
                                <input type="text" wire:model="barangay_hidden" id="barangay_hidden" name="barangay_hidden" hidden readonly  class="form-control">
                                <div id='response'></div>
                                @error('barangay_id')
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                                </div>
                                <div class="col-md-6">
                                    <input id="street" type="text" wire:model="street" class="form-control @error('street') is-invalid @enderror" name="street" value="{{ old('street') }}" placeholder="Street Number"  autocomplete="street" autofocus />
                                    @error('street')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div> 
                            <!-- occu-oftag-marChanel-purpose -->
                            <div class="form-group row">
                                <label for="name" class="col-md col-form-label float-left">{{ __('Occupation') }}</label>
                            </div>
                            <div wire:ignore class="form-group row">
                                <div  class="col-md-6"  >
                                <select  id="occupation1" wire:model="occupation" name="occupation1" class="form-control">
                                <option value="">Select Occupation</option>
                                @foreach($occupations as $item)
                                <option  value="{{$item->id}}">{{$item->name}}</option>
                                @endforeach
                                <option >Other</option>
                                </select>
                                </div>

                                   @if($this->occupation == null)
                                <div  class="col-md-6"  >
                                <input id="other_occupation" wire:model="other_occupation"  type="text" class="form-control  @error('other_occupation') is-invalid @enderror" name="other_occupation" value="{{ old('other_occupation') }}" placeholder="Occupation"  autocomplete="other_occupation" autofocus />
                                </div>
                                @endif
                            </div>
                            <div class="form-group row">
                                <label for="name" class="col-md col-form-label float-left">{{ __('Do you have relative/s working or residing abroad or are you and Oversees filipino working/residing abroad?') }}</label>
                            </div>
                            <div class="form-group row">
                                <div  class="col-md" >
                                <select id="of_tag" name="of_tag" wire:model="of_tag" class="form-control">
                                <option value="">Select</option>
                                <option value= 0 >I am Overseas Filipinos working/residing abroad</option>
                                <option value= 1 >I have relative/s working/residing abroad</option>
                                <option  value= 2 >I dont have relatives nor myself working/residing abroad</option>
                                </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="name" class="col-md col-form-label float-left">{{ __('Where did you hear about Most Holy Rosary Coop?') }}</label>
                            </div>
                            <div class="form-group row">
                                <div  class="col-md" >
                                <select id="marketing_channel" wire:model="marketing_channel" name="marketing_channel" class="form-control">
                                <option value="">Select</option>
                                @foreach($marketingChannel as $item)
                                <option  value="{{$item->id}}">{{$item->name}}</option>
                                @endforeach
                                </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="name" class="col-md col-form-label float-left">{{ __('Why did you choose to be a member with us?') }}</label>
                            </div>
                            <div class="form-group row">
                                <div  class="col-md" >
                                <select id="purpose_tag" wire:model="purpose_tag" name="purpose_tag" class="form-control">
                                <option value="">Select</option>
                                <option value= 0 >For investment only</option>
                                <option value= 1 >For investment and savings</option>
                                <option value= 2 >To avail loan products only</option>
                                <option value= 3 >To avail trading products and discounts</option>
                                <option value= 4 >To avail all products and services offered (investment, savings, loans, tradings, etc.</option>
                                </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="card-header1">{{ __('Membership Preference') }}</div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-5">
                                <label for="name" class="col-md-10 col-form-label float-left">{{ __('Membership Status') }}</label>
                                </div>
                                <div class="col-md-6">
                                <label for="name" class="text-muted col-md-10 col-form-label float-left">{{ __('Please choose select to reset the dropdown') }}</label>
                                </div>
                            </div>
                            <div  class="form-group row">
                                <div class="col-md-7">
                                    <select id="member_status"  v-model="status" wire:model="member_status" name="member_status" class="form-control">
                                    <option value="">Select</option>
                                    <option  value= "0" >New Member</option>
                                    <option value= "1" >Returning Member</option>
                                    <option value= "2" >From Associate Member</option>
                                    </select>
                                    <div id='response'></div>
                                    @error('member_status')
                                    <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                
                                @if($this->member_status == "")
                                @elseif($this->member_status == 0)
                                <div  class="col-md-5">
                                    <input  id="staff_referral_cd" wire:model="staff_referral_cd"   placeholder="Staff Referal Code" name="staff_referral_cd" class="form-control"></input>
                                    @error('staff_referral_cd')
                                    <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                               @else
                                <div  class="col-md-5">
                                    <input   id="member_no" wire:model="member_no" placeholder="Member No" name="member_no"  class="form-control"></input>
                                    @error('member_no')
                                    <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                @endif
                               
                            </div>
                            <div class="form-group row">
                                <div class="col-md-4">     
                                    <label for="name" class="col-md-10 col-form-label float-left">{{ __('Online or Physical Seminar') }}</label>
                                </div>
                                <div class="col-md-4">
                                    <label for="name" class="col-md-10 col-form-label float-left">{{ __('Preferred MHRMPC Branch Office') }}</label>
                                </div>
                                <div class="col-md-4">
                                    <label for="name" class="col-md-10 col-form-label float-left">{{ __('Preferred Schedule') }}</label>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-4">
                                    <select id="seminar_pref" wire:model="seminar_pref" name="seminar_pref" class="form-control">
                                    <option value="">Select</option>
                                    <option value="0">Online Video Conference</option>
                                    <option value="1">Walk-in / Physical Appearance</option>
                                    </select>
                                    <div id='response'></div>
                                    @error('seminar_pref')
                                    <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                <div class="col-md-4">
                                    <select id="branch_id" wire:model="branch_id" name="branch_id" class="form-control">
                                    <option value="">Select</option>
                                    @foreach($branches as $item)
                                    <option value="{{$item->id}}">{{$item->name}}</option>
                                    @endforeach
                                    </select>
                                    <div id='response'></div>
                                    @error('branch_id')
                                    <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                </div>
                                <div class="col-md-4">
                                    <select id="batch_id1" wire:model="selectedBatch" name="batch_id1" class="form-control">
                                    <option value="">Select</option>
                                    @foreach($batch as $data)
                                    <option  value="{{$data->id}}" >{{ \Carbon\Carbon::parse($data->sched_date)->format('m-d-Y')}}</option>
                                    @endforeach
                                    </select>
                                    <div id='response'></div>
                                    @error('batch_id')
                                    <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-10 ">
                                <label for="name">{{ __('Please click yes if your are giving us the consent to use the data your submited') }}</label>
                                </div>
                                <div style="justify-content: center;" class="col-md-1 ">
                                <label for="name">{{ __('Yes') }}</label>
                                </div>
                                <div class="col-md-1 ">
                                <input name="data_consent_fl" wire:model="data_consent_fl" type="checkbox" >
                                </div>
                            </div>

                        
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" wire:click.prevent="cancel()" tabindex="-1" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" wire:click.prevent="update()" class="btn btn-primary" data-dismiss="modal">Save</button>
            </div>
       </div>
    </div>
</div>

