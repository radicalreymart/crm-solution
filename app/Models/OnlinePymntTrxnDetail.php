<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\OnlinePymntTrxnMaster;
use App\Models\AcctMaster;

class OnlinePymntTrxnDetail extends Model
{
    use HasFactory;
    protected $table = 'online_pymnt_trxn_details';
    // use HasFactory;
    protected $fillable = [
        'online_pymnt_id',
        'acct_id',
        'amount',
        'crea_by',
        'created_at',
        'upd_by',
        'updated_at',
        'soft_deleted_at',
    ];
    public function optm()
    {
        return $this->belongsTo('App\Models\OnlinePymntTrxnMaster', 'online_pymnt_id','id');
    }
    public function account()
    {
        return $this->belongsTo('App\Models\AcctMaster','acct_id', 'id');
    }
}
