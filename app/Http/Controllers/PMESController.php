<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\PMES;
use App\Models\Batch;
use App\Models\Client;
use App\Models\STM;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendMail;
use Illuminate\Support\Facades\Route;
use App\Exports\PMESExport;
use Maatwebsite\Excel\Facades\Excel;
use Alert;
use DateTime;


class PMESController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $department = auth()->user()->department_id;
        $branch = auth()->user()->branch_id;
        $isAdmin = auth()->user()->role_id;
        if($department == 1 || $isAdmin == 0){
            $pmes1 = PMES::with('client', 'batch');

            if($branch == 9){
                $pmes1->whereNull('soft_deleted_at')
                ->orderBy('serv_status');
            }else{
                $pmes1->where('client_branch', $branch)
                ->whereNull('soft_deleted_at')
                ->orderBy('serv_status');
            }
                $pmes1->orwhereNull('batch_id')
                ->whereHas('batch', function($q){
                $now = Carbon::now();
                $q->whereDate('sched_date', '>=' , $now)
                ;})->orderBy('serv_status');

            $pmes = $pmes1->get();

            $now1 = Carbon::now()->format('Y-m-d');
            $now = Carbon::now();
            $batches = Batch::whereDate('sched_date', '>=' , $now)
                    ->whereNull('soft_deleted_at')->get();
            // dd($pmes);
            return view('pmes.index', compact('pmes','batches', 'now1'));    
        }else{
            Alert::error("You do not have the permission to access this page.");
            return redirect()->route('home');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $clients = Client::where('sched_stat', 'like', 0)->get();
        $now = Carbon::now();
        $batches = Batch::whereDate('sched_date', '>' , $now)->whereNull('soft_deleted_at')->get();
        return view('pmes.create', compact('batches', 'clients'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        PMES::create([
             'batch_id' => $request['batch_id'],
             'serv_status' => '1',
            'crea_by' => auth()->user()->uname,
        ]);
            $meeting_cd = Batch::where('id', $request['batch_id'])->first()->meeting_cd;
            $sched_date = Batch::where('id', $request['batch_id'])->first()->sched_date;
            $pmes  = PMES::where($request['batch_id'])->first();

            $editclient =  Client::where('id', 'like', $pmes->client_id)->first();
            $details = [
            'meeting_cd' => $meeting_cd,
            'sched_date' => $sched_date,
            'resched_remarks' => '.'
            ];
            $editclient = Client::find($request['client_id']);
                $editclient->sched_date = $sched_date;
                $editclient->save();
            // dd($meeting_cd);
            Mail::to($editclient->gmail_address)->queue(new SendMail($details));
       
       return redirect()->route('pmes.index')
            ->with('success', 'PMES created successfully.');
    }
    public function storepmes(Request $request)
    {
        // PMES::updateOrCreate([
        //      'batch_id' => $request['batch_id'],
        //      'serv_status' => '1',
        //     'crea_by' => auth()->user()->uname,
        // ]);
        $meeting_cd = Batch::where('id', $request['batch_id'])->first()->meeting_cd;
        $sched_date = Batch::where('id', $request['batch_id'])->first()->sched_date;
        $pmes  = PMES::where('batch_id','like', $request['batch_id'])->first();
        $editclient =  Client::where('id', 'like', $request['id'])->first();
        if(isset($request->id)){
            $upd_by = auth()->user()->uname;
            $updated_at = Carbon::now();
        }else{
            $upd_by = null;
            $updated_at = null;
        }
        $pmes = PMES::find($request->id);
        if($pmes->batch_id != $request->get('batch_id'))
        {
            if(isset($pmes->batch_id)){
            $prevparticipant = Batch::where('id', $pmes->batch_id)->first()->participant;
            Batch::whereId($pmes->batch_id)->update([
                'participant' => $prevparticipant-1,
            ]);
            }
            $curparticipant = Batch::where('id', $request['batch_id'])->first()->participant;
            Batch::whereId($request['batch_id'])->update([
                'participant' => $curparticipant+1,
            ]);
        }
        $pmes->client_id      = $pmes->client_id;    
        $pmes->batch_id       = $request->get('batch_id');
        $pmes->membership       = $request->get('membership');
        $pmes->serv_status     = 1;    
        $pmes->updated_at     = $updated_at;
        $pmes->upd_by         = $upd_by;
        $pmes->save();

        // STM::create([
        //              'pmes_id' => $pmes->id,
        //              'service_details'=> '.',
        //             'service_status' => 2,
        //             'service_type_id' => 1,
        //              'crea_by' => auth()->user()->uname,
        //         ]);
            if($pmes->wasChanged('membership')){
                // dd('memberhip has changed');
            }else{
            $meeting_cd = Batch::where('id', $request['batch_id'])->first()->meeting_cd;
            $sched_date = Batch::where('id', $request['batch_id'])->first()->sched_date;
            $vid_tool = Batch::where('id', $request['batch_id'])->first()->online_vid_tool;
            $pmes  = PMES::where('batch_id','like', $request['batch_id'])->first();
            $editclient =  Client::where('id', 'like', $pmes->client_id)->first();
            if($vid_tool == 0){
                $link = 'https://meet.google.com/'.$meeting_cd;
                $description = 'Online Pre-Membership Education Webinar';
            }elseif($vid_tool == 1){
                $link = 'https://us05web.zoom.us/j/86189281691?pwd=aWJSMVRDM2xRQURIcTNVcXRrOTMvQT09';
                $description = 'Online Pre-Membership Education Webinar';
            }else{
                $link = null;
                $meeting_cd = null;
                $description = 'Online Pre-Membership Education Seminar';
            }
            $editclient = Client::find($pmes->client_id);
            $editclient->sched_date = $sched_date;
            $editclient->sched_stat = 2;
            $editclient->save();
            // https://meet.google.com/{{ $details['meeting_cd'] }}
            $start_date = \Carbon\Carbon::parse($sched_date)->format('Ymd\THis');
            $end_date = \Carbon\Carbon::parse($sched_date)->addHour(2)->format('Ymd\THis');
            $details = [
                'vid_tool' => $vid_tool,
                'link'   => $link,
                'meeting_cd' => $meeting_cd,
                'sched_date' => $sched_date,
                'resched_remarks' => $request['resched_remarks'],
                'text' => 'OLPMEW'.\Carbon\Carbon::parse($sched_date)->format('Ymd'),
                'description' => $description,
                'startdate' => $start_date,
                'enddate' => $end_date,
                'email' => $editclient->gmail_address,
            ];
                // return response()->json(['success' => $sched_date1], 200);
            // dd($meeting_cd);
            Mail::to($editclient->gmail_address)->queue(new SendMail($details));
            // dd('hello');
            }
             $request->session()->flash('success', 'PMES updated successfully!');
                return response()->json(['success' => 'success'], 200);
        // return redirect()->route('pmes.index')->with('success', 'PMES created successfully.');
      
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\PMES_Master  $pmes_Master
     * @return \Illuminate\Http\Response
     */
    // public function show($id, PMES $pmes)
    // {
    //     $pmes = PMES::find($id);
    //     // dd($pmes);
    //     return view('pmes.show', compact('pmes'));  
    // }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\PMES_Master  $pmes_Master
     * @return \Illuminate\Http\Response
     */
    public function edit($id, PMES $pmes)
    {
        //
        $pmes = PMES::find($id);
        // dd($pmes->client_id);
        $clients =  Client::where('id', 'like', $pmes->client_id)->get();
        $now = Carbon::now();
        // $batches = Batch::whereDate('sched_date', '<' , $now)->get();
        // dd($request);
        
        return view('pmes.edit', [
        'pmes' => $pmes,'clients' => $clients,'batches' => $batches,
        ]);
    }
    public function editmodal(Request $request, PMES $pmes)
    {
        //
        $where = array('id' => $request->id);
        $pmes  = PMES::where($where)->first();
        $editclient =  Client::where('id', 'like', $pmes->client_id)->first();
        if(isset($pmes->batch_id))
        {
        $editbatch =  Batch::where('id', 'like', $pmes->batch_id)->first();
        // dd($pmes);
        $sched_date = Carbon::parse($editbatch->sched_date)->format('M d,Y');
        $sched_time = Carbon::parse($editbatch->sched_date)->format('H:i');
        $online_vid_tool = $editbatch->online_vid_tool;
        }else
        {
            $editbatch = null;
            $sched_date = null;
            $sched_time = null;
            $online_vid_tool = null;
        }
        $json = array('id' => $request->id
            , 'fullname' => $editclient->fullname
            , 'sched_date' => $sched_date
            ,'sched_time' => $sched_time
            , 'tool' => $online_vid_tool
            , 'batch_id' => $pmes->batch_id
            , 'attendance' => $pmes->attendance
            , 'membership' => $pmes->membership);
         $request->session()->flash('success', 'PMES updated successfully!');
        return response()->json($json);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\PMES_Master  $pmes_Master
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request, PMES $pmes)
    {
        // $pmes  = PMES::where('batch_id','like', $request['batch_id'])->first();
        // $pmes  = PMES::with('client')->where('id', $id)->first();
        // dd($pmes->client->gmail_address);

         $rules = array(
            'batch_id' => 'required ',
            'client_id' => 'required',
        );
        $validator = Validator::make($request->all(), $rules);
        $pmes = PMES::find($id);
        $pmes->client_id      = $request->get('client_id');    
        $pmes->batch_id       = $request->get('batch_id');    
        $pmes->membership       = $request->get('membership');
        $pmes->serv_status     = 1;    
        $pmes->updated_at     = Carbon::now();
        $pmes->upd_by         = auth()->user()->uname;
        // dd($request);
        $pmes->save();

        $meeting_cd = Batch::where('id', $request['batch_id'])->first()->meeting_cd;
        $sched_date = Batch::where('id', $request['batch_id'])->first()->sched_date;
        // dd($sched_date);
        $email = Client::where('id', $request['client_id'])->first()->gmail_address;
        $details = [
        'meeting_cd' => $meeting_cd,
        'sched_date' => $sched_date,
            'resched_remarks' => '.'
        ];

        $client = Client::find($request->get('client_id'));
        $client->sched_stat = 1;
        $client->sched_date = $sched_date;
        $client->save();
            // dd($meeting_cd);
            Mail::to($email)->queue(new SendMail($details));

        return redirect()->route('pmes.index')
            ->with('success', 'PMES updated successfully');
    }
    
    public function destroy($id)
    {
        $pmes = PMES::find($id);
        $pmes->delete();

        return response()->json(['success'=>"PMES deleted successfully.", 'tr'=>'tr_'.$id]);
    }
    public function deleteAll(Request $request)
    {
        $ids = $request->ids;
        $pmess = PMES::whereIn('id',explode(",",$ids))->whereNull('soft_deleted_at')->get();
        if($pmess->count())
        {
            foreach($pmess as $data)
            {
                $pmes = PMES::find($data->id);
                $pmes->soft_deleted_at = Carbon::now();
                $pmes->save();
                $batchs = Batch::where('id',$data->batch_id)->whereNull('soft_deleted_at')->get();
                $clients = Client::where('id',$data->client_id)->whereNull('soft_deleted_at')->get();
                if($batchs->count())
                {
                    foreach($batchs as $data)
                    {
                        $participant = Batch::where('id', $data->id)->first()->participant;
                        Batch::whereId($data->id)->update([
                            'participant' => $participant-1,
                        ]);
                    }
                }
                if($clients->count())
                {
                    foreach($clients as $data)
                    {
                        $clients = Client::find($data->id);
                        $clients->sched_stat = 0;
                        $clients->save();
                    }
                }
                // return response()->json($data->id);
            }
        }
        // if($batchs->count())
        // {
        //     foreach($batchs as $data)
        //     {
        //         $batchs = Batch::find($data->id);
        //         $batchs->participant -= 1;
        //         $batchs->save();
        //     }
        // }
        // if($batchs->count())
        // {
        //     foreach($batchs as $data)
        //     {
        //         $batchs = Batch::find($data->id);
        //         $batchs->participant -= 1;
        //         $batchs->save();
        //     }
        // }
        // if($clients->count())
        // {
        //     foreach($clients as $data)
        //     {
        //         $clients = Client::find($data->id);
        //         $clients->sched_stat = 0;
        //         $clients->save();
        //     }
        // }

        // return response()->json([$pmess, $batchs, $clients]);
        return response()->json(['success'=>"PMES deleted successfully."]);
    }
    public function attendance($id, Request $request, PMES $pmes)
    {
        $attendance = $request['attendance'];
        // dd($id);
       $pmes = PMES::find($id);
       $pmes->attendance     =  $request->get('attendance');    
       $pmes->save();

        return redirect()->route('pmes.index')->with('success', 'PMES attendance edited successfully');
    }
    public function exportExcel(Request $request) 
    {
        $start = $request->start.' 00:00:00';
        $end = $request->end.' 23:59:59';
        $start_end = $start.', '.$end;
        $date = date('M-d-Y', strtotime($start)).'-'.date('M-d-Y', strtotime($end));
        return \Excel::download(new PMESExport($start, $end), 'PMES('.$date.').'.'xlsx');
    }
}

