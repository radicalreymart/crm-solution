<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Branch;
use App\Models\Department;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\UserRequest;
use Alert;

class UserController extends Controller
{
    /**
     * Display a listing of the users
     *
     * @param  \App\Models\User  $model
     * @return \Illuminate\View\View
     */
    public function index(User $model)
    {
        $department = auth()->user()->department_id;
        $isAdmin = auth()->user()->role_id;
        $branch = auth()->user()->branch_id;
        if($isAdmin == 0){

            $users = User::with('branch', 'department');
            // ->whereNull('soft_deleted_at')
            if($branch == 9){
               $users->whereNull('soft_deleted_at');
           }else{
               $users->where('branch_id', $branch)
               ->whereNull('soft_deleted_at');
           }
            $users = $users->get();
            $branch  = Branch::whereNull('soft_deleted_at')
                    // ->Where('cd', '!=', 'TRD')
                    // ->Where('cd', '!=', 'FR')
                    ->Where('cd', '!=', 'ADMIN')
                    ->get();
            $department = Department::all();
            return view('userv2.index', compact('users', 'branch', 'department'));
        }else{
            Alert::error("You do not have the permission to access this page.");
            return redirect()->route('home');
        }
        
    }
    public function storeuser(Request $request)
    {
        $message = 
        [
            'uname.required' => 'Username is required.',
            'branch_id.required' => 'Branch is required.',
            'department_id.required' => 'Department is required.',
            'employee_role.required' => 'Employee designation is required.',
            'email.required' => 'Email is required.',
            'password.required' => 'Password is required.',
            'email.unique' => 'Email is already taken.',
        ];
        $validatedDate = $request->validate
        ([
            'uname' => 'required|string|max:255',
            'branch_id' => 'required',
            'department_id' => 'required',
            'employee_role' => 'required',
            'email' => 'required|string|email|max:255|unique:users,email,'.$request['id'],
            'password' => 'required|string|min:8|same:password_confirm',
        ], $message);
        if(isset($request->id)){
            $upd_by = auth()->user()->uname;
            $updated_at = Carbon::now();
        }else{
            $upd_by = null;
            $updated_at = null;
        }
        $user_upd = 
        [
            'id' => $request['id'],
        ];
        $user_create = 
        [
             'email' => $request['email'],
            'uname' => $request['uname'],
             'branch_id' => $request['branch_id'],
             'department_id' => $request['department_id'],
            'password' => Hash::make($request['password']),
            'role_id' =>1,
            'employee_role' =>$request['employee_role'],
            'crea_by' => auth()->user()->uname,
            'upd_by'         => $upd_by,
            'updated_at'     => $updated_at,
        ];
        User::updateOrcreate($user_upd, $user_create);
        if($request->id != null){
        $request->session()->flash('success', 'User updated successfully!');
        }else{
        $request->session()->flash('success', 'User created successfully!');    
        }
                return response()->json(['success' => 'success'], 200);
    }
    public function editmodal(Request $request, User $user)
    {
        //
        $where = array('id' => $request->id);
        $user  = User::where($where)->first();
        $json = array(
            'id' => $request->id,
            'uname'     => $user->uname,
            'email'     => $user->email,
            'branch_id'    => $user->branch_id,
            'department_id'    => $user->department_id,
            'password' => $user->password,
            'employee_role'        => $user->employee_role,
        );
         // $request->session()->flash('success', 'acctMaster updated successfully!');
        return response()->json($json);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate([
            'uname' => 'required|string|max:255',
            'branch_id' => 'required',
            'employee_role' => 'required',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:8|confirmed',
        ]);
        $checked = $request->has('employee_role') ? 1 : 0;
       User::create([
             'uname' => $request['uname'],
             'email' => $request['email'],
             'branch_id' => $request['branch_id'],
             'department_id' => $request['department_id'],
            'password' => Hash::make($request['password']),
            'role_id' =>1,
            'employee_role' =>$checked,
            'crea_by' => auth()->user()->uname,
        ]);
       
       
        return redirect()->route('userv2.index')
            ->with('success', 'User created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Project  $project
     * @return \Illuminate\Http\Response
     */
    // public function show($id, User $user)
    // {
    //     $user = User::find($id);
        
        
       
    //       return view('userTable.show', [
    //     'user' => $user
    //     ]);
    // }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        $branches  = Branch::whereNull('soft_deleted_at')
        ->Where('cd', '!=', 'TRD')
        ->Where('cd', '!=', 'FR')
        ->Where('cd', '!=', 'ADMIN')
        ->get();
       
       return view('users.edit', [
        'branches' => $branches,
        'user' => $user
        ]);

    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request, User $user)
    {
        $rules = array(
            'uname' => 'required|string|max:255',
            'branch_id' => 'required',
            'employee_role' => 'required',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:8|confirmed',

        );
        $checked = $request->has('employee_role') ? 1 : 0;
         $validator = Validator::make($request->all(), $rules);
         $user = User::find($id);
            $user->uname       = $request->get('uname');
            $user->branch_id      = $request->get('branch_id');
            $user->department_id      = $request->get('department_id');
            $user->email      = $request->get('email');
            $user->employee_role      = $checked;
            $user->password      = Hash::make($request['password']);
            $user->updated_at      = Carbon::now();
            $user->upd_by = auth()->user()->uname;
            $user->save();
       
        return redirect()->route('userv2.index')
            ->with('success', 'User updated successfully');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Project  $project
     * @return \Illuminate\Http\Response
     */
    
    public function destroy($id)
    {
        $user = User::find($id);
        $user->delete();

        return response()->json(['success'=>"User deleted successfully.", 'tr'=>'tr_'.$id]);
     
    }
    public function deleteAll(Request $request)
    {
        $ids = $request->ids;
        $users = User::whereIn('id',explode(",",$ids))->whereNull('soft_deleted_at')->get();
        if($users->count())
        {
            foreach($users as $data)
            {
                $user = User::find($data->id);
                $user->soft_deleted_at = Carbon::now();
                $user->save();
            }
        }
        return response()->json(['success'=>"User deleted successfully."]);
    }

}
