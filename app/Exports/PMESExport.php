<?php

namespace App\Exports;

use App\Models\PMES;
use App\Http\Controllers\PMESController;
use Maatwebsite\Excel\Concerns\FromCollection;
use Carbon;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithCustomCsvSettings;
use PhpOffice\PhpSpreadsheet\Cell\Cell;
use PhpOffice\PhpSpreadsheet\Cell\DataType;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;


class PMESExport implements FromQuery, WithHeadings, WithMapping, ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */
    protected $from_date;
    protected $to_date;

    public function __construct($from_date, $to_date) {
        $this->from_date = $from_date;
        $this->to_date = $to_date;
    }
    public function query()
    {
        $from = $this->from_date;
        $to = $this->to_date;
        // $to = Carbon\Carbon::now();
            $branch = auth()->user()->branch_id;
            $pmes  = PMES::with('batch','client','branch','client.region', 'client.province', 'client.city', 'client.barangay', 'client.marketingChannel');
            // dd($miuf->get());
            if($branch == 9){
                // $pmes->query();
                $pmes->whereNull('soft_delete_at');
            }else{
                // $pmes->query()
                $pmes->where('client_branch', $branch)->whereNull('soft_delete_at');
                // ->orderBy('serv_status');
            }
            $pmes->whereBetween('created_at', [$from, $to]);
            $pmes = $pmes->orderBy('id');
            // dd($pmes->get());
        return $pmes;
    }
    public function headings(): array
    {
        return [
         'client_id'
                ,'client_status'
                ,'fullname'
                ,'member_status'
              ,'gender'
              ,'contact_no'
              ,'fb_link'
              ,'seminar_pref'
              ,'gmail_address'
              ,'birthdate'
              ,'age'
              ,'street'
              ,'barangay_id'
              ,'city_id'
              ,'province_id'
              ,'region_id'
              ,'occupation'
              ,'is OFW'
              ,'OFW relative'
              ,'marketing_channel'
              ,'purpose_tag'
              ,'prefered_batch'
              ,'client_branch'
        ,'batch_id'
        ,'batch_sched_date'
            ,'batch_cd'
            ,'online_vid_tool'
            ,'meeting_cd'
            ,'meeting_pw'
            ,'resched_remarks'
            ,'attendance'
        ,'membership'
        ,'crea_by'
        ,'upd_by'
        ,'date_created'
        ,'date_updated'
        ];
    }
    public function map($pmes): array
    {
        $from = $this->from_date;
        $to = $this->to_date;
        // dd($pmes);
        $prefered_batch = $pmes->client->batch->sched_date.', '.$pmes->client->batch->batch_cd;
        if($pmes->client->gender == 0){
            $gender = 'Male';
        }elseif($pmes->client->gender == 1){
            $gender = 'Female';
        }else{
            $gender = 'Rather not say';
        }
        
        $of_tag = explode(',',$pmes->client->of_tag);
        if($of_tag[0] == 1){
            $self = 'Yes';
        }else{
            $self = "No";
        }
        if($of_tag[1] == 1){
            $relative = 'Yes';
        }else{
            $relative = "No";
        }

        if($pmes->client->sched_stat == 0){
            $sched_stat = 'Unverified Email';
        }elseif($pmes->sched_stat == 1){
            $sched_stat = 'Batch Pending';
        }else{
            $sched_stat = 'Email Sent';
        }

        if($pmes->client->seminar_pref == 0){
            $seminar_pref = 'Online Seminar';
        }else{
            $seminar_pref = 'Walk-in';
        }
        if($pmes->client->member_status == 0){
            $member_status = $pmes->client->member_no.':Returning Member';

        }else if($pmes->client->member_status == 1){
            $member_status = $pmes->client->member_no.':From Associate Member';
        }else{
            $member_status = 'Non-member';
        }

        if($pmes->batch_id != null){
            $online_vid_tool = $pmes->batch->online_vid_tool;
            if($online_vid_tool == 0){
                $vid_tool = 'Google Meet';
            }else if($online_vid_tool == 1){
                $vid_tool = 'Zoom';
            }else{
                $vid_tool = 'Walk-in';
            }

            $batch_cd = $pmes->batch->batch_cd;
            $resched_remarks = $pmes->batch->resched_remarks;
            $meeting_pw = $pmes->batch->meeting_pw;
            $meeting_cd = $pmes->batch->meeting_cd;
        }else{
            $batch_cd = null;
            $resched_remarks = null;
            $vid_tool = null;
            $meeting_pw = null;
            $meeting_cd = null;
        }

        if($pmes->client->region == null){
            $barangay = null;
             $city = null;
             $province = null;
             $region = null;
        }else{
             $barangay = $pmes->client->barangay->name;
             $city = $pmes->client->city->name;
             $province = $pmes->client->province->name;
             $region = $pmes->client->region->name;
        }

        if($pmes->client->occupation_id == null){
        $occupation = $pmes->client->other_occupation;
        }else{
        $occupation = $pmes->client->occupation->name;
        }

        if($pmes->attendance == 1){
        $attendance = 'Present';
        }else if($pmes->attendance == 2){
        $attendance = 'Absent';
        }else{
        $attendance = null;
        }
        switch ($pmes->client->purpose_tag) {
            case '0':
                $purpose_tag = 'For investment only';
                break;
            case '1':
                $purpose_tag = 'For investment and savings';
                break;
            case '2':
                $purpose_tag = 'To avail loan products only';
                break;
            case '3':
                $purpose_tag = 'To avail trading products and discounts';
                break;
            case '4':
                $purpose_tag = 'To avail all products and services offered (investment, savings, loans, tradings, etc.';
                break;
            
            default:
                $purpose_tag = null;
                break;
        }
        return [
            // 'hello'
              "'".$pmes->client_id
            , $sched_stat
            , $pmes->client->fullname
            ,$member_status
            , $gender
            ,"'".$pmes->client->contact_no
              ,$pmes->client->fb_link
              ,$seminar_pref
              ,$pmes->client->gmail_address
              ,$pmes->client->birthdate
              ,$pmes->client->age
              ,$pmes->client->street
            , $barangay
            , $city
            , $province
            , $region
            , $occupation
            , $self
            , $relative
            , $pmes->client->marketingChannel->name
            , $purpose_tag
            ,$prefered_batch
            , $pmes->branch->name
            , $pmes->batch_id
            , $pmes->batch_sched_date
            ,$batch_cd
            ,$vid_tool
            ,$meeting_cd
            ,$meeting_pw
            ,$resched_remarks 
            , $attendance
            , $pmes->membership
            , $pmes->crea_by
            , $pmes->upd_by
            , Carbon\Carbon::parse($pmes->created_at)->format('m-d-Y')
            , Carbon\Carbon::parse($pmes->updated_at)->format('m-d-Y')
        ];
    }
}
