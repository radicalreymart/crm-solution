@extends('layouts.app1')
@section('content')
<header class="masthead mb-1">
    
</header>
    @include('flash-message')
    @include('sweetalert::alert')
<div class="container mt-5 col-md-8" id="sign_up">
    <div class="card p-2 pt-3 bg-light border-0">
        <div class="row"> 
            <div class="col-md-12 order-md-1">
                <div class="card mx-3 shadow-lg border-0">
                    <div class="card-header bg-light text-primary py-2 pl-3 border-0">
                        <h6 class="m-0 default-primary">Email Verification 
              <a class=" hvr-icon-push float-right" id="logoutverification"  data-url="{{ url('force-logout') }}">{{ __('Logout') }}<i class="now-ui-icons media-1_button-power hvr-icon"></i></a></h6>
                    </div>
                    <div class="card-body px-3">
                        <div class="justify-content">

                            @if (session('resent'))
                                <div class="alert alert-success" role="alert">
                                    A fresh verification link has been sent to your email address.
                                </div>
                            @endif

                            Before proceeding, please check your email for a verification link. If you did not receive the email,
                            <form action="{{ route('verification.resend') }}" method="POST" class="d-inline">
                                @csrf
                                <button type="submit" class="btn btn-primary btn-send">
                                    Click here to request another
                                </button>.
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
  $('.btn-send').click(function (event) {
        let $form = $(this).closest('form');
          $(".btn-send").html('Please Wait...');
          $(".btn-send"). attr("disabled", true);
            $form.submit();
  });

    $(function() {
        
                var base_path = $("#url").val();
        $('#logoutverification').on('click', function(e) {
          var role = $('#role').val();
            const swalWithBootstrapButtons = Swal.mixin({
              customClass: {
                confirmButton: 'btn btn-success',
                cancelButton: 'btn btn-danger'
              },
              buttonsStyling: false
            })

              swalWithBootstrapButtons.fire({
                title: 'Are you sure you want to Logout?',
                icon: 'question',
                showCancelButton: true,
                confirmButtonText: 'Yes',
                cancelButtonText: 'No',
                reverseButtons: true
              }).then((result) => {
                if (result.isConfirmed) {
                  // alert(role);
                  
                  swalWithBootstrapButtons.fire(
                      'Logout Successful'
                    ).then((result) => {
                      // window.location.reload();
                    window.location.href = $(this).data('url');
                    })
                   // $.ajax({
                   //        url: $(this).data('url'),
                   //        type: 'POST',
                   //        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                   //        // data: 'ids='+join_selected_values,
                   //        success: function (data) {
                   //            swalWithBootstrapButtons.fire(
                   //              'Logout Successful'
                   //            ).then((result) => {
                   //              window.location.reload();
                   //            })
                   //          }
                   //      });
                  } else if (
                    /* Read more about handling dismissals below */
                    result.dismiss === Swal.DismissReason.cancel
                  ) {
                  
                    }
              })
            
                
                
        });

});
</script>
        
@endsection