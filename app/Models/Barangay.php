<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\ClientMaster;
use App\Models\City;


class Barangay extends Model
{
    use HasFactory;
    protected $table = 'barangays';
    
    public function city()
    {
        return $this->belongsTo(City::class);
    }
    public function clientMaster()
    {
        return $this->hasMany(ClientMaster::class);
    }
}
