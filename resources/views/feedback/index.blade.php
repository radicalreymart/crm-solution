@extends('layouts.app', [
    'class' => 'sidebar-mini ',
    'namePage' => 'Feedback Index',
    'activePage' => 'feedback-management',
    'activeNav' => 'settings',
])

@section('content')

<div class="panel-header panel-header-sm">
  </div>
  <div class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header">
            @include('flash-message')
                <div class="float-left">
                    <h4 class="card-title">Walk-in Feedback</h4>
                    @php($department = Auth::user()->department_id)
                    @php($ifdept = ($department === 4 ? 'false' : ($department === 5 ? 'false' : 'true')))
                    @if($ifdept == 'true')
                    <a  class="btn btn-warning" href="{{ url('/sitefeedback') }}">
                        Website Feedback
                    </a>
                    @endif
                </div>
                <div class="row float-right">
                    
                    <form action="{{ route('exportFeedback') }}"  >
                        {{ csrf_field() }}
                        Start Date
                    <input style="right: 5px;" class="form-control  start" max="{{ Carbon\Carbon::now()->format('Y-m-d'); }}" value="{{Carbon\Carbon::now()->format('Y-m-d'); }}" type="date" id="start" name="start">
                        End Date
                    <input style="right: 5px;" class="form-control  start" max="{{ Carbon\Carbon::now()->format('Y-m-d'); }}" value="{{Carbon\Carbon::now()->format('Y-m-d'); }}" type="date" id="end" name="end">
                        <button class="btn btn-primary" id="start"   >Download Excel </button>
                    </form>
                </div>
          </div>
          <div class="card-body">
            <div id="modalCatcher" class="table-responsive">
              <input type="hidden" value="{{url('/')}}" id="url" name="url">
                <table id="datatablesSimple" class="table">
                <thead class=" text-primary">
                    <th >Action</th>
                    <th>Member</th>
                    <th>Last Feedback</th>
                    <th>Date Created</th>
                    <th>Date Updated</th>
                    <th>Status</th>
                </thead>
                <tbody>
                    @if($feedback->count())
                    @foreach ($feedback as $data)
                    <tr>
                    <td>
                      <a href="javascript:void(0)" title="Show Feedback Details" class="show" data-id="{{ $data->id }}">
                        <i class="fas fa-eye  fa-lg"></i>
                      </a>
                    @csrf
                    @method('DELETE')
                    </td>
                    <td>{{ $data->member_no }} : {{$data->fullname}}</td>
                    <td>{{ Carbon\Carbon::parse($data->last_feedback)->subMonth(3)->format('m-d-Y'); }}</td>
                    <td>{{ date_format($data->created_at, 'm-d-Y') }}</td>

                    @if(isset($data->updated_at))
                    <td>{{ date_format($data->updated_at, 'jS M Y') }}</td>
                    @else
                    <td></td>
                    @endif
                    <td>
                    @if(isset($data->status))
                    Uploaded
                    @else
                    Pending for upload
                    @endif
                    </td>
                    </tr>
                    @endforeach
                    @endif
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      
    </div>
  </div>
    <!-- boostrap model -->

<div class="modal fade" id="ajax-feedback-model" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
            <h4 class="modal-title" id="ajaxFeedbackModel"></h4>
            <button type="button" class="btn btn-default"  id="close-modal" data-dismiss="modal">Close</button>
            </div>
            <div class="modal-body">
                <table class="table" id="dynamicAddRemove">
                    <tr>
                        <td>Member Number</td>
                        <td id="member_no"></td>
                    </tr>
                    <tr>
                        <td>Fullname </td>
                        <td id="fullname"></td>
                    </tr>
                    <tr>
                        <td>Gender </td>
                        <td id="gender"></td>
                    </tr>
                    <tr>
                        <td>Branch </td>
                        <td id="branch_id"></td>
                    </tr>
                    <tr>
                        <td>Feedback for </td>
                        <td id="branch_loc"></td>
                    </tr>
                    <tr>
                        <td>Date of Experince </td>
                        <td id="date_exp"></td>
                    </tr>
                    <tr>
                        <td>Member Type </td>
                        <td id="member_type"></td>
                    </tr>
                    <tr>
                        <td>Transaction Nature </td>
                        <td id="transaction_nat"></td>
                    
                    </tr>
                    <tr>
                        <td>Transaction Experience </td>
                        <td id="transaction_exp"></td>
                    </tr>
                    <tr>
                        <td>Comment/Suggestion </td>
                        <td id="comment_suggestion"></td>
                    </tr>
                    <tr>
                        <td>Signature</td>
                        <td id="signiture">
                            <a id="href" target="_blank" rel="noopener noreferrer">
                                <img id="sigimg" height="100" width="200">
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <td>Last Feedback</td>
                        <td id="last_feedback"></td>
                    </tr>
                    <tr>
                        <td>Date Created </td>
                        <td id="created_at"></td>
                    </tr>
                    <tr>
                        <td>Created By</td>
                        <td id="crea_by"></td>
                    </tr>
                    <tr>
                        <td>Date Updated</td>
                        <td id="updated_at"></td>
                    </tr>
                    <tr>
                        <td>Updated By</td>
                        <td id="upd_by"></td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- end bootstrap model -->
<!-- <script src="http://www.datejs.com/build/date.js" type="text/javascript"></script> -->

<script>
  // window.addEventListener("load", function() {
  //   window.addEventListener('DOMContentLoaded', event => {
  //   // Simple-DataTables
  //   // https://github.com/fiduswriter/Simple-DataTables/wiki
    
  //   const datatablesSimple = document.getElementById('datatablesSimple');
  //   if (datatablesSimple) {
  //       new simpleDatatables.DataTable(datatablesSimple, {
  //   fixedHeight: true
  //       });
  //   }
  // });
$(document).ready(function(){
    $('#datatablesSimple').DataTable({

  });
  const swalWithBootstrapButtons = Swal.mixin({
                  customClass: {
                    confirmButton: 'btn btn-success',
                    cancelButton: 'btn btn-danger'
                  },
                  buttonsStyling: false
                })
  $('#close-modal').click(function () {
       $('#ajax-user-model').modal('hide');
    });
 $.clearInput = function () {
        $('insert_form').find('input[type=text], input[type=password], input[type=number], input[type=email], select, textarea').val('');
    };
     $('.modal').on('hidden.bs.modal', function(){
             // window.location.reload();
             // $('#insert_form').removeData('bs.modal')
    $.clearInput();
    // $(this).find('form')[0].reset();
    });

    $.ajaxSetup({
        headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    
      $('#modalCatcher').on('click', '.show', function () 
    {
        $('#ajaxFeedbackModel').html("Feedback Details");
        $('#ajax-feedback-model').modal('show');
        var id = $(this).data('id');
        var base_path = $("#url").val();
        var transaction_nat = [];
        $.ajax({
                type:"get",
                url: base_path+"/empfeedbackAPI/"+id,
                data: { id: id },
                dataType: 'json',
                success: function(res){
                  // console.log(res.regions);
                var mydate = new Date(res[0].last_feedback);
                var month = ["January", "February", "March", "April", "May", "June",
                "July", "August", "September", "October", "November", "December"][mydate.getMonth()-3];
                var str = month + ' '+mydate.getDate()+ ', ' + mydate.getFullYear();
                // alert(str);
                
                  if(JSON.stringify(res[0].branch_loc['id']) == 7){
                    var obj = res[0].transaction_nat;
                    $.each( obj, function( key, value ) {
                          transaction_nat.push(key+',<br>');
                        });
                  }else if(JSON.stringify(res[0].branch_loc['id']) == 8){
                    var obj = res[0].transaction_nat;
                    $.each( obj, function( key, value ) {
                          transaction_nat.push(key+',<br>');
                        });
                  }else{
                    if(res[0].transaction_nat['psdeposit'] != null ){
                        transaction_nat.push('Purpose Savings Deposit,<br>');
                    }if(res[0].transaction_nat['rsdeposit'] != null ){
                        transaction_nat.push('Regular Savings Deposit,<br>');
                    }if(res[0].transaction_nat['time_deposit'] != null ){
                        transaction_nat.push('Time Deposit,<br>');
                    }if(res[0].transaction_nat['share_capital'] != null ){
                        transaction_nat.push('Share Capital,<br>');
                    }if(res[0].transaction_nat['rswithdraw'] != null ){
                        transaction_nat.push('Regular Savings Withdraw,<br>');
                    }if(res[0].transaction_nat['psithdraw'] != null ){
                        transaction_nat.push('Member Capital,<br>');
                    }if(res[0].transaction_nat['member_app'] != null ){
                        transaction_nat.push('Member Application,<br>');
                    }if(res[0].transaction_nat['member_termination'] != null ){
                        transaction_nat.push('Member Termination,<br>');
                    }if(res[0].transaction_nat['member_benefits'] != null ){
                        transaction_nat.push('Member Benefits,<br>');
                    }if(res[0].transaction_nat['membership_inquiry'] != null ){
                        transaction_nat.push('Membership Inquiry,<br>');
                    }if(res[0].transaction_nat['PMES'] != null ){
                        transaction_nat.push('PMES,<br>');
                    }if(res[0].transaction_nat['loan_payment'] != null ){
                        transaction_nat.push('Loan Payment,<br>');
                    }if(res[0].transaction_nat['loan_application'] != null ){
                        transaction_nat.push('Loan Application,<br>');
                    }if(res[0].transaction_nat['loan_release'] != null ){
                        transaction_nat.push('Loan Release,<br>');
                    }if(res[0].transaction_nat['bayad_center'] != null ){
                        transaction_nat.push('Bayad Center,<br>');
                    }
                  }
                // alert(transaction_nat);
                  $('#member_no').html(res[0].member_no);
                  $('#fullname').html(res[0].fullname);
                  $('#transaction_nat').html(transaction_nat);
                  $('#transaction_exp').html('<a title="" href="javascript:void(0);">Staff was courteous and helpful</a><p>'+res[0].transaction_exp['rate1']+'★</p><br><a title="" href="javascript:void(0);">Guard was courteous and helpful</a><p>'+res[0].transaction_exp['rate2']+'★</p><br><a title="Staff provide complete and accurate information to you" href="javascript:void(0);">Staff provide complete and accurate information to you</a><p>'+res[0].transaction_exp['rate3']+'★</p><br><a title="" href="javascript:void(0);">A timely response was provided</a><p>'+res[0].transaction_exp['rate4']+'★</p><br><a title="" href="javascript:void(0);">Office facility appearance is comfortable</a><p>'+res[0].transaction_exp['rate5']+'★</p><br><a title="" href="javascript:void(0);">My over all experience is positive</a><p>'+res[0].transaction_exp['rate6']+'★</p><br>');
                    // 
                  if(res[0].gender = 1){
                      $('#gender').html('Male');
                  }else{
                      $('#gender').html('Female');  
                  }
                  $('#branch_id').html(res[0].branch.name);
                  $('#branch_loc').html(res[0].branch_loc.name);
                  $('#date_exp').html(res[0].exp_date);
                  $('#comment_suggestion').html(res[0].comment_suggestion);
                  if(res[0].member_type = 1){
                      $('#member_type').html('Associate');
                  }else{
                      $('#member_type').html('Regular');  
                  }
                  // $('#signiture').html(res[0].signiture);
                  var src = res[0].signiture;
                    var splitfile = res[0].signiture.split('-');
                    var imgsrc1 = base_path+'/assets/img/uploads/feedback/'+splitfile[0]+'/'+src;
                    var img1 = document.getElementById("sigimg");
                    var aid1 = document.getElementById("href");
                    var src1 = document.createAttribute("src");
                    var href1 = document.createAttribute("href");
                    src1.value = imgsrc1;
                    img1.setAttributeNode(src1);  
                    href1.value = imgsrc1;
                    aid1.setAttributeNode(href1);
                  $('#last_feedback').html(str);
                    // date_format(, 'jS M Y'));
                  $('#upload_by').html(res[0].upload_by);
                  $('#upload_date').html(res[0].upload_date);
                  $('#crea_by').html(res[0].crea_by);
                  $('#upd_by').html(res[0].upd_by);
                  $('#created_at').html(res[0].created_at.split('T')[0]);
                  $('#updated_at').html(res[0].updated_at.split('T')[0]);
                }
                ,error: function (data) {
                     // alert('please delete the preloan connected to this preloan/preloan');
                     alert(data.responseText);  
                }
            });
    });
});
</script>
@endsection


<!-- {
"deposit":null
,"savings":null
,"time_deposit":"1"
,"share_capital":null
,"withdraw":null
,"member_capital":null
,"member_app":null
,"member_termination":"1"
,"member_benefits":null
,"membership_inquiry":null
,"PMES":null
,"loan_payment":null
,"loan_application":null
,"loan_release":"1"
,"bayad_center":null
}
{"rate1":"3"
,"rate2":"4"
,"rate3":"4"
,"rate4":"4"
,"rate5":"3"
,"rate6":"2"
}



 -->
   

