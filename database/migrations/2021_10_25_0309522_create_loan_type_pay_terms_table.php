<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLoanTypePayTermsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('loan_type_pay_terms', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('loan_type_id');
            $table->foreign('loan_type_id', 'loan_fk_loan_types_payterm_id')->references('id')->on('loan_types');
            
            $table->string('loan_type_name');

            $table->unsignedBigInteger('payterm_id');
            
            $table->string('payterm_name');


            $table->string('crea_by');
            $table->string('upd_by')->nullable();
            $table->datetime('soft_deleted_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('loan_type_pay_terms');
    }
}
