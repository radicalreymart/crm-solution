$(function() {
   //  (function ($) {
   // $(document).
                $('#province').hide();
                $('#city').hide();
                $('#barangay').hide();
                var base_path = $("#url").val();
                
                $('#region_id').change(function(){
                var region_code = $(this).val();
                // alert(region_code);
                // let url = "{{ route('getHiddenRegion', '/region_code') }}";
                if(region_code){
                    $.ajax({
                            type:"GET",
                            // url:'/getHiddenRegion/'+region_code,
                            url: base_path+'/getHiddenRegion/'+region_code,
                            success:function(res){               
                            if(res){
                            $.each(res,function(key,value){
                                $('#region_hidden').val(value['id']);
                            });
                            }else{
                            }
                            }
                            });
                    $.ajax({
                            type:"GET",
                            // base_path+'getProvince/'+region_code,
                            url:base_path+'/getProvince/'+region_code,
                            success:function(res){               
                            if(res){
                            $("#province_id").empty();
                            $("#province_id").attr('readonly', false);
                            $("#province_id").append('<option value="">Select Province</option>');                            
                            $("#city_id").empty();
                            $("#city_id").append('<option value="">Select City</option>');
                            $("#barangay_id").empty();
                            $("#barangay_id").append('<option value="">Select Barangay</option>');
                                // alert(JSON.stringify(res));
                            $.each(res,function(key,value){
                                $("#province_id").append('<option class="form-control" value="'+ value['province_id'] +'">'+ value['name'] +'</option>');
                            });
                            }else{
                               $("#province_id").empty();
                                $("#city_id").empty();
                            }
                            }
                            });
                            }else{
                            $("#province_id").empty();
                            $("#city_id").empty();
                            }      
                });
                $('#province_id').change(function(){
                    var province_code = $(this).val();
                    // alert(province_code);
                    if(province_code){
                        $.ajax({
                                type:"GET",
                                url:base_path+'/getHiddenProvince/'+province_code,
                                success:function(res){               
                                if(res){
                                $.each(res,function(key,value){
                                    // alert(value['id']);
                                    $('#province_hidden').val(value['id']);
                                });
                                }else{
                                }
                                }
                                });
                        $.ajax({
                           type:"GET",
                           url:base_path+'/getCities/'+province_code,
                           success:function(res){  
                            if(res){
                                $("#city_id").attr('readonly', false);
                                $("#city_id").empty();
                                $("#city_id").append('<option value="">Select City</option>');
                                $("#barangay_id").empty();
                                $("#barangay_id").append('<option value="">Select Barangay</option>');
                                $.each(res,function(key,value){
                                    $("#city_id").append('<option class="form-control" value="'+ value['city_id'] +'">'+ value['name'] +'</option>');
                                });
                            }else{
                               $("#city_id").empty();
                            }
                           }
                            });
                    }else{
                        $("#city_id").empty();
                    }      
                   });
                $('#city_id').change(function(){
                    var city_code = $(this).val();
                    // alert(city_code);
                    if(city_code){
                        $.ajax({
                                type:"GET",
                                url:base_path+'/getHiddenCities/'+city_code,
                                success:function(res){               
                                if(res){
                                $.each(res,function(key,value){
                                    // alert(JSON.stringify(value['id']));
                                    $('#city_hidden').val(value['id']);
                                });
                                }else{
                                }
                                }
                                });
                        $.ajax({
                           type:"GET",
                           url:base_path+'/getBarangays/'+city_code,
                           success:function(res){  
                            if(res){
                               $("#barangay_id").attr('readonly', false);
                               $("#barangay_id").empty();
                                $("#barangay_id").append('<option value="">Select Barangay</option>');
                                $.each((res),function(key,value){
                                    $("#barangay_id").append('<option class="form-control" value="'+ value['code'] +'">'+ value['name'] +'</option>');
                                });
                            }else{
                               $("#barangay_id").empty();
                            }
                           }
                        });
                    }else{
                        $("#barangay_id").empty();
                    }      
                   });
                $('#barangay_id').change(function(){
                    var barangay_code = $(this).val();
                    // alert(barangay_code);
                    if(barangay_code){
                        $.ajax({
                                type:"GET",
                                url:base_path+'/getHiddenBarangays/'+barangay_code,
                                success:function(res){               
                                if(res){
                                $.each(res,function(key,value){
                                    //alert(JSON.stringify(value['id']));
                                    $('#barangay_hidden').val(value['id']);
                                });
                                }else{
                                }
                                }
                                });
                    }else{
                        $("#barangay_id").empty();
                    }      
                   });
    // member status
            $('#staff_referral_cd').attr('hidden', true);
            $('#div-staffcd').attr('hidden', true);
            $('#member_no').attr('hidden', true);
            $('#div-memno').attr('hidden', true);
    $('#member_status').change(function(){
        // alert($('#member_status').val());
        if($('#member_status').val() == 2) 
        {
            //returning member
            $('#member_no').attr('hidden', true);
            $('#member_no').val('');
            $('#div-memno').attr('hidden', true);
        }
        else
        {
            $('#member_no').attr('hidden', false);
            $('#div-memno').attr('hidden', false);
        }  
     });


    


// }(jQuery));

 });