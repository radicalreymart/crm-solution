@extends('layouts.app', [
    'class' => 'sidebar-mini ',
    'namePage' => 'OPTM Index',
    'activePage' => 'optm-management',
    'activeNav' => 'settings',
])

@section('indexdeleteall')
  <script src="{{ asset('assets') }}/js/indexdeleteall.js"></script>

@endsection
@section('content')
    @include('auth.passwords.confirmmodal')
<style>
  [type=file] {
      position: absolute;
      filter: alpha(opacity=0);
      opacity: 0;
  }
  input,
  [type=file] + label {
    border: 1px solid black;
    border-radius: 3px;
    text-align: left;
    padding: 0px;
    width: 100%;
    height: 30px;
    margin: 0;
    left: 0;
    position: relative;
  }
  [type=file] + label {
    text-align: center;
    left: 0em;
    top: 0.5em;
    /* Decorative */
    background: blue;
    color: #fff;
    border: black;
    cursor: pointer;
  }
  [type=file] + label:hover {
    background: #3399ff;
  }
</style>

<div class="panel-header panel-header-sm">
  </div>
  <div class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header">
            @include('flash-message')
       @include('sweetalert::alert')
                <div class="float-left">
                    <div style="padding-left: 20px;" class="row"><p class="card-title h4">OPTM</p><i style="padding-top:25px; padding-left: 10px;" class="text-muted">Online Payment Transaction Master</i></div>
                </div>
                <div class="float-right">
                    <button class="btn btn-danger delete_all" data-url="{{ url('DeleteAllOPTM') }}">Delete All Selected</button>
                    <form  action="{{ route('exportOPTM') }}"  >
                        {{ csrf_field() }}
                        Start Date
                    <input style="right: 5px;" class="form-control  start" max="{{ Carbon\Carbon::now()->format('Y-m-d'); }}" value="{{Carbon\Carbon::now()->format('Y-m-d'); }}" type="date" id="start" name="start">
                        End Date
                    <input style="right: 5px;" class="form-control  start" max="{{ Carbon\Carbon::now()->format('Y-m-d'); }}" value="{{Carbon\Carbon::now()->format('Y-m-d'); }}" type="date" id="end" name="end">
                        <button class="btn btn-primary " id="start"   >Download Excel </button>
                    </form>
                </div>
          </div>
          <div class="card-body">
            <div id="modalCatcher" class="table-responsive">
              <input type="hidden" value="{{url('/')}}" id="url" name="url">
                <table id="datatablesSimple" class="table">
                <thead class=" text-primary">
                    <th><input type="checkbox" id="master"></th>
                    <th >Action</th>
                    <th>Member</th>
                    <th>Bank</th>
                    <th>Total Amount</th>
                    <th>Receive Date</th>
                    <th>OR Entry Date</th>
                    <th>OR Reference Number</th>
                    <th>OR Given By</th>
                    <th>Status</th>
                    <th>Created By</th>
                    <th>Date Created</th>
                    <th>Updated By</th>
                    <th>Date Updated</th>
                </thead>
                <tbody>
                    @if($optm->count())
                    @foreach ($optm as $data)
                    <tr>
                    <td ><input type="checkbox" class="sub_chk" data-id="{{$data->id}}"></td>
                    <td>
                      <a href="javascript:void(0)" title="Payment Details" class="show" data-id="{{ $data->id }}">
                        <i class="fas fa-eye  fa-lg"></i>
                      </a>
                       @if($data->status != 1 || $data->status != null)
                        <a href="javascript:void(0)" title="Show OPTD" class="OPTD" data-id="{{ $data->id }}">
                          <i  class="fas fa-dollar-sign hvr-pulse fa-lg" aria-hidden="true"></i>
                        </a>
                       @endif
                      @if($data->file_path != null)
                      <a href="javascript:void(0)" title="Show Attachment" class="attachment" data-id="{{ $data->id }}">
                        <i  class="fas fa-file-image hvr-pulse fa-lg" aria-hidden="true"></i>
                      </a>
                      @endif
                    @csrf
                    @method('DELETE')
                    </td>
                    <td>{{ $data->usermember->crea_by}}</td>
                    <td>{{ $data->bank->cd }}</td>
                    <td>{{ number_format($data->total_amt, 2) }}</td>
                    <td>{{\Carbon\Carbon::parse($data->date_trxn_rcvd)->format('m-d-Y')}}</td>
                    <td>{{$data->date_trxn_or_entry ? \Carbon\Carbon::parse($data->date_trxn_or_entry)->format('m-d-Y') : null}}</td>
                    <td class="justify-content">
                       @if($data->or_ref_no == null)
                      <a href="javascript:void(0)" title="Add OR" class="add-or" data-id="{{ $data->id }}">
                        <i class="fas fa-receipt fa-lg"></i>
                      </a>
                      @else
                      {{ $data->or_ref_no }}
                      <a href="{{ route('filedoc',  $data->file_path['optmor'])}}" target="_blank"  title="OR PDF"  data-id="{{ $data->file_path['optmor']}}">
                        <i class="fas fa-receipt fa-lg"></i>
                      </a>

                      @endif
                    </td>
                    <td>{{ $data->or_by }}</td>
                    @if( $data->status == 2)
                    <td>OR Pending</td>
                    @else
                    <td>Complete</td>
                    @endif
                    <td>{{ $data->crea_by }}</td>
                    <td>{{ date_format($data->created_at, 'jS M Y') }}</td>

                    @if(isset($data->upd_by))
                    <td>{{ $data->upd_by }}</td>
                    @else
                    <td></td>
                    @endif
                    @if(isset($data->updated_at))
                    <td>{{ date_format($data->updated_at, 'jS M Y') }}</td>
                    @else
                    <td></td>
                    @endif
                    
                    </tr>
                    @endforeach
                    @endif
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      
    </div>
  </div>
    <!-- boostrap model -->
  <div class="modal fade" id="ajax-OR-model" aria-hidden="true" data-backdrop="static" data-keyboard="false">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title" id="ajaxORModel"></h4>
            <button type="button" class="btn btn-default close-modal" id="close-modal" >Close</button>
          </div>
          <div class="modal-body">
            <form id="insert_form" action="{{ route('giveOR') }}" enctype="multipart/form-data" class="form-horizontal" method="POST">
              @csrf
              <input type="hidden" name="id" id="id">
 
              <div class="form-group">
                <label for="name" class="col-sm-12 control-label">OR Reference Number</label>
                <div class="col-sm-12">
                  <input  required type="name" class="form-control" id="or_ref_no" name="or_ref_no" value="" maxlength="20" required="">
                </div>
              </div> 
              <div class="form-group">
                <label for="name" class="col-sm-12 control-label">OR Document</label>
                <div class="col-sm-12">
                  <input  required type="file" accept="application/pdf" class="form-control" id="or_doc" name="or_doc" value="1" required="">
                  <label id="or_label" style="color: #fff" title="OR Document" for="or_doc">OR Document</label>
                </div>
              </div> 


              <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-primary btn-save" id="btn-save">Save
                </button>
              </div>
            </form>
          </div>
          <div class="modal-footer">
            
          </div>
        </div>
      </div>
    </div>


<!-- Modal -->
<div class="modal fade" id="ajax-attachment-model" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
            <h4 class="modal-title" id="ajaxAttachmentModel"></h4>
            <button type="button" class="btn btn-default"  id="close-modal" data-dismiss="modal">Close</button>
            </div>
            <div class="modal-body">
                <div class="card-body" >
                    <div class="">
                      <div class="slide s01">
                        <a id="href" target="_blank" rel="noopener noreferrer">
                            <h4 id="name"></h4>
                        <img id="img" height="300" width="400">
                        </a>
                      </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

 <div class="modal fade" id="ajax-OPT-model" aria-hidden="true" data-backdrop="static" data-keyboard="false">
      <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
            <h4 class="modal-title" id="ajaxOPTModel"></h4>
            <button type="button" class="btn btn-default"   data-dismiss="modal">Close</button>
            </div>
            <div class="col-md-8">
            <p id="created_at"></p>
            </div>
            <div class="modal-body">
                      <input type="hidden" name="id" id="id">
                      <input type="hidden" value="{{url('/')}}" id="url" name="url">
                      <input type="hidden" id="file_path" name="file_path">                             
                      <div class="form-group row">
                         <div class="card-header bg-light text-primary py-2 pl-3 border-0">
                            <h6 class="m-0 default-primary">Payment Details
                            </h6>
                          </div>
                      </div>
                      <div class="form-group row">
                          <label  for="name" class="col-md-6 col-form-label float-left">{{ __('Member Number') }}</label>
                          <div  class="col-md-6" >
                              <input type="text" class="form-control form-control-sm @error('member_no') is-invalid @enderror" readonly id="member_no" name="member_no">
                          </div>
                      </div>
                      <div class="form-group row">
                          <label  for="name" class="col-md-6 col-form-label float-left">{{ __('Member Name') }}</label>
                          <div  class="col-md-6" >
                              <input type="text" class="form-control form-control-sm @error('long_name') is-invalid @enderror" readonly id="long_name" name="long_name">
                          </div>
                      </div>
                      <div class="form-group row">
                          <label  for="name" class="col-md-6 col-form-label float-left">{{ __('Payment Amount') }}</label>
                          <div  class="col-md-6" >
                              <input type="number" class="form-control form-control-sm @error('total_amt') is-invalid @enderror"  min="0.00" readonly step="0.05" id="total_amt" name="total_amt">
                          </div>
                      </div>
                      <div class="form-group row">
                          <label  for="name" class="col-md-6 col-form-label float-left">{{ __('Bank') }}</label>
                          <div  class="col-md-6" >
                              <select disabled class="custom-select  form-control m @error('bank_id') is-invalid @enderror"   id="bank_id"  required  type="text" name="bank_id" autocomplete="bank_id" >
                                  <option value="" selected disabled>Select</option>
                                   @foreach($bank as $item)
                                  <option  value="{{$item->id}}" @if (old('bank_id') == '{{$item->id}}') selected="selected" @endif>{{$item->description}}</option>
                                  @endforeach
                                </select>
                          </div>
                      </div>
                      <div class="form-group row">
                          <label for="name" class="col-md-6 col-form-label float-left">{{ __('OR Entry') }}</label>
                          <div  class="col-md-6" >
                                <input readonly type="date"   id="date_trxn_or_entry"  class="form-control @error('date_trxn_or_entry') is-invalid @enderror" name="date_trxn_or_entry" value="{{ old('date_trxn_or_entry') }}">
                          </div>
                      </div>
                      <div class="form-group row">
                          <label  for="name" class="col-md-6 col-form-label float-left">{{ __('OR') }}</label>
                          <div  class="col-md-6" >
                              <input readonly type="text" class="form-control form-control-sm @error('or_ref_no1') is-invalid @enderror"   id="or_ref_no1" name="or_ref_no1">
                          </div>
                      </div>
                      <div class="form-group row">
                          <label for="name" class="col-md-6 col-form-label float-left">{{ __('Payment Date') }}</label>
                          <div  class="col-md-6" >
                                <input readonly type="date"   id="date_trxn_rcvd"  class="form-control @error('date_trxn_rcvd') is-invalid @enderror" name="date_trxn_rcvd" value="{{ old('date_trxn_rcvd') }}">
                          </div>
                      </div>
                      <div class="form-group row">
                          <label  for="name" class="col-md-6 col-form-label float-left">{{ __('OR given by') }}</label>
                          <div  class="col-md-6" >
                              <input readonly type="text" class="form-control form-control-sm @error('or_by') is-invalid @enderror" id="or_by" name="or_by">
                          </div>
                      </div>
                      <div class="form-group row">
                          <label  for="name" class="col-md-6 col-form-label float-left">{{ __('Status') }}</label>
                          <div  class="col-md-6" >
                              <select disabled id="status" name="status"  class="form-control">
                                <option value="">Select</option>
                                <option value= 3 >Complete</option>
                                <option value= 2 >OR Pending</option>
                              </select>
                          </div>
                      </div>
                      <div class="form-group row">
                          <label  for="name" class="col-md-6 col-form-label float-left">{{ __('Created By') }}</label>
                          <div  class="col-md-6" >
                              <input type="text" class="form-control form-control-sm @error('crea_by') is-invalid @enderror"  min="0.00" readonly step="0.05" id="crea_by" name="crea_by">
                          </div>
                      </div>
                      <div class="form-group row">
                          <label  for="name" class="col-md-6 col-form-label float-left">{{ __('Updated By') }}</label>
                          <div  class="col-md-6" >
                              <input type="text" class="form-control form-control-sm @error('upd_by') is-invalid @enderror"  min="0.00" readonly step="0.05" id="upd_by" name="upd_by">
                          </div>
                      </div>
            </div>
        </div>
    </div>
</div>


<!-- Modal -->
<div class="modal fade" id="ajax-OPTD-model" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
            <h4 class="modal-title" id="ajaxOPTDModel"></h4>
            <button type="button" class="btn btn-default"  id="close-modal" data-dismiss="modal">Close</button>
            </div>
            <div class="modal-body">
            <div class="card-header">
              <p id="ajaxOPTDModel1" class='text-muted'>Online Payment Transaction Detail</p>
            </div>
              <div class="card-body" >
                <table class="table" id="dynamicAddRemove">
                    <tr>
                        <!-- <th>OPTM ID</th> -->
                        <th>Amount</th>
                        <th>Account</th>
                    </tr>
                    <tbody id="tbody">
                      
                    </tbody>
                </table>
              </div>
            </div>
        </div>
    </div>
</div>

<!-- end bootstrap model -->  
<script type="text/javascript">
  //   window.addEventListener('DOMContentLoaded', event => {

  // // window.addEventListener("load", function() {
  //   // Simple-DataTables
  //   // https://github.com/fiduswriter/Simple-DataTables/wiki
    
  //   const datatablesSimple = document.getElementById('datatablesSimple');
  //   if (datatablesSimple) {
  //       new simpleDatatables.DataTable(datatablesSimple, {
  //   fixedHeight: true
  //       });
  //   }
  // });
$(document).ready(function(){
$('#datatablesSimple').DataTable({

  });
  const swalWithBootstrapButtons = Swal.mixin({
                  customClass: {
                    confirmButton: 'btn btn-success',
                    cancelButton: 'btn btn-danger'
                  },
                  buttonsStyling: false
                }) 
    $.ajaxSetup({
        headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $('.close-modal').click(function () {
    var modal_array = 
    [
      '#ajax-OR-model'
    ];
    closeModal(modal_array);
       // $('#ajax-pmes-model').modal('hide');
    });
    $.clearInput = function () {
        $('#insert_form').find('input[type=text], input[type=password], input[type=number], input[type=email], input[file], label, select, textarea').val('');
        $('#or_label').html('OR Document');
    };
    $('.modal').on('hidden.bs.modal', function(){
    $.clearInput();
    // $(this).find('form')[0].reset();
    $('#dynamicAddRemove').find("#tbody").empty();
    });
     var i = 1;
    
      $('#modalCatcher').on('click', '.OPTD', function () 
    {
        $('#ajaxOPTDModel').html("OPTD");
        $('#ajax-OPTD-model').modal('show');
        var id = $(this).data('id');
        var base_path = $("#url").val();
        $.ajax({
                type:"post",
                url: base_path+"/OPTDmodal1",
                data: { id: id },
                dataType: 'json',
                success: function(res){
                  // console.log(res.acount);
                  $.each( res, function( key, value ) {
                  var table = document.getElementById("tbody");
                  var one = 1;
                  $("#tbody").append('');
                  var row = table.insertRow(0);
                  var cell1 = row.insertCell(0);
                  var cell2 = row.insertCell(1);
                  cell1.innerHTML = value.amount;
                  
                  cell2.innerHTML = value.description;
                });
                }
                ,error: function (data) {
                     // alert('please delete the preloan connected to this preloan/preloan');
                     alert(data.responseText);  
                }
            });
    });

    $(document).on('click', '.remove-input-field', function () {
        $(this).parents('tr').remove();
      });
    
    $('#modalCatcher').on('click', '.attachment', function () 
    {
        $('#ajaxAttachmentModel').html("Image Attach");
        $('#ajax-attachment-model').modal('show');
        var id = $(this).data('id');
        var base_path = $("#url").val();
        $.ajax({
                type:"post",
                url: base_path+"/attachment",
                data: { id: id },
                dataType: 'json',
                success: function(res){
                  var src = res.file_path['optmdoc'];
                    var splitfile = res.file_path['optmdoc'].split('-');
                    var imgsrc1 = base_path+'/assets/img/uploads/payments/'+splitfile[0]+'/'+src;
                    var img1 = document.getElementById("img");
                    var aid1 = document.getElementById("href");
                    var src1 = document.createAttribute("src");
                    var href1 = document.createAttribute("href");
                    document.getElementById("name").innerHTML = splitfile[1];
                    src1.value = imgsrc1;
                    img1.setAttributeNode(src1);  
                    href1.value = imgsrc1;
                    aid1.setAttributeNode(href1);
                }
                ,error: function (data) {
                     // alert('please delete the preloan connected to this preloan/preloan');
                     alert(data.responseText);  
                }
            });
    });

      // $('#insert_form').on('submit', function(event){
      //   // alert(JSON.parse($('#insert_form').serializeArray()));
      //     // var data = $('#insert_form').serializeArray();
      //     event.preventDefault();
      //     $("#OPTD-save").html('Please Wait...');
      //     $("#OPTD-save"). attr("disabled", true);
      //     $.ajax({
      //      type:"POST",
      //       url: "{{ url('add-optd') }}",
      //       data: jQuery('#insert_form').serialize(),
      //       // data: data+'&_token={{csrf_token()}}',
      //       dataType: 'json',
      //       success: function(res){
      //         swalWithBootstrapButtons.fire({
      //             title: 'Success!',
      //             icon: 'success',
      //             confirmButtonText: 'Confirmed',
      //           }).then((result) => {
      //        // window.location.reload();
      //       $("#OPTD-save").html('Submit');
      //       $("#OPTD-save"). attr("disabled", false);
      //       $('#ajax-OPTD-model').modal('hide');
      //        document.getElementById("insert_form").reset();
           
      //      })
      //         }
      //      ,
      //       error: function (data) {
      //            console.log(data.responseText);
      //        }
      //     //     ,error: function(xhr, status, error){
      //     //  var errorMessage = xhr.status + ': ' + xhr.statusText
      //     //  alert('Error? - ' + errorMessage);
      //     // } 
      //     });  
      
      // });
    
    $('#addNewOPTM').click(function () {
       $('#addEditOPTMForm').trigger("reset");
       $('#ajaxORModel').html("Add OR");
       $('#ajax-OR-model').modal('show');
      $('#member_no').attr('readonly', false);
    });
      // $('#modalCatcher').on('click', '#btn-save', function () {
    
     $('#modalCatcher').on('click', '.create-OPTD', function () {
        var id = $(this).data('id');
                var base_path = $("#url").val();
              // alert(id);
        // ajax
        var amt = 0;
        $.ajax({
            type:"POST",
            url: base_path+"/create-OPTD",
            data: { id: id },
            dataType: 'json',
            success: function(res){
          // alert( JSON.stringify(res));
          $.each( res, function( key, value ) {
            var dif =   res[0].total_amt - value.amount
            // alert(dif);
          });
              $('#ajaxOPTDModel').html("Edit OPTD");
              $('#ajax-OPTD-model').modal('show');
            $("#btn-save"). attr("disabled", false);
            $.each( res, function( key, value ) {
              var table = document.getElementById("tbody");
              var one = 1;
              $("#tbody").append('<input type="text" class="form-control" id="hidden_id1" value="'+value.id+'" hidden="true" name="hidden_idd1b"><input type="text" class="form-control" id="optm_id" value="'+value.online_pymnt_id+'" hidden="true" name="optm_id">');
              var row = table.insertRow(0);
              var cell1 = row.insertCell(0);
              var cell2 = row.insertCell(1);
              var cell3 = row.insertCell(2);
              var cell4 = row.insertCell(3);
              cell1.innerHTML = '<input type="text" class="form-control" id="optd_id['+value.count+']" value="'+value.id+'" hidden="true" name="optd_id['+value.count+']"><input type="text" class="form-control" id="online_pymnt_id['+value.count+']" value="'+value.online_pymnt_id+'" name="online_pymnt_id['+value.count+']">';
              
              cell2.innerHTML = '<input type="number" class="form-control m-bot15 @error("amount") is-invalid @enderror" onchange="(function(el){el.value=parseFloat(el.value).toFixed(2);})(this)" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" maxlength="6" min="0.00" max="9999999.00" step="0.05" id="amount" value="'+value.amount+'" name="amount['+value.count+']">';
              // cell2.innerHTML = '<input type="text" class="form-control m-bot15" id="amount" value="'+value.amount+'" name="amount['+value.count+']">';
              cell3.innerHTML = '<select  class="form-control" id="acct_id" name="acct_id['+value.count+']">@if ($account->count())<option value="" >Select Account</option>@foreach($account as $data)<option value="{{ $data->id }} " >{{ $data->acct_cd }}</option>@endforeach @endif</select>';
              var acct_id = document.getElementById('acct_id');
              var opts = acct_id.options.length;
              for (var i=0; i<opts; i++){
                  if (acct_id.options[i].value == value.acct_id){
                      acct_id.options[i].selected = true;
                      break;
                  }
              }
              cell4.innerHTML = '<a class="btn btn-outline-danger" type="button" href="{{url("/optd")}}"><li class="fas fa-minus-circle fa-lg" aria-hidden="true"></li></a>';
              
             amt += parseInt(value.amount);
            });
            var dif = parseInt(res[0].total_amt) - amt;
            // alert(dif);
              $('#OPTMTotalAmount').html("Total Amount: "+dif);
              // $('#id').val(res.id);
              // $('#online_pymnt_id').val(res.online_pymnt_id);
              // $('#acct_id').val(res.acct_id);
              // $('#amount').val(res.amount);
              },
            error: function (data) {
                 alert(data.responseText);
             }
        });
    });

     $('#modalCatcher').on('click', '.show', function () {
        var id = $(this).data('id');
        var base_path = $("#url").val();
        $.ajax({
            type:"POST",
            url: base_path+"/edit-optm",
            data: { id: id },
            dataType: 'json',
            success: function(res){
                 // alert(JSON.stringify(res));
              $('#ajaxOPTModel').html("Show Payment Details");
              $('#ajax-OPT-model').modal('show');
              $('#id').val(res.id);
              $('#total_amt').val(res.total_amt);
              $('#bank_id').val(res.bank_id);
              $('#date_trxn_rcvd').val(res.date_trxn_rcvd);
              $('#date_trxn_or_entry').val(res.date_trxn_or_entry);
              $('#or_by').val(res.or_by);
              $('#status').val(res.status);
              $('#crea_by').val(res.crea_by);
              $('#upd_by').val(res.upd_by);
              $('#long_name').val(res.long_name);
              $('#member_no').val(res.member_no);
              $('#or_ref_no1').val(res.or_ref_no);
           },
            error: function (data) {
                 alert(data.responseText);
             }
        });
    });
     $("[type=file]").on("change", function(){
      // Name of file and placeholder
        var file = this.files[0].name;
        var dflt = $(this).attr("placeholder");
        // console.log([file, dflt]);
        if($(this).val()!=""){
          $(this).next().text(file);
        } else {
          $(this).next().text(dflt);
        }
      });
      $(document).on('keydown', 'input[pattern]', function(e){
        var input = $(this);
        var oldVal = input.val();
        var regex = new RegExp(input.attr('pattern'), 'g');
        // console.log([input, oldVal, regex]);
        setTimeout(function(){
          var newVal = input.val();
          if(!regex.test(newVal)){
            input.val(oldVal); 
          }
        }, 1);
      });

    $('#modalCatcher').on('click', '.add-or', function () {
        var id = $(this).data('id');
                var base_path = $("#url").val();
        // ajax
          // alert( OPTM_no );
        $.ajax({
            type:"POST",
            url: base_path+"/edit-optm",
            data: { id: id },
            dataType: 'json',
            success: function(res){
                 // alert(JSON.stringify(res));
              $('#ajaxORModel').html("Add OR Code");
              $('#ajax-OR-model').modal('show');
              $('#id').val(res.id);
              $('#total_amt').val(res.total_amt);
              $('#date_trxn_rcvd').val(res.date_trxn_rcvd);
              $('#or_ref_no').val(res.or_ref_no);
           },
            error: function (data) {
                 alert(data.responseText);
             }
        });
    });
    $('.btn-save').click(function (event) {
      try{
        var or_ref_no = $("#or_ref_no").val();
        var or_doc = $("#or_doc").val();

        $('.btn-save').html('Please Wait');
        $('.btn-save').attr('disabled', true);
        $('.close-btn').html('Please Wait');
        $('.close-btn').attr('disabled', true);
        $(this).closest('form').submit(); 
      }
      catch(err) {
            swalWithBootstrapButtons.fire({
                  title: 'Error!',
                  text: err,
                  icon: 'warning',
                  confirmButtonText: 'Confirmed',
                }).then((result) => {
             // window.location.reload();

            $('.btn-save').html('Submit');
            $('.btn-save').attr('disabled', false);
            $('.close-btn').html('Close');
            $('.close-btn').attr('disabled', false);
            // $("#btn-save").html('Submit');
            // $("#btn-save"). attr("disabled", false);
                })
            }
    });
});
</script>
@endsection

   

