@extends('layouts.app1')
@section('loanstyle')
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.3/css/bootstrap.css">
<style>
.alert{
  color: black !important;
}
[type=file] {
    position: absolute;
    filter: alpha(opacity=0);
    opacity: 0;
}
input,
[type=file] + label {
  border: 1px solid black;
  border-radius: 3px;
  text-align: left;
  padding: 0px;
  width: 100%;
  height: 30px;
  margin: 0;
  left: 0;
  position: relative;
}
[type=file] + label {
  text-align: center;
  left: 0em;
  top: 0.5em;
  /* Decorative */
  background: blue;
  color: #fff;
  border: black;
  cursor: pointer;
}
[type=file] + label:hover {
  background: #3399ff;
}
label.shorten_text1 {
 max-width: 100%;
  /*padding: px;*/
  text-overflow: ellipsis;
  white-space: nowrap;
  overflow: hidden;
}

label.shorten_text1:hover {
  overflow: visible;
}
label.shorten_text1:before{
  content: attr(title);
}
</style>
@endsection

@section('content')
    <!-- Masthead-->
    <header class="masthead mb-1">
      <div class="container mt-1 pt-1">
       @include('sweetalert::alert')
     
          <div class="masthead-subheading m-3">Magandang Buhay!</div>
          <div class="masthead-heading text-uppercase m-3">Be a member! <br> Be one of us!</div>
          <p class="">Thank you for having interesting membership with us. Please fill up the necessary fields so we can reach you as soon as possible.</p>
          <a class="btn btn-outline-light btn-xl text-uppercase" tabindex="-1" type="button" href="#sign_up">Proceed to Online Payment</a><br>
      </div>
    </header>
    
    <div class="container mt-5 col-md-8" id="sign_up">
      <div class="card p-2 pt-3 bg-light border-0">
        <div class="row"> 
          <div class="col-md-12 order-md-1">
            <!-- <form id="insert_form" name='formname' enctype="multipart/form-data" > -->
             <!-- method="post" action="{{ route('enduser.optm.store') }}"> -->  
            @foreach ($optm as $data)
            <input type="text" class="form-control" id="optm_id" value="{{$data->id}}" hidden="true" name="optm_id">
              <input type="hidden" value="{{url('/')}}" id="url" name="url">
                    @csrf
              <div class="card mx-3 shadow-lg border-0">
                <div class="card-header bg-light text-primary py-2 pl-3 border-0">
                    <input type="hidden" class="total_amt" name="total_amt" id="total_amt" value="{{ $data->total_amt }}">
                  <h6 class="m-0 default-primary">Online Payment Details</h6>
                </div>

              
                <div class="card-body px-3">
                  <!-- <form id="insert_form" > -->
                    <!-- method="POST"> -->
                    @csrf
                    @include('flash-message')
                    
                    <table class="table" id="dynamicAddRemove">
                        <tr>
                            <th>OPTM ID</th>
                            <th>Amount</th>
                            <th>Account</th>
                            <th>
                              <button type="button" name="add" id="dynamic-ar" class="btn btn-outline-success">
                                <li class="fas fa-plus-circle fa-lg" aria-hidden="true"></li>
                              </button>
                           </th>
                        </tr>
                        <tbody id="tbody">
                            <tr>
                                <td>
                                    <input type="text"  class="form-control" id="optd_id[1]" value="" hidden="true" name="optd_id[1]">
                                    <input type="text" readonly tabindex="-1" class="form-control" id="[online_pymnt_id][1]" value="{{$data->id}}" name="online_pymnt_id[1]">
                                </td>
                                <td>
                                    <input type="number" class="amount form-control @error('amount') is-invalid @enderror " onchange="(function(el){el.value=parseFloat(el.value).toFixed(2);})(this)" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" maxlength="9" min="0.00" max="9999999.00" step="0.05" id="amount[1]"  name="amount[]" onkeyup="findTotal()">
                                    <input type="text" hidden tabindex="-1" class="form-control" id="[amount_hidden[]"  name="amount_hidden[1]">
                                </td>
                                <td >
                                    <select class="form-control custom-select" id="acct_id" name="acct_id[1]">
                                        @if ($account->count())
                                        <option value="" selected>Select Account</option>
                                        @foreach($account as $data)
                                        <option value="{{ $data->id }}" >{{ $data->acct_cd }}</option>@endforeach
                                        @endif
                                    </select>
                                </td>
                                <td>
                                    <button type="button" tabindex="-1" class="btn btn-outline-danger remove-input-field">
                                        <li class="fas fa-minus-circle fa-lg" aria-hidden="true"></li>
                                    </button>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <table class="table">
                      <tbody>
                        <tr>
                          <td>Current Amount</td>
                          <td id="ptotal-amt"></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
                        <tr>
                          <td>Total Amount</td>
                          <td id="ttotal-amt"></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
                      </tbody>
                    </table>
                    @endforeach
                    <!-- <button type="submit" id="OPTD-save" class="btn btn-outline-success btn-block">Save</button> -->
                </div>
              </div>
      
              <button class="btn btn-secondary mt-5 mb-5 btn-block" style="background-color: rgb(7,17,174);" type="submit" id="btn-save-optd"  >Submit</button>
            <!-- </form> -->
          </div>
      </div>
  </div>
  </div>

<script>
    // sweetalert
  const swalWithBootstrapButtons = Swal.mixin({
    customClass: {
      confirmButton: 'btn btn-success',
      cancelButton: 'btn btn-danger'
    },
    buttonsStyling: false
  }) 
  //--
  // amount oufocus
  function findTotal(){
        var arr = document.getElementsByName('amount[]');
        var tot=0;
        for(var i=0;i<arr.length;i++){
            if(parseInt(arr[i].value))
                tot += parseFloat(arr[i].value);
        }
        // document.getElementById('total').value = tot;
        $('#ptotal-amt').html(tot);
        var total_amt1 = document.getElementById('total_amt').value;
        // alert(total_amt1);
        $('#ttotal-amt').html(total_amt1);
    };
   document.addEventListener("DOMContentLoaded", function(event) {
       findTotal();
    });
    // $(".amount").focusout(function (){
    // // $("input[id='amount[]']").focusout(function (){
    // // alert('hello');
    //     // var values = [];
    //     // $("input[id='amount[]']").each(function() {
    //     //     values.push($(this).val());
    //     // });
    //     // var sum = 0;
    //     // for (var i = 0; i < values.length; i++) {
    //     // sum += parseFloat(values[i]);
    //     // }
    //     // var total_amt = $('#total_amt').val();
    //     // console.log(sum);
    //     // alert(JSON.parse(sum));
    //     // $('#ptotal-amt').html(sum);
    // });
  //--
  // save OPTD
   $('#btn-save-optd').click(function (event) {
        event.preventDefault();
        var values = [];
        var amount = [];
        $("input[name='amount[]']").each(function() {
            values.push($(this).val());
        });
        var sum = 0;
        for (var i = 0; i < values.length; i++) {
        sum += parseFloat(values[i]);
        }
        for (var a = 1; a <= values.length; a++) {
        amount[a] = $("input[id='amount["+a+"]'").val();
        $("input[name='amount_hidden["+a+"]'").val(amount[a]);
        // console.log(amount[a]);
        }
        var total_amt = $('#total_amt').val();
        // console.log(jQuery('#insert_form').serializeArray())
        if(total_amt == sum){
            // alert('confirm');
             $.ajax({
           type:"POST",
            url: "{{ url('enduser-add-optd') }}",
            data: jQuery('#insert_form').serialize(),
            // data: data+'&_token={{csrf_token()}}',
            dataType: 'json',
            success: function(res){
           // alert(JSON.stringify(res));
              swalWithBootstrapButtons.fire({
                  title: 'Success!',
                  icon: 'success',
                  confirmButtonText: 'Confirmed',
                }).then((result) => {
               document.location.href = "{{ url('enduser/home') }}";
            $("#OPTD-save").html('Submit');
            $("#OPTD-save"). attr("disabled", false);
            $('#ajax-OPTD-model').modal('hide');
             document.getElementById("insert_form").reset();
           })
              }
           ,error: function (data) {
                 console.log(data.responseText);
             }
          //     ,error: function(xhr, status, error){
          //  var errorMessage = xhr.status + ': ' + xhr.statusText
          //  alert('Error? - ' + errorMessage);
          // } 
          });  
        }else{
            alert('total amount: ('+total_amt+') and sum ('+sum+') of the listed amount must be equal');
        }

   });
  

  //--
  // file upload
  //   $("[type=file]").on("change", function(){
  // // Name of file and placeholder
  //   var file = this.files[0].name;
  //   var dflt = $(this).attr("placeholder");
  //   if($(this).val()!=""){
  //     $(this).next().text(file);
  //   } else {
  //     $(this).next().text(dflt);
  //   }
  // });
  // $(document).on('keydown', 'input[pattern]', function(e){
  //   var input = $(this);
  //   var oldVal = input.val();
  //   var regex = new RegExp(input.attr('pattern'), 'g');

  //   setTimeout(function(){
  //     var newVal = input.val();
  //     if(!regex.test(newVal)){
  //       input.val(oldVal); 
  //     }
  //   }, 1);
  // });
  //--
  // optd dynamic array
   $(document).on('click', '.remove-input-field', function () {
        $(this).parents('tr').remove();
      });
     var i = 1;
  $("#dynamic-ar").click(function () {
        ++i;
        var id = document.getElementById('optm_id').value;
                var base_path = $("#url").val();
                // alert(id);
         $("#tbody").append('<tr><td><input type="text" class="form-control" id="optd_id['+i+']" value="" hidden="true" name="optd_id['+i+']"><input type="text" class="form-control" id="[online_pymnt_id]['+i+']" readonly value="'+id+'"  tabindex="-1" name="online_pymnt_id['+i+']"></td><td><input type="number" class="amount form-control @error("amount") is-invalid @enderror " onchange="(function(el){el.value=parseFloat(el.value).toFixed(2);})(this)" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" maxlength="9" min="0.00" max="9999999.00" step="0.05" id="amount['+i+']"  name="amount[]" onkeyup="findTotal()"><input type="text" hidden tabindex="-1" class="form-control" id="[amount_hidden[]"  name="amount_hidden['+i+']"></td><td ><select class="form-control custom-select" id="acct_id" name="acct_id['+i+']">@if ($account->count())<option value="" selected>Select Account</option>@foreach($account as $data)<option value="{{ $data->id }}" >{{ $data->acct_cd }}</option>@endforeach @endif</select></td><td><button type="button" tabindex="-1" class="btn btn-outline-danger remove-input-field"><li class="fas fa-minus-circle fa-lg" aria-hidden="true"></li></button></td></tr>');
    });
  //--


  
</script>
  <script src="{{ asset('assets') }}/js/extrajs.js"></script>
@endsection

