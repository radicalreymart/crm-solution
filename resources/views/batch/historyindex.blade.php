@extends('layouts.app', [
    'class' => 'sidebar-mini ',
    'namePage' => 'Batch Index',
    'activePage' => 'batch-management',
    'activeNav' => '',
])

@section('indexdeleteall')
  <script src="{{ asset('assets') }}/js/indexdeleteall.js"></script>
@endsection
@section('content')
    @include('auth.passwords.confirmmodal')
    @include('batch.modal')
<div class="panel-header panel-header-sm">
  </div>
  <div class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header">
            @include('flash-message')
                <div class="float-left">
                    <h4 class="card-title">Batch History</h4>
                </div>
                <div class="float-right">
                    <a class="btn btn-primary" href="{{ url('/batchv2') }}">
                        Main Index
                    </a>
                </div>
          </div>
          <div class="card-body">
            <div id="modalCatcher" class="table-responsive">
              <input type="hidden" value="{{url('/')}}" id="url" name="url">
              <table id="datatablesSimple" class="table">
                <thead class=" text-primary">
                    <!-- <th ><input type="checkbox" id="master"></th> -->
                    <th >Batch Code</th>
                    <th>Schedule Date</th>
                    <th>Schedule Time</th>
                    <th>Pref. Application</th>
                    <th class="meeting-col">Meeting Link</th>
                    <th>Participant Number</th>
                    <th>Batch Status</th>
                    <th class="shorten_text">Reschedule Remarks</th>
                    <th>Meeting Password</th>
                    <th>Date Created</th>
                    <th>Created By</th>
                </thead>
                <tbody>
                   @if($batch->count())
                    @foreach ($batch as $data)
                    <tr>
                    <!-- <td ><input type="checkbox" class="sub_chk" data-id="{{$data->id}}"></td> -->
                    <td>{{ $data->batch_cd }}</td>
                    
                    <td>{{ \Carbon\Carbon::parse($data->sched_date)->format('m-d-Y')}}</td>
                    
                    <td>{{ \Carbon\Carbon::parse($data->sched_date)->format('h:i a')}}</td>
                    
                    @if($data->online_vid_tool == 0)
                    <td>Google Meet</td>
                    @elseif($data->online_vid_tool == 1)
                    <td>Zoom</td>
                    @else
                    <td>Walk-in</td>
                    @endif
                    
                    <td class="meeting-col"><a href="https://meet.google.com/{{$data->meeting_cd}}">https://meet.google.com/{{$data->meeting_cd}}</a></td>

                     @if($data->participant != 0)
                    <td>
                      <a href="javascript:void(0)" data-id="{{$data->id}}" class="btn btn-info" id="participant" style="font-size: medium; font-weight:bold; ">
                     {{$data->participant}}
                      </a>
                    </td>
                    @else
                    <td></td>
                    @endif

                    @if(\Carbon\Carbon::parse($data->sched_date)->format('m/d/Y') == $now)
                    <td>Today</td>
                    @elseif(\Carbon\Carbon::parse($data->sched_date)->format('m/d/Y') <= $now)
                    <td>Expired</td>
                    @elseif($data->resched_remarks !== '.')
                    <td>Rescheduled</td>
                    @elseif($data->participant < 5)
                    <td>Insufficient Participant</td>
                    @else
                    <td>Pending</td>
                    @endif
                    
                    <td>{{$data->resched_remarks}}</td>
                    
                    @if(is_null($data->meeting_pw))
                    <td></td>
                    @else
                    <td>{{$data->meeting_pw}}</td>
                    @endif
                    
                    <td>{{ date_format($data->created_at, 'm-d-Y') }}</td>
                    
                    <td>{{ $data->crea_by }}</td>
                    </tr>
                    @endforeach
                    @endif
                </tbody>
              </table>

            </div>
          </div>
        </div>
      </div>
      
    </div>
  </div>
    
    <!-- boostrap model -->
    
<!-- end bootstrap model -->

<script src="{{ asset('assets') }}/js/batchdatepicker.js"></script>
<script>
   // window.addEventListener("load", function() {
  //   window.addEventListener('DOMContentLoaded', event => {
  //   // Simple-DataTables
  //   // https://github.com/fiduswriter/Simple-DataTables/wiki
    
  //   const datatablesSimple = document.getElementById('datatablesSimple');
  //   if (datatablesSimple) {
  //       new simpleDatatables.DataTable(datatablesSimple, {
  //   fixedHeight: true
  //       });
  //   }
  // });
$(document).ready(function(){
  $('#datatablesSimple').DataTable({

  });
    $('#close-modal').click(function () {
       $('#ajax-batch-model').modal('hide');
       $('#ajax-participant-model').modal('hide');
    });

    $.ajaxSetup({
        headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    
    $.clearInput = function () {
        $('insert_form').find('input[type=text], input[type=password], input[type=number], input[type=email], select, textarea').val('');
    };
     $('.modal').on('hidden.bs.modal', function(){
    $.clearInput();
    // $(this).find('table')[0].reset();
    $('#participant-table').find("#tbody").empty();
    });

     $('#modalCatcher').on('click', '#participant', function () {

        var id = $(this).data('id');
                var base_path = $("#url").val();
        // ajax
          // alert( id );
        $.ajax({
            type:"POST",
            url: base_path+"/participant",
            data: { id: id },
            dataType: 'json',
            success: function(res){
                // alert(JSON.stringify(res));
                // var date = res.sched_date->format('m-d-Y');
              $('#ajaxParticipantModel').html("Batch Participant");
              $('#ajax-participant-model').modal('show');
            $.each( res, function( key, value ) {
              var table = document.getElementById("tbody");
              var row = table.insertRow(0);
              var cell1 = row.insertCell(0);
              var cell2 = row.insertCell(1);
              var cell3 = row.insertCell(2);
              var cell4 = row.insertCell(3);
              cell1.innerHTML = '<td>'+value.client.fullname+'</td>';
              cell2.innerHTML = '<td>'+value.client.contact_no+'</td>';
              cell3.innerHTML = '<td>'+value.client.gmail_address+'</td>';
              cell4.innerHTML = '@if('+value.client.attendance+' == 1)<td>Present</td>@elseif('+value.client.attendance+' == 2)<td>Absent</td>@else<td>Pending</td>@endif';
            });
           },
            error: function (data) {
                 alert(data.responseText);
                 
             }
        });
    });
 });
</script>
@endsection


