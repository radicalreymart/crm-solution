

@component('mail::layout')
{{-- Header --}}

@slot('header')
@component('mail::header', ['url' => config('app.url')])

@endcomponent
@endslot
<body style="margin:0;padding:0;word-spacing:normal;background-color:#939297;">
  <div role="article" aria-roledescription="email" lang="en" style="text-size-adjust:100%;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;background-color:lightgrey;">
    <table role="presentation" style="width:100%;border:none;border-spacing:0;">
      <tr>
        <td align="center" style="padding:0;">
          <!--[if mso]>
          <table role="presentation" align="center" style="width:600px;">
          <tr>
          <td>
          <![endif]-->
          <table role="presentation" style="width:94%;max-width:600px;border:none;border-spacing:0;text-align:left;font-family:Arial,sans-serif;font-size:16px;line-height:22px;color:#363636;">
            <tr>
              <td style="padding:40px 30px 30px 30px;text-align:center;font-size:24px;font-weight:bold;">
                <a href="" style="text-decoration:none;"><img src="{{ asset('assets/img/mhrlarge-removebg.png') }}" width="115" alt="" style="width:115px;max-width:80%;margin-bottom:20px;"></a>
              </td>
            </tr>
            <tr>
              <td style="padding:30px;background-color:#ffffff;">
                <h1 style="margin-top:0;margin-bottom:16px;font-size:26px;line-height:32px;font-weight:bold;letter-spacing:-0.02em;">
                    MAGANDANG BUHAY!<br>
                </h1>
                <p style="margin:0;">Future Kamay-ari! Maraming Salamat sa inyong pag-gamit nang CMSRS Online-Loan Application. </p>
              </td>
            </tr>
            <tr>
              <td style="padding:0;font-size:24px;line-height:28px;font-weight:bold;">
                <a href="" style="text-decoration:none;"></a>
              </td>
            </tr>
            <tr>
              <td style="padding:35px 30px 11px 30px;font-size:0;background-color:#ffffff;border-bottom:1px solid #f0f0f5;border-color:rgba(201,201,207,.35);">
                
                  <p style="margin-top:0;margin-bottom:12px;">
Ang iyong loan ay @if($details['decline_type'] == 'temp')pansamantala munang naka-hold <br>dahil <br>@elseif($details['decline_type'] == 'final') declined dahil @else approved @endif
</p>
                  <p style="margin-top:0;margin-bottom:18px;">
                    <br>{{ $details['remarks'] }}
                  </p>

<br><br><p class="screen-1__paragraph-2" style=" font-family: 'Montserrat', sans-serif;
    font-weight: 700;
    text-align: center;">
  </p>
<p class="screen-1__paragraph-5" style="font-family: 'Montserrat', sans-serif;
    font-weight: 500;
    text-align: center;">
Kung mayroon kang anumang mga katanungan o feedback makipag-ugnayan sa amin sa:
<br>Tel no.: (02)-296-53-59 / 8579-3853
<br>Messenger:
<a href="https://m.me/mostholycoop" target="_blank" >mostholycoop</a>
</p><br>
<p class="screen-1__paragraph-2" style=" font-family: 'Montserrat', sans-serif;
    font-weight: 700;
    text-align: center;">
 
</p>

<p class="screen-1__paragraph-5" style="font-family: 'Montserrat', sans-serif;
    font-weight: 500;
    text-align: center;">
 
<b> </b>   <b> </b>.
</p><br>
<p class="screen-1__paragraph-5" style="font-family: 'Montserrat', sans-serif;
    font-weight: 500;
    text-align: center;">
  <br>  
</p>
                  <p style="margin:0;"></p>
                <!--[if mso]>
                </td>
                </tr>
                </table>
                <![endif]-->
              </td>
            </tr>
            <tr>
              <td style="padding:30px;font-size:24px;line-height:28px;font-weight:bold;background-color:#ffffff;border-bottom:1px solid #f0f0f5;border-color:rgba(201,201,207,.35);">
                <a href="" style="text-decoration:none;"></a>
              </td>
            </tr>
            <tr>
              <td style="padding:30px;background-color:#ffffff;">
                <p style="margin:0;"></p>
              </td>
            </tr>
            <tr>
              <td style="padding:30px;text-align:center;font-size:12px;background-color:#404040;color:#cccccc;">
              </td>
            </tr>
          </table>
          <!--[if mso]>
          </td>
          </tr>
          </table>
          <![endif]-->
        </td>
      </tr>
    </table>
  </div>
</body>

{{-- Footer --}}
@slot('footer')
@component('mail::footer')

@endcomponent
@endslot
@endcomponent