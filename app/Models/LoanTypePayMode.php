<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LoanTypePayMode extends Model
{
    use HasFactory;

    protected $table = 'loan_type_pay_modes';
    protected $fillable = [
    'loan_type_id',
    'loan_type_name',
    'paymode_id',
    'paymode_name',
    'crea_by',
    'upd_by',
    'soft_deleted_at',
    ];

}
