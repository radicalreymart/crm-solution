<?php

namespace App\Http\Controllers;

use App\Models\Occupation;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\UserRequest;

class OccupationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $occupation  = Occupation::whereNull('soft_deleted_at')->get();
        return view('occupation.index')
            ->with('occupation', $occupation );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('occupation.create');
     
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate([
            'cd' => 'required',
            'name' => 'required',
        ]);
       Occupation::create([
             'cd' => $request['cd'],
             'name' => $request['name'],
            'crea_by' => auth()->user()->uname,
        ]);
       
       
        return redirect()->route('occupation.index')
            ->with('success', 'Occupation created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Project  $project
     * @return \Illuminate\Http\Response
     */
    // public function show($id, User $user)
    // {
    //     $user = User::find($id);
        
        
       
    //       return view('userTable.show', [
    //     'user' => $user
    //     ]);
    // }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $occupation  = Occupation::find($id);
       
       return view('occupation.edit', [
        'occupation' => $occupation 
        ]);

    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request)
    {
        $rules = array(
            'cd' => 'required',
            'name' => 'required',
        );
         $validator = Validator::make($request->all(), $rules);
         $occupation  = Occupation::find($id);
            $occupation ->cd      = $request->get('cd');
            $occupation ->name       = $request->get('name');
            $occupation ->updated_at      = Carbon::now();
            $occupation ->upd_by = auth()->user()->uname;
            $occupation ->save();
       
        return redirect()->route('occupation.index')
            ->with('success', 'Occupation updated successfully');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Project  $project
     * @return \Illuminate\Http\Response
     */
    
    public function destroy($id)
    {
        $occupation  = Occupation::find($id);
        $occupation ->delete();

        return response()->json(['success'=>"Occupation deleted successfully.", 'tr'=>'tr_'.$id]);

    }
    public function deleteAll(Request $request)
    {

        $ids = $request->ids;
        $occupations = Occupation::whereIn('id',explode(",",$ids))->whereNull('soft_deleted_at')->get();
        if($occupations->count())
        {
            foreach($occupations as $data)
            {
                $occupation = Occupation::find($data->id);
                $occupation->soft_deleted_at = Carbon::now();
                $occupation->save();
            }
        }
        return response()->json(['success'=>"Occupation deleted successfully."]);
    }
}
