<?php

namespace App\Exports;

use App\Models\PreLoanApplication;
use Maatwebsite\Excel\Concerns\FromCollection;
use Carbon;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithCustomCsvSettings;
use PhpOffice\PhpSpreadsheet\Cell\Cell;
use PhpOffice\PhpSpreadsheet\Cell\DataType;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class LoanExport implements  FromQuery, WithHeadings, WithMapping, ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */
    protected $from_date;
    protected $to_date;

    public function __construct($from_date, $to_date) {
        $this->from_date = $from_date;
        $this->to_date = $to_date;
    }

    public function query()
    {
        $from = $this->from_date;
        $to = $this->to_date;
        // $to = Carbon\Carbon::now();
            $branch = auth()->user()->branch_id;
            $loan  = PreLoanApplication::with('occupation','marketing','usermember','branch', 'loan_type', 'terms');
            // dd($miuf->get());
            if($branch == 9){
                // $pmes->query();
                $loan->whereNull('soft_delete_at');
            }else{
                // $pmes->query()
                $loan->where('branch_id', $branch)->whereNull('soft_delete_at');
                // ->orderBy('serv_status');
            }
            $loan->whereBetween('created_at', [$from, $to]);
            // $loan = $loan->get();
            // dd($loan[0]->terms->name);
            $loan = $loan->orderBy('id');
            // dd($loan-);
        return $loan;

    }

    public function headings(): array
    {
        return [
             "member_no"
            ,"contact_no" 
            ,"branch_id"
            ,"birthdate" 
            ,"fb_link" 
            ,"street" 
            ,"barangay_id" 
            ,"city_id"
            ,"province_id" 
            ,"region_id"
            ,"fullname" 
            ,'share_capital'
            ,'branch_id'
            ,'occupation'
            ,'co_maker'
            ,'documents'
            ,'signiture'
            ,'loan_status'
            ,'ref_number'
            ,'amount_needed'
            ,'bonusOrsalary_amount'
            ,'loanable_amount'
            ,'amount_approved'
            ,'approval_remarks'
            ,'loan_product'
            ,'loan_purpose'
            ,'payment_terms'
            ,'payment_mode'
            ,'montly_income'
            ,'montly_expense'
            ,'marketing_channel_id'
            ,'crea_by'
            ,'upd_by'
            ,'date_created'
            ,'date_updated'
            ];
    }

    public function map($loan): array
    {
        // dd($loan);
        switch($loan->loan_product){
            case 4:
            $bonusOrsalary_amount = "Time Deposit";
              break;
            case 5:
            $bonusOrsalary_amount = "Wealth Building Program";
              break;
            case 6:
            $bonusOrsalary_amount = "Wealth Building Program";
              break;
            case 18:
            $bonusOrsalary_amount = "Salary";
              break;
            case 19:
            $bonusOrsalary_amount = "Salary";
              break;
            case 20:
            $bonusOrsalary_amount = "Mid-Year Bonus";
              break;
            case 21:
            $bonusOrsalary_amount = "Year-End Bonus";
              break;
            case 22:
            $bonusOrsalary_amount = "Performance Evaluation Incentives";
              break;
            case 23:
            $bonusOrsalary_amount = "Performance Based Bonus";
              break;
            case 24:
            $bonusOrsalary_amount = "Salary";
              break;
            case 25:
            $bonusOrsalary_amount = "Mid-Year Bonus";
              break;
            case 26:
            $bonusOrsalary_amount = "Year-End Bonus";
              break;
            default:
            $bonusOrsalary_amount = null;
              
              break;  
          }
         if($loan->usermember->occupation_id == null){
            $occupation = $loan->usermember->other_occupation;
            }else{
            $occupation = $loan->usermember->occupation->name;
        }
            if($loan->loan_status == 1){
                $loan_status = 'Approved';
            }else if($loan->loan_status == 2){
                $loan_status = 'Temporarily Decline';
            }else if($loan->loan_status == 3){
                $loan_status = 'Decline';
            }else{
                $loan_status = 'Pending for Approval';
            }

        if($loan->usermember->region == null){
            $barangay = null;
             $city = null;
             $province = null;
             $region = null;
        }else{
             $barangay = $loan->usermember->barangay->name;
             $city = $loan->usermember->city->name;
             $province = $loan->usermember->province->name;
             $region = $loan->usermember->region->name;
        }
        switch ($loan->loan_purpose) {
            case '0':
                $loan_purpose = "Business-Related Expense";
                break;
            case '1':
                $loan_purpose = "Debt Consolidation";
                break;
            case '2':
                $loan_purpose = "Education  / Tuition fees";
                break;
            case '3':
                $loan_purpose = "Health / Medical fees";
                break;
            case '4':
                $loan_purpose = "Home Renovation / Improvements";
                break;
            case '5':
                $loan_purpose = "Life Events / Celebration";
                break;
            case '6':
                $loan_purpose = "Personal Consumption";
                break;
            case '7':
                $loan_purpose = "Purchase of Appliance / Gadget / Furniture";
                break;
            case '8':
                $loan_purpose = "Start-up Business";
                break;
            case '9':
                $loan_purpose = "Vacation / Travel Expense";
                break;
            case '10':
                $loan_purpose = "Vehicle Repair / Purchase";
                break;
            default:
                $loan_purpose = $loan->other_purpose;
                break;
        }
        switch ($loan->payment_mode) {
            case '1':
                $payment_mode = 'Daily';
                break;
            case '2':
                $payment_mode = 'Weekly';
                break;
            case '3':
                $payment_mode = 'Semi-Monthly';
                break;
            case '4':
                $payment_mode = 'Monthly';
                break;
            case '5':
                $payment_mode = 'Lumpsum';
                break;
            case '6':
                $payment_mode = 'Lumpsum (for 1 month only)';
                break;
            default:
                $payment_mode = null;
                break;
        }
        return [
            // 'hello'
             $loan->usermember->member_no
            ,$loan->usermember->contact_no
            ,$loan->usermember->branch_id
            ,$loan->usermember->birthdate
            ,$loan->usermember->fb_link
            ,$loan->usermember->street
            , $barangay
            , $city
            , $province
            , $region
            ,$loan->usermember->crea_by
            ,number_format($loan->share_capital, 2)
            ,$loan->branch->name
            ,$occupation
            ,$loan->co_maker
            ,$loan->file_path1
            ,$loan->signiture
            ,$loan_status
            ,$loan->ref_number
            ,number_format($loan->amount_needed, 2)
            ,$bonusOrsalary_amount.', '.number_format($loan->savings_salary_amount, 2)
            ,number_format($loan->loanable_amount, 2)
            ,number_format($loan->amount_approved, 2)
            ,$loan->approval_remarks
            ,$loan->loan_type->name
            ,$loan_purpose
            ,$loan->terms->name
            ,$payment_mode
            ,number_format($loan->montly_income, 2)
            ,number_format($loan->montly_expense, 2)
            ,$loan->marketing->name
            ,$loan->crea_by
            ,$loan->upd_by
            ,$loan->created_at
            ,$loan->updated_at
        ];
    }
}
