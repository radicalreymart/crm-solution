<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class STM extends Model
{
    use HasFactory;
    protected $table = 'service_trxn_master';

    protected $fillable = [
        'pmes_id',
        'optm_id',
        'preloan_id',
        'service_details',
        'service_type_id',
        'service_status',
        'crea_by',
        'upd_by',
        'soft_deleted_at',
    ];

}
