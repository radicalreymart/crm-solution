<?php

namespace App\Http\Livewire;


use App\Models\User;
use App\Models\Branch;
use Livewire\Component;
use Livewire\WithPagination;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\UserRequest;

class Users extends Component
{
        use WithPagination;
    public $searchTerm;
    public $users, $uname, $branch_id, $employee_role, $email, $password, $password_confirmation, $user_id;
    public $updateMode = false;

    public function render()
    {   
        $searchTerm = '%'.$this->searchTerm.'%';
        $users1 = User::whereNull('soft_deleted_at')
            ->Where(function ($query) {
            $searchTerm = '%'.$this->searchTerm.'%';
            $query->where('uname','like', $searchTerm)
                    ->orWhere('email','like', $searchTerm);
            })
        
        ->paginate(10);

        $branch = Branch::all();

        // dd($users1);
        return view('livewire.user.index',
        compact('users1', 'branch'));
    }
    private function resetInputFields(){
        $this->uname = '';
        $this->email = '';
        $this->password = '';
        $this->branch_id = '';
        $this->employee_role = '';
        $this->password_confirmation = '';
    }

    public function store(Request $request)
    {
        
        $validatedDate = $this->validate([
            'uname' => 'required|string|max:255',
            'branch_id' => 'required',
            'employee_role' => 'required',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:8|confirmed',
        ]);
        // $checked = $this->employee_role ? 1 : 0;
        // dd($this->employee_role);
       User::create([
             'uname' => $this->uname,
             'email' => $this->email,
             'branch_id' => $this->branch_id,
            'password' => Hash::make($this->password),
            'role_id' =>1,
            'employee_role' =>$this->employee_role,
            'crea_by' => auth()->user()->uname,
        ]);

        session()->flash('success', 'Users Created Successfully.');

        $this->resetInputFields();

        $this->emit('userStore'); // Close model to using to jquery

    }
    public function edit($id)
    {
        $this->updateMode = true;
        $user = User::where('id',$id)->first();
        $this->user_id = $id;
        $this->uname = $user->uname;
        $this->email = $user->email;
        $this->branch_id = $user->branch_id;
        $this->employee_role = $user->employee_role;
        $this->password = $user->password;
        $this->password_confirmation = $user->password;
        // dd($this);
        
    }

    public function cancel()
    {
        $this->updateMode = false;
        $this->resetInputFields();


    }

    public function update()
    {
        $validatedDate = $this->validate([
            'uname' => 'required|string|max:255',
            'branch_id' => 'required',
            'employee_role' => 'required',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:8|confirmed',
        ]);

        if ($this->user_id) {
            $user = User::find($this->user_id);
            $user->update([
                'uname' => $this->uname,
                'email' => $this->email,
                'branch_id' => $this->branch_id,
                'employee_role' => $this->employee_role,
                'password' => $this->password,
            ]);
            $this->updateMode = false;
            session()->flash('success', 'Users Updated Successfully.');
            $this->resetInputFields();

        }
    }
}
