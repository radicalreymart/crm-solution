<footer class="m-0 p-5 text-light text-center text-small bg-dark">
  <p class="mb-0">© 2021 Most Holy Rosary Multi-Purpose Cooperative</p>
  <p class="mb-0">All rights reserved</p>
  @yield('enduser')
  @yield('preloan')
</footer>
